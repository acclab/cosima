/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "elstopfile.h"

#include <QDir>
#include <QFile>

/*!
 * \brief If a file path is given, checks if the file name follows PARCAS naming convention, and if it does, saves the
 * element and the substrate into the corresponding attributes. The validity of the name can later be checked with
 * isValid(). If no path is given, constructs a blank object.
 * \param pathToFile Path to the examined file
 */
ElstopFile::ElstopFile(const QString& pathToFile) {
    m_path = pathToFile;
    QStringList list = QDir(m_path).dirName().split(".");

    if (list.count() < 3) {
        m_isValid = false;
        return;
    }

    if (list[0] == "elstop") {
        element = list[1];
        substrate = list[2];
        m_isValid = true;
    }
}

/*!
 * \brief Returns the validity of the file path that was given in the constructor.
 * \return \c true if the given fileName follows PARCAS naming convention
 */
bool ElstopFile::isValid() const {
    return m_isValid;
}

/*!
 * \brief Checks if the file specified in the constructor exists.
 * \return \c true if the file exists
 */
bool ElstopFile::exists() const {
    QFile file(m_path);
    return file.exists();
}

/*!
 * \brief Generates a filename that follows PARCAS naming convention based on the element and the substrate.
 * \return Generated file name
 */
QString ElstopFile::name() const {
    return QString("elstop.%1.%2.in").arg(element, substrate);
}

/*!
 * \brief Returns the file path given in the constructor
 * \return File path
 */
const QString& ElstopFile::path() const {
    return m_path;
}
