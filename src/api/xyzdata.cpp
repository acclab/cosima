/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "xyzdata.h"

#include <QFile>

#include <glm/vec3.hpp>

/*!
 * \brief Reads the data from a file.
 *
 * The file should follow PARCAS xyz-file format:\n
 * element x[Å]  y [Å]  z[Å]  type_number  v_x[Å/fs]  v_y[Å/fs]  v_z[Å/fs]
 *
 * \param path Path to the file
 * \return \c true if the reading is successful
 */
bool XyzData::read(const QString& path) {
    *this = XyzData();

    QFile inputFile(path);
    if (!inputFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't open the file...";
        return false;
    }

    QTextStream in(&inputFile);

    /*int atomCount = */ in.readLine().toInt();

    // Reading simulation cell size
    QString line = in.readLine();
    QStringList list = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    double dx = 0;
    double dy = 0;
    double dz = 0;
    for (int i = 0; i < list.length() - 3; ++i) {
        if (list[i] == "boxsize") {
            dx = list[i + 1].toDouble();
            dy = list[i + 2].toDouble();
            dz = list[i + 3].toDouble();
        }
    }

    for (int i = 0; i < list.length() - 3; ++i) {
        if (list[i] == "pbc") {
            pbc.x = static_cast<bool>(list[i + 1].toInt());
            pbc.y = static_cast<bool>(list[i + 2].toInt());
            pbc.z = static_cast<bool>(list[i + 3].toInt());
        }
    }

    cellLims = Box(-dx / 2, dx / 2, -dy / 2, dy / 2, -dz / 2, dz / 2);

    // Reading atoms and limits
    double xmin = std::numeric_limits<double>::max();
    double xmax = std::numeric_limits<double>::lowest();
    double ymin = std::numeric_limits<double>::max();
    double ymax = std::numeric_limits<double>::lowest();
    double zmin = std::numeric_limits<double>::max();
    double zmax = std::numeric_limits<double>::lowest();

    while (!in.atEnd()) {
        line = in.readLine();
        Atom a;
        QTextStream lineStream(&line);
        QString word;
        lineStream >> word;
        a.symbol = word;
        lineStream >> word;
        a.position.x = word.toDouble();
        lineStream >> word;
        a.position.y = word.toDouble();
        lineStream >> word;
        a.position.z = word.toDouble();
        lineStream >> word;
        a.type = word.toInt();
        lineStream >> word;
        a.identifier = word.toInt();

        lineStream >> word;
        a.velocity.x = word.toDouble();
        lineStream >> word;
        a.velocity.y = word.toDouble();
        lineStream >> word;
        a.velocity.z = word.toDouble();

        atoms.push_back(a);

        amounts[a.type]++;

        xmin = std::min(xmin, a.position.x);
        xmax = std::max(xmax, a.position.x);
        ymin = std::min(ymin, a.position.y);
        ymax = std::max(ymax, a.position.y);
        zmin = std::min(zmin, a.position.z);
        zmax = std::max(zmax, a.position.z);
    }

    if (atoms.size() > 0) {
        atomLims = Box(xmin, xmax, ymin, ymax, zmin, zmax);
    }

    return true;
}

/*!
 * \brief Writes the data into a file.
 *
 * The file will follow PARCAS xyz-file format:
 * element x[Å] y [Å] z[Å] type_number v_x[Å/fs] v_y[Å/fs] v_z[Å/fs]
 *
 * \param path Path to the file
 * \return \c true if the writing is successful
 */
bool XyzData::write(const QString& path) const {
    QFile file(path);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
        QTextStream stream(&file);
        stream << atoms.size() << endl;
        Box b = cellLims;
        double dx = b.x2 - b.x1;
        double dy = b.y2 - b.y1;
        double dz = b.z2 - b.z1;

        QString dxs, dys, dzs;
        stream << QString("boxsize %1 %2 %3")
                      .arg(dxs.sprintf("%13.6f", dx))
                      .arg(dys.sprintf("%13.6f", dy))
                      .arg(dzs.sprintf("%13.6f", dz));
        stream << QString(" pbc %1 %2 %3")
                      .arg(static_cast<int>(pbc.x))
                      .arg(static_cast<int>(pbc.y))
                      .arg(static_cast<int>(pbc.z))
               << endl;

        for (const Atom& a : atoms) {
            QString x, y, z;
            stream << a.symbol.leftJustified(6, ' ');
            stream << x.sprintf("%13.6f", a.position.x) << y.sprintf("%13.6f", a.position.y)
                   << z.sprintf("%13.6f", a.position.z) << "   ";
            stream << QString::number(a.type).rightJustified(6, ' ');
            stream << QString::number(a.identifier).rightJustified(10, ' ');

            QString vx, vy, vz;
            stream << vx.sprintf("%13.7f", a.velocity.x) << vy.sprintf("%13.7f", a.velocity.y)
                   << vz.sprintf("%13.7f", a.velocity.z);

            stream << endl;
        }
    } else {
        return false;
    }
    file.close();
    return true;
}

/*!
 * \brief Centers the cell
 *
 * Shifts the atoms and the simulation cell boundary so that the center point of the boundary is at the origin.
 */
void XyzData::center() {
    Box& b = cellLims;

    glm::dvec3 shiftV = -b.center();

    cellLims.x1 += shiftV.x;
    cellLims.x2 += shiftV.x;
    cellLims.y1 += shiftV.y;
    cellLims.y2 += shiftV.y;
    cellLims.z1 += shiftV.z;
    cellLims.z2 += shiftV.z;

    atomLims.x1 += shiftV.x;
    atomLims.x2 += shiftV.x;
    atomLims.y1 += shiftV.y;
    atomLims.y2 += shiftV.y;
    atomLims.z1 += shiftV.z;
    atomLims.z2 += shiftV.z;

    for (Atom& a : atoms) a.position = a.position + shiftV;
}

/*!
 * \brief Updates the atom limits based on the atom vector
 */
void XyzData::updateAtomLims() {
    Box newAtomLims(std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest(),
                    std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest(),
                    std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest());

    for (const Atom& atom : atoms) {

        newAtomLims.x1 = std::min(newAtomLims.x1, atom.position.x);
        newAtomLims.x2 = std::max(newAtomLims.x2, atom.position.x);
        newAtomLims.y1 = std::min(newAtomLims.y1, atom.position.y);
        newAtomLims.y2 = std::max(newAtomLims.y2, atom.position.y);
        newAtomLims.z1 = std::min(newAtomLims.z1, atom.position.z);
        newAtomLims.z2 = std::max(newAtomLims.z2, atom.position.z);
    }

    atomLims = atoms.count() > 0 ? newAtomLims : Box(0, 0, 0, 0, 0, 0);
}

/*!
 * \brief Updates the amounts based on the atom vector.
 */
void XyzData::updateAmounts() {
    amounts.clear();
    for (const Atom& atom : atoms) {
        amounts[atom.type]++;
    }
}

QDataStream& XyzData::save(QDataStream& out) const {
    return out << atoms << atomLims << cellLims << amounts << pbc;
}

QDataStream& XyzData::load(QDataStream& in) {
    return in >> atoms >> atomLims >> cellLims >> amounts >> pbc;
}
