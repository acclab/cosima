/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typetablesettings.h"

#include "api/programsettings.h"

#include "gui/widgets/utils/clickablelabel.h"

#include <QColorDialog>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QTableWidget>
#include <QVBoxLayout>


TypeTableSettings::TypeTableSettings(QWidget* parent) : QWidget(parent) {
    m_table = new QTableWidget;

    QVBoxLayout* l1 = new QVBoxLayout;
    l1->setContentsMargins(0, 0, 0, 0);
    QHBoxLayout* l2 = new QHBoxLayout;
    l2->setContentsMargins(0, 0, 0, 0);

    l2->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
    l1->addWidget(m_table);
    l1->addItem(l2);
    setLayout(l1);

    m_table->setStyleSheet("QTableWidget::item:selected{ background-color: lightblue }");
    m_table->setAutoScroll(false);
    m_table->setTabKeyNavigation(false);
    m_table->setDragDropOverwriteMode(false);
    m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_table->setFocusPolicy(Qt::NoFocus);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_table->setSelectionMode(QAbstractItemView::SingleSelection);
    m_table->setContextMenuPolicy(Qt::CustomContextMenu);

    m_table->verticalHeader()->setVisible(false);

    m_table->horizontalHeader()->setHighlightSections(false);
    m_table->setColumnCount(6);
    m_table->setHorizontalHeaderItem(0, new QTableWidgetItem("Type"));
    m_table->setHorizontalHeaderItem(1, new QTableWidgetItem("Free Color"));
    m_table->setHorizontalHeaderItem(2, new QTableWidgetItem("Free Radius"));
    m_table->setHorizontalHeaderItem(3, new QTableWidgetItem("Fixed Color"));
    m_table->setHorizontalHeaderItem(4, new QTableWidgetItem("Fixed Radius"));
    m_table->setHorizontalHeaderItem(5, new QTableWidgetItem(""));  // Empty column to fill the window width

    m_table->horizontalHeader()->setStretchLastSection(true);
    m_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
}


/*!
 * \brief Update the fields with the colors and radii of the atoms.
 * \param settings
 */
void TypeTableSettings::setSettings(const VisualSettings& settings) {
    m_colors = settings.atomTypeColors();
    m_radii = settings.atomTypeSizes();

    m_table->setRowCount(10);
    for (int i = 0; i < 10; ++i) {
        setRowData(i, 40);
    }
    m_table->resizeColumnsToContents();
    m_table->setFixedHeight(40 * 11);
}


/*!
 * \brief Helper function to create the row containing the visual settings of the atom types.
 * \param type
 * \param rowHeight
 */
void TypeTableSettings::setRowData(int type, int rowHeight) {
    // Type number widget
    QLabel* typeLabel = new QLabel;
    typeLabel->setFixedWidth(35);
    typeLabel->setText(QString::number(type));
    m_table->setCellWidget(type, 0, createCellWidget(typeLabel));

    // Free color widget
    ClickableLabel* freeColorLabel = new ClickableLabel;
    freeColorLabel->setColor(m_colors[type]);
    freeColorLabel->setFixedSize(20, 20);
    m_table->setCellWidget(type, 1, createCellWidget(freeColorLabel));

    connect(freeColorLabel, &ClickableLabel::clicked, [this, freeColorLabel, type] {
        if (updateTypeColor(type)) {
            freeColorLabel->setColor(m_colors[type]);
        }
    });

    // Free radius widget
    QDoubleSpinBox* freeRadiusSpinBox = new QDoubleSpinBox;
    freeRadiusSpinBox->setMinimumWidth(80);
    freeRadiusSpinBox->setSingleStep(0.1);
    freeRadiusSpinBox->setValue(static_cast<double>(m_radii[type]));
    connect(freeRadiusSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this, type](double value) { m_radii[type] = static_cast<float>(value); });
    m_table->setCellWidget(type, 2, createCellWidget(freeRadiusSpinBox));

    if (type != 0) {
        // Fixed color widget
        ClickableLabel* fixedColorLabel = new ClickableLabel;
        fixedColorLabel->setColor(m_colors[-type]);
        fixedColorLabel->setFixedSize(20, 20);
        m_table->setCellWidget(type, 3, createCellWidget(fixedColorLabel));

        connect(fixedColorLabel, &ClickableLabel::clicked, [this, fixedColorLabel, type] {
            if (updateTypeColor(-type)) {
                fixedColorLabel->setColor(m_colors[-type]);
            }
        });

        // Fixed radius widget
        QDoubleSpinBox* fixedRadiusSpinBox = new QDoubleSpinBox;
        fixedRadiusSpinBox->setMinimumWidth(80);
        fixedRadiusSpinBox->setSingleStep(0.1);
        fixedRadiusSpinBox->setValue(static_cast<double>(m_radii[-type]));
        connect(fixedRadiusSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                [this, type](double value) { m_radii[type] = static_cast<float>(value); });
        m_table->setCellWidget(type, 4, createCellWidget(fixedRadiusSpinBox));
    }

    m_table->setRowHeight(type, rowHeight);
}


QWidget* TypeTableSettings::createCellWidget(QWidget* childWidget) {
    QWidget* widget = new QWidget;
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(childWidget);
    widget->setLayout(layout);
    return widget;
}


bool TypeTableSettings::updateTypeColor(int type) {
    QColorDialog dialog(this);
    dialog.setOption(QColorDialog::DontUseNativeDialog, true);
    dialog.setWindowTitle("Color Picker");
    dialog.setCurrentColor(m_colors[type]);

    if (dialog.exec() == QDialog::Accepted) {
        if (m_colors[type] != dialog.selectedColor()) {
            m_colors[type] = dialog.selectedColor();
            return true;
        }
    }
    return false;
}
