/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTATOMSELECTORWIDGET_H
#define ABSTRACTATOMSELECTORWIDGET_H

#include "api/atomselections.h"
#include "api/entry.h"
#include "plotcellglwidget.h"

#include <QWidget>

#include <QVector>

class AbstractAtomSelectorWidget : public QWidget {
    Q_OBJECT
  public:
    explicit AbstractAtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr) : QWidget(parent) {
        m_context = context;
    }

    virtual const AbstractAtomSelection& selector() = 0;

    virtual void setEntry(const ExtendedEntryData* entry) {
        m_entry = entry;
        init();
    }
    virtual void init() {}
    virtual void updateAtoms() {}
    virtual void updateCellLims() {}
    virtual void updateTypes() {}

    virtual void connectToPlot() = 0;
    virtual void hidePlottables() {}

  protected:
    const ExtendedEntryData* m_entry = nullptr;
    PlotCellGLWidget* m_context = nullptr;
};

#endif  // ABSTRACTATOMSELECTORWIDGET_H
