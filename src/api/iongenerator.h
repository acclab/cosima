/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IONGENERATOR_H
#define IONGENERATOR_H

#include "api/data/iontransformdata.h"
#include "api/entry.h"
#include "api/geometries.h"
#include "api/state/parameters/baseparameters.h"

#include <glm/vec3.hpp>

#include <fstream>
#include <iomanip>

/*!
 * \brief A class for generating the ion files for the irradiation simulations.
 *
 * Used to generate ions and write them into a file. Each line of the file corresponds to one ion. The format of a line
 * is:
 * x[Å] y[Å] z[Å] theta[deg] phi[deg] energy[eV]
 */
class IonGenerator {
  public:
    /*!
     * \brief A struct that holds information about an ion.
     */
    struct Ion {
        IonTransformData transform;
        double energy = 0.0;
    };

    IonGenerator();

    void generate(const ExtendedEntryData& entry, const api::BaseParameters& generalParameters,
                  const api::BaseParameters& parameters);

    const QVector<Ion> ions() const;
    QVector<Ion> mutableIons() const;
    bool write(const QString& path) const;

  private:
    QVector<Ion> m_ions;
};

#endif  // IONGENERATOR_H
