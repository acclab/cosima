/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

class TypeTableSettings;


namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog {
    Q_OBJECT

  public:
    explicit SettingsDialog(QWidget* parent = nullptr);
    ~SettingsDialog();

    void load();
    void save();

    QString locateFilePath(const QString& message, const QString& currentFilePath);
    QString locateDirectoryPath(const QString& message, const QString& currentPath);

  private:
    Ui::SettingsDialog* ui;

    TypeTableSettings* m_typeTableSettingsWidget;

  private slots:
    void on_pushButton_openParcasPath_clicked();
    void on_pushButton_openFrameProcessorPath_clicked();
    void on_pushButton_openRuntimePath_clicked();
    void on_pushButton_openProjectPath_clicked();
    void on_checkBox_enableSpeedup_clicked(bool checked);

  signals:
    void enableSpeedupModule(bool enable);
};

#endif  // SETTINGSDIALOG_H
