/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "spheremodel.h"

#include "api/util.h"
#include "gui/rendering/linemesh.h"
#include "gui/rendering/trianglemesh.h"


SphereModel::SphereModel(const Sphere& sphere, QColor faceColor, QColor lineColor, unsigned int resolution,
                         QObject* parent)
        : AbstractGeometryModel(parent) {
    m_faceColor = faceColor;
    m_lineColor = lineColor;
    m_resolution = resolution;

    setupVertexData();

    setSphere(sphere);
}


void SphereModel::setupVertexData() {
    // constants
    const float PI = 3.1415926f;
    const float H_ANGLE = PI / 180 * 72;    // 72 degree = 360 / 5
    const float V_ANGLE = atanf(1.0f / 2);  // elevation = 26.565 degree

    std::vector<glm::vec3> tmpPositions(12);
    float z, xy;  // coords
    std::size_t i1, i2;
    float hAngle1 = -PI / 2 - H_ANGLE / 2;  // start from -126 deg at 1st row
    float hAngle2 = -PI / 2;                // start from -90 deg at 2nd row

    // the first top vertex at (0, 0, r)
    tmpPositions[0] = glm::vec3(0, 0, 1.0f);

    // compute 10 vertices at 1st and 2nd rows
    for (std::size_t i = 1; i <= 5; ++i) {
        z = 1.0f * sinf(V_ANGLE);   // elevation
        xy = 1.0f * cosf(V_ANGLE);  // length on XY plane

        i1 = i;
        i2 = i + 5;
        tmpPositions[i1] = glm::vec3(xy * cosf(hAngle1), xy * sinf(hAngle1), z);
        tmpPositions[i2] = glm::vec3(xy * cosf(hAngle2), xy * sinf(hAngle2), -z);

        // next horizontal angles
        hAngle1 += H_ANGLE;
        hAngle2 += H_ANGLE;
    }

    // the last bottom vertex at (0, 0, -r)
    tmpPositions[11] = glm::vec3(0, 0, -1.0f);


    qDebug() << "    ICOSPHERE POSITION COUNT" << tmpPositions.size();

    // clear memory of prev arrays
    std::vector<glm::vec3>().swap(m_positions);
    std::vector<unsigned int>().swap(m_faceIndices);
    std::vector<unsigned int>().swap(m_lineIndices);

    const glm::vec3 *v0, *v1, *v2, *v3, *v4, *v11;  // vertex positions
    unsigned int index = 0;

    // compute and add 20 tiangles of icosahedron first
    v0 = &tmpPositions[0];    // 1st vertex
    v11 = &tmpPositions[11];  // 12th vertex
    for (std::size_t i = 1; i <= 5; ++i) {
        // 4 vertices in the 2nd row
        v1 = &tmpPositions[i];
        if (i < 5)
            v2 = &tmpPositions[(i + 1)];
        else
            v2 = &tmpPositions[1];

        v3 = &tmpPositions[(i + 5)];
        if ((i + 5) < 10)
            v4 = &tmpPositions[(i + 6)];
        else
            v4 = &tmpPositions[6];

        // add a triangle in 1st row
        m_positions.push_back(*v0);
        m_positions.push_back(*v1);
        m_positions.push_back(*v2);
        m_faceIndices.push_back(index);
        m_faceIndices.push_back(index + 1);
        m_faceIndices.push_back(index + 2);

        // add 2 triangles in 2nd row
        m_positions.push_back(*v1);
        m_positions.push_back(*v3);
        m_positions.push_back(*v2);
        m_faceIndices.push_back(index + 3);
        m_faceIndices.push_back(index + 4);
        m_faceIndices.push_back(index + 5);

        m_positions.push_back(*v2);
        m_positions.push_back(*v3);
        m_positions.push_back(*v4);
        m_faceIndices.push_back(index + 6);
        m_faceIndices.push_back(index + 7);
        m_faceIndices.push_back(index + 8);

        // add a triangle in 3rd row
        m_positions.push_back(*v3);
        m_positions.push_back(*v11);
        m_positions.push_back(*v4);
        m_faceIndices.push_back(index + 9);
        m_faceIndices.push_back(index + 10);
        m_faceIndices.push_back(index + 11);

        // add 6 edge lines per iteration
        //  i
        //  /   /   /   /   /       : (i, i+1)                              //
        // /__ /__ /__ /__ /__                                              //
        // \  /\  /\  /\  /\  /     : (i+3, i+4), (i+3, i+5), (i+4, i+5)    //
        //  \/__\/__\/__\/__\/__                                            //
        //   \   \   \   \   \      : (i+9,i+10), (i+9, i+11)               //
        //    \   \   \   \   \                                             //
        m_lineIndices.push_back(index);      // (i, i+1)
        m_lineIndices.push_back(index + 1);  // (i, i+1)
        m_lineIndices.push_back(index + 3);  // (i+3, i+4)
        m_lineIndices.push_back(index + 4);
        m_lineIndices.push_back(index + 3);  // (i+3, i+5)
        m_lineIndices.push_back(index + 5);
        m_lineIndices.push_back(index + 4);  // (i+4, i+5)
        m_lineIndices.push_back(index + 5);
        m_lineIndices.push_back(index + 9);  // (i+9, i+10)
        m_lineIndices.push_back(index + 10);
        m_lineIndices.push_back(index + 9);  // (i+9, i+11)
        m_lineIndices.push_back(index + 11);

        // next index
        index += 12;
    }

    divideIcosphere();

    std::vector<glm::vec4> faceColors(m_positions.size());
    std::fill(std::begin(faceColors), std::end(faceColors), util::colorToVector(m_faceColor));

    std::vector<glm::vec4> lineColors(m_positions.size());
    std::fill(std::begin(lineColors), std::end(lineColors), util::colorToVector(m_lineColor));


    setLineVertexData(m_positions, lineColors, m_lineIndices);
    setFaceVertexData(m_positions, faceColors, m_faceIndices);
}


/*!
 * Helper function for deviding the inital icosahedron into more faces.
 *
 * Devide each edge in half to create 4 triangles around every corner. By iterating it is possible to make very smooth
 * spherical meshes.
 *
 * \brief SphereModel::divideIcosphere
 */
void SphereModel::divideIcosphere() {
    std::vector<glm::vec3> tmpVertices;
    std::vector<unsigned int> tmpIndices;
    std::size_t indexCount;
    const glm::vec3 *v1, *v2, *v3;  // ptr to original vertices of a triangle
    glm::vec3 newV1, newV2, newV3;  // new vertex positions
    unsigned int index = 0;         // new index value
    std::size_t i;
    std::size_t j;

    // iteration
    for (i = 1; i <= m_resolution; ++i) {
        // copy previous arrays
        tmpVertices = m_positions;
        tmpIndices = m_faceIndices;

        // clear previous arrays
        m_positions.clear();
        m_faceIndices.clear();
        m_lineIndices.clear();

        index = 0;
        indexCount = tmpIndices.size();
        for (j = 0; j < indexCount; j += 3) {
            // get 3 vertice and texcoords of a triangle
            v1 = &tmpVertices[tmpIndices[j]];
            v2 = &tmpVertices[tmpIndices[j + 1]];
            v3 = &tmpVertices[tmpIndices[j + 2]];

            // get 3 new vertices by spliting half on each edge
            newV1 = computeMiddlePoints(*v1, *v2);
            newV2 = computeMiddlePoints(*v2, *v3);
            newV3 = computeMiddlePoints(*v1, *v3);

            // add 4 new triangles
            m_positions.push_back(*v1);
            m_positions.push_back(newV1);
            m_positions.push_back(newV3);
            m_faceIndices.push_back(index);
            m_faceIndices.push_back(index + 1);
            m_faceIndices.push_back(index + 2);

            m_positions.push_back(newV1);
            m_positions.push_back(*v2);
            m_positions.push_back(newV2);
            m_faceIndices.push_back(index + 3);
            m_faceIndices.push_back(index + 4);
            m_faceIndices.push_back(index + 5);

            m_positions.push_back(newV1);
            m_positions.push_back(newV2);
            m_positions.push_back(newV3);
            m_faceIndices.push_back(index + 6);
            m_faceIndices.push_back(index + 7);
            m_faceIndices.push_back(index + 8);

            m_positions.push_back(newV3);
            m_positions.push_back(newV2);
            m_positions.push_back(*v3);
            m_faceIndices.push_back(index + 9);
            m_faceIndices.push_back(index + 10);
            m_faceIndices.push_back(index + 11);

            // add new line indices per iteration
            m_lineIndices.push_back(index);  // i1 - i2
            m_lineIndices.push_back(index + 1);
            m_lineIndices.push_back(index + 1);  // i2 - i6
            m_lineIndices.push_back(index + 9);
            m_lineIndices.push_back(index + 1);  // i2 - i3
            m_lineIndices.push_back(index + 4);
            m_lineIndices.push_back(index + 1);  // i2 - i4
            m_lineIndices.push_back(index + 5);
            m_lineIndices.push_back(index + 9);  // i6 - i4
            m_lineIndices.push_back(index + 5);
            m_lineIndices.push_back(index + 4);  // i3 - i4
            m_lineIndices.push_back(index + 5);
            m_lineIndices.push_back(index + 5);  // i4 - i5
            m_lineIndices.push_back(index + 11);

            // next index
            index += 12;
        }
    }
}


/*!
 * Find the middle point between the given vertex positions.
 * NOTE: The new vertex positions is rescaled to equal the radius of the sphere.
 */
glm::vec3 SphereModel::computeMiddlePoints(const glm::vec3 v1, const glm::vec3 v2) {
    glm::vec3 newV(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
    newV *= 1.0f / glm::length(newV);
    return newV;
}


void SphereModel::setSphere(const Sphere& sphere) {
    transform.setPosition(sphere.center);
    transform.setScale(glm::vec3(sphere.radius, sphere.radius, sphere.radius));
    //! \note no need for a rotation as this is a spherical object
}


void SphereModel::setFaceColor(const QColor& color) {
    m_faceColor = color;
    setupVertexData();
}


void SphereModel::setLineColor(const QColor& color) {
    m_lineColor = color;
    setupVertexData();
}
