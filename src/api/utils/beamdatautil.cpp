/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "beamdatautil.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/transform.hpp>


/*!
 * \brief The position of the beam in the global Cartesian coordinates
 *
 * If the beam type is #Focused, this will just be #position. If the type is #Broad, the position is at the center
 * of the selected simulation cell face.
 *
 * \param cellLims The simulation cell limits
 * \return The position vector (x,y,z)
 */
glm::dvec3 BroadBeamUtil::position(const Box& cellLims, api::enums::beam::Face face) {
    switch (face) {
        case api::enums::beam::X_lower:
            return glm::dvec3(cellLims.x1, 0, 0);

        case api::enums::beam::X_upper:
            return glm::dvec3(cellLims.x2, 0, 0);

        case api::enums::beam::Y_lower:
            return glm::dvec3(0, cellLims.y1, 0);

        case api::enums::beam::Y_upper:
            return glm::dvec3(0, cellLims.y2, 0);

        case api::enums::beam::Z_lower:
            return glm::dvec3(0, 0, cellLims.z1);

        case api::enums::beam::Z_upper:
            return glm::dvec3(0, 0, cellLims.z2);

        default:
            return glm::dvec3();
    }
}


/**
 * @brief Align the beam arrow with the chosen face
 * @param inclination
 * @param azimuth
 * @param face
 * @return the euler rotation
 */
glm::vec3 BroadBeamUtil::rotation(double inclination, double azimuth, api::enums::beam::Face face) {
    glm::quat rotation;
    switch (face) {
        case api::enums::beam::X_lower:
            rotation = glm::quat(glm::radians(glm::vec3(0, 90, 0)));   // turn to the right side
            rotation *= glm::quat(glm::radians(glm::vec3(0, 0, 90)));  // always incline to the right
            break;

        case api::enums::beam::X_upper:
            rotation = glm::quat(glm::radians(glm::vec3(0, -90, 0)));   // turn to the right side
            rotation *= glm::quat(glm::radians(glm::vec3(0, 0, -90)));  // always incline to the right
            break;

        case api::enums::beam::Y_lower:
            rotation = glm::quat(glm::radians(glm::vec3(-90, 0, 0)));   // turn to the right side
            rotation *= glm::quat(glm::radians(glm::vec3(0, 0, 180)));  // always incline to the right
            break;

        case api::enums::beam::Y_upper:
            rotation = glm::quat(glm::radians(glm::vec3(90, 0, 0)));  // turn to the right side
            break;

        case api::enums::beam::Z_lower:
            rotation = glm::quat(glm::radians(glm::vec3(0, 0, 0)));     // turn to the right side
            rotation *= glm::quat(glm::radians(glm::vec3(0, 0, 180)));  // always incline to the right
            break;

        case api::enums::beam::Z_upper:
            rotation = glm::quat(glm::radians(glm::vec3(0, 180, 0)));
            break;
    }

    //! \note When the inclination is at the highest value, there are graphical glitches reminding of division by
    //! zero. I wasn't able to track down the cause of this, so I just introduced an offset here instead.
    double clampedInclination = std::min(89.9999, std::max(0.0001, inclination));
    rotation *= glm::quat(glm::radians(glm::vec3(0, clampedInclination, -azimuth)));

    return glm::eulerAngles(rotation);
}


glm::vec3 BroadBeamUtil::direction(const glm::vec3& rotation) {
    double pitch = -rotation.y + glm::pi<double>() / 2;  // Correcting the angle to follow the arrow orientation.
    double yaw = rotation.z;
    double x = cos(yaw) * cos(pitch);
    double y = sin(yaw) * cos(pitch);
    double z = sin(pitch);
    return glm::normalize(glm::vec3(x, y, z));
}


glm::vec3 BroadBeamUtil::direction(double inclination, double azimuth, api::enums::beam::Face face) {
    return direction(rotation(inclination, azimuth, face));
}
