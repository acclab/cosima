/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "cubemodel.h"

#include "api/util.h"

#include <QOpenGLContext>


CubeModel::CubeModel(const Box& box, const QColor& faceColor, const QColor& lineColor, unsigned int resolution,
                     QObject* parent)
        : AbstractGeometryModel(parent) {
    m_faceColor = faceColor;
    m_lineColor = lineColor;
    m_resolution = resolution;

    setupVertexData();

    setBox(box);
}


void CubeModel::setupVertexData() {
    float x1 = -0.5f;
    float x2 = 0.5f;
    float y1 = -0.5f;
    float y2 = 0.5f;
    float z1 = -0.5f;
    float z2 = 0.5f;

    //------------------------
    // Setup face vertex data
    //------------------------

    std::vector<glm::vec3> positions{
        glm::vec3(x1, y1, z1),  // vertex 0
        glm::vec3(x1, y2, z1),  // vertex 1
        glm::vec3(x2, y2, z1),  // vertex 2
        glm::vec3(x2, y1, z1),  // vertex 3
        glm::vec3(x1, y1, z2),  // vertex 4
        glm::vec3(x1, y2, z2),  // vertex 5
        glm::vec3(x2, y2, z2),  // vertex 6
        glm::vec3(x2, y1, z2)   // vertex 7
    };

    std::vector<unsigned int> faceIndices{
        0, 1, 2,  // bottom, face 1
        2, 3, 0,  // bottom, face 2
        4, 7, 6,  // top,    face 1
        6, 5, 4,  // top,    face 2
        0, 4, 5,  // left,   face 1
        5, 1, 0,  // left,   face 2
        3, 7, 4,  // front,  face 1
        4, 0, 3,  // front,  face 2
        2, 6, 7,  // right,  face 1
        7, 3, 2,  // right,  face 2
        1, 5, 6,  // back,   face 1
        6, 2, 1   // back,   face 2
    };


    //-----------------------------
    // Setup wireframe vertex data
    //-----------------------------

    // Place vertices evenly distributed on all 24 edges to devide it into a wireframe.
    float sx = x2 - x1;
    float sy = y2 - y1;
    float sz = z2 - z1;

    float dx = sx / (m_resolution + 1);
    float dy = sy / (m_resolution + 1);
    float dz = sz / (m_resolution + 1);

    for (std::size_t i = 1; i <= m_resolution; ++i) {
        // In X-direction
        positions.push_back(glm::vec3(x1 + dx * i, y1, z1));  // bottom front
        positions.push_back(glm::vec3(x1 + dx * i, y1, z2));  // top front
        positions.push_back(glm::vec3(x1 + dx * i, y2, z2));  // top back
        positions.push_back(glm::vec3(x1 + dx * i, y2, z1));  // bottom back

        // In Y-direction
        positions.push_back(glm::vec3(x1, y1 + dy * i, z1));  // bottom left
        positions.push_back(glm::vec3(x1, y1 + dy * i, z2));  // top left
        positions.push_back(glm::vec3(x2, y1 + dy * i, z2));  // top right
        positions.push_back(glm::vec3(x2, y1 + dy * i, z1));  // bottom right

        // In Z-direction
        positions.push_back(glm::vec3(x1, y1, z1 + dz * i));  // bottom left
        positions.push_back(glm::vec3(x1, y2, z1 + dz * i));  // top left
        positions.push_back(glm::vec3(x2, y2, z1 + dz * i));  // top right
        positions.push_back(glm::vec3(x2, y1, z1 + dz * i));  // bottom right
    }


    std::vector<unsigned int> lineIndices{
        0, 1, 1, 2, 2, 3, 3, 0,  // Bottom face
        4, 5, 5, 6, 6, 7, 7, 4,  // Top face
        0, 4, 1, 5, 2, 6, 3, 7   // connecting top and bottom
    };

    for (unsigned int i = 0; i < m_resolution; ++i) {
        for (unsigned int j = 0; j < 12; j += 4) {
            // 1. Offset the index to begin after the the corner vertices (i.e. +7)
            // 2. Offset by the amount of vertices added every m_wireframeDivision (i.e. +12*i)
            // 3. Loop over the different directions (i.e. +j)
            // 4. Connect the GL_LINE ends.
            lineIndices.push_back(7 + i * 12 + j + 1);
            lineIndices.push_back(7 + i * 12 + j + 2);
            lineIndices.push_back(7 + i * 12 + j + 2);
            lineIndices.push_back(7 + i * 12 + j + 3);
            lineIndices.push_back(7 + i * 12 + j + 3);
            lineIndices.push_back(7 + i * 12 + j + 4);
            lineIndices.push_back(7 + i * 12 + j + 4);
            lineIndices.push_back(7 + i * 12 + j + 1);
        }
    }

    std::vector<glm::vec4> faceColors(positions.size());
    std::fill(std::begin(faceColors), std::end(faceColors), util::colorToVector(m_faceColor));

    std::vector<glm::vec4> lineColors(positions.size());
    std::fill(std::begin(lineColors), std::end(lineColors), util::colorToVector(m_lineColor));


    setLineVertexData(positions, lineColors, lineIndices);
    setFaceVertexData(positions, faceColors, faceIndices);
}


void CubeModel::setBox(const Box& box) {
    double sx = box.x2 - box.x1;
    double sy = box.y2 - box.y1;
    double sz = box.z2 - box.z1;
    transform.setPosition(glm::vec3(sx / 2 + box.x1, sy / 2 + box.y1, sz / 2 + box.z1));
    transform.setScale(glm::vec3(sx, sy, sz));
}


void CubeModel::setResolution(unsigned int resolution) {
    m_resolution = resolution;
    setupVertexData();
}


void CubeModel::setFaceColor(const QColor& color) {
    m_faceColor = color;
    setupVertexData();
}


void CubeModel::setLineColor(const QColor& color) {
    m_lineColor = color;
    setupVertexData();
}
