/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "outputwidget.h"
#include "ui_outputwidget.h"

#include "api/util.h"

#include "gui/widgets/utils/wheelguard.h"


OutputWidget::OutputWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::OutputWidget) {
    ui->setupUi(this);

    QList<QWidget*> wheelGuardedWidgets = {ui->spinBox_logInterval, ui->spinBox_movieStepInterval,
                                           ui->doubleSpinBox_movieTimeInterval};

    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    setFocusProxy(ui->spinBox_logInterval);


    connect(ui->radioButton_movieStepInterval, &QRadioButton::toggled, this, [=](bool checked) {
        ui->spinBox_movieStepInterval->setEnabled(checked);
        emit modified();
    });
    connect(ui->radioButton_movieTimeInterval, &QRadioButton::toggled, this, [=](bool checked) {
        ui->doubleSpinBox_movieTimeInterval->setEnabled(checked);
        emit modified();
    });
    connect(ui->spinBox_logInterval, QOverload<int>::of(&QSpinBox::valueChanged), this, [=] { emit modified(); });
    connect(ui->spinBox_movieStepInterval, QOverload<int>::of(&QSpinBox::valueChanged), this, [=] { emit modified(); });
    connect(ui->doubleSpinBox_movieTimeInterval, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->groupBox_movieParameters, &QGroupBox::clicked, this, [=] {
        ui->spinBox_movieStepInterval->setEnabled(ui->radioButton_movieStepInterval->isChecked());
        ui->doubleSpinBox_movieTimeInterval->setEnabled(ui->radioButton_movieTimeInterval->isChecked());
        emit modified();
    });
}

OutputWidget::~OutputWidget() {
    delete ui;
}


void OutputWidget::setParameters(api::BaseParameters parameters) {
    QSignalBlocker blocker1(ui->spinBox_logInterval);
    QSignalBlocker blocker2(ui->spinBox_movieStepInterval);
    QSignalBlocker blocker3(ui->doubleSpinBox_movieTimeInterval);
    QSignalBlocker blocker4(ui->radioButton_movieStepInterval);
    QSignalBlocker blocker5(ui->radioButton_movieTimeInterval);
    QSignalBlocker blocker6(ui->groupBox_movieParameters);


    ui->spinBox_logInterval->setValue(parameters[api::parameters::output::LOG_INTERVAL].value().toInt());
    ui->spinBox_movieStepInterval->setValue(
        parameters[api::parameters::output::MOVIE_STEP_INTERVAL].value().toDouble());
    util::updateDoubleSpinBoxValue(ui->doubleSpinBox_movieTimeInterval,
                                   parameters[api::parameters::output::MOVIE_TIME_INTERVAL].value().toDouble());

    // Update the visuals of the movie output box
    ui->radioButton_movieStepInterval->setChecked(
        parameters[api::parameters::output::MOVIE_STEP_COUNTER_ON].value().toBool());
    ui->radioButton_movieTimeInterval->setChecked(
        !parameters[api::parameters::output::MOVIE_STEP_COUNTER_ON].value().toBool());

    ui->spinBox_movieStepInterval->setEnabled(parameters[api::parameters::output::MOVIE_ON].value().toBool() &&
                                              ui->radioButton_movieStepInterval->isChecked());
    ui->doubleSpinBox_movieTimeInterval->setEnabled(parameters[api::parameters::output::MOVIE_ON].value().toBool() &&
                                                    ui->radioButton_movieTimeInterval->isChecked());

    ui->groupBox_movieParameters->setChecked(parameters[api::parameters::output::MOVIE_ON].value().toBool());
}


api::BaseParameters OutputWidget::parameters() {
    api::BaseParameters params;
    params.set(api::parameters::output::LOG_INTERVAL, api::value_t(ui->spinBox_logInterval->value()));
    params.set(api::parameters::output::MOVIE_STEP_INTERVAL, api::value_t(ui->spinBox_movieStepInterval->value()));
    params.set(api::parameters::output::MOVIE_TIME_INTERVAL,
               api::value_t(ui->doubleSpinBox_movieTimeInterval->value()));
    params.set(api::parameters::output::MOVIE_ON, api::value_t(ui->groupBox_movieParameters->isChecked()));
    params.set(api::parameters::output::MOVIE_STEP_COUNTER_ON,
               api::value_t(ui->radioButton_movieStepInterval->isChecked()));
    return params;
}
