/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef VISUALSETTINGS_H
#define VISUALSETTINGS_H

#include <QColor>
#include <QDataStream>
#include <QDebug>
#include <QHash>
#include <QObject>

/*!
 * \brief Visual settings for frame plotting
 *
 * Includes atom colors, atom sizes, cell boundary color, and background color.
 */
class VisualSettings : public QObject {
    Q_OBJECT

  public:
    explicit VisualSettings(QObject* parent = nullptr);
    VisualSettings(const VisualSettings& v);
    ~VisualSettings() {}

    /*!
     * \brief Assignment operator
     *
     * Overwrites all all the settings. If something changes, corresponding signals are emitted.
     *
     * \param v The new settings
     */
    void operator=(const VisualSettings& v) {
        changeTypeSizes(v.atomTypeSizes());
        changeTypeColors(v.atomTypeColors());
        changeBackgroundColor(v.backgroundColor());
        changeCellBoundaryColor(v.cellBoundaryColor());
        changeCellBoundaryEnabled(v.cellBoundaryEnabled());
        changeDepthPeelCount(v.depthPeelCount());
    }

    static VisualSettings defaultSettings();

    const QHash<int, QColor>& atomTypeColors() const;
    const QHash<int, float>& atomTypeSizes() const;
    const QColor& backgroundColor() const;
    const QColor& cellBoundaryColor() const;
    bool cellBoundaryEnabled() const;
    int depthPeelCount() const;

  public slots:
    void changeTypeColors(const QHash<int, QColor>& colors);
    void changeTypeSizes(const QHash<int, float>& sizes);

    void changeBackgroundColor(const QColor& color);
    void changeCellBoundaryColor(const QColor& color);
    void changeCellBoundaryEnabled(bool enabled);

    void changeDepthPeelCount(int count);

  signals:
    void atomTypeColorsChanged(
        const QHash<int, QColor>& typeColors);  //!< \brief Emitted when the atom type colors change.
    void atomTypeSizesChanged(const QHash<int, float>& typeRadii);  //!< \brief Emitted when the atom type sizes change.

    void backgroundColorChanged(const QColor&);     //!< \brief Emitted when the background color changes.
    void cellBoundaryColorChanged(const QColor&);   //!< \brief Emitted when the cell boundary color changes.
    void cellBoundaryEnabledChanged(bool enabled);  //!< \brief Emitted when the cell boudnary checkbox is toggled

    void depthPeelCountChanged(int count);  //!< \brief Emitted when the depth peel counter is changed.

  private:
    QHash<int, QColor> m_atomTypeColors;
    QHash<int, float> m_atomTypeSizes;
    QColor m_backgroundColor;
    QColor m_cellBoundaryColor;
    bool m_cellBoundaryEnabled;
    int m_depthPeels;
};

Q_DECLARE_METATYPE(VisualSettings)

QDataStream& operator<<(QDataStream& stream, const VisualSettings& settings);
QDataStream& operator>>(QDataStream& stream, VisualSettings& settings);

QDebug operator<<(QDebug d, const VisualSettings& settings);

#endif  // VISUALSETTINGS_H
