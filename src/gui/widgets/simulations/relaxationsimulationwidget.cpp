/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "relaxationsimulationwidget.h"
#include "ui_relaxationsimulationwidget.h"

#include "api/utils/baseparameterutil.h"

#include <QDebug>
#include <QMessageBox>
#include <QSpacerItem>


RelaxationSimulationWidget::RelaxationSimulationWidget(QWidget* parent)
        : AbstractSimulationWidget(parent), ui(new Ui::RelaxationSimulationWidget) {
    ui->setupUi(this);

    m_controlWidget = new ControlWidget(this);
    m_interactionWidget = new InteractionWidget(this);

    QVBoxLayout* layout1 = new QVBoxLayout;
    layout1->addWidget(m_controlWidget);
    layout1->addItem(new QSpacerItem(20, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->scrollAreaWidgetContents->setLayout(layout1);

    QVBoxLayout* layout2 = new QVBoxLayout;
    layout2->addWidget(m_interactionWidget);
    ui->scrollAreaWidgetContents_2->setLayout(layout2);

    connect(m_controlWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified control widget";
        setModified(true);
        setTabStatusIcon(ui->relaxationTab, loadIcon(gui::enums::state::Status::Modified));
    });

    connect(m_interactionWidget, &AbstractComponentWidget::modified, this, [=](bool errors) {
        qDebug() << "Modified interaction widget";
        setModified(true);
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Modified));
        }
    });
    connect(m_interactionWidget, &AbstractComponentWidget::needsAttention, this, [=](bool errors) {
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Status::Attention));
        }
    });
}


RelaxationSimulationWidget::~RelaxationSimulationWidget() {
    delete ui;
}


void RelaxationSimulationWidget::connectToPlot(PlotCellGLWidget* glWidget) {
    glWidget->setPlotMode(PlotCellGLWidget::Cooling);
    m_controlWidget->connectToPlot(glWidget);
}


void RelaxationSimulationWidget::disconnectFromPlot(PlotCellGLWidget* glWidget) {
    glWidget->setPlotMode(PlotCellGLWidget::Plain);
    m_controlWidget->disconnectFromPlot(glWidget);
}


void RelaxationSimulationWidget::setControllerParametersToWidget(ParcasBaseController* controller) {
    // Remove the unsaved indicator from the tabs
    setModified(false);
    setErrors(false);
    setNeedsAttention(false);

    setTabStatusIcon(ui->relaxationTab, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->interactionTab, loadIcon(gui::enums::state::Ok));


    m_unmodifiedParameters = controller->parameters();

    m_controlWidget->setParameters(controller->parameters());
    m_interactionWidget->setParameters(controller->parameters());
}


void RelaxationSimulationWidget::setWidgetParametersToController(ParcasBaseController* controller) {
    controller->insertParameters(m_controlWidget->parameters());
    controller->insertParameters(m_interactionWidget->parameters());
}


// bool RelaxationSimulationWidget::isModified() {
//    api::BaseParameters tempParams;
//    tempParams.insert(m_controlWidget->parameters());
//    tempParams.insert(m_interactionWidget->parameters());

//    for (const api::key_t& key : tempParams.values().keys()) {
//        qDebug() << key << tempParams[key].value() << m_unmodifiedParameters[key].value();
//        if (tempParams[key].value() != m_unmodifiedParameters[key].value()) {
//            return true;
//        }
//    }

//    return false;
//}


void RelaxationSimulationWidget::setComponentEntry(ExtendedEntryData* componentEntry) {
    m_controlWidget->setEntry(componentEntry);
    m_interactionWidget->setEntry(componentEntry);
}


void RelaxationSimulationWidget::setTabStatusIcon(QWidget* tabWidget, const QIcon& icon) {
    if (!tabWidget) return;
    ui->tabWidget->setTabIcon(ui->tabWidget->indexOf(tabWidget), icon);
}
