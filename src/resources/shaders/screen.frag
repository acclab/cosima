#version 330
out vec4 fragColor;

in vec2 texCoords;

uniform vec4 backgroundColor;
uniform sampler2D colorTexture;

void main()
{
    vec4 frontColor = texture(colorTexture, texCoords);
    fragColor = frontColor + backgroundColor * frontColor.a;
}
