/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef HISTORYTREEVIEW_H
#define HISTORYTREEVIEW_H

#include "gui/utils/historytreemodel.h"

#include <QPointer>
#include <QTreeView>


class HistoryTreeView : public QTreeView {
    Q_OBJECT
  public:
    explicit HistoryTreeView(QWidget* parent = nullptr);
    ~HistoryTreeView();

    int entry();
    int frame();

  public slots:
    void addEntry(const Entry* entry);
    void setHistory(const History&);
    void setSelectedFrame(int entry, int frame);

  private:
    QPointer<HistoryTreeModel> m_historyModel = nullptr;
    QModelIndex selectedEntry, selectedFrame;

    void selectSelected();

  private slots:
    void handleSelection(const QItemSelection&, const QItemSelection&);

  signals:
    void selectionModified(int entry, int frame);
};

#endif  // HISTORYTREEVIEW_H
