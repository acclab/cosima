/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "entryeditorwidget.h"
#include "ui_entryeditorwidget.h"

#include "api/atom.h"
#include "api/programsettings.h"
#include "api/project.h"
#include "api/util.h"

#include "gui/widgets/orientationactionwidget.h"

#include <QDebug>
#include <QScrollBar>


EntryEditorWidget::EntryEditorWidget(EntryEditor* editor, QWidget* parent)
        : QWidget(parent), ui(new Ui::EntryEditorWidget), m_editor(editor) {
    qDebug() << "EntryEditorWidget::EntryEditorWidget()" << this;

    ui->setupUi(this);

    m_typeTable = new TypeTable(m_editor, this);
    ui->atomTypesGB->layout()->addWidget(m_typeTable);

    m_selector = new AtomSelectorWidget(ui->plotWidget->glWidget());

    m_selector->setEntry(&m_editor->modifiedEntry());
    m_selector->setClosable(false);

    ui->dimensionSelectorWidget->setPeriodicBoundaries(m_editor->modifiedEntry().frame.pbc);

    m_actionWidgets.push_back(new EmptyActionWidget());
    m_actionWidgets.push_back(new RemoveAtomsWidget(m_editor, m_selector));
    m_actionWidgets.push_back(new FixAtomsWidget(m_editor, m_selector));
    m_actionWidgets.push_back(new ChangeAtomTypeWidget(m_editor, m_selector));
    m_actionWidgets.push_back(new OrientationActionWidget(m_editor));
    m_actionWidgets.push_back(new ShiftCellActionWidget(m_editor));

    for (AbstractActionWidget* w : m_actionWidgets) {
        ui->actionGB->layout()->addWidget(w);
        ui->actionCB->addItem(w->name());
    }

    on_changeAction(0);

    connect(ui->actionCB, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            &EntryEditorWidget::on_changeAction);

    // Initializing the plot
    ui->plotWidget->setEntry(m_editor->modifiedEntry());
    connect(&ProgramSettings::getInstance().visualSettings(), &VisualSettings::atomTypeColorsChanged, this,
            &EntryEditorWidget::on_updateAtoms);

    on_updateAtoms();
    on_updateBoundingBox();
    on_updateTypes();
    m_selector->init();
    m_selector->connectToPlot();

    // Connecting to the editor
    connect(m_editor, &EntryEditor::atomsChanged, this, &EntryEditorWidget::on_updateAtoms);
    connect(m_editor, &EntryEditor::typesChanged, this, &EntryEditorWidget::on_updateTypes);
    connect(m_editor, &EntryEditor::cellLimsChanged, this, &EntryEditorWidget::on_updateBoundingBox);
    connect(m_editor, &EntryEditor::changed, this, [this](bool changed) {
        qDebug() << m_selector;
        if (m_selector) {
            qDebug() << m_selector->isVisible();
            if (!m_selector->isVisible() && changed) {
                m_selector->zero();
            }
        }
    });

    connect(m_editor, &EntryEditor::entryChanged, this, [this] {

        ui->plotWidget->setEntry(m_editor->modifiedEntry());
        m_selector->setEntry(&m_editor->modifiedEntry());

        ui->dimensionSelectorWidget->setPeriodicBoundaries(m_editor->modifiedEntry().frame.pbc);

        m_selector->zero();
        on_changeAction(0);
        on_updateAtoms();
        on_updateBoundingBox();
        on_updateTypes();
        m_selector->init();
    });

    connect(ui->dimensionSelectorWidget, &DimensionSelector::dimensionsEdited, [this] {
        EntryEditor::CellLimitsEditAction a(ui->dimensionSelectorWidget->getDimensions());
        m_editor->edit(a);
    });
    connect(ui->dimensionSelectorWidget, &DimensionSelector::pbcEdited, [this] {
        EntryEditor::PeriodicBoundaryEditAction a(ui->dimensionSelectorWidget->getPeriodicBoundaries());
        m_editor->edit(a);
    });

    ui->actionCB->setFocusPolicy(Qt::StrongFocus);
    ui->actionCB->installEventFilter(new WheelGuard(this));

    setFocusProxy(ui->dimensionSelectorWidget);

    //    connect(&Project::getInstance().history, &History::historyChanged, [this] {
    //        ui->historyTreeView->changeData(Project::getInstance().history);
    //        // ui->frameSelectorWidget->setNumberOfFrames(Project::getInstance().history.latestEntry().frameCount());
    //        //        startLoading(Project::getInstance().history.entryCount() - 1,
    //        //                     Project::getInstance().history.latestEntry().frameCount() - 1);
    //    });


    ui->splitter->setCollapsible(0, true);
    ui->splitter->setCollapsible(1, false);
    ui->splitter->setCollapsible(2, false);

    util::decorateSplitterHandle(ui->splitter, 1);
    util::decorateSplitterHandle(ui->splitter, 2);


    ui->splitter->setSizes(QList<int>() << 0 << 400 << 300);


    ui->historyTreeView->setHistory(Project::getInstance().history);

    connect(ui->historyTreeView, &HistoryTreeView::selectionModified, [this](int entryIndex, int frameIndex) {
        const Entry* entry = Project::getInstance().history.entry(entryIndex, frameIndex);
        m_editor->setEntry(entry->extendedData());
    });

    connect(&Project::getInstance().history, &History::entryAdded, this, [this] {
        qDebug() << "entryAdded in EntryEditorWidget";
        ui->historyTreeView->addEntry(Project::getInstance().history.newestEntry());
    });
}


EntryEditorWidget::~EntryEditorWidget() {
    qDebug() << this << "::~EntryEditorWidget()";
    // Making sure that m_selector is not deleted by ui. Guarantees that ui->plotwidget is not deleted before
    // m_selector (even though it should not be a problem)
    m_selector->setParent(nullptr);
    delete m_selector;
    delete ui;
}


QWidget* EntryEditorWidget::lastWidget() {
    return ui->plotWidget->lastWidget();
}


void EntryEditorWidget::on_updateAtoms() {
    on_updateTypes();  // If the amounts have changed, the type table has to be redrawn
    m_selector->updateAtoms();
    ui->plotWidget->entryUpdated();
}

void EntryEditorWidget::on_updateTypes() {
    m_selector->updateTypes();
}

void EntryEditorWidget::on_changeAction(int index) {
    qDebug() << this << "::on_changeAction()";

    ui->actionCB->setCurrentIndex(index);
    for (QWidget* w : m_actionWidgets) w->setVisible(false);
    AbstractActionWidget* w = m_actionWidgets[index];
    AtomSelectionActionWidget* newWidget = qobject_cast<AtomSelectionActionWidget*>(w);
    if (newWidget) {
        qDebug() << "    "
                 << "newWidget exists";
        newWidget->displayAtomSelector();
    } else {
        qDebug() << "    "
                 << "newWidget doesn't exist";
        m_selector->hidePlottables();
    }
    w->setVisible(true);
}

void EntryEditorWidget::on_updateBoundingBox() {
    qDebug() << "EntryEditorWidget::updateBoundingBox()";

    ui->dimensionSelectorWidget->setDimensions(m_editor->modifiedEntry().frame.cellLims);
    m_selector->updateCellLims();
    ui->plotWidget->bbUpdated(m_editor->modifiedEntry().frame.cellLims);
}
