/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "abstractmesh.h"

#include "api/util.h"

#include <QDebug>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>


AbstractMesh::AbstractMesh(QObject* parent) : QObject(parent) {}


AbstractMesh::AbstractMesh(const std::vector<glm::vec3>& positions, const std::vector<glm::vec4>& colors,
                           const std::vector<unsigned int>& indices, QObject* parent)
        : QObject(parent) {
    m_positions = positions;
    m_colors = colors;
    m_indices = indices;
}


AbstractMesh::~AbstractMesh() {
    if (m_positionVbo) m_positionVbo->destroy();
    if (m_colorVbo) m_colorVbo->destroy();
    if (m_vao) m_vao->destroy();

    delete m_positionVbo;
    delete m_colorVbo;
    delete m_vao;
}


void AbstractMesh::initialize() {
    if (!QOpenGLContext::currentContext()) {
        qDebug() << "    NO OPENGL CONTEXT YET!";
        return;
    }

    if (isInitialized()) {
        qDebug() << "    Already initialized!";
        return;
    }

    setupVertexBufferObjects();

    m_vao = new QOpenGLVertexArrayObject;
    m_vao->create();
    m_vao->bind();
    setupVertexArrayObject();
    m_vao->release();
}


void AbstractMesh::setupVertexBufferObjects() {
    m_positionVbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_positionVbo->create();
    m_positionVbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    updateVertexBufferObject(m_positionVbo, m_positions);

    m_colorVbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_colorVbo->create();
    m_colorVbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    updateVertexBufferObject(m_colorVbo, m_colors);
}


void AbstractMesh::setupVertexArrayObject() {
    QOpenGLFunctions* glFuncs = QOpenGLContext::currentContext()->functions();

    m_positionVbo->bind();
    glFuncs->glEnableVertexAttribArray(0);
    glFuncs->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    m_colorVbo->bind();
    glFuncs->glEnableVertexAttribArray(1);
    glFuncs->glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
}


void AbstractMesh::setPositions(const std::vector<glm::vec3>& positions) {
    m_positions = positions;
    if (isInitialized()) {
        updateVertexBufferObject(m_positionVbo, m_positions);
    }
}


void AbstractMesh::setColors(const std::vector<glm::vec4>& colors) {
    m_colors = colors;
    if (isInitialized()) {
        updateVertexBufferObject(m_colorVbo, m_colors);
    }
}


void AbstractMesh::setIndices(const std::vector<unsigned int>& indices) {
    m_indices = indices;
}


const std::vector<unsigned int>& AbstractMesh::indices() {
    return m_indices;
}


int AbstractMesh::indexCount() {
    return static_cast<int>(m_indices.size());
}
