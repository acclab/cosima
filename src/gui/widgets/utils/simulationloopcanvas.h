/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SIMULATIONLOOPCANVAS_H
#define SIMULATIONLOOPCANVAS_H

#include "resizeablegraphicsview.h"

#include <QDebug>
#include <QGraphicsRectItem>

#include <cmath>

/*!
 * \brief ResizeableGraphicsView for visualizing the simulation loop.
 *
 * Can be used to show a schematic drawing of the simulation loop parameters.
 *
 * \sa GeneralIrradiationSettingsWidget
 */
class SimulationLoopCanvas : public ResizeableGraphicsView {

  public:
    explicit SimulationLoopCanvas(QWidget* parent = nullptr);
    ~SimulationLoopCanvas();

    void setUseRelax(bool useRelax);
    void setStartWithRelax(bool startWithRelax);
    void setCascadeTime(float cascadeTime);
    void setRelaxTime(float relaxTime);
    void setRepeatCounter(int counter);

    void draw();

  private:
    struct TextRectStruct {
        QGraphicsRectItem* rect;
        QGraphicsTextItem* text;

        int spacingX;
        int spacingY;

        void addToScene(int x, int y, int width, int height, QColor penColor, QColor color, QFont font,
                        QGraphicsScene* scene) {
            rect = scene->addRect(x, y, width, height, QPen(penColor), QBrush(color));
            text = scene->addText("", font);
        }

        void setVisible(bool visible) {
            rect->setVisible(visible);
            text->setVisible(visible);
        }

        void setText(QString textValue) { text->setPlainText(textValue); }

        void render(int x, int y) {
            rect->setPos(x, y);
            text->setPos(rect->pos() + rect->boundingRect().center() - text->boundingRect().center());
        }
    };

    struct ArrowStruct {
        QGraphicsPolygonItem* arrowHead;
        QList<QGraphicsLineItem*> arrowLines;

        void addToScene(int numberOfLines, QColor color, QGraphicsScene* scene) {
            QPolygon head;
            head.append(QPoint(0, 0));
            head.append(QPoint(20, 10));
            head.append(QPoint(0, 20));

            for (int i = 0; i < numberOfLines; i++) {
                arrowLines.append(scene->addLine(0, 0, 1, 1, QPen(color)));
            }
            arrowHead = scene->addPolygon(head, QPen(color), QBrush(color));
        }

        void setVisible(bool visible) {
            foreach (QGraphicsLineItem* item, arrowLines) { item->setVisible(visible); }
            arrowHead->setVisible(visible);
        }

        void render(QList<QLine> lines) {
            QLine lastLine;
            for (int i = 0; i < arrowLines.size(); i++) {
                arrowLines[i]->setLine(lines[i]);
                lastLine = lines[i];
            }
            float angle = atan2(lastLine.p2().y() - lastLine.p1().y(), lastLine.p2().x() - lastLine.p1().x());
            arrowHead->setTransformOriginPoint(QPoint(20, 10));
            arrowHead->setRotation(angle * 180 / 3.14159);
            arrowHead->setPos(lastLine.p2() - QPoint(20, 10));
        }
    };

    int m_viewMargin = 50;
    int m_margin = 35;
    int m_spacing = 100;

    bool m_useRelax;
    bool m_startWithRelax;
    float m_cascadelTime;
    float m_relaxTime;
    int m_repeatCounter;

    QGraphicsScene* scene;

    QGraphicsRectItem* m_boundingRect1;
    QGraphicsRectItem* m_boundingRect2;

    TextRectStruct m_cascadeTimeBox;
    TextRectStruct m_cascadeTextBox;
    TextRectStruct m_repeatBox;

    TextRectStruct m_relaxTimeBox;
    TextRectStruct m_relaxTextBox;

    ArrowStruct m_arrowRelax;
    ArrowStruct m_arrowRepeat;


    void renderCascadeBox(int m_x, int m_y);
    void renderRelaxBox(int m_x, int m_y);
    void renderRepeatLabel();
};

#endif  // SIMULATIONLOOPCANVAS_H
