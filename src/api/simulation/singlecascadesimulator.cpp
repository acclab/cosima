/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "singlecascadesimulator.h"

#include "api/project.h"

#include <QDir>


SingleCascadeSimulator::SingleCascadeSimulator(ParcasCascadeController* controller, QObject* parent)
        : Simulator(controller, parent) {}


ParcasCascadeController* SingleCascadeSimulator::controller() {
    return static_cast<ParcasCascadeController*>(Simulator::controller());
}


void SingleCascadeSimulator::preSimulationStep() {

    QString fromPath = runtimePath() + "/frames/frame_0/mdlat.xyz";
    QString toPath = runtimePath() + "/in/mdlat.in.xyz";
    if (!QFile::copy(fromPath, toPath)) {
        return;
    }

    const ExtendedEntryData& entry = currentEntry();
    controller()->writeInputFile(runtimePath() + "/in/md.in", entry);

    // Make sure the loop continues to the next step.
    Simulator::preSimulationStep();
}


void SingleCascadeSimulator::postSimulationStep() {
    // make the output directory
    QDir dir(runtimePath() + "/frames/frame_1/");
    if (!dir.isEmpty()) {
        qDebug() << "The target directory is already in use!";
        m_running = false;
        return;
    }
    if (dir.mkpath(dir.path())) {
        qDebug() << "    "
                 << "Created path: " << dir.path();
    }

    ExtendedEntryData entry;
    entry.frame.read(runtimePath() + "/out/end.xyz");

    m_editor.setEntry(entry);

    // Merge the ion wth the correct atom type if the setting is enabled.
    if (controller()->parameters()[api::parameters::ion::BASE_TYPE_MERGE_ON].value().toBool()) {
        qDebug() << "Merge the ion type with the selected atom type";
        ElementReader reader;
        int zTarget = controller()->parameters()[api::parameters::ion::BASE_TYPE_ATOM_NUMBER].value().toInt();
        AtomType targetType;
        targetType.mass = reader.getMass(zTarget);
        targetType.symbol = reader.getSymbol(zTarget);
        targetType.atomNumber = zTarget;
        targetType.typeNumber = controller()->parameters()[api::parameters::ion::BASE_TYPE].value().toInt();
        TypeSelection selection(0);
        EntryEditor::MergeToTypeAction action(targetType, true, &selection);
        m_editor.edit(action);
    }

    m_editor.saveEntryData(runtimePath() + "/frames/frame_1/mdlat.xyz");

    //    QFile::copy(runtimePath() + "/out/end.xyz", runtimePath() + "/frames/frame_1/mdlat.xyz");
    QFile::copy(runtimePath() + "/in/md.in", runtimePath() + "/frames/frame_1/md.in");
    QFile::copy(runtimePath() + "/parcas.log", runtimePath() + "/frames/frame_1/parcas.log");

    //! \todo: write "Cascade" to the info.txt file

    if (controller()->parameters()[api::parameters::output::MOVIE_ON].value().toBool()) {
        QFile::rename(runtimePath() + "/out/md.movie", runtimePath() + "/frames/frame_1/md.movie");
    }

    emit newFrame(runtimePath() + "/frames/frame_1/");

    // Stop the simulation
    m_running = false;

    // Make sure the loop continues to the next step.
    Simulator::postSimulationStep();
}
