/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATOMSELECTORARRAYWIDGET_H
#define ATOMSELECTORARRAYWIDGET_H

#include "abstractatomselectorwidget.h"
#include "api/atomselections.h"
#include "atomselectorwidget.h"

#include <QGroupBox>


namespace Ui {
class AtomSelectorArrayWidget;
}

class AtomSelectorArrayWidget : public AbstractAtomSelectorWidget {
    Q_OBJECT

  public:
    explicit AtomSelectorArrayWidget(PlotCellGLWidget* context, QWidget* parent = nullptr);
    ~AtomSelectorArrayWidget() override;

    // virtual PlottableArray* geometry();

    const AbstractAtomSelection& selector() override;
    void setEntry(const ExtendedEntryData* entry) override;
    void init() override;
    void updateAtoms() override;
    void updateCellLims() override;
    void updateTypes() override;
    void connectToPlot() override;
    void hidePlottables() override;

    void setMode(ArrayAtomSelection::Mode mode);

    void addSelector();

  private:
    Ui::AtomSelectorArrayWidget* ui;

    QVector<AtomSelectorWidget*> m_selectors;

    ArrayAtomSelection m_selector;
};

#endif  // ATOMSELECTORARRAYWIDGET_H
