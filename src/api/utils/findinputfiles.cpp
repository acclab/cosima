/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "findinputfiles.h"

#include <QDir>


void FindInputFiles::readReppotFiles(ExtendedEntryData* entry) {
    QDir potDir(entry->path + "/potentials");
    // Loop over all type pairs to find the needed files.
    for (const AtomType type1 : entry->data.types.allTypes) {
        if (type1.atomNumber < 1) continue;

        for (const AtomType type2 : entry->data.types.allTypes) {
            if (type2.atomNumber < 1) continue;
            if (type2.typeNumber < type1.typeNumber) continue;

            // Only list unique symbol pairs.
            ElementPair pair(type1.symbol, type2.symbol);
            if (!entry->data.potentialFiles.contains(pair)) {
                // Create the file entry
                DataFile<ReppotFileData> file;
                // Try and read it from file, the file will remember if it got any data, and the GUI will prompt the
                // user for more input if it couldn't find anything.
                QString filename = QString("reppot.%1.%2.in").arg(type1.symbol).arg(type2.symbol);
                file.read(potDir.path() + "/" + filename);
                entry->data.potentialFiles[pair] = file;
            }
        }
    }
}


void FindInputFiles::readElstopFiles(ExtendedEntryData* entry, const QString& substrate) {
    QDir elstopDir(entry->path + "/elstop");
    // Loop over all atom types to dinf the needed files.
    for (const AtomType& type : entry->data.types.allTypes) {
        if (type.atomNumber < 1) continue;

        if (!entry->data.elstopFiles.contains(type.symbol)) {
            // Create the file entry
            DataFile<ElstopFileData> file;
            // Try and read it from file, the file will remember if it got any data, and the GUI will prompt the
            // user for more input if it couldn't find anything.
            QString filename = QString("elstop.%1.%2.in").arg(type.symbol).arg(substrate);
            file.read(elstopDir.path() + "/" + filename);
            entry->data.elstopFiles[type.symbol] = file;
        }
    }
}
