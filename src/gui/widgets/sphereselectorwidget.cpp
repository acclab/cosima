/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "sphereselectorwidget.h"
#include "ui_sphereselectorwidget.h"

#include "utils/wheelguard.h"

#include <QColorDialog>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include <cmath>

SphereSelectorWidget::SphereSelectorWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent), ui(new Ui::SphereSelectorWidget) {
    qDebug() << "SphereSelectorWidget::SphereSelectorWidget()";

    ui->setupUi(this);

    connect(ui->xSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->ySB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->zSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->radiusSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });

    connect(ui->colorLabel, &ClickableLabel::clicked, [this] {
        QColorDialog dialog(this);
        dialog.setOption(QColorDialog::DontUseNativeDialog, true);
        dialog.setOption(QColorDialog::ShowAlphaChannel, true);
        dialog.setWindowTitle("Color Picker");
        dialog.setCurrentColor(m_color);

        if (dialog.exec() == QDialog::Accepted && dialog.currentColor() != m_color) {
            setColor(dialog.currentColor());
        }
    });

    QVector<QWidget*> wheelGuardedWidgets = {ui->radiusSB, ui->xSB, ui->ySB, ui->zSB};
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    setFocusProxy(ui->xSB);
}


SphereSelectorWidget::~SphereSelectorWidget() {
    if (m_context) {
        m_context->removeModel(m_sphereModel);
    }

    delete m_sphereModel;

    delete ui;
}


const AbstractAtomSelection& SphereSelectorWidget::selector() {
    return m_selector;
}


void SphereSelectorWidget::init() {
    if (!m_entry) return;
    blockSignalsFromComponents(true);

    //! \todo setLimits() and setDimensions()
    const Box& lims = m_entry->frame.cellLims;

    ui->xSB->setValue((lims.x1 + lims.x2) / 2);
    ui->ySB->setValue((lims.y1 + lims.y2) / 2);
    ui->zSB->setValue((lims.z1 + lims.z2) / 2);

    ui->radiusSB->setValue(glm::length(lims.diagonal()) / 2);


    m_color = QColor(0, 0, 255, 100);

    m_sphereModel = new SphereModel(getDimension(), m_color, m_color, 3);

    m_sphereModel->setShowFaces(true);
    m_sphereModel->setShowLines(false);

    connect(ui->surfaceCB, &QCheckBox::clicked, [this](bool checked) {
        m_sphereModel->setShowFaces(checked);
        m_context->update();
    });
    connect(ui->wireframeCB, &QCheckBox::clicked, [this](bool checked) {
        m_sphereModel->setShowLines(checked);
        m_context->update();
    });

    setColor(m_color);

    ui->surfaceCB->setCheckState(Qt::Checked);
    ui->wireframeCB->setCheckState(Qt::Unchecked);

    blockSignalsFromComponents(false);

    updateDimensions();
}


void SphereSelectorWidget::connectToPlot() {
    //    m_context->addMesh(m_lineMesh);
    //    if (m_context) {
    //        disconnect(m_context, 0, this, 0);
    //        glWidget->removePlottableArray(&plottableArray);
    //    }
    //    AbstractAtomSelectorWidget::connectToPlot(glWidget);
    //    if (m_context) {
    //        glWidget->addPlottableArray(&plottableArray);
    //        connect(m_context, &PlotCellGLWidget::destroyed, this, [this, glWidget] {
    //            if (m_context == glWidget) m_context = nullptr;
    //        });
    //    }
    m_context->addModel(m_sphereModel);
}


void SphereSelectorWidget::hidePlottables() {
    QSignalBlocker b1(ui->wireframeCB);
    QSignalBlocker b2(ui->surfaceCB);

    ui->wireframeCB->setChecked(false);
    ui->surfaceCB->setChecked(false);
}


Sphere SphereSelectorWidget::getDimension() {
    double radius = ui->radiusSB->value();
    glm::vec3 center(ui->xSB->value(), ui->ySB->value(), ui->zSB->value());
    return Sphere(center, radius);
}


void SphereSelectorWidget::setDimension(const Sphere&) {}


void SphereSelectorWidget::blockSignalsFromComponents(bool b) {
    ui->xSB->blockSignals(b);
    ui->ySB->blockSignals(b);
    ui->zSB->blockSignals(b);
    ui->radiusSB->blockSignals(b);
    ui->wireframeCB->blockSignals(b);
    ui->surfaceCB->blockSignals(b);
}


void SphereSelectorWidget::updateDimensions() {
    Sphere s(glm::dvec3(ui->xSB->value(), ui->ySB->value(), ui->zSB->value()), ui->radiusSB->value());
    m_selector = SphereAtomSelection(s);
    m_sphereModel->setSphere(s);

    m_context->update();
}


void SphereSelectorWidget::setColor(QColor color) {
    m_color = color;
    ui->colorLabel->setStyleSheet(QString("background-color: rgba(%1,%2,%3,%4);border: 1px solid black;")
                                      .arg(color.red())
                                      .arg(color.green())
                                      .arg(color.blue())
                                      .arg(color.alpha()));

    m_sphereModel->setFaceColor(color);
    m_sphereModel->setLineColor(color.rgb());
}
