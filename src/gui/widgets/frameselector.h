/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FRAMESELECTOR_H
#define FRAMESELECTOR_H

#include "api/history.h"

#include <QMap>
#include <QWidget>


namespace Ui {
class FrameSelector;
}

/*!
 * \brief QWidget for selecting a frame.
 *
 * Includes a slider for selecting a frame from the list determined by the given FrameHandler.
 *
 * \sa OutputTab
 */
class FrameSelector : public QWidget {
    Q_OBJECT

  public:
    explicit FrameSelector(QWidget* parent = nullptr);
    ~FrameSelector();

  public slots:
    void setNumberOfFrames(int);
    void setValue(int);

  private:
    Ui::FrameSelector* ui;

    int frame, frames;

  private slots:
    void update();
    void on_frameSelectorPrevPB_clicked();
    void on_frameSelectorNextPB_clicked();
    void on_frameSelectorLE_editingFinished();

  signals:
    void selectionChanged(int);
};

#endif  // FRAMESELECTOR_H
