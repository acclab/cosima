/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef UTIL_H
#define UTIL_H

#include <QColor>
#include <QDoubleSpinBox>
#include <QSplitter>
#include <QString>
#include <glm/vec4.hpp>

/*!
 * Namespace for utility functions
 */
namespace util {
bool copyDirectory(const QString& srcPath, const QString& destPath);
bool updateDoubleSpinBoxValue(QDoubleSpinBox* sb, double value);
QColor vectorToColor(const glm::vec4& v);
glm::vec4 colorToVector(const QColor& c);
void decorateSplitterHandle(QSplitter* splitter, int index);
}  // namespace util

#endif  // UTIL_H
