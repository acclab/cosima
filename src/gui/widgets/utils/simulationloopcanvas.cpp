/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "simulationloopcanvas.h"

#include <QDebug>
#include <QGraphicsRectItem>


SimulationLoopCanvas::SimulationLoopCanvas(QWidget* parent) : ResizeableGraphicsView(parent) {
    scene = new QGraphicsScene();
    setScene(scene);
    QFont font;
    font.setPixelSize(40);

    m_boundingRect1 = scene->addRect(0, 0, 300, 300, QPen(Qt::black), QBrush(Qt::transparent));
    m_boundingRect2 = scene->addRect(0, 0, 300, 300, QPen(Qt::black), QBrush(Qt::transparent));

    m_cascadeTimeBox.addToScene(0, 0, 200, 100, Qt::transparent, Qt::white, font, scene);
    m_cascadeTimeBox.setText("");
    m_cascadeTextBox.addToScene(0, 0, 200, 100, Qt::transparent, QColor(255, 100, 100), font, scene);
    m_cascadeTextBox.setText("Cascade");

    m_repeatBox.addToScene(0, 0, 200, 100, Qt::white, Qt::white, font, scene);
    m_repeatBox.setText("");

    m_relaxTimeBox.addToScene(0, 0, 200, 100, Qt::transparent, Qt::white, font, scene);
    m_relaxTimeBox.setText("");
    m_relaxTextBox.addToScene(0, 0, 200, 100, Qt::transparent, QColor(100, 255, 100), font, scene);
    m_relaxTextBox.setText("Relax");


    m_arrowRelax.addToScene(1, Qt::black, scene);
    m_arrowRepeat.addToScene(3, Qt::black, scene);

    m_useRelax = true;
    m_startWithRelax = true;

    m_cascadelTime = 100;
    m_relaxTime = 1000;
    m_repeatCounter = 10;

    draw();
}

SimulationLoopCanvas::~SimulationLoopCanvas() {
    scene->clear();
    delete scene;
}


void SimulationLoopCanvas::setUseRelax(bool useRelax) {
    m_useRelax = useRelax;
}

void SimulationLoopCanvas::setStartWithRelax(bool startWithRelax) {
    m_startWithRelax = startWithRelax;
}

void SimulationLoopCanvas::setCascadeTime(float cascadeTime) {
    m_cascadelTime = cascadeTime;
}

void SimulationLoopCanvas::setRelaxTime(float relaxTime) {
    m_relaxTime = relaxTime;
}

void SimulationLoopCanvas::setRepeatCounter(int counter) {
    m_repeatCounter = counter;
}

/*!
 * Redraws the canvas.
 */
void SimulationLoopCanvas::draw() {
    int xCascade;
    int xRelax;

    if (m_useRelax && m_startWithRelax) {
        xCascade = m_boundingRect2->rect().width() + m_spacing;
        xRelax = m_margin;
    } else {
        xCascade = m_margin;
        xRelax = m_boundingRect1->rect().width() + m_spacing;
    }

    int yCascade = m_margin;
    int yRelax = m_margin;

    renderCascadeBox(xCascade, yCascade);
    renderRelaxBox(xRelax, yRelax);
    renderRepeatLabel();

    m_arrowRelax.setVisible(m_useRelax);
    m_arrowRepeat.setVisible(m_useRelax);
    if (m_useRelax) {
        int xArrowRelaxStart;
        int xArrowRelaxEnd;

        int xArrowRepeatStart;
        int yArrowRepeatStart;
        int xArrowRepeatEnd;
        int yArrowRepeatEnd;

        if (m_startWithRelax) {
            xArrowRelaxStart = m_boundingRect2->rect().right();
            xArrowRelaxEnd = m_boundingRect1->rect().left();

            xArrowRepeatStart = m_boundingRect1->rect().right() - m_boundingRect1->rect().width() / 2.0;
            yArrowRepeatStart = m_boundingRect1->rect().bottom();
            xArrowRepeatEnd = m_boundingRect2->rect().right() - m_boundingRect2->rect().width() / 2.0;
            yArrowRepeatEnd = m_boundingRect2->rect().bottom();
        } else {
            xArrowRelaxStart = m_boundingRect1->rect().right();
            xArrowRelaxEnd = m_boundingRect2->rect().left();

            xArrowRepeatStart = m_boundingRect2->rect().right() - m_boundingRect2->rect().width() / 2.0;
            yArrowRepeatStart = m_boundingRect2->rect().bottom();
            xArrowRepeatEnd = m_boundingRect1->rect().right() - m_boundingRect1->rect().width() / 2.0;
            yArrowRepeatEnd = m_boundingRect1->rect().bottom();
        }

        int offset = m_margin;
        if (!m_startWithRelax) {
            offset += qAbs(m_boundingRect1->rect().height() - m_boundingRect2->rect().height());
        }

        int yArrowRelaxStart = m_cascadeTimeBox.rect->pos().y() + m_cascadeTimeBox.rect->rect().height() / 2.0;
        int yArrowRelaxEnd = yArrowRelaxStart;

        QPoint pRelax1(xArrowRelaxStart, yArrowRelaxStart);
        QPoint pRelax2(xArrowRelaxEnd, yArrowRelaxEnd);
        QList<QLine> linesRelax;
        linesRelax << QLine(pRelax1, pRelax2);
        m_arrowRelax.render(linesRelax);

        QPoint pRepeat1(xArrowRepeatStart, yArrowRepeatStart);
        QPoint pRepeat2(xArrowRepeatStart, yArrowRepeatStart + offset);
        QPoint pRepeat3(xArrowRepeatEnd, yArrowRepeatStart + offset);
        QPoint pRepeat4(xArrowRepeatEnd, yArrowRepeatEnd);
        QList<QLine> linesRepeat;
        linesRepeat << QLine(pRepeat1, pRepeat2) << QLine(pRepeat2, pRepeat3) << QLine(pRepeat3, pRepeat4);
        m_arrowRepeat.render(linesRepeat);
    }

    QPoint viewCorner = m_boundingRect1->pos().toPoint() - QPoint(m_viewMargin, m_viewMargin);
    int viewW = 2 * m_viewMargin + m_boundingRect1->rect().width() + m_boundingRect2->rect().width() + 2 * m_margin;
    int viewH = 2 * m_viewMargin + m_boundingRect1->rect().height();
    setView(viewCorner.x(), viewCorner.y(), viewW, viewH);
}

void SimulationLoopCanvas::renderCascadeBox(int x, int y) {
    int bbW = 2 * m_margin;
    int bbH = 2 * m_margin;

    // Render Time and Text labels
    m_cascadeTimeBox.setText(QString::number(m_cascadelTime) + " fs");
    m_cascadeTimeBox.render(x, y);

    x += m_cascadeTimeBox.rect->rect().width();
    m_cascadeTextBox.render(x, y);

    bbW += m_cascadeTimeBox.rect->rect().width() + m_cascadeTextBox.rect->rect().width();
    bbH += m_cascadeTimeBox.rect->rect().height();

    // Render the surrounding bounding box
    x = m_cascadeTimeBox.rect->pos().x() - m_margin;
    y = m_cascadeTimeBox.rect->pos().y() - m_margin;
    m_boundingRect1->setRect(QRect(x, y, bbW, bbH));
}

void SimulationLoopCanvas::renderRelaxBox(int x, int y) {
    m_relaxTimeBox.setVisible(m_useRelax);
    m_relaxTextBox.setVisible(m_useRelax);
    m_boundingRect2->setVisible(m_useRelax);

    if (m_useRelax) {
        // Render Time and Text labels
        m_relaxTimeBox.setText(QString::number(m_relaxTime) + " fs");
        m_relaxTimeBox.render(x, y);
        x += m_relaxTimeBox.rect->rect().width();
        m_relaxTextBox.render(x, y);

        // Render the surrounding bounding box
        x = m_relaxTimeBox.rect->pos().x() - m_margin;
        y = m_relaxTimeBox.rect->pos().y() - m_margin;
        int bbW = 2 * m_margin + m_relaxTimeBox.rect->rect().width() + m_relaxTextBox.rect->rect().width();
        int bbH = 2 * m_margin + m_relaxTimeBox.rect->rect().height();
        m_boundingRect2->setRect(QRect(x, y, bbW, bbH));
    }
}

void SimulationLoopCanvas::renderRepeatLabel() {
    m_repeatBox.setVisible(m_useRelax);
    if (m_useRelax) {
        // Render Repeat label
        m_repeatBox.setText("x" + QString::number(m_repeatCounter));
        m_repeatBox.rect->setRect(m_repeatBox.text->boundingRect());

        // m_repeat.rect->setRect(m_repeat.rect->boundingRect());
        int x = m_boundingRect1->rect().center().x() - m_repeatBox.rect->boundingRect().center().x();
        int y = -m_repeatBox.rect->boundingRect().center().y();
        m_repeatBox.render(x, y);
    }
}
