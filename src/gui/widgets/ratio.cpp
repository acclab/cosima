/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ratio.h"
#include "ui_ratio.h"

#include <QSet>


Ratio::Ratio(QWidget* parent) : OutputModule(parent), ui(new Ui::Ratio) {
    graphInitiated = false;
    ui->setupUi(this);

    connect(ui->selector, &GraphSelector::selectionChanged, this, &Ratio::changeGraph);

    ui->graph->setRes(3);
    ui->graph->setResZ(50);
    ui->graph->setTypes(1, 2);

    ui->selector->setRes(3);

    ui->resLE->setText("3");
    ui->resZLE->setText("50");

    ui->selector->init();
}

Ratio::~Ratio() {
    delete ui;
}

/*!
 * Redraws the graph and refreshes the atom types.
 */
void Ratio::draw(const XyzData* cell) {
    graphInitiated = false;
    atomData = cell;
    ui->graph->readAtoms(atomData);
    ui->graph->plotRatio(ui->selector->getI(), ui->selector->getJ());
    updateTypes();
    graphInitiated = true;
}

void Ratio::changeGraph(int i, int j) {
    ui->graph->plotRatio(i, j);
}

void Ratio::updateGraphResolution(int res) {
    ui->graph->setRes(res);
    ui->graph->readAtoms(atomData);
}

/*!
 * Updates the selectable atom types based on what were found.
 */
void Ratio::updateTypes() {
    QSet<QString> typeSet = types.keys().toSet();
    types.clear();
    ui->type1CB->clear();
    ui->type2CB->clear();

    for (Atom a : atomData->atoms) {
        if (!typeSet.contains(a.symbol)) {
            types.insert(a.symbol, a.type);
            typeSet.insert(a.symbol);
        }
    }

    foreach (const QString& symbol, typeSet) {
        ui->type1CB->addItem(symbol);
        ui->type2CB->addItem(symbol);
    }
    ui->type1CB->setCurrentText(ui->graph->getTypeSymbol1());
    ui->type2CB->setCurrentText(ui->graph->getTypeSymbol2());
}

void Ratio::on_resLE_editingFinished() {
    ui->resLE->setText(QString::number(ui->resLE->text().toInt()));
    int res = ui->resLE->text().toInt();
    if (res > 0) {
        ui->graph->setRes(res);
        ui->graph->readAtoms(atomData);
        ui->graph->plotRatio(res / 2, res / 2);
        ui->selector->setRes(res);
        ui->selector->init();
    } else {
        ui->resLE->setText(QString::number(ui->graph->getRes()));
    }
}

void Ratio::on_resZLE_editingFinished() {
    ui->resZLE->setText(QString::number(ui->resZLE->text().toInt()));
    int resz = ui->resZLE->text().toInt();
    if (resz > 0) {
        ui->graph->setResZ(resz);
        ui->graph->readAtoms(atomData);
        ui->graph->plotRatio(ui->selector->getI(), ui->selector->getJ());
    } else {
        ui->resLE->setText(QString::number(ui->graph->getRes()));
    }
}


void Ratio::on_type1CB_currentIndexChanged(const QString& text) {
    if (!graphInitiated) return;

    ui->graph->setTypes(text, ui->type2CB->currentText());
    ui->graph->plotRatio(ui->selector->getI(), ui->selector->getJ());
}

void Ratio::on_type2CB_currentIndexChanged(const QString& text) {
    if (!graphInitiated) return;

    ui->graph->setTypes(ui->type1CB->currentText(), text);
    ui->graph->plotRatio(ui->selector->getI(), ui->selector->getJ());
}
