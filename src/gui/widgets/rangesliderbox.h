/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef RANGESLIDERBOX_H
#define RANGESLIDERBOX_H

#include "gui/widgets/utils/distancefromlimitsb.h"
#include "gui/widgets/utils/qxtspanslider/qxtspanslider.h"

#include <QWidget>


/*!
 * \brief QWidget for selecting a range within the given limits.
 *
 * Includes a QxtSpanSlider and one DistanceFromLimitSB for both ends values of the slider.
 *
 * \sa DistanceFromLimitSB
 */
class RangeSliderBox : public QWidget {
    Q_OBJECT

  public:
    explicit RangeSliderBox(QWidget* parent = nullptr);
    ~RangeSliderBox() {}

    void setPrecision(int);
    void setMirrored(bool);
    void setHighlightInside(bool);
    void setShowDistanceFromLimit(bool);

    double lowerValue();
    double upperValue();

  private:
    DistanceFromLimitSB* m_doubleSpinBox_lower;
    DistanceFromLimitSB* m_doubleSpinBox_upper;
    QxtSpanSlider* m_slider;

    bool m_mirrored = false;
    double m_min = 0;
    double m_max = 10;
    double m_lower = 0;
    double m_upper = 10;

  public slots:
    void setLowerValue(double);
    void setUpperValue(double);
    void setRange(double, double);
    void setLimits(double, double);

  private slots:
    void updateWidgets();

  signals:
    void rangeEdited(double val1, double val2);
};

#endif  // RANGESLIDERBOX_H
