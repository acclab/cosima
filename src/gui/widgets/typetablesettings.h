/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TYPETABLESETTINGS_H
#define TYPETABLESETTINGS_H

#include <QWidget>


class QTableWidget;
class VisualSettings;

class TypeTableSettings : public QWidget {
    Q_OBJECT

  public:
    explicit TypeTableSettings(QWidget* parent = nullptr);

    void setSettings(const VisualSettings& settings);

    const QHash<int, QColor>& colors() { return m_colors; }
    const QHash<int, float>& radii() { return m_radii; }

  private:
    QTableWidget* m_table;

    QHash<int, QColor> m_colors;
    QHash<int, float> m_radii;

    void setRowData(int type, int height);
    QWidget* createCellWidget(QWidget* childWidget);

    bool updateTypeColor(int type);
};

#endif  // TYPETABLESETTINGS_H
