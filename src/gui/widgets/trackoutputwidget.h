/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TRACKOUTPUTWIDGET_H
#define TRACKOUTPUTWIDGET_H

#include "gui/utils/historytreemodel.h"
#include "outputmodule.h"
#include "plotcell.h"

#include <QItemSelection>
#include <QWidget>

#include <vector>


namespace Ui {
class TrackOutputWidget;
}

/*!
 * \brief QWidget for output visualization.
 *
 * An interactive plot of the chosen frame of the simulation is plotted using PlotCell.
 * The chosen frame can be analyzed using OutputModules.
 *
 * \sa PlotCell, OutputModule
 */
class TrackOutputWidget : public QWidget {
    Q_OBJECT

  public:
    explicit TrackOutputWidget(QWidget* parent = nullptr);
    ~TrackOutputWidget();
    void updateOutput(int, int, int);

  private:
    Ui::TrackOutputWidget* ui;
    OutputModule* selectedModule = nullptr;

    QPair<int, int> beingLoaded, loaded;
    ExtendedEntryData loadedEntry;
    FrameReaderThread frameReader;

  private slots:
    void startLoading(int entry, int frame);
    void loadingFailed();
    void loadingFinished();

    void closeOutputModule();

    void on_comboBox_currentIndexChanged(const QString& arg1);
};

#endif  // TRACKOUTPUTWIDGET_H
