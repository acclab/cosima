/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "plotcell.h"
#include "ui_plotcell.h"

#include "api/programsettings.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>

#include <QColorDialog>
#include <QSignalMapper>


PlotCell::PlotCell(QWidget* parent) : QWidget(parent), ui(new Ui::PlotCell) {
    ui->setupUi(this);

    setVisuals(&ProgramSettings::getInstance().visualSettings());

    ui->orientationCB->addItem("Left", Left);
    ui->orientationCB->addItem("Right", Right);
    ui->orientationCB->addItem("Back", Back);
    ui->orientationCB->addItem("Front", Front);
    ui->orientationCB->addItem("Bottom", Bottom);
    ui->orientationCB->addItem("Top", Top);

    ui->orientationCB->addItem("Ortho", Orthogonal);
    ui->orientationCB->addItem("Perspective", Perspective);

    ui->widget->usePerspective(false);
    changeViewOption(Right);

    connect(ui->orientationCB, QOverload<int>::of(&QComboBox::currentIndexChanged),
            [this] { changeViewOption(static_cast<ViewOption>(ui->orientationCB->currentData().toInt())); });
    connect(ui->widget, &PlotCellGLWidget::orientationChanged, this, &PlotCell::freeOrientation);

    connect(ui->bbCB, &QCheckBox::clicked, [this](bool checked) { m_visuals->changeCellBoundaryEnabled(checked); });
    connect(ui->widget, &PlotCellGLWidget::mouseMoved, this, &PlotCell::updateHoveredParticle);
    connect(ui->widget, &PlotCellGLWidget::clicked, this, &PlotCell::updateClickedParticle);
    connect(ui->widget, &PlotCellGLWidget::widgetLeft, ui->hoverParticleLabel, &QLineEdit::hide);

    QIcon icon;
    QPixmap iconPixmap(":/icons/triangle.png");
    iconPixmap = iconPixmap.scaled(QSize(20, 20));
    icon.addPixmap(iconPixmap, QIcon::Normal, QIcon::Off);
    QMatrix rm;
    rm.rotate(180);
    iconPixmap = iconPixmap.transformed(rm);
    icon.addPixmap(iconPixmap, QIcon::Normal, QIcon::On);
    ui->showMenuPB->setIcon(icon);
    ui->showMenuPB->setCheckable(true);

    showMenus(false);
    connect(ui->showMenuPB, &QPushButton::clicked, [this] { showMenus(ui->showMenuPB->isChecked()); });

    ui->hoverParticleLabel->setVisible(false);
    ui->clickedParticleWidget->setVisible(false);
    ui->clickedParticleWidget->setObjectName("SelectedParticleWidget");
    ui->clickedParticleWidget->setStyleSheet("#SelectedParticleWidget {border: 1px solid gray; background: white}");
    ui->clickedParticleWidget->setAutoFillBackground(true);

    connect(ui->bbColorPB, &QPushButton::clicked, this, &PlotCell::outlineColorChangeEvent);
    connect(ui->bgColorPB, &QPushButton::clicked, this, QOverload<>::of(&PlotCell::backgroundColorChangeEvent));

    ui->orientationCB->installEventFilter(new WheelGuard(this));
    ui->orientationCB->setFocusPolicy(Qt::StrongFocus);

    setFocusProxy(ui->showMenuPB);

    ui->spinBox_depthPeels->setValue(m_visuals->depthPeelCount());
}

PlotCell::~PlotCell() {
    if (m_grid) delete m_grid;
    delete ui;
}


//! \todo Deprecated function, should be replaced somehow
void PlotCell::updateParticles() {
    if (m_grid) m_grid->update();
    ui->widget->on_atomsUpdated();
}


void PlotCell::updateBackground(const QColor& color) {
    qDebug() << "PlotCell::updateBackground()";
    ui->bgColorPB->setStyleSheet(QString("background-color: rgb(%1,%2,%3);border: 1px solid black;")
                                     .arg(color.red())
                                     .arg(color.green())
                                     .arg(color.blue()));
    //! \todo Move the setBackground to the widget itself. This makes it more consistent with the sginal/slot system.
    //! Just make the glwidet listen for backgroundChange signals from the VisualSettings.
    ui->widget->setBackgroundColor(color);
}


void PlotCell::updateOutline(const QColor& color) {
    qDebug() << "PlotCell::updateOutline()";
    ui->bbColorPB->setStyleSheet(QString("background-color: rgb(%1,%2,%3);border: 1px solid black;")
                                     .arg(color.red())
                                     .arg(color.green())
                                     .arg(color.blue()));
}


/*!
 * Refreshes the atom type table.
 */
void PlotCell::updateList() {
    if (!m_entry) return;

    qDeleteAll(m_gridWidgets);
    m_gridWidgets.clear();

    QWidget* last = ui->showMenuPB;

    int i = 0;

    for (int typeNumber : m_entry->frame.amounts.keys()) {
        if (m_entry->frame.amounts.value(typeNumber) == 0) continue;

        QString s;
        if (m_entry->data.types.typeList.contains(typeNumber)) {
            s = m_entry->data.types.typeList[typeNumber].symbol;
        } else if (m_entry->data.types.typeList.contains(-typeNumber)) {
            s = m_entry->data.types.typeList[-typeNumber].symbol;
        }

        QLabel* label_type = new QLabel;
        QCheckBox* checkBox_hide = new QCheckBox;
        QDoubleSpinBox* spinBox_radius = new QDoubleSpinBox;
        ClickableLabel* label_color = new ClickableLabel;

        label_type->setText(QString("%1 (%2)").arg(typeNumber).arg(s));
        label_type->setFixedWidth(40);

        connect(checkBox_hide, &QCheckBox::clicked, [this, typeNumber](bool c) {
            ui->widget->on_hideType(typeNumber, !c);
            ui->widget->update();
        });
        checkBox_hide->setCheckState(Qt::CheckState::Checked);

        spinBox_radius->setDecimals(1);
        spinBox_radius->setSingleStep(0.1);
        spinBox_radius->setValue(static_cast<double>(m_visuals->atomTypeSizes().value(typeNumber)));
        spinBox_radius->installEventFilter(new WheelGuard(spinBox_radius));
        spinBox_radius->setFocusPolicy(Qt::StrongFocus);

        // When the type radius value changes, it is only updated locally to the PlottableFrame object. When the editing
        // is finished, it is updated to VisualSettings, and thus to all other plots.
        connect(spinBox_radius, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                [this, typeNumber](float val) { ui->widget->on_setTypeRadius(typeNumber, val); });

        connect(spinBox_radius, &QDoubleSpinBox::editingFinished, [this, typeNumber, spinBox_radius] {
            QHash<int, float> sizes = m_visuals->atomTypeSizes();
            sizes[typeNumber] = static_cast<float>(spinBox_radius->value());
            m_visuals->changeTypeSizes(sizes);
        });

        connect(m_visuals, &VisualSettings::atomTypeSizesChanged, spinBox_radius, [this, spinBox_radius, typeNumber] {
            QSignalBlocker b(spinBox_radius);
            spinBox_radius->setValue(static_cast<double>(m_visuals->atomTypeSizes()[typeNumber]));
        });


        label_color->setFixedSize(20, 20);
        label_color->setColor(m_visuals->atomTypeColors().value(typeNumber));

        connect(label_color, &ClickableLabel::clicked, [this, typeNumber] { particleColorChangeEvent(typeNumber); });

        connect(m_visuals, &VisualSettings::atomTypeColorsChanged, label_color, [this, label_color, typeNumber] {
            label_color->setColor(m_visuals->atomTypeColors().value(typeNumber));
        });

        ui->atomGridLayout->addWidget(label_type, i, 0);
        ui->atomGridLayout->addWidget(label_color, i, 1);
        ui->atomGridLayout->addWidget(checkBox_hide, i, 2);
        ui->atomGridLayout->addWidget(spinBox_radius, i, 3);
        ++i;

        setTabOrder(last, label_color);
        setTabOrder(label_color, checkBox_hide);
        setTabOrder(checkBox_hide, spinBox_radius);

        last = spinBox_radius;

        m_gridWidgets << label_type << label_color << checkBox_hide << spinBox_radius;
    }

    // Process resize events in order to get correct size hint for the contents
    qApp->processEvents();

    ui->typesGB->setMaximumHeight(ui->scrollAreaWidgetContents->sizeHint().height() + 50);

    setTabOrder(last, ui->bbCB);
    setTabOrder(ui->bbCB, ui->bbColorPB);
    setTabOrder(ui->bbColorPB, ui->bgColorPB);
    setTabOrder(ui->bgColorPB, ui->orientationCB);
    emit tabOrderUpdated(ui->orientationCB);
}


/*!
 * Sets a new entry to be plotted.
 */
void PlotCell::setEntry(const ExtendedEntryData& entry) {
    qDebug() << "PlotCell::setEntry()";

    m_entry = &entry;

    if (m_grid) delete m_grid;
    m_grid = new ParticleGrid(m_entry->frame, false);

    ui->widget->setVisualSettings(*m_visuals);
    ui->widget->setFrame(m_entry->frame);

    entryUpdated();
}


void PlotCell::entryUpdated() {
    if (!m_entry) return;

    updateList();
    updateParticles();
}


/*!
 * Sets external visual settings that will be used for plotting the frame.
 */
void PlotCell::setVisuals(VisualSettings* settings) {
    if (m_visuals) {
        disconnect(m_visuals);
    }

    m_visuals = settings;
    //    if (m_plottableFrame) m_plottableFrame->changeVisualSettings(*settings);
    ui->widget->setVisualSettings(*m_visuals);

    connect(m_visuals, &VisualSettings::backgroundColorChanged, this, &PlotCell::updateBackground);
    connect(m_visuals, &VisualSettings::cellBoundaryColorChanged, this, &PlotCell::updateOutline);
    connect(m_visuals, &VisualSettings::cellBoundaryEnabledChanged,
            [this](bool checked) { ui->bbCB->setChecked(checked); });
    connect(m_visuals, &VisualSettings::depthPeelCountChanged,
            [this](int peels) { ui->spinBox_depthPeels->setValue(peels); });

    updateList();
    // updateParticles();
    updateBackground(m_visuals->backgroundColor());
    updateOutline(m_visuals->cellBoundaryColor());
    ui->bbCB->setChecked(m_visuals->cellBoundaryEnabled());
}


/*!
 * Orients the camera or changes the perspective. Supported arguments are
 * Top, Bottom, Right, Left, Front, Back, Perspective and Ortho.
 */
void PlotCell::changeViewOption(ViewOption option) {
    QSignalBlocker blocker(ui->orientationCB);
    ui->orientationCB->setCurrentIndex(ui->orientationCB->findData(option));

    // Projection changes
    if (option == Perspective || option == Orthogonal) {
        ui->widget->usePerspective(option == Perspective);
        ui->widget->update();
        return;
    }

    // Orientation changes

    float angleX = 0;
    float angleY = 0;
    float angleZ = 0;

    switch (option) {
        case Top:
            break;
        case Bottom:
            angleX = 180;
            break;
        case Left:
            angleY = 90;
            angleX = 270;
            break;
        case Right:
            angleY = 270;
            angleX = 270;
            break;
        case Front:
            angleX = 270;
            break;
        case Back:
            angleX = 90;
            angleZ = 180;
            break;

        default:
            return;
    }

    ui->widget->setModelOrientation(angleX, angleY, angleZ);
    ui->widget->centerCell();
}


/*!
 * When the camera orientation is changed by interactiong with the plot,
 * the text implying the current orientation is changed to "Perspective" or "Ortho".
 */
void PlotCell::freeOrientation() {
    QSignalBlocker(ui->orientationCB);
    ui->orientationCB->setCurrentIndex(
        ui->orientationCB->findData(ui->widget->isPerspective() ? Perspective : Orthogonal));
}


void PlotCell::updateHoveredParticle(const glm::dvec3& mousePos) {
    ui->hoverParticleLabel->setText("");
    if (!m_visuals) return;

    bool canHitAtom = glm::length2(mousePos - m_entry->frame.atomLims.center()) <
                      glm::length2(m_entry->frame.atomLims.diagonal() * 2.0);

    if (canHitAtom) {
        const QHash<int, float>& radii = m_visuals->atomTypeSizes();
        const Atom* a = m_grid->closestAtom(mousePos, &radii);
        if (a) {
            ui->hoverParticleLabel->setText(
                QString("(%1, %2, %3)").arg(a->position.x).arg(a->position.y).arg(a->position.z));
        }
    }

    if (!ui->hoverParticleLabel->isVisible() && !ui->clickedParticleWidget->isVisible()) {
        ui->hoverParticleLabel->setVisible(true);
    }
}


void PlotCell::updateClickedParticle(const glm::dvec3& mousePos) {
    ui->clickedParticleWidget->setVisible(false);
    if (!m_visuals) return;

    bool canHitAtom = glm::length2(mousePos - m_entry->frame.atomLims.center()) <
                      glm::length2(m_entry->frame.atomLims.diagonal() * 2.0);

    if (canHitAtom) {
        const QHash<int, float>& radii = m_visuals->atomTypeSizes();
        const Atom* a = m_grid->closestAtom(mousePos, &radii);
        if (a) {
            ui->clickedParticleTypeLE->setText(QString::number(a->type));
            ui->clickedParticlePosLE->setText(
                QString("(%1, %2, %3)").arg(a->position.x).arg(a->position.y).arg(a->position.z));
            ui->hoverParticleLabel->setVisible(false);
            ui->clickedParticleWidget->setVisible(true);
        }
    }
}


/*!
 * Shows a color picker and changes the color of the atoms with a particular type number.
 */
void PlotCell::particleColorChangeEvent(int typeNumber) {
    QColorDialog dialog(this);
    dialog.setOption(QColorDialog::DontUseNativeDialog, true);
    dialog.setWindowTitle("Color Picker");
    dialog.setCurrentColor(m_visuals->atomTypeColors()[typeNumber]);

    if (dialog.exec() == QDialog::Accepted) {
        QHash<int, QColor> colors = m_visuals->atomTypeColors();
        colors[typeNumber] = dialog.selectedColor();
        m_visuals->changeTypeColors(colors);
    }
}


void PlotCell::backgroundColorChangeEvent() {
    QColorDialog dialog(this);
    dialog.setOption(QColorDialog::DontUseNativeDialog, true);
    dialog.setWindowTitle("Color Picker");
    dialog.setCurrentColor(ui->widget->getBackgroundColor());

    if (dialog.exec() == QDialog::Accepted) {
        m_visuals->changeBackgroundColor(dialog.selectedColor());
    }
}


void PlotCell::outlineColorChangeEvent() {
    QColorDialog dialog(this);
    dialog.setOption(QColorDialog::DontUseNativeDialog, true);
    dialog.setWindowTitle("Color Picker");
    dialog.setCurrentColor(m_visuals->cellBoundaryColor());

    if (dialog.exec() == QDialog::Accepted) {
        m_visuals->changeCellBoundaryColor(dialog.selectedColor());
    }
}


/*!
 * When the cell limits of the connected frame have been changed,
 * this function can be called in order to update the plot.
 */
void PlotCell::bbUpdated(const Box& box) {
    qDebug() << "PlotCell::bbUpdated()";
    //    if (m_plottableFrame) m_plottableFrame->updateCellBoundary();
    ui->widget->on_setOutlineBox(box);
}


/*!
 * Returns a pointer to the PlotCellGLWidget used in this widget.
 */
PlotCellGLWidget* PlotCell::glWidget() {
    return ui->widget;
}


QWidget* PlotCell::lastWidget() {
    return ui->orientationCB;
}


void PlotCell::showMenus(bool b) {
    ui->typesGB->setVisible(b);
    ui->settingsGB->setVisible(b);
    ui->showMenuPB->setChecked(b);
}


void PlotCell::on_spinBox_depthPeels_valueChanged(int peels) {
    m_visuals->changeDepthPeelCount(peels);
}
