/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef APIPARAMETERS_H
#define APIPARAMETERS_H

#include "api/abstractstreamable.h"
#include "api/state/parameters/parameterkeys.h"

#include <QMap>
#include <QObject>
#include <QVariant>


namespace api {

typedef struct ParameterValue {
  public:
    ParameterValue() : ParameterValue(QVariant(), false, true) {}
    ParameterValue(QVariant value, bool isModified = true, bool isDefault = false) {
        m_value = value;
        m_isModified = isModified;
        m_isDefault = isDefault;
    }

    const QVariant& value() { return m_value; }
    bool isModified() { return m_isModified; }
    bool isDefault() { return m_isDefault && !m_isModified; }


    friend QDataStream& operator<<(QDataStream& stream, const ParameterValue& value) {
        stream << value.m_value;
        stream << value.m_isModified;
        stream << value.m_isDefault;
        return stream;
    }

    friend QDataStream& operator>>(QDataStream& stream, ParameterValue& value) {
        stream >> value.m_value;
        stream >> value.m_isModified;
        stream >> value.m_isDefault;
        return stream;
    }

  private:
    QVariant m_value;
    bool m_isModified = false;
    bool m_isDefault = true;
} value_t;


class BaseParameters : public AbstractStreamable {

  public:
    BaseParameters();

    void setup();

    void set(const key_t& key, const value_t& value);

    void insert(const BaseParameters& other);
    void insert(const QMap<key_t, value_t>& other);

    void clear();

    const QMap<key_t, value_t>& values() const;

    value_t operator[](const key_t& key) const;

    QDataStream& save(QDataStream& out) const override;
    QDataStream& load(QDataStream& in) override;


  private:
    virtual void setupInitialValues();
    void setupDefaultValues();

    QMap<key_t, value_t> m_defaultParameters;
    QMap<key_t, value_t> m_parameters;

  signals:
};

QDebug operator<<(QDebug d, const BaseParameters& p);

}  // namespace api

#endif  // APIPARAMETERS_H
