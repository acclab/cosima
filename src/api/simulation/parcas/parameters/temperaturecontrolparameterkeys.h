/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEMPERATURECONTROLPARAMETERKEYS_H
#define TEMPERATURECONTROLPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter MTEMP{"mtemp", true, 0};
const parameter INITEMP{"initemp", false, 0.0};
const parameter TEMP{"temp", false, QVariant()};
const parameter BTCTAU{"btctau", false, QVariant()};
const parameter TSCALETH{"tscaleth", false, QVariant()};
const parameter TSCALXOUT{"tscalxout", false, QVariant()};
const parameter TSCALYOUT{"tscalyout", false, QVariant()};
const parameter TSCALZMIN{"tscalzmin", false, QVariant()};
const parameter TSCALZMAX{"tscalzmax", false, QVariant()};
const parameter TEMP0{"temp0", false, QVariant()};
const parameter TRATE{"trate", false, QVariant()};
const parameter TIMEINI{"timeini", false, QVariant()};

const QList<parameter> temperatureControlKeys{MTEMP,     TEMP,      INITEMP,   BTCTAU, TSCALETH, TSCALXOUT,
                                              TSCALYOUT, TSCALZMIN, TSCALZMAX, TEMP0,  TRATE,    TIMEINI};

#endif  // TEMPERATURECONTROLPARAMETERKEYS_H
