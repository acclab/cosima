/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typeatomselectorwidget.h"

#include "api/programsettings.h"
#include "utils/clickablelabel.h"

#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>

TypeAtomSelectorWidget::TypeAtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent) {
    m_selector.setMode(ArrayAtomSelection::Union);
    QVBoxLayout* l = new QVBoxLayout(this);
    setLayout(l);
}

const AbstractAtomSelection& TypeAtomSelectorWidget::selector() {
    m_selector.clear();

    for (int type : m_selected.keys()) {
        if (m_selected[type]) m_selector.addInternalSelection(new TypeSelection(type));
    }

    return m_selector;
}

void TypeAtomSelectorWidget::init() {
    updateTypes();
}

void TypeAtomSelectorWidget::updateTypes() {
    if (m_contents) delete m_contents;

    m_contents = new QWidget(this);
    QGridLayout* grid = new QGridLayout(m_contents);

    QHash<int, bool> newSelected;

    QVector<int> sortedTypes = m_entry->data.types.typeList.keys().toVector();
    std::sort(sortedTypes.begin(), sortedTypes.end());

    int row = 0;
    for (int typeNumber : sortedTypes) {
        QLabel* l = new QLabel(
            QString("%1 (%2)").arg(QString::number(typeNumber), m_entry->data.types.typeList[typeNumber].symbol));
        ClickableLabel* colorFreeLabel = new ClickableLabel();
        ClickableLabel* colorFixedLabel = new ClickableLabel();
        QCheckBox* freeCB = new QCheckBox("Free");
        QCheckBox* fixedCB = new QCheckBox("Fixed");

        colorFreeLabel->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[typeNumber]);
        colorFreeLabel->setFixedSize(20, 20);

        colorFixedLabel->setColor(ProgramSettings::getInstance().visualSettings().atomTypeColors()[-typeNumber]);
        colorFixedLabel->setFixedSize(20, 20);

        if (m_selected.contains(typeNumber)) {
            newSelected[typeNumber] = m_selected[typeNumber];
        } else {
            newSelected[typeNumber] = false;
        }

        if (m_selected.contains(-typeNumber)) {
            newSelected[-typeNumber] = m_selected[-typeNumber];
        } else {
            newSelected[-typeNumber] = false;
        }

        freeCB->setChecked(newSelected[typeNumber]);
        fixedCB->setChecked(newSelected[-typeNumber]);

        connect(freeCB, &QCheckBox::clicked, [this, typeNumber](bool checked) { m_selected[typeNumber] = checked; });
        connect(fixedCB, &QCheckBox::clicked, [this, typeNumber](bool checked) { m_selected[-typeNumber] = checked; });

        grid->addWidget(l, row, 0);
        grid->addWidget(colorFreeLabel, row, 1);
        grid->addWidget(freeCB, row, 2);
        grid->addWidget(colorFixedLabel, row, 3);
        grid->addWidget(fixedCB, row, 4);
        ++row;
    }
    grid->addItem(new QSpacerItem(99999, 20, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding), 0, 5);
    m_selected = newSelected;
    m_contents->setLayout(grid);
    layout()->addWidget(m_contents);
}
