/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef RATIO_H
#define RATIO_H

#include "api/atom.h"
#include "gui/widgets/utils/graphselector.h"
#include "gui/widgets/utils/ratiograph.h"
#include "outputmodule.h"

#include <QWidget>

#include <vector>


class GraphSelector;

namespace Ui {
class Ratio;
}

/*! \brief OutputModule that plots ratios between atom types in simulatio cell.
 *
 * Plots the ratio of two atom types in the cell as a function of depth.
 * GraphSelector can be used to inspect a smaller part of the sample.
 *
 * \sa OutputModule, GraphSelector, RatioGraph
 */
class Ratio : public OutputModule {
    Q_OBJECT

  public:
    explicit Ratio(QWidget* parent = nullptr);
    ~Ratio();
    void draw(const XyzData*);

  private:
    Ui::Ratio* ui;

    QHash<QString, int> types;
    int type1, type2;
    bool graphInitiated;

  public slots:
    void changeGraph(int, int);

  private slots:
    void updateGraphResolution(int);
    void updateTypes();

    void on_resLE_editingFinished();
    void on_resZLE_editingFinished();

    void on_type1CB_currentIndexChanged(const QString& text);

    void on_type2CB_currentIndexChanged(const QString& text);
};

#endif  // RATIO_H
