/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "createscratchwidget.h"
#include "ui_createscratchwidget.h"

#include "api/util.h"

#include "gui/dialogs/chooseelementdialog.h"

#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QSpinBox>
#include <QVector>


CreateScratchWidget::CreateScratchWidget(QWidget* parent) : QWidget(parent), ui(new Ui::CreateScratchWidget) {
    ui->setupUi(this);

    m_entry = new ExtendedEntryData();

    ui->widget->setEntry(*m_entry);


    QVector<QWidget*> wheelGuardedWidgets = {ui->spinBox_nx,
                                             ui->spinBox_ny,
                                             ui->spinBox_nz,
                                             ui->spinBox_type,
                                             ui->doubleSpinBox_latticeA,
                                             ui->doubleSpinBox_latticeB,
                                             ui->doubleSpinBox_latticeC};
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(this));
    }

    connect(ui->pushButton_create, &QPushButton::clicked, this, [this] { createAtoms(); });

    connect(ui->lineEdit_elementSymbol, &QLineEdit::textEdited, this, [this](QString text) {
        if (text.length() > 2) ui->lineEdit_elementSymbol->setText(text.left(2));
    });

    connect(ui->pushButton_changeElement, &QPushButton::clicked, this, [this] {
        ChooseElementDialog e(this);
        e.showLatticeData(true);
        if (e.exec() == QDialog::Accepted) {
            int z = e.Z();
            populateFields(z);
        }
    });


    ui->splitter->setCollapsible(0, false);
    ui->splitter->setCollapsible(1, false);
    util::decorateSplitterHandle(ui->splitter, 1);
    util::decorateSplitterHandle(ui->splitter, 2);

    ui->splitter->setSizes(QList<int>() << 150 << 150);

    populateFields(14);
}


CreateScratchWidget::~CreateScratchWidget() {
    delete m_entry;
    delete ui;
}


const ExtendedEntryData& CreateScratchWidget::entry() {
    return *m_entry;
}

void CreateScratchWidget::populateFields(int z) {
    ElementReader r;
    ui->lineEdit_elementSymbol->setText(r.getSymbol(z));

    QString lattice = r.latticeType(z);

    if (lattice == "SC") {
        ui->radioButton_sc->setChecked(true);
    } else if (lattice == "BCC") {
        ui->radioButton_bcc->setChecked(true);
    } else if (lattice == "FCC") {
        ui->radioButton_fcc->setChecked(true);
    } else if (lattice == "DIA") {
        ui->radioButton_dia->setChecked(true);
    } else if (lattice == "HCP" || lattice == "HEX") {
        ui->radioButton_hcp->setChecked(true);
    }

    double latticeConstant = r.latticeConstant(z);
    ui->doubleSpinBox_latticeA->setValue(latticeConstant);
    ui->doubleSpinBox_latticeB->setValue(latticeConstant);
    ui->doubleSpinBox_latticeC->setValue(latticeConstant);
}


void CreateScratchWidget::createAtoms() {

    m_entry->frame.atoms.clear();

    float lx = static_cast<float>(ui->doubleSpinBox_latticeA->value());
    float ly = static_cast<float>(ui->doubleSpinBox_latticeB->value());
    float lz = static_cast<float>(ui->doubleSpinBox_latticeC->value());


    // Reading atoms and limits
    double xmin = std::numeric_limits<double>::max();
    double xmax = std::numeric_limits<double>::lowest();
    double ymin = std::numeric_limits<double>::max();
    double ymax = std::numeric_limits<double>::lowest();
    double zmin = std::numeric_limits<double>::max();
    double zmax = std::numeric_limits<double>::lowest();

    double dx = static_cast<double>(lx / 4);
    double dy = static_cast<double>(ly / 4);
    double dz = static_cast<double>(lz / 4);

    QList<glm::vec3> offsets;
    if (ui->radioButton_sc->isChecked()) {
        offsets << glm::vec3(0, 0, 0);
        dx = static_cast<double>(lx / 2);
        dy = static_cast<double>(ly / 2);
        dz = static_cast<double>(lz / 2);
    } else if (ui->radioButton_bcc->isChecked()) {
        offsets << glm::vec3(0, 0, 0) << glm::vec3(0.5, 0.5, 0.5);
    } else if (ui->radioButton_fcc->isChecked()) {
        offsets << glm::vec3(0, 0, 0) << glm::vec3(0.5, 0.5, 0) << glm::vec3(0.5, 0, 0.5) << glm::vec3(0, 0.5, 0.5);
    } else if (ui->radioButton_dia->isChecked()) {
        offsets << glm::vec3(0, 0, 0) << glm::vec3(0.5, 0.5, 0) << glm::vec3(0, 0.5, 0.5) << glm::vec3(0.5, 0, 0.5)
                << glm::vec3(0.25, 0.25, 0.25) << glm::vec3(0.75, 0.75, 0.25) << glm::vec3(0.25, 0.75, 0.75)
                << glm::vec3(0.75, 0.25, 0.75);
        dx = static_cast<double>(lx / 8);
        dy = static_cast<double>(ly / 8);
        dz = static_cast<double>(lz / 8);
    } else if (ui->radioButton_hcp->isChecked()) {
        offsets << glm::vec3(0, 0, 0) << glm::vec3(0.5, 0.5, 0) << glm::vec3(0, 1.0 / 3.0, 0.5)
                << glm::vec3(0.5, 5.0 / 6.0, 0.5);
        dx = static_cast<double>(lx / 4);
        dy = static_cast<double>(ly / 12);
        dz = static_cast<double>(lz / 4);
    }

    int id = 0;
    for (int i = 0; i < ui->spinBox_nx->value(); i++) {
        for (int j = 0; j < ui->spinBox_ny->value(); j++) {
            for (int k = 0; k < ui->spinBox_nz->value(); k++) {

                for (int l = 0; l < offsets.size(); l++) {
                    Atom a;
                    a.symbol = ui->lineEdit_elementSymbol->text();
                    a.position = glm::vec3((i + offsets[l].x) * lx, (j + offsets[l].y) * ly, (k + offsets[l].z) * lz);
                    a.type = ui->spinBox_type->value();
                    a.identifier = id++;

                    m_entry->frame.atoms.push_back(a);

                    xmin = std::min(xmin, a.position.x);
                    xmax = std::max(xmax, a.position.x);
                    ymin = std::min(ymin, a.position.y);
                    ymax = std::max(ymax, a.position.y);
                    zmin = std::min(zmin, a.position.z);
                    zmax = std::max(zmax, a.position.z);
                }
            }
        }
    }


    m_entry->frame.cellLims = Box(xmin - dx, xmax + dx, ymin - dy, ymax + dy, zmin - dz, zmax + dz);
    m_entry->frame.pbc = glm::bvec3(true, true, true);

    m_entry->frame.updateAmounts();
    m_entry->frame.updateAtomLims();

    m_entry->frame.center();

    ui->widget->setEntry(*m_entry);

    ui->groupBox_inspector->setTitle(QString::number(m_entry->frame.atoms.size()) + " atoms");

    emit entryCreated();
}
