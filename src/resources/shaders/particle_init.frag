#version 330

in vec4 color;
uniform sampler2D textureSampler;

out vec4 colorOut;

void main(void)
{
    vec4 col = texture(textureSampler, gl_PointCoord) * color;
    vec2 shifted_coords = gl_PointCoord - vec2(0.5, 0.5);
    if(dot(shifted_coords, shifted_coords) >= 0.22) discard;
    colorOut = vec4(col.rgb * col.a, 1.0 - col.a);
}
