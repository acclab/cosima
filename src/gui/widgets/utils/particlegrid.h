/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARTICLEGRID_H
#define PARTICLEGRID_H

#include "api/geometries.h"
#include "api/xyzdata.h"

#include <glm/vec3.hpp>

#include <vector>

using std::vector;

/*!
 * \brief A grid for particles
 *
 * Can be used to find the particle that is located in a given coordinate.
 */
class ParticleGrid {

  public:
    ParticleGrid(const XyzData& frame, bool initialize = true);
    const Atom* closestAtom(const glm::dvec3& position, const QHash<int, float>* radii = nullptr) const;
    void update();
    void update(unsigned int resX, unsigned int resY, unsigned int resZ);

  private:
    const XyzData& m_atoms;
    vector<vector<vector<vector<const Atom*> > > > m_grid;
    unsigned int m_resX;
    unsigned int m_resY;
    unsigned int m_resZ;
};

#endif  // PARTICLEGRID_H
