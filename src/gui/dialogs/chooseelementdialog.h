/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CHOOSEELEMENTDIALOG_H
#define CHOOSEELEMENTDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>


namespace Ui {
class ChooseElementDialog;
}

/*!
 * \brief A dialog for selecting an element.
 *
 * Draws the periodic table of elements. Clicking on a symbol shows the user information about the element.
 *
 * Example:
 * \code{.cpp}
 * ChooseElementDialog pt;
 * if (pt.exec() == QDialog::Accepted)
 *      int z = pt.Z();
 * \endcode
 */
class ChooseElementDialog : public QDialog {
    Q_OBJECT

  public:
    explicit ChooseElementDialog(QWidget* parent = nullptr);
    ~ChooseElementDialog();

    int Z() const;

    void showLatticeData(bool show);

  private:
    int indexOfElement(const QString& symbol);

    Ui::ChooseElementDialog* ui;
    int m_z = 0;

  private slots:
    void initTable();
    void loadInfo(const QString& symbol);
};

#endif  // CHOOSEELEMENT_H
