/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "api.h"

#include "api/binarysettingsio.h"
#include "api/programsettings.h"
#include "api/project.h"
#include "api/util.h"

#include <QDebug>


/*!
 * \brief Constructs an object with the given parent object.
 *
 * Loads the program settings. Because of the current singleton-based design (see Project), having multiple instances of
 * Api is not adviceable.
 *
 * \param parent Parent object
 * \sa ProgramSettings
 */
Api::Api(QObject* parent) : QObject(parent) {
    qRegisterMetaType<Api::SimulationStatus>();
    ProgramSettings::getInstance().load();
}


bool Api::isProjectOpen() const {
    return Project::getInstance().isOpen();
}


/*!
 * \brief Loads a COSIRMA project based on its directory path
 * \param path Directory path of the project
 */
void Api::loadProject(const QString& path) {
    qDebug() << "Api::loadProject()" << path;

    Project::getInstance().setPath(path);

    // Reuse the .cosirmaproject identfier for storing settings as well as identifying a project folder.
    BinarySettingsIO reader(Project::getInstance().path() + "/.cosirmaproject");

    //! \todo This could possible be moved later in the function to use the signals/slots defined below instead of
    //! calling the entryEditor.setEntry() function explicitly.
    //! Currently there are history-dependent settings in stored in the .cosirmaproject file (i.e. the interaction
    //! types), so the history has to be loaded prior to the project settings.
    Project::getInstance().loadHistory();

    if (!reader.load(simulationList)) {
        //! \todo emit error
        return;
    }

    if (!Project::getInstance().isOpen()) {
        qDebug() << "Loading failed.";
        //! \todo emit error
        return;
    }

    connect(&Project::getInstance().history, &History::entryUpdated, [this](int entryIndex, int frameIndex) {
        const Entry* entry = Project::getInstance().history.entry(entryIndex, frameIndex);
        entryEditor.setEntry(entry->extendedData());
    });

    emit projectOpened();
}


/*!
 * \brief Closes the project that is currently open.
 * \param save If \c true, calls saveProject().
 */
void Api::closeProject(bool save) {
    qDebug() << "close project";
    emit projectClosing();
    if (save && Project::getInstance().isOpen()) saveProject();
    Project::getInstance().setPath("");
    Project::getInstance().unloadHistory();
    emit projectClosed();
}


/*!
 * \brief Saves the project that is currently open.
 *
 * Saves the current simulation settings to the hidden .cosirmaproject file located in the root of every project.
 */
void Api::saveProject() {
    qDebug() << "save project";
    if (!Project::getInstance().isOpen()) return;
    // Reuse the .cosirmaproject identfier for storing settings
    BinarySettingsIO writer(Project::getInstance().path() + "/.cosirmaproject");
    if (!writer.save(simulationList)) {
        //! \todo emit error
        qDebug() << "failed to write the simulation list state.";
    }
}


bool Api::exportStandaloneSimulation(const QString& path, const ExportData& data, const ExtendedEntryData& entryData) {
    // If the simulator hasn't been used, initialize it with the currently selected simulator.
    if (!simulator) {
        simulator = simulationList.createSimulator();
        if (!simulator) {
            qDebug() << "Please select a simulation!";
            return false;
        }
    }

    if (simulator->isRunning()) {
        qDebug() << "The simulator is already running!";
        return false;
    }

    simulator->controller()->createStandaloneSimulation(path, entryData, data);

    delete simulator;
    simulator = nullptr;

    return true;
}


/*!
 * \brief Starts a simulation based on the selection and settings in #simulationList. This creates a new simulation
 * entry in the history and frames will appear during the simulation.
 * \return \c true If the process is started
 */
bool Api::startSimulation(const ExtendedEntryData& entryData) {
    qDebug() << "Api::startSimulation()";

    // If the simulator hasn't been used, initialize it with the currently selected simulator.
    if (!simulator) {
        simulator = simulationList.createSimulator();
        if (!simulator) {
            qDebug() << "Please select a simulation!";
            return false;
        }
    }

    if (simulator->isRunning()) {
        qDebug() << "The simulator is already running!";
        emit simulationStatusChanged(SimulationStatus::Aborted, "Simulation already running.");
        return false;
    }


    EntryInfo info;
    info.type = EntryInfo::Simulation;
    info.description = "Simulation Started";

    // Make a copy of the current frame (this should be safe as the same data is used to create the runtime directory).

    ExtendedEntryData simulationEntryData = entryData;
    simulationEntryData.data.info = info;

    try {
        m_simulationEntry = Project::getInstance().history.createEntry(simulationEntryData, false);
    } catch (HistoryException& e) {
        qDebug() << e.message;
        emit simulationStatusChanged(SimulationStatus::Error, e.message);
        if (simulatorThread) simulatorThread->exit(1);
        return false;
    }

    std::pair<bool, QString> status = simulator->initRuntime(entryData);
    if (!status.first) {
        emit simulationStatusChanged(SimulationStatus::Error, status.second);
        return false;
    }

    simulatorThread = new QThread;
    simulator->moveToThread(simulatorThread);

    connect(simulator, &Simulator::finished, simulatorThread, &QThread::quit);
    connect(simulator, &Simulator::statusUpdate, this, &Api::handleSimulatorStatusChanged);
    connect(simulator, &Simulator::finished, simulator, &QObject::deleteLater);
    connect(simulator, &Simulator::newFrame, this, [this](const QString& path) {
        if (m_simulationEntry) {
            m_simulationEntry->addFrame(path);
        }
    });
    connect(this, &Api::stopSimulationSignal, simulator, &Simulator::stop);

    connect(simulatorThread, &QThread::started, simulator, &Simulator::simulate);
    connect(simulatorThread, &QThread::finished, simulatorThread, &QObject::deleteLater);

    simulatorThread->start();

    // If for some reason the simulation still didn't start, just return the state from the process handler.
    return simulatorThread->isRunning();
}


void Api::stopSimulation() {
    qDebug() << this << "::stopSimulation()";
    emit stopSimulationSignal();
}


/*!
 * \brief Emits the correct signal based on the current status of the running process.
 * \param status The status signaled by the worker thread.
 */
void Api::handleSimulatorStatusChanged(Simulator::Status status, const QString& message) {
    qDebug() << "handleProcessStatusChanged";

    switch (status) {
        case Simulator::Status::Started:
            qDebug() << "    STARTED";
            emit simulationStatusChanged(Api::SimulationStatus::Started);
            break;
        case Simulator::Status::Stopping:
            qDebug() << "    STOPPING";
            emit simulationStatusChanged(Api::SimulationStatus::Stopping);
            break;
        case Simulator::Status::Finished:
            qDebug() << "    FINISHED";
            if (m_simulationEntry) {
                if (!m_simulationEntry->isValid()) {
                    Project::getInstance().history.removeEntry(m_simulationEntry);
                }
            }
            emit simulationStatusChanged(Api::SimulationStatus::Finished);
            break;
        case Simulator::Status::Error:
            qDebug() << message;
            emit errorMessage(message);
            break;
    }
}
