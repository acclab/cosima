#version 330

in vec4 color;

out vec4 colorOut;

void main(void)
{
    colorOut = vec4(color.rgb * color.a, 1.0 - color.a);
}

