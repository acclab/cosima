/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "binarysettingsio.h"

#include <QDataStream>
#include <QDebug>
#include <QFile>

/*!
 * \brief Constructs an object that connects to the given file.
 * \param path Path to the file
 */
BinarySettingsIO::BinarySettingsIO(const QString& path) : m_path(path) {}

/*!
 * \brief Streams \a settings into the file
 * \param settings The object that is saved into the file
 * \return \c true if the saving is successful
 */
bool BinarySettingsIO::save(const AbstractStreamable& settings) {
    qDebug() << "BinarySettingsIO::save()" << m_path;

    QFile file(m_path);
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "    Couldn't open file";
        return false;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_7);

    out << settings;

    file.flush();
    file.close();

    return true;
}

bool BinarySettingsIO::hasContent() {
    QFile file(m_path);
    return file.size() > 0;
}

/*!
 * \brief Streams the contents of the file into \a settings
 * \param settings The object where the file contents are loaded to
 * \return \c true if the loading is successful
 */
bool BinarySettingsIO::load(AbstractStreamable& settings) {
    qDebug() << "BinarySettingsIO::load()" << m_path;

    QFile file(m_path);
    // If file is empty, the prject hasn't been saved and should not be loaded.
    if (file.size() == 0) {
        return true;
    }

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "    Couldn't open file";
        return false;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_7);

    in >> settings;

    file.close();

    return true;
}
