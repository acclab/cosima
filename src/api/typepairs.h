/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TYPEPAIR_STRUCT_H
#define TYPEPAIR_STRUCT_H

#include "atomtype.h"

#include <QDataStream>
#include <QDebug>
#include <QHash>
#include <QString>

/*!
 * \brief Pair of element symbols
 *
 * A sruct of two QStrings. The order of the elemets does not matter.
 */
struct ElementPair {
    QString element1; /*!< \brief Symbol of the first element*/
    QString element2; /*!< \brief Symbol of the second element*/

    /*!
     * \brief Constructs a blank object.
     */
    ElementPair() {}

    /*!
     * \brief Constructs an object with given attribute values.
     * \param e1 The first element symbol
     * \param e2 The second element symbol
     */
    ElementPair(const QString e1, const QString e2) {
        element1 = e1;
        element2 = e2;
    }

    /*!
     * \brief Equality operator
     * \param p The pair being compared to
     * \return \c true if the pairs contain the same element symbols
     */
    bool operator==(const ElementPair& other) const {
        return (element1 == other.element1 && element2 == other.element2) ||
               (element1 == other.element2 && element2 == other.element1);
    }

    //    /*!
    //     * \brief Warning this function is implemented poorly and doesn't work... I'll get back to it later if needed,
    //     for now the QHash type is good enough, no need for QMap.
    //     */
    //    friend bool operator<(const ElementPair& pair1, const ElementPair& pair2) {
    //        return (pair1.element1 < pair2.element1 && pair1.element2 < pair2.element2) || (pair1.element1 <
    //        pair2.element2 && pair1.element2 < pair2.element1);
    //    }

    /*!
     * \brief Hashing function for ElementPair
     *
     * The function is symmetric in terms of ElementPair#element1 and ElementPair#element2.
     *
     * \param The element pair
     * \return The hash value
     */
    friend uint qHash(const ElementPair& key) { return qHash(key.element1) + qHash(key.element2); }
};


/*!
 * \brief Operator for streaming ElementPair objects into QDataStream
 * \param stream The stream
 * \param e The element pair being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const ElementPair& e) {
    return stream << e.element1 << e.element2;
}

/*!
 * \brief Operator for streaming ElementPair objects from QDataStream
 * \param stream The stream
 * \param e The element pair being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, ElementPair& e) {
    stream >> e.element1;
    stream >> e.element2;
    return stream;
}

/*!
 * \brief Operator for streaming ElementPair objects into QDebug
 * \param stream The stream
 * \param e The element pair being streamed
 * \return The stream afterwards
 */
inline QDebug& operator<<(QDebug& debug, const ElementPair& e) {
    debug << QString("ElementPairStruct(%1, %2)").arg(e.element1, e.element2);
    return debug;
}


/*!
 * \brief Pair of atom type numbers
 *
 * A sruct of two integers. The numbers are always in increasing order.
 */
struct TypeNumberPair {
    /*!
     * \brief Constructs an object from two integers.
     *
     * The inegers are sorted so that later type1() accesses the smaller one, and type2() the larger one.
     *
     * \param t1 The first integer
     * \param t2 The second integer
     */
    TypeNumberPair(int t1 = -1, int t2 = -1) {
        if (t1 < t2) {
            type1_m = t1;
            type2_m = t2;
        } else {
            type1_m = t2;
            type2_m = t1;
        }
    }

    /*!
     * \brief Getter for the smaller type number
     * \return The smaller type number
     */
    int type1() const { return type1_m; }

    /*!
     * \brief Getter for the larger type number
     * \return The larger type number
     */
    int type2() const { return type2_m; }

    /*!
     * \brief Equality operator
     * \param t The pair being compared to
     * \return \c true if the smaller and larger values of the pairs are the same
     */
    bool operator==(const TypeNumberPair& t) const { return (type1() == t.type1()) && (type2() == t.type2()); }

  private:
    int type1_m, type2_m;
};

/*!
 * \brief < comparison operator for type number pairs
 * \param t1 The first type number pair
 * \param t2 The second type number pair
 * \return \c true if the smaller value of \a t1 is smaller than the one of \a t2. If the smaller values are the same,
 * the larger ones are compared instead.
 */
inline bool operator<(const TypeNumberPair& t1, const TypeNumberPair& t2) {
    if (t1.type1() == t2.type1())
        return t1.type2() < t2.type2();
    else
        return t1.type1() < t2.type1();
}

/*!
 * \brief Hashing function for type number pairs
 * \param key The type number pair
 * \return The hash value
 */
inline uint qHash(const TypeNumberPair& key) {
    return qHash(key.type1()) + qHash(key.type2());
}

/*!
 * \brief Operator for streaming TypeNumberPair objects into QDataStream
 * \param stream The stream
 * \param t The type number pair being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const TypeNumberPair& t) {
    return stream << t.type1() << t.type2();
}

/*!
 * \brief Operator for streaming TypeNumberPair objects from QDataStream
 * \param stream The stream
 * \param t The type number pair being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, TypeNumberPair& t) {
    int t1, t2;
    stream >> t1;
    stream >> t2;
    t = TypeNumberPair(t1, t2);
    return stream;
}

/*!
 * \brief Operator for streaming TypeNumberPair objects into QDebug
 * \param stream The stream
 * \param t The type number pair being streamed
 * \return The stream afterwards
 */
inline QDebug& operator<<(QDebug& debug, const TypeNumberPair& t) {
    debug << QString("TypeNumberPairStruct(%1, %2)").arg(t.type1()).arg(t.type2());
    return debug;
}

#endif  // TYPEPAIR_STRUCT_H
