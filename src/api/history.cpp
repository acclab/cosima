/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "history.h"

#include "api/binarysettingsio.h"
#include "api/fileio.h"
#include "api/framereaderwriter.h"

#include <QDebug>
#include <QDir>

#include <algorithm>


/*!
 * \brief Constructs an object that is connected to the given directory.
 *
 * Reads the entries in the given directory.
 *
 * \param path Path to the directory
 * \param parent The parent object
 */
History::History(const QString& path, QObject* parent)
        : QObject(parent), m_directoryPath(path), m_emptyEntry(new Entry("")) {}


History::~History() {
    delete m_emptyEntry;
    for (Entry* e : m_entries) delete e;
    m_entries.clear();
}


/*!
 * \brief The number of entries in the current directory.
 * \return The number of entries
 */
int History::entryCount() const {
    return m_entries.count();
}


/*!
 * \brief Accesses the \a ith entry in the directory. The entries are sorted based on their timestamps.
 * \param i The index of the accessed entry
 * \return const pointer to the \a ith entry. If \a i is outside the range [0, count-1] a \c nullptr is returned.
 */
const Entry* History::entry(int entryIndex, int frameIndex) const {
    if (entryIndex < 0) return nullptr;
    if (entryIndex >= m_entries.count()) return nullptr;

    if (frameIndex != -1) {
        m_entries[entryIndex]->selectFrame(frameIndex);
    }

    return m_entries[entryIndex];
}

// Entry* History::mutableEntry(int i) {
//    if (i < 0) return nullptr;
//    if (i >= m_entries.count()) return nullptr;
//    return m_entries[i];
//}

/*!
 * \brief Adds an entry to the history and writes it to the history directory.
 * \param entryData The ExtendedEntryData object of the new entry
 * \return std::pair<bool, QString>, where the boolean is \c true if the adding is successful, and the QString includes
 * the error message in the case that the adding is unsuccessful.
 */
Entry* History::addNewEntry(ExtendedEntryData& entryData) {
    qDebug() << "History::addNewEntry()";

    try {
        Entry* entry = createEntry(entryData, true);

        int entryIndex = m_entries.size() - 1;
        int frameIndex = m_entries.at(entryIndex)->frameCount() - 1;
        emit entryUpdated(entryIndex, frameIndex);

        return entry;
    } catch (HistoryException& e) {
        qDebug() << e.message;
    }

    return nullptr;
}


Entry* History::createEntry(ExtendedEntryData& entryData, bool writeFrameData) {
    qDebug() << "History::createEntry()";

    QString message;

    int maxNumber = 0;

    for (const Entry* e : m_entries) {
        maxNumber = std::max(maxNumber, QDir(e->directoryPath()).dirName().split("_")[1].toInt());
    }

    QString entryPath = QString("%1/entry_%2").arg(m_directoryPath).arg(maxNumber + 1);
    entryData.data.info.timestamp = QDateTime::currentDateTime();

    Entry* newEntry = new Entry(entryPath, entryData);

    std::pair<bool, QString> writing = newEntry->create(writeFrameData);
    message += writing.second;

    if (!writing.first) {
        throw HistoryException(message);
        return nullptr;
    }

    addEntry(newEntry);

    emit entryCreated(m_entries.count() - 1);
    return newEntry;
}


void History::addEntry(Entry* entry) {
    qDebug() << this << "::addEntry()";
    if (!m_entries.isEmpty()) m_entries.last()->free();

    int entryIndex = m_entries.size();
    m_entries.push_back(entry);

    connectEntry(entry, entryIndex);

    emit entryAdded(m_entries.size() - 1);
}


bool History::removeEntry(const Entry* entry) {
    auto it = std::remove(m_entries.begin(), m_entries.end(), entry);
    if (it != m_entries.end()) {
        if ((*it)->deleteDirectory()) {
            int entryIndex = std::distance(m_entries.begin(), it);
            m_entries.erase(it);
            emit entryRemoved(entryIndex);

            entryIndex = m_entries.size() - 1;
            int frameIndex = m_entries.at(entryIndex)->frameCount() - 1;
            emit entryUpdated(entryIndex, frameIndex);

            return true;
        }
    }
    return false;
}


void History::updateEntry(int entryIndex, const ExtendedEntryData& entryData) {
    m_entries[entryIndex]->setExtendedData(entryData);
}


/*!
 * \brief Accesses the latest entry in the history.
 * \return The entry with the largest timestamp. If there are no entries, an empty entry is returned.
 */
const Entry* History::newestEntry() const {
    if (m_entries.isEmpty()) return m_emptyEntry;
    // Select the last frame of the last entry when using this function.
    m_entries.last()->selectFrame(m_entries.last()->frameCount() - 1);
    return m_entries.last();
}

// Entry* History::mutableNewestEntry() {
//    if (m_entries.isEmpty()) return m_emptyEntry;
//    // Select the last frame of the last entry when using this function.
//    m_entries.last()->selectFrame(m_entries.last()->frameCount() - 1);
//    return m_entries.last();
//}


/*!
 * Reads the entries in the directory and updates the entry list based on the directories that were found. Sorts the
 * entries based on their timestamps.
 */
void History::load(const QString& path) {
    qDebug() << "History::load()";
    qDebug() << "    " << path;

    unload();

    m_directoryPath = path;

    if (m_directoryPath.isEmpty()) {
        qDebug() << "History path empty";
        return;
    }

    QDir historyDir(m_directoryPath);
    if (!historyDir.exists()) {
        qDebug() << "History folder was not found.";
        return;
    }

    QStringList dirNames = historyDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

    if (dirNames.size() == 0) {
        qDebug() << "History folder was empty.";
        return;
    }

    // If we found a history tree containing entries, it is safe to create the entry list.
    for (const QString& dirName : dirNames) {
        QString path = QString("%1/%2").arg(historyDir.path()).arg(dirName);
        Entry* entry = new Entry(path);
        if (entry->isValid()) {
            m_entries.push_back(entry);
        }
    }

    std::sort(m_entries.begin(), m_entries.end(),
              [](const Entry* a, const Entry* b) -> bool { return a->info().timestamp < b->info().timestamp; });

    for (int i = 0; i < m_entries.size(); ++i) {
        Entry* entry = m_entries.at(i);
        connectEntry(entry, i);
    }

    int entryIndex = m_entries.size() - 1;
    int frameIndex = m_entries.at(entryIndex)->frameCount() - 1;
    emit entryUpdated(entryIndex, frameIndex);
    emit loaded();
}


/*!
 * \brief Helper function to connect an event listener to the selected entry. Consider adding the event listener to
 * newly created entries.
 * \param entry The entry to add the listener to
 * \param entryIndex The index of the selected entry in the History list.
 */
void History::connectEntry(Entry* entry, int entryIndex) {
    connect(entry, &Entry::frameAdded, this, [this, entryIndex, entry](int frameIndex) {
        //! \todo Make the "info" message more useful!
        emit frameAdded("info", entryIndex, frameIndex);
        emit entryUpdated(entryIndex, frameIndex);
    });
}


/*!
 * \brief Cleanup the history by removing all entries from the History list and clearing the loaded path.
 */
void History::unload() {
    for (Entry* entry : m_entries) {
        delete entry;
        entry = nullptr;
    }
    m_entries.clear();
    m_directoryPath = "";

    emit unloaded();
}

const QVector<Entry*>& History::entries() const {
    return m_entries;
}
