/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ACTIONWIDGETS_H
#define ACTIONWIDGETS_H

#include "api/entryeditor.h"
#include "atomselectorwidget.h"

#include <QBoxLayout>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QWidget>


class AbstractActionWidget : public QWidget {
    Q_OBJECT
  public:
    explicit AbstractActionWidget(EntryEditor* editor, QWidget* parent = nullptr) : QWidget(parent), m_editor(editor) {}
    virtual QString name() = 0;

  protected:
    EntryEditor* m_editor;
};


class AtomSelectionActionWidget : public AbstractActionWidget {
    Q_OBJECT
  public:
    explicit AtomSelectionActionWidget(EntryEditor* editor, AtomSelectorWidget* atomSelector, QWidget* parent = nullptr)
            : AbstractActionWidget(editor, parent), m_selector(atomSelector) {}
    virtual void displayAtomSelector() = 0;

  protected:
    AtomSelectorWidget* m_selector;
};


class GeneralAtomSelectionActionWidget : public AtomSelectionActionWidget {
    Q_OBJECT
  public:
    explicit GeneralAtomSelectionActionWidget(EntryEditor* editor, AtomSelectorWidget* atomSelector,
                                              QWidget* parent = nullptr)
            : AtomSelectionActionWidget(editor, atomSelector, parent) {

        m_parameterWidget = new QWidget;
        gb = new QGroupBox("Select Atoms");
        QPushButton* applyPB = new QPushButton("Apply");


        QHBoxLayout* l2 = new QHBoxLayout;
        l2->addItem(new QSpacerItem(40, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
        l2->addWidget(applyPB);

        QVBoxLayout* l1 = new QVBoxLayout;
        l1->addWidget(m_parameterWidget);
        l1->addWidget(gb);
        l1->addItem(l2);
        setLayout(l1);

        connect(applyPB, &QPushButton::clicked, [this] { applyAction(); });
    }

    void displayAtomSelector() {
        if (!gb->layout()) {
            gb->setLayout(new QVBoxLayout);
        }
        gb->layout()->addWidget(m_selector);
    }

    QString name() = 0;

  protected:
    virtual void applyAction() = 0;

    QGroupBox* gb;
    QWidget* m_parameterWidget;
};


class RemoveAtomsWidget : public GeneralAtomSelectionActionWidget {
    Q_OBJECT
  public:
    explicit RemoveAtomsWidget(EntryEditor* editor, AtomSelectorWidget* atomSelector, QWidget* parent = nullptr)
            : GeneralAtomSelectionActionWidget(editor, atomSelector, parent) {}

  protected:
    virtual void applyAction() {
        const AbstractAtomSelection& s = m_selector->selector();
        EntryEditor::AtomDeleteAction a(&s);
        m_editor->edit(a);
    }

    QString name() { return "Remove Atoms"; }
};


class FixAtomsWidget : public GeneralAtomSelectionActionWidget {
    Q_OBJECT
  public:
    explicit FixAtomsWidget(EntryEditor* editor, AtomSelectorWidget* atomSelector, QWidget* parent = nullptr)
            : GeneralAtomSelectionActionWidget(editor, atomSelector, parent) {

        m_fix = new QRadioButton("Fix");
        m_unfix = new QRadioButton("Unfix");
        QVBoxLayout* l = new QVBoxLayout;
        l->addWidget(m_fix);
        l->addWidget(m_unfix);
        m_parameterWidget->setLayout(l);
        m_fix->setChecked(true);
    }

  protected:
    virtual void applyAction() override {
        const AbstractAtomSelection& s = m_selector->selector();
        EntryEditor::AtomFixAction a(m_fix->isChecked(), &s);
        m_editor->edit(a);
    }

    QString name() override { return "Fix Atoms"; }

  private:
    QRadioButton* m_fix;
    QRadioButton* m_unfix;
};


class ChangeAtomTypeWidget : public GeneralAtomSelectionActionWidget {
    Q_OBJECT
  public:
    explicit ChangeAtomTypeWidget(EntryEditor* editor, AtomSelectorWidget* atomSelector, QWidget* parent = nullptr)
            : GeneralAtomSelectionActionWidget(editor, atomSelector, parent) {

        m_type = new QComboBox;
        QHBoxLayout* l = new QHBoxLayout;
        l->addWidget(new QLabel("New Type: "));
        l->addWidget(m_type);
        l->addItem(new QSpacerItem(99999, 20, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding));
        m_parameterWidget->setLayout(l);
        updateTypes();
        connect(m_editor, &EntryEditor::typesChanged, this, &ChangeAtomTypeWidget::updateTypes);
    }


  protected:
    virtual void applyAction() {
        const AbstractAtomSelection& s = m_selector->selector();
        AtomType targetType = m_editor->modifiedEntry().data.types.typeList.value(m_type->currentData().toInt());
        EntryEditor::MergeToTypeAction a(targetType, true, &s);
        m_editor->edit(a);
    }

    QString name() { return "Change Atom Types"; }

  private:
    void updateTypes() {
        int selected = m_type->currentData().toInt();
        m_type->clear();
        QVector<int> sortedTypes = m_editor->modifiedEntry().data.types.typeList.keys().toVector();
        std::sort(sortedTypes.begin(), sortedTypes.end());
        for (int typeNumber : sortedTypes) {
            const AtomType& t = m_editor->modifiedEntry().data.types.typeList[typeNumber];
            m_type->addItem(QString("%1 (%2)").arg(QString::number(typeNumber), t.symbol), typeNumber);
        }

        int selectedIndex = m_type->findData(selected);
        if (selectedIndex >= 0) m_type->setCurrentIndex(selectedIndex);
    }

    QComboBox* m_type;
};


class ShiftCellActionWidget : public AbstractActionWidget {

  public:
    explicit ShiftCellActionWidget(EntryEditor* editor, QWidget* parent = nullptr)
            : AbstractActionWidget(editor, parent) {

        QVBoxLayout* wrapper = new QVBoxLayout;

        QGridLayout* grid = new QGridLayout;

        QLabel* xLabel = new QLabel("x");
        xLabel->setMaximumWidth(30);
        grid->addWidget(xLabel, 0, 0);

        m_doubleSpinBox_shiftX = new QDoubleSpinBox(this);
        m_doubleSpinBox_shiftX->setValue(0);
        m_doubleSpinBox_shiftX->setDecimals(3);
        m_doubleSpinBox_shiftX->setMinimumWidth(200);
        grid->addWidget(m_doubleSpinBox_shiftX, 0, 1);

        QLabel* yLabel = new QLabel("y");
        yLabel->setMaximumWidth(30);
        grid->addWidget(yLabel, 1, 0);

        m_doubleSpinBox_shiftY = new QDoubleSpinBox(this);
        m_doubleSpinBox_shiftY->setValue(0);
        m_doubleSpinBox_shiftY->setDecimals(3);
        m_doubleSpinBox_shiftY->setMinimumWidth(200);
        grid->addWidget(m_doubleSpinBox_shiftY, 1, 1);

        QLabel* zLabel = new QLabel("z");
        zLabel->setMaximumWidth(30);
        grid->addWidget(zLabel, 2, 0);

        m_doubleSpinBox_shiftZ = new QDoubleSpinBox(this);
        m_doubleSpinBox_shiftZ->setValue(0);
        m_doubleSpinBox_shiftZ->setDecimals(3);
        m_doubleSpinBox_shiftZ->setMinimumWidth(200);
        grid->addWidget(m_doubleSpinBox_shiftZ, 2, 1);

        grid->addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Maximum), 2, 2);

        QHBoxLayout* buttonLayout = new QHBoxLayout;
        QPushButton* pushButton_apply = new QPushButton("Apply");
        buttonLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::Expanding, QSizePolicy::Maximum));
        buttonLayout->addWidget(pushButton_apply);

        connect(pushButton_apply, &QPushButton::clicked, [this] { applyAction(); });

        wrapper->addLayout(grid);
        wrapper->addLayout(buttonLayout);

        setLayout(wrapper);
    }


  protected:
    void applyAction() {
        qDebug() << "Shifting the structure";
        EntryEditor::ShiftCellAction a(glm::dvec3(m_doubleSpinBox_shiftX->value(), m_doubleSpinBox_shiftY->value(),
                                                  m_doubleSpinBox_shiftZ->value()));
        m_editor->edit(a);
    }

    QString name() override { return "Shift Cell"; }

  private:
    QDoubleSpinBox* m_doubleSpinBox_shiftX;
    QDoubleSpinBox* m_doubleSpinBox_shiftY;
    QDoubleSpinBox* m_doubleSpinBox_shiftZ;
};


class EmptyActionWidget : public AbstractActionWidget {
    Q_OBJECT
  public:
    explicit EmptyActionWidget(EntryEditor* editor = 0, QWidget* parent = nullptr)
            : AbstractActionWidget(editor, parent) {}
    virtual QString name() override { return "Choose Action"; }
};


#endif  // ACTIONWIDGETS_H
