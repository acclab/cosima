/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OUTPUTMODULE_H
#define OUTPUTMODULE_H

#include "api/atom.h"
#include "api/entry.h"

#include <QWidget>

#include <vector>


/*! \brief QWidget that analyzes a frame a simulation.
 *
 * An abstract class that is ment to be used in simulation cell analyzation.
 *
 * \sa OutputTab
 */
class OutputModule : public QWidget {
    Q_OBJECT

  public:
    explicit OutputModule(QWidget* parent = nullptr) : QWidget(parent) {}

    virtual void draw(const XyzData*) = 0;

  protected:
    const XyzData* atomData;
};

#endif  // OUTPUTMODULE_H
