#version 330

layout(location = 0) in highp vec3 position;
layout(location = 1) in highp vec4 faceColor;
layout(location = 2) in highp vec4 lineColor;
layout(location = 3) in float radius;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;

uniform vec2 vpSize;
uniform float pointScalingFactor;
uniform bool perspective;

uniform bool beamActive;
uniform int beamType;
uniform float beamWidth;
uniform vec3 beamIniPos;
uniform vec3 beamDirection;

out vec4 color;


bool isHighlighted()
{
    // BeamType == Broad
    if (beamType == 0)
        return false;

    vec3 ap = beamIniPos - position;
    vec3 dif = ap - dot(ap, beamDirection) * beamDirection/dot(beamDirection, beamDirection);
    vec3 d = -ap + dif;
    bool isBehind = (d.x != 0 && beamDirection.x/d.x <= 0) ||
            (d.y != 0 && beamDirection.y/d.y <= 0) ||
            (d.z != 0 && beamDirection.z/d.z <= 0);
    if (!isBehind) {

        // BeamType == Focused + Circular
        if (beamType == 2 && dot(dif, dif) < beamWidth*beamWidth/4)
            return true;

        // BeamType == Focused + Square
        else if (beamType == 1) {
            float difx2 = dif.y * dif.y;
            if (beamDirection.y != 0) {
                float BxBy = beamDirection.x/beamDirection.y;
                difx2 = 1/(1 + BxBy*BxBy) * dot(dif.x - BxBy*dif.y, dif.x - BxBy*dif.y);
            }
            float dify2 = dot(dif, dif) - difx2;
            if (difx2 < beamWidth*beamWidth/4 && dify2 < beamWidth*beamWidth/4)
                return true;
        }
    }

    return false;
}


void main(void)
{
    gl_Position = p * v * m * vec4(position, 1);
    if (perspective) {
        vec4 eye_position = v * m * vec4(position, 1);
        gl_PointSize = max(vpSize.y * p[1][1] * radius / (eye_position.z * p[2][3] + p[3][3]), 3);
    } else {
        gl_PointSize = max(2.5 * radius * pointScalingFactor, 3);
    }

    if (beamActive) {
        // Recolor atoms that are in the beam. Light colors are made darker and dark colors lighter.
        if (isHighlighted()) {
            vec3 temp = faceColor.xyz;
            if (dot(temp, temp) > 0.7) temp *= 0.7;
            else temp = (temp + 0.1) * 1.4;
            color = vec4(temp, faceColor.a);
        } else {
            color = faceColor;
        }
    } else {
        color = faceColor;
    }
}
