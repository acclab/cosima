/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "dimensionselector.h"

#include "api/util.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QLabel>
#include <QLayout>


DimensionSelector::DimensionSelector(QWidget* parent) : QWidget(parent) {

    auto labelGenerator = [this](const QString& text) {
        QLabel* label = new QLabel(this);
        label->setText(text);
        label->setMaximumWidth(30);
        return label;
    };

    auto spinBoxGenerator = [this] {
        QDoubleSpinBox* sb = new QDoubleSpinBox(this);
        sb->setDecimals(6);
        sb->setRange(-999999.999999, 999999.999999);
        sb->setSuffix(" Å");
        return sb;
    };

    auto checkBoxGenerator = [this](const QString& text) {
        QCheckBox* checkBox = new QCheckBox(this);
        checkBox->setText(text);
        checkBox->setChecked(true);
        return checkBox;
    };

    QLabel* xLabel = labelGenerator("x");
    QLabel* yLabel = labelGenerator("y");
    QLabel* zLabel = labelGenerator("z");

    m_xLower = spinBoxGenerator();
    m_xUpper = spinBoxGenerator();
    m_yLower = spinBoxGenerator();
    m_yUpper = spinBoxGenerator();
    m_zLower = spinBoxGenerator();
    m_zUpper = spinBoxGenerator();

    m_pbcX = checkBoxGenerator("Periodic");
    m_pbcY = checkBoxGenerator("Periodic");
    m_pbcZ = checkBoxGenerator("Periodic");

    QGridLayout* grid = new QGridLayout;
    // Row 0 (x values)
    grid->addWidget(xLabel, 0, 0);
    grid->addWidget(m_xLower, 0, 1);
    grid->addWidget(m_xUpper, 0, 2);
    grid->addWidget(m_pbcX, 0, 3);
    // Row 1 (y values)
    grid->addWidget(yLabel, 1, 0);
    grid->addWidget(m_yLower, 1, 1);
    grid->addWidget(m_yUpper, 1, 2);
    grid->addWidget(m_pbcY, 1, 3);
    // Row 2 (z values)
    grid->addWidget(zLabel, 2, 0);
    grid->addWidget(m_zLower, 2, 1);
    grid->addWidget(m_zUpper, 2, 2);
    grid->addWidget(m_pbcZ, 2, 3);

    setLayout(grid);
    setFocusPolicy(Qt::NoFocus);

    connect(m_xLower, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.x1 == val) return;
        m_dimensions.x1 = val;
        updateWidgets();
        emit dimensionsEdited();
    });
    connect(m_xUpper, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.x2 == val) return;
        m_dimensions.x2 = val;
        updateWidgets();
        emit dimensionsEdited();
    });
    connect(m_yLower, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.y1 == val) return;
        m_dimensions.y1 = val;
        updateWidgets();
        emit dimensionsEdited();
    });
    connect(m_yUpper, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.y2 == val) return;
        m_dimensions.y2 = val;
        updateWidgets();
        emit dimensionsEdited();
    });
    connect(m_zLower, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.z1 == val) return;
        m_dimensions.z1 = val;
        updateWidgets();
        emit dimensionsEdited();
    });
    connect(m_zUpper, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double val) {
        if (m_dimensions.z2 == val) return;
        m_dimensions.z2 = val;
        updateWidgets();
        emit dimensionsEdited();
    });


    connect(m_pbcX, &QCheckBox::clicked, [this](bool checked) {
        m_pbc.x = checked;
        emit pbcEdited();
    });
    connect(m_pbcY, &QCheckBox::clicked, [this](bool checked) {
        m_pbc.y = checked;
        emit pbcEdited();
    });
    connect(m_pbcZ, &QCheckBox::clicked, [this](bool checked) {
        m_pbc.z = checked;
        emit pbcEdited();
    });

    WheelGuard* guard = new WheelGuard(this);
    QList<QWidget*> guardedWidgets = {m_xLower, m_xUpper, m_yLower, m_yUpper, m_zLower, m_zUpper};
    for (QWidget* w : guardedWidgets) {
        w->installEventFilter(guard);
        w->setFocusPolicy(Qt::StrongFocus);
    }
}

/*!
 * Constructs a box based on the selected values and returns it.
 */
const Box& DimensionSelector::getDimensions() {
    return m_dimensions;
}

/*!
 * Sets the values of the QDoubleSpinBoxes based on the given box.
 */
void DimensionSelector::setDimensions(const Box& box) {
    if (m_dimensions != box) {
        m_dimensions = box;
        updateWidgets();
    }
}


const glm::bvec3& DimensionSelector::getPeriodicBoundaries() {
    return m_pbc;
}

void DimensionSelector::setPeriodicBoundaries(const glm::bvec3& pbc) {
    if (m_pbc != pbc) {
        m_pbc = pbc;
        updateWidgets();
    }
}


void DimensionSelector::updateWidgets() {
    QSignalBlocker blocker1(m_xLower);
    QSignalBlocker blocker2(m_xUpper);
    QSignalBlocker blocker3(m_yLower);
    QSignalBlocker blocker4(m_yUpper);
    QSignalBlocker blocker5(m_zLower);
    QSignalBlocker blocker6(m_zUpper);

    m_xLower->setMaximum(m_dimensions.x2);
    m_xUpper->setMinimum(m_dimensions.x1);
    m_yLower->setMaximum(m_dimensions.y2);
    m_yUpper->setMinimum(m_dimensions.y1);
    m_zLower->setMaximum(m_dimensions.z2);
    m_zUpper->setMinimum(m_dimensions.z1);

    util::updateDoubleSpinBoxValue(m_xLower, m_dimensions.x1);
    util::updateDoubleSpinBoxValue(m_xUpper, m_dimensions.x2);
    util::updateDoubleSpinBoxValue(m_yLower, m_dimensions.y1);
    util::updateDoubleSpinBoxValue(m_yUpper, m_dimensions.y2);
    util::updateDoubleSpinBoxValue(m_zLower, m_dimensions.z1);
    util::updateDoubleSpinBoxValue(m_zUpper, m_dimensions.z2);

    m_pbcX->setChecked(m_pbc.x);
    m_pbcY->setChecked(m_pbc.y);
    m_pbcZ->setChecked(m_pbc.z);
}
