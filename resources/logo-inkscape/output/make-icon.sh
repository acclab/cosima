#!/bin/bash

convert cosima-logo.png -scale 32 icon/icon-32.png
convert cosima-logo.png -scale 48 icon/icon-48.png
convert cosima-logo.png -scale 64 icon/icon-64.png
convert cosima-logo.png -scale 96 icon/icon-96.png
convert cosima-logo.png -scale 128 icon/icon-128.png

convert icon/icon-32.png icon/icon-48.png icon/icon-64.png icon/icon-96.png icon/icon-128.png icon.ico
