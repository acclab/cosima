#version 330

layout(location = 0) in highp vec3 position;
layout(location = 1) in highp vec4 colorIn;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;

out vec4 color;

void main(void)
{
    gl_Position = p * v * m * vec4(position, 1);
    color = colorIn;
}
