/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "singlecascadesimulationwidget.h"
#include "ui_singlecascadesimulationwidget.h"

#include "api/programsettings.h"
#include "api/project.h"

#include "gui/widgets/utils/collapsiblegroupbox.h"


SingleCascadeSimulationWidget::SingleCascadeSimulationWidget(QWidget* parent)
        : AbstractSimulationWidget(parent), ui(new Ui::SingleCascadeSimulationWidget) {
    ui->setupUi(this);

    // Tab Cascade
    QVBoxLayout* l2 = new QVBoxLayout;

    CollapsibleGroupBox* ionGB = new CollapsibleGroupBox;
    ionGB->setTitle("Ion");
    ionGB->setLayout(new QVBoxLayout);
    m_ionWidget = new IonWidget();
    ionGB->layout()->addWidget(m_ionWidget);
    l2->addWidget(ionGB);

    CollapsibleGroupBox* ionTransformWidget = new CollapsibleGroupBox();
    ionTransformWidget->setTitle("Ion Transform");
    ionTransformWidget->setLayout(new QVBoxLayout);
    m_ionTransformWidget = new IonTransformWidget();
    ionTransformWidget->layout()->addWidget(m_ionTransformWidget);
    l2->addWidget(ionTransformWidget);

    m_controlWidget = new ControlWidget();
    l2->addWidget(m_controlWidget);
    l2->addItem(new QSpacerItem(40, 10000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->widget_cascadeContent->setLayout(l2);

    // Tab Interactions
    QVBoxLayout* l4 = new QVBoxLayout;
    m_interactionWidget = new InteractionWidget();
    l4->addWidget(m_interactionWidget);
    ui->widget_interactionsContent->setLayout(l4);

    // Tab Speedup
    QVBoxLayout* speedupLayout = new QVBoxLayout();
    m_speedupWidget = new SpeedupWidget();
    speedupLayout->addWidget(m_speedupWidget);
    speedupLayout->addItem(new QSpacerItem(40, 1000, QSizePolicy::Expanding, QSizePolicy::Maximum));
    ui->widget_speedupContent->setLayout(speedupLayout);


    connect(ui->tabWidget, &QTabWidget::currentChanged, this, &SingleCascadeSimulationWidget::onChangeTab);


    connect(m_ionWidget, &IonWidget::entryTypesUpdated, m_interactionWidget,
            &InteractionWidget::updatePotentialInteractions);


    connect(m_ionWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified ion widget";
        setModified(true);
        setTabStatusIcon(ui->tab_cascade, loadIcon(gui::enums::state::Status::Modified));
    });
    connect(m_ionTransformWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified ion transform widget";
        setModified(true);
        setTabStatusIcon(ui->tab_cascade, loadIcon(gui::enums::state::Status::Modified));
    });
    connect(m_controlWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified control widget";
        setModified(true);
        setTabStatusIcon(ui->tab_cascade, loadIcon(gui::enums::state::Status::Modified));
    });

    connect(m_interactionWidget, &AbstractComponentWidget::modified, this, [=](bool errors) {
        qDebug() << "Modified interaction widget";
        setModified(true);
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->tab_interactions, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->tab_interactions, loadIcon(gui::enums::state::Status::Modified));
        }
    });
    connect(m_interactionWidget, &AbstractComponentWidget::needsAttention, this, [=](bool errors) {
        setErrors(errors);
        if (errors) {
            setTabStatusIcon(ui->tab_interactions, loadIcon(gui::enums::state::Status::Error));
        } else {
            setTabStatusIcon(ui->tab_interactions, loadIcon(gui::enums::state::Status::Attention));
        }
    });

    connect(m_speedupWidget, &AbstractComponentWidget::modified, this, [=] {
        qDebug() << "Modified speedup widget";
        setModified(true);
        setTabStatusIcon(ui->tab_speedup, loadIcon(gui::enums::state::Status::Modified));
    });

    emit changePlotMode(PlotCellGLWidget::Beam);
}


SingleCascadeSimulationWidget::~SingleCascadeSimulationWidget() {
    delete m_ionWidget;
    delete m_ionTransformWidget;
    delete m_controlWidget;
    delete m_interactionWidget;
    delete m_speedupWidget;

    delete ui;
}


void SingleCascadeSimulationWidget::connectToPlot(PlotCellGLWidget* glWidget) {
    m_controlWidget->connectToPlot(glWidget);

    connect(this, &SingleCascadeSimulationWidget::changePlotMode, glWidget, &PlotCellGLWidget::setPlotMode);
    connect(m_ionTransformWidget, &IonTransformWidget::transformUpdated, this, [=] {

        // Create a transform object from the parameter set and give it to the OpenGL widget for drawing the beam
        // graphics.

        api::BaseParameters params = m_ionTransformWidget->parameters();
        double x = params[api::parameters::ion_transform::X_POSITION].value().toDouble();
        double y = params[api::parameters::ion_transform::Y_POSITION].value().toDouble();
        double z = params[api::parameters::ion_transform::Z_POSITION].value().toDouble();

        IonTransformData ionTransform;
        ionTransform.phi = params[api::parameters::ion_transform::PHI].value().toDouble();
        ionTransform.theta = params[api::parameters::ion_transform::THETA].value().toDouble();
        ionTransform.position = glm::dvec3(x, y, z);

        glWidget->setBeam(ionTransform);
    });

    onChangeTab(0);
}


void SingleCascadeSimulationWidget::disconnectFromPlot(PlotCellGLWidget* glWidget) {
    m_controlWidget->disconnectFromPlot(glWidget);

    disconnect(this, &SingleCascadeSimulationWidget::changePlotMode, glWidget, &PlotCellGLWidget::setPlotMode);
    disconnect(m_ionTransformWidget);

    glWidget->setPlotMode(PlotCellGLWidget::Cooling);
}


void SingleCascadeSimulationWidget::setControllerParametersToWidget(ParcasBaseController* controller) {
    // Remove the unsaved indicator from the tabs
    setModified(false);
    setErrors(false);
    setNeedsAttention(false);

    setTabStatusIcon(ui->tab_cascade, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->tab_interactions, loadIcon(gui::enums::state::Ok));
    setTabStatusIcon(ui->tab_speedup, loadIcon(gui::enums::state::Ok));

    m_unmodifiedParameters = controller->parameters();

    m_ionWidget->setParameters(controller->parameters());
    m_ionTransformWidget->setParameters(controller->parameters());
    m_controlWidget->setParameters(controller->parameters());
    m_interactionWidget->setParameters(controller->parameters());
    m_speedupWidget->setParameters(controller->parameters());
}


void SingleCascadeSimulationWidget::setWidgetParametersToController(ParcasBaseController* controller) {
    controller->insertParameters(m_ionWidget->parameters());
    controller->insertParameters(m_ionTransformWidget->parameters());
    controller->insertParameters(m_controlWidget->parameters());
    controller->insertParameters(m_interactionWidget->parameters());
    controller->insertParameters(m_speedupWidget->parameters());
}


// bool SingleCascadeSimulationWidget::isModified() {
//    api::BaseParameters tempParams;
//    tempParams.insert(m_ionWidget->parameters());
//    tempParams.insert(m_ionTransformWidget->parameters());
//    tempParams.insert(m_controlWidget->parameters());
//    tempParams.insert(m_interactionWidget->parameters());
//    tempParams.insert(m_speedupWidget->parameters());

//    for (const api::key_t& key : tempParams.values().keys()) {
//        qDebug() << key << tempParams[key].value() << m_unmodifiedParameters[key].value();
//        if (tempParams[key].value() != m_unmodifiedParameters[key].value()) {
//            return true;
//        }
//    }

//    return false;
//}


void SingleCascadeSimulationWidget::setComponentEntry(ExtendedEntryData* componentEntry) {
    m_ionWidget->setEntry(componentEntry);
    m_controlWidget->setEntry(componentEntry);
    m_interactionWidget->setEntry(componentEntry);
    m_speedupWidget->setEntry(componentEntry);
}


void SingleCascadeSimulationWidget::onChangeTab(int /*index*/) {
    QWidget* current = ui->tabWidget->currentWidget();
    emit changePlotMode((current == ui->tab_cascade) ? PlotCellGLWidget::Beam : PlotCellGLWidget::Cooling);
    m_controlWidget->showControlRegion(current == ui->tab_cascade);
}


void SingleCascadeSimulationWidget::setTabStatusIcon(QWidget* tabWidget, const QIcon& icon) {
    ui->tabWidget->setTabIcon(ui->tabWidget->indexOf(tabWidget), icon);
}


void SingleCascadeSimulationWidget::enableSpeedupModule(bool enabled) {
    ui->tabWidget->setTabEnabled(SpeedupTab, enabled);
    ui->tabWidget->setTabToolTip(SpeedupTab,
                                 enabled ? "" : "Enable the speedup from the backend settings \"edit/settings/\"");
}
