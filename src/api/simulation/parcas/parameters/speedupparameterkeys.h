/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SPEEDUPPARAMETERKEYS_H
#define SPEEDUPPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter MOVEMODE{"movemode", false, QVariant()};
const parameter ALLMOVET{"allmovet", false, QVariant()};
const parameter MOVECUTH{"movecuth", false, QVariant()};
const parameter MOVECUTC{"movecutc", false, QVariant()};
const parameter MOVECUTF{"movecutf", false, QVariant()};
const parameter FCRITA{"FcritA", false, QVariant()};
const parameter ECRITA{"EcritA", false, QVariant()};
const parameter FCRITFACT{"FcritFact", false, QVariant()};
const parameter ECRITFACT{"EcritFact", false, QVariant()};
const parameter MCRITT{"mcritt", false, QVariant()};  // Array 1D
const parameter FCRIT{"Fcrit", false, QVariant()};    // Array 1D
const parameter ECRIT{"Ecrit", false, QVariant()};

const QList<parameter> speedupKeys{MOVEMODE, ALLMOVET,  MOVECUTH,  MOVECUTC, MOVECUTF, FCRITA,
                                   ECRITA,   FCRITFACT, ECRITFACT, MCRITT,   FCRIT,    ECRIT};

#endif  // SPEEDUPPARAMETERKEYS_H
