/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CONTROLREGIONMODEL_H
#define CONTROLREGIONMODEL_H

#include "gui/rendering/abstractmodel.h"
#include "gui/rendering/linemesh.h"
#include "gui/rendering/quadmodel.h"
#include "gui/rendering/trianglemesh.h"

#include <QObject>


class ControlRegionModel : public AbstractModel {
    Q_OBJECT

  public:
    explicit ControlRegionModel(QObject* parent = nullptr);
    ~ControlRegionModel() override;

    void draw(const glm::mat4& model, QOpenGLShaderProgram& program) override;

  private:
    void updateQuad(std::size_t quadId, double x, double y, double z, glm::vec3 rotation, double sizeX, double sizeY);

    const glm::vec3 ROT_X1 = glm::vec3(0, -90, 0);
    const glm::vec3 ROT_X2 = glm::vec3(0, 90, 0);
    const glm::vec3 ROT_Y1 = glm::vec3(90, 0, 0);
    const glm::vec3 ROT_Y2 = glm::vec3(-90, 0, 0);
    const glm::vec3 ROT_Z1 = glm::vec3(0, -180, 0);
    const glm::vec3 ROT_Z2 = glm::vec3(0, 0, 0);

    std::vector<QuadModel*> quads;
    bool m_show;

  public slots:
    void onShow(bool onShow);
    void onUpdateValues(const Box& limits, double xWidth, double yWidth, double zLower, double zUpper);
    void onUpdateEnabledRegions(bool x, bool y, bool z);
};

#endif  // CONTROLREGIONMODEL_H
