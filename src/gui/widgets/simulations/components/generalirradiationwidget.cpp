/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "generalirradiationwidget.h"
#include "ui_generalirradiationwidget.h"

#include "gui/widgets/utils/wheelguard.h"

#include <QDebug>


GeneralIrradiationWidget::GeneralIrradiationWidget(QWidget* parent)
        : AbstractComponentWidget(parent), ui(new Ui::GeneralIrradiationWidget) {
    ui->setupUi(this);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);

    QList<QWidget*> wheelGuardedWidgets{ui->spinBox_numberOfIons, ui->spinBox_relaxationInterval};

    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    connect(ui->spinBox_numberOfIons, QOverload<int>::of(&QSpinBox::valueChanged), this, [=] { emit modified(); });
    connect(ui->groupBox_relaxation, &QGroupBox::clicked, this, [=](bool checked) {
        emit relaxationEnabled(checked);
        emit modified();
    });
    connect(ui->spinBox_relaxationInterval, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->checkBox_startWithRelaxation, &QCheckBox::stateChanged, this, [=] { emit modified(); });
}


GeneralIrradiationWidget::~GeneralIrradiationWidget() {
    delete ui;
}


void GeneralIrradiationWidget::setParameters(api::BaseParameters parameters) {

    QSignalBlocker blocker1(ui->spinBox_numberOfIons);
    QSignalBlocker blocker2(ui->groupBox_relaxation);
    QSignalBlocker blocker3(ui->checkBox_startWithRelaxation);
    QSignalBlocker blocker4(ui->spinBox_relaxationInterval);

    int numberOfIons = parameters[api::parameters::irradiation::NUMBER_OF_IONS].value().toInt();
    bool useRelax = parameters[api::parameters::irradiation::RELAX_ON].value().toBool();
    bool startWithRelax = parameters[api::parameters::irradiation::START_WITH_RELAX_ON].value().toBool();
    int relaxationInterval = parameters[api::parameters::irradiation::RELAX_INTERVAL].value().toInt();

    ui->spinBox_numberOfIons->setValue(numberOfIons);
    ui->groupBox_relaxation->setChecked(useRelax);
    ui->checkBox_startWithRelaxation->setChecked(startWithRelax);
    ui->spinBox_relaxationInterval->setValue(relaxationInterval);
}


api::BaseParameters GeneralIrradiationWidget::parameters() {

    api::BaseParameters params;

    params.set(api::parameters::irradiation::NUMBER_OF_IONS, api::value_t(ui->spinBox_numberOfIons->value()));
    params.set(api::parameters::irradiation::RELAX_ON, api::value_t(ui->groupBox_relaxation->isChecked()));
    params.set(api::parameters::irradiation::START_WITH_RELAX_ON,
               api::value_t(ui->checkBox_startWithRelaxation->isChecked()));
    params.set(api::parameters::irradiation::RELAX_INTERVAL, api::value_t(ui->spinBox_relaxationInterval->value()));

    return params;
}
