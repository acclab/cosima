/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include "abstractcomponentwidget.h"

#include "gui/widgets/plotcellglwidget.h"
#include "gui/widgets/simulations/components/outputwidget.h"
#include "gui/widgets/simulations/components/pressurecontrolwidget.h"
#include "gui/widgets/simulations/components/temperaturecontrolwidget.h"
#include "gui/widgets/simulations/components/timewidget.h"

#include <QWidget>


/*!
 * \brief The ControlWidget class
 *
 * Wrapping parameter widgets to form a larger unity.
 *
 * \sa TimeWidget
 * \sa TemperatureControlWidget
 * \sa PressureControlWidget
 * \sa OutputWidget
 */
class ControlWidget : public AbstractComponentWidget {
    Q_OBJECT

  public:
    explicit ControlWidget(QWidget* parent = nullptr);

    void setEntry(ExtendedEntryData* entry) override;

    void setParameters(api::BaseParameters parameters) override;
    api::BaseParameters parameters() override;

    void connectToPlot(PlotCellGLWidget* glWidget);
    void disconnectFromPlot(PlotCellGLWidget* glWidget);

  private:
    TimeWidget* m_timeWidget;
    TemperatureControlWidget* m_temperatureWidget;
    PressureControlWidget* m_pressureWidget;
    OutputWidget* m_outputWidget;

    PlotCellGLWidget* m_openglContext;

  public slots:
    void showControlRegion(bool show);
};

#endif  // CONTROLWIDGET_H
