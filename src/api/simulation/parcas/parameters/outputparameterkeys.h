/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef OUTPUTPARAMETERKEYS_H
#define OUTPUTPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter NDUMP{"ndump", false, QVariant()};
const parameter NRESTART{"nrestart", false, QVariant()};
const parameter FINALXYZ{"finalxyz", false, QVariant()};
const parameter MOVIEMODE{"moviemode", false, QVariant()};
const parameter NMOVIE{"nmovie", false, QVariant()};
const parameter DTMOV{"dtmov", false, QVariant()};  // 1D array
const parameter TMOV{"tmov", false, QVariant()};    // 1D array

const QList<parameter> outputKeys{NDUMP, NRESTART, FINALXYZ, MOVIEMODE, NMOVIE, DTMOV, TMOV};

#endif  // OUTPUTPARAMETERKEYS_H
