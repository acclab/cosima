/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "interactiveplot.h"


InteractivePlot::InteractivePlot(QWidget* parent) : QCustomPlot(parent) {
    zoomRect = new QCPItemRect(this);
    zoomRect->setVisible(false);
}

InteractivePlot::~InteractivePlot() {
    delete zoomRect;
}


/*!
 * Overrides QCustomPlot::mousePressEvent().
 */
void InteractivePlot::mousePressEvent(QMouseEvent* event) {
    grabbedCoordinate.x = xAxis->pixelToCoord(event->pos().x());
    grabbedCoordinate.y = yAxis->pixelToCoord(event->pos().y());
    QCustomPlot::mousePressEvent(event);
    lastCursorPos = event->pos();
}


/*!
 * Overrides QCustomPlot::mouseMoveEvent().
 */
void InteractivePlot::mouseMoveEvent(QMouseEvent* event) {
    if (event->buttons() == Qt::LeftButton) {
        double topLeftx = std::min<double>(grabbedCoordinate.x, xAxis->pixelToCoord(event->x()));
        double topLefty = std::max<double>(grabbedCoordinate.y, yAxis->pixelToCoord(event->y()));
        double bottomRightx = std::max<double>(grabbedCoordinate.x, xAxis->pixelToCoord(event->x()));
        double bottomRighty = std::min<double>(grabbedCoordinate.y, yAxis->pixelToCoord(event->y()));

        zoomRect->topLeft->setCoords(topLeftx, topLefty);
        zoomRect->bottomRight->setCoords(bottomRightx, bottomRighty);
        zoomRect->setVisible(true);
        replot();

    } else if (event->buttons() == Qt::RightButton) {
        double width = xAxis->range().upper - xAxis->range().lower;
        double height = yAxis->range().upper - yAxis->range().lower;
        double x1 = xAxis->pixelToCoord(event->pos().x()) - xAxis->range().lower;
        double y1 = yAxis->pixelToCoord(event->pos().y()) - yAxis->range().lower;

        double newMinx = xAxis->pixelToCoord(lastCursorPos.x()) - x1;
        double newMiny = yAxis->pixelToCoord(lastCursorPos.y()) - y1;

        xAxis->setRangeLower(newMinx);
        yAxis->setRangeLower(newMiny);
        xAxis->setRangeUpper(newMinx + width);
        yAxis->setRangeUpper(newMiny + height);

        replot();
    }
    lastCursorPos = event->pos();
}

/*!
 * Overrides QCustomPlot::mouseReleaseEvent().
 */
void InteractivePlot::mouseReleaseEvent(QMouseEvent* event) {
    if (zoomRect->visible()) {
        xAxis->setRangeLower(zoomRect->topLeft->coords().x());
        yAxis->setRangeLower(zoomRect->bottomRight->coords().y());
        xAxis->setRangeUpper(zoomRect->bottomRight->coords().x());
        yAxis->setRangeUpper(zoomRect->topLeft->coords().y());
    }
    zoomRect->setVisible(false);
    replot();
    QCustomPlot::mouseReleaseEvent(event);
}
