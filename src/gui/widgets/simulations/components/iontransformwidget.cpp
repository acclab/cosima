/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "iontransformwidget.h"
#include "ui_iontransformwidget.h"

#include "api/util.h"

#include "gui/widgets/utils/wheelguard.h"


IonTransformWidget::IonTransformWidget(QWidget* parent)
        : AbstractComponentWidget(parent), ui(new Ui::IonTransformWidget) {
    ui->setupUi(this);


    // Protect against fields catching the scroll
    QVector<QWidget*> wheelGuardedWidgets = {
        ui->doubleSpinBox_xPosition, ui->doubleSpinBox_yPosition, ui->doubleSpinBox_zPosition, ui->jumpSlider_phi,
        ui->jumpSlider_theta,        ui->doubleSpinBox_phi,       ui->doubleSpinBox_theta};

    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    connect(ui->doubleSpinBox_xPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit transformUpdated();
        emit modified();
    });
    connect(ui->doubleSpinBox_yPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit transformUpdated();
        emit modified();
    });
    connect(ui->doubleSpinBox_zPosition, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] {
        emit transformUpdated();
        emit modified();
    });

    connect(ui->jumpSlider_phi, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_phi->setValue(value);
        // The modified signal is sent from the other setValue function.
    });
    connect(ui->doubleSpinBox_phi, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_phi->setValue(value);
        emit transformUpdated();
        emit modified();
    });

    connect(ui->jumpSlider_theta, &JumpSlider::valueChanged, [this](int value) {
        ui->doubleSpinBox_theta->setValue(value);
        // The modified signal is sent from the other setValue function.
    });
    connect(ui->doubleSpinBox_theta, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double value) {
        ui->jumpSlider_theta->setValue(value);
        emit transformUpdated();
        emit modified();
    });
}


IonTransformWidget::~IonTransformWidget() {
    delete ui;
}


void IonTransformWidget::setParameters(api::BaseParameters parameters) {
    QSignalBlocker blocker1(ui->doubleSpinBox_xPosition);
    QSignalBlocker blocker2(ui->doubleSpinBox_yPosition);
    QSignalBlocker blocker3(ui->doubleSpinBox_zPosition);
    QSignalBlocker blocker4(ui->doubleSpinBox_phi);
    QSignalBlocker blocker5(ui->doubleSpinBox_theta);

    ui->doubleSpinBox_xPosition->setValue(parameters[api::parameters::ion_transform::X_POSITION].value().toDouble());
    ui->doubleSpinBox_yPosition->setValue(parameters[api::parameters::ion_transform::Y_POSITION].value().toDouble());
    ui->doubleSpinBox_zPosition->setValue(parameters[api::parameters::ion_transform::Z_POSITION].value().toDouble());
    ui->doubleSpinBox_phi->setValue(parameters[api::parameters::ion_transform::PHI].value().toDouble());
    ui->doubleSpinBox_theta->setValue(parameters[api::parameters::ion_transform::THETA].value().toDouble());
}


api::BaseParameters IonTransformWidget::parameters() {
    api::BaseParameters params;
    params.set(api::parameters::ion_transform::X_POSITION, api::value_t(ui->doubleSpinBox_xPosition->value()));
    params.set(api::parameters::ion_transform::Y_POSITION, api::value_t(ui->doubleSpinBox_yPosition->value()));
    params.set(api::parameters::ion_transform::Z_POSITION, api::value_t(ui->doubleSpinBox_zPosition->value()));
    params.set(api::parameters::ion_transform::PHI, api::value_t(ui->doubleSpinBox_phi->value()));
    params.set(api::parameters::ion_transform::THETA, api::value_t(ui->doubleSpinBox_theta->value()));
    return params;
}
