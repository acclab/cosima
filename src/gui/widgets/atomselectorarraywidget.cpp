/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "atomselectorarraywidget.h"
#include "ui_atomselectorarraywidget.h"

#include <QDebug>


AtomSelectorArrayWidget::AtomSelectorArrayWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent), ui(new Ui::AtomSelectorArrayWidget) {
    ui->setupUi(this);

    connect(ui->addPB, &QPushButton::clicked, this, &AtomSelectorArrayWidget::addSelector);
}

AtomSelectorArrayWidget::~AtomSelectorArrayWidget() {
    delete ui;
}

const AbstractAtomSelection& AtomSelectorArrayWidget::selector() {
    m_selector.clear();

    for (AbstractAtomSelectorWidget* s : m_selectors) {
        m_selector.addExternalSelection(&s->selector());
    }

    return m_selector;
}

void AtomSelectorArrayWidget::setEntry(const ExtendedEntryData* entry) {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->setEntry(entry);
    }
    AbstractAtomSelectorWidget::setEntry(entry);
}

void AtomSelectorArrayWidget::init() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->init();
    }
}

void AtomSelectorArrayWidget::updateAtoms() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->updateAtoms();
    }
}

void AtomSelectorArrayWidget::updateCellLims() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->updateCellLims();
    }
}

void AtomSelectorArrayWidget::updateTypes() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->updateTypes();
    }
}

void AtomSelectorArrayWidget::connectToPlot() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->connectToPlot();
    }
}

void AtomSelectorArrayWidget::hidePlottables() {
    for (AbstractAtomSelectorWidget* s : m_selectors) {
        s->hidePlottables();
    }
}

void AtomSelectorArrayWidget::setMode(ArrayAtomSelection::Mode mode) {
    m_selector.setMode(mode);
}

void AtomSelectorArrayWidget::addSelector() {
    if (m_context == nullptr) return;

    QGroupBox* gb = new QGroupBox(this);
    QVBoxLayout* l = new QVBoxLayout;
    AtomSelectorWidget* s = new AtomSelectorWidget(m_context);

    s->setEntry(m_entry);
    s->connectToPlot();
    l->addWidget(s);
    gb->setLayout(l);
    ui->selectorLayout->addWidget(gb);

    m_selectors.push_back(s);

    connect(s, &AtomSelectorWidget::closeRequested, [this, s, gb] {
        delete gb;
        m_selectors.removeOne(s);
    });
}
