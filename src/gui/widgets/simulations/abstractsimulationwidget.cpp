/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "abstractsimulationwidget.h"

#include <QIcon>


void AbstractSimulationWidget::setEntry(const ExtendedEntryData* entry) {
    // Make a mutable copy of the entry data given to this widget. This copy will be used by reference among all the
    // subwidgets to treat them as a single larger widget.
    m_componentEntry = new ExtendedEntryData(*entry);
    setComponentEntry(m_componentEntry);
}


void AbstractSimulationWidget::setModified(bool modified) {
    m_modified = modified;
}


bool AbstractSimulationWidget::isModified() {
    return m_modified;
}


void AbstractSimulationWidget::setErrors(bool errors) {
    m_errors = errors;
}


bool AbstractSimulationWidget::hasErrors() {
    return m_errors;
}


void AbstractSimulationWidget::setNeedsAttention(bool attention) {
    m_attention = attention;
}


bool AbstractSimulationWidget::needsAttention() {
    return m_attention;
}


QIcon AbstractSimulationWidget::loadIcon(gui::enums::state::Status status) {
    QSize size(15, 15);
    QIcon icon;
    switch (status) {
        case gui::enums::state::Error:
            icon.addFile(":/icons/times-circle-red", size);
            break;

        case gui::enums::state::Warning:
            icon.addFile(":/icons/exclamation-triangle-orange", size);
            break;

        case gui::enums::state::Modified:
            icon.addFile(":/icons/save", size);
            break;

        case gui::enums::state::Attention:
            icon.addFile(":/icons/info-circle-blue", size);
            break;

        case gui::enums::state::Ok:
            icon.addFile(":/icons/check-circle-green", size);
            break;

        default:
            break;
    }
    return icon;
}
