/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef DIALOGSETUPBACKEND_H
#define DIALOGSETUPBACKEND_H

#include <QDialog>


namespace Ui {
class DialogSetupBackend;
}

/*!
 * \todo Remove as redundant after the program settings widget has been implemented.
 */
class DialogSetupBackend : public QDialog {
    Q_OBJECT

  public:
    explicit DialogSetupBackend(QWidget* parent = nullptr);
    explicit DialogSetupBackend(QWidget* parent, QString parcasPath);
    ~DialogSetupBackend();

    bool checkParcasFile(QString path);
    void setParcasPath(QString path);
    QString parcasPath();

  private:
    Ui::DialogSetupBackend* ui;

    void tryLocateParcasFile();

  private slots:
    void locateParcasDialog();
};

#endif  // DIALOGSETUPBACKEND_H
