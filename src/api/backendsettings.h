/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BACKENDSETTINGS_H
#define BACKENDSETTINGS_H

#include "abstractstreamable.h"

#include <QCoreApplication>
#include <QThread>


struct BackendSettings : AbstractStreamable {
    // Enable the Adaptive Moving Environment module
    bool speedupEnabled;
    // Local simulation
    QString parcasPath;
    bool showSimulationStartDialog;
    bool useMpi;
    int mpiCores;
    // Standalone simulation
    QString processorPath;

    static BackendSettings defaultSettings() {
        BackendSettings settings;
        settings.speedupEnabled = true;
        settings.parcasPath = QCoreApplication::applicationDirPath() + "/bin/parcas";
        settings.showSimulationStartDialog = true;
        settings.useMpi = true;
        settings.mpiCores = QThread::idealThreadCount();
        settings.processorPath = QCoreApplication::applicationDirPath() + "/bin/tools/processor";
        return settings;
    }

    QDataStream& save(QDataStream& out) const override {
        out << speedupEnabled;
        out << parcasPath;
        out << showSimulationStartDialog;
        out << useMpi;
        out << mpiCores;
        out << processorPath;
        return out;
    }

    QDataStream& load(QDataStream& in) override {
        in >> speedupEnabled;
        in >> parcasPath;
        in >> showSimulationStartDialog;
        in >> useMpi;
        in >> mpiCores;
        in >> processorPath;
        return in;
    }

    QDebug debug(QDebug d) const override {
        d << "BackendSettings:" << endl;
        d << "  speedupEnabled:            " << speedupEnabled << endl;
        d << "  parcasPath:                " << parcasPath << endl;
        d << "  showSimulationStartDialog: " << showSimulationStartDialog << endl;
        d << "  useMpi:                    " << useMpi << endl;
        d << "  mpiCores:                  " << mpiCores << endl;
        d << "  processorPath:             " << processorPath << endl;
        return d;
    }
};

Q_DECLARE_METATYPE(BackendSettings)

#endif  // BACKENDSETTINGS_H
