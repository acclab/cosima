/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef POTENTIALFILE_H
#define POTENTIALFILE_H

#include "api/abstractstreamable.h"
#include "api/typepairs.h"

#include <QDir>
#include <QFile>

/*!
 * \brief A class for handling potential files
 *
 * In PARCAS, atom interactions can be defined by selecting a built-in potential through "potmode" pramater. Many of
 * these potentials require potential files that tell how different element pairs interact. PARCAS potentials may
 * require repulsive potential (reppot) files, or EAM files.
 *
 * This class can be used to check if a potential file follows PARCAS naming conventions, and what are the element pair
 * and file type that it handles. On the other hand, it can also be used to determine the file name from the given
 * element pair and file type. Works very similarly to ElstopFile.
 *
 * The parcas naming convention is reppot.<element1>.<element2>.in (e.g. reppot.Si.O.in) for reppot files, and
 * eam.<element1>.<element2>.in (e.g. eam.Si.O.in) for EAM files.
 */
class PotentialFile : public AbstractStreamable {
  public:
    /*!
     * \brief Type of the potential file
     *
     * PARCAS potentials require different kinds of files.
     */
    enum FileType { None, Reppot, EAM };

    /*!
     * \brief A struct consisting of an element symbol pair and a potential file type
     *
     * PARCAS can only handle one potential file per element pair per potential type. This is why it makes sense to map
     * files based on this struct.
     */
    struct PairAndType {
        ElementPair pair;                     //!< \brief Element symbol pair
        PotentialFile::FileType type = None;  //!< \brief Potential file type

        /*!
         * \brief Constructs an object with default attribute values.
         */
        PairAndType() {}

        /*!
         * \brief Constructs an element with given parameters.
         * \param e Element symbol pair
         * \param t Potential file type
         */
        PairAndType(const ElementPair& e, const PotentialFile::FileType& t) {
            pair = e;
            type = t;
        }

        /*!
         * \brief Equality operator
         * \param a Object being compared to
         * \return \c true if the pair and the type are the same. The order of the pair does not matter.
         */
        bool operator==(const PairAndType& a) const { return (a.pair == pair) && (a.type == type); }
    };


    PotentialFile(const QString& pathToFile = "");

    const QString& path() const;
    bool exists() const;
    bool isValid() const;
    QString name() const;

    QDataStream& save(QDataStream& out) const override;
    QDataStream& load(QDataStream& in) override;

    /*!
     * \brief Element pair and file type
     */
    PairAndType pairAndType;
    const ElementPair& pair() const;
    FileType type() const;

    /*!
     * \brief Equality operator
     * \param f File being compared to
     * \return \c true if the files have identical paths, element pair, and file type. Order of the element pair does
     * not matter.
     */
    bool operator==(const PotentialFile& f) const { return (m_path == f.m_path) && (pairAndType == f.pairAndType); }
    /*!
     * \brief inequality operator
     * \param f File being compared to
     * \return Negation of operator==()
     */
    bool operator!=(const PotentialFile& f) const { return !operator==(f); }

  private:
    QString m_path;
    bool m_isValid;
};

/*!
 * \brief Operator for streaming potential file types into QDataStream
 * \param stream The stream
 * \param type Potential file type
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const PotentialFile::FileType& type) {
    stream << static_cast<int>(type);
    return stream;
}

/*!
 * \brief Operator for streaming potential file types from QDataStream
 * \param stream The stream
 * \param type Potential file type
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, PotentialFile::FileType& type) {
    int t;
    stream >> t;
    type = static_cast<PotentialFile::FileType>(t);
    return stream;
}

/*!
 * \brief qHash Hashing function for PotentialFile#PairAndType objects
 * \param key PotentialFile#PairAndType object
 * \param seed The random seed
 * \return Hashed value
 */
inline uint qHash(const PotentialFile::PairAndType& key, uint seed) {
    return qHash(key.pair, seed) ^ key.type;
}

/*!
 * \brief Operator for streaming PotentialFile#PairAndType objects into QDataStream
 * \param stream The stream
 * \param type PotentialFile#PairAndType objects
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const PotentialFile::PairAndType& a) {
    return stream << a.pair << a.type;
}

/*!
 * \brief Operator for streaming PotentialFile#PairAndType objects from QDataStream
 * \param stream The stream
 * \param type PotentialFile#PairAndType objects
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, PotentialFile::PairAndType& a) {
    return stream >> a.pair >> a.type;
}
#endif  // POTENTIALFILE_H
