/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ENTRY_H
#define ENTRY_H

#include "api/data/datafile.h"
#include "api/data/elstopfiledata.h"
#include "api/data/reppotfiledata.h"
#include "api/entryinfo.h"
#include "api/framereaderthread.h"
#include "api/framereaderwriter.h"
#include "api/typedata.h"
#include "api/typepairs.h"

#include <QObject>

/*!
 * \brief An entry data struct that includes atom types, info, elstop files, and potential files.
 *
 * This is data that can be read from an entry directory excluding the frames directory.
 *
 * \sa Entry
 *
 * \todo It might be more convinient to just store the paths of potential and elstop files. PotentialFile and ElstopFile
 * would then only be used for reading and generating the file names.
 */
struct EntryData {
    QHash<ElementPair, DataFile<ReppotFileData>> potentialFiles;
    QMap<QString, DataFile<ElstopFileData>> elstopFiles;
    TypeData types;
    EntryInfo info;
};

/*!
 * \brief An extended entry data struct that contains one XyzData object on top of the entry data.
 *
 * The idea here is that only one frame of an entry can be loaded at the same time. A new entry can be created just with
 * one frame, and when handling an existing entry, it is enough to only access the latest frame. Thus, only having one
 * frame should be enough.
 *
 * \sa Entry
 */
struct ExtendedEntryData {
    QString path;    //!< \brief Path to the entry
    EntryData data;  //!< \brief %Entry-level data
    XyzData frame;   //!< \brief %Atom data of one frame
};


/*!
 * \brief A class for handling an entry directory.
 *
 * In COSIRMA, entries are the building blocks of a project's history. For more information on COSIRMA's project
 * structure, check \sa Project.
 *
 * Entry object can be connected to an entry directory. The content of the directory is read into the underlying
 * ExtendedEntryData object when the data is requested from the Entry. This makes the Entry loading faster and saves on
 * memory. Also, the Entry can be used to write a new entry directory based on an ExtendedEntryData object.
 */
class Entry : public QObject {
    Q_OBJECT

  public:
    explicit Entry(const QString& path, QObject* parent = nullptr);
    explicit Entry(const QString& path, const ExtendedEntryData& entryData, QObject* parent = nullptr);
    explicit Entry(const QString& path, const ExtendedEntryData& entryData, bool loaded, QObject* parent = nullptr);
    ~Entry();

    std::pair<bool, QString> create(bool writeFrameData = true);
    bool deleteDirectory();
    void free();

    const QString& directoryPath() const;
    QString name() const;

    bool isValid() const;
    bool isEmpty() const;

    const EntryData& data() const;
    const ExtendedEntryData& extendedData() const;
    void setExtendedData(const ExtendedEntryData& data);
    //    ExtendedEntryData* mutableExtendedData();
    const XyzData& frame() const;

    const TypeData& types() const;
    const EntryInfo& info() const;
    const QHash<ElementPair, DataFile<ReppotFileData>>& potentialFiles() const;
    void loadReppotFiles(const QString& path);
    const QMap<QString, DataFile<ElstopFileData>>& elstopFiles() const;
    void loadElstopFiles(const QString& path);

    bool addFrame(const XyzData& newFrame);
    bool addFrame(const QString& fromPath = nullptr, const QString& toPath = nullptr);
    int frameCount() const;
    QString framePath(int i) const;
    void selectFrame(int frameIndex);

  private:
    void readPotentialFiles(const QString& path) const;
    void readElstopFiles(const QString& path) const;
    void readFrames() const;
    bool readInfo() const;
    bool readTypes() const;
    bool loadSelectedFrame() const;

    QString writePotentialFiles();
    QString writeElstopFiles();

    QString nextFramePath() const;

    mutable QVector<QString> framePaths;

    mutable ExtendedEntryData m_data;

    mutable bool m_framesRead = true;
    mutable bool m_potentialsRead = true;
    mutable bool m_elstopFilesRead = true;
    mutable bool m_typesRead = true;
    mutable bool m_infoRead = true;
    mutable bool m_frameLoaded = true;

    mutable int m_selectedFrame = 0;
    mutable int m_prevSelectedFrame = -1;

    // Relative paths
    QString framesPath = "/frames";
    QString typePath = "/types.bin";
    QString potentialsPath = "/potentials";
    QString elstopPath = "/elstop";
    QString infoPath = "/info.txt";

  signals:
    void frameAdded(int frameIndex);
};

#endif  // ENTRY_H
