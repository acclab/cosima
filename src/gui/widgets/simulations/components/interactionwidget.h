/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef INTERACTIONWIDGET_H
#define INTERACTIONWIDGET_H

#include "abstractcomponentwidget.h"

#include "api/data/generators/elstopgenerator.h"
#include "api/potential.h"
#include "api/state/enums.h"

#include "gui/state/enums.h"
#include "gui/widgets/utils/clickablelabel.h"
#include "gui/widgets/utils/typepairwidget.h"

#include <QComboBox>
#include <QMap>
#include <QWidget>


namespace Ui {
class InteractionWidget;
}

class InteractionWidget : public AbstractComponentWidget {
    Q_OBJECT

  public:
    explicit InteractionWidget(QWidget* parent = nullptr);
    ~InteractionWidget();

    void setEntry(ExtendedEntryData* entry) override;

    void setParameters(api::BaseParameters parameters) override;
    api::BaseParameters parameters() override;

  private:
    void updateFieldsBasedOnEntry();
    void createElstopData();

    void updatePotentialInformationText(const Potential& potential);
    QString potentialFileError(const TypeNumberPair& pair, const Potential& selectedPotential,
                               api::enums::interaction::Mode mode);

    bool hasElstopErrors();
    bool hasReppotErrors();

    Ui::InteractionWidget* ui;

    QList<QWidget*> m_gridWidgets;
    QMap<TypeNumberPair, api::enums::interaction::Mode> m_currentInteractions;

    ElstopGenerator m_generator;

    gui::enums::state::Status potentialStatus = gui::enums::state::Status::None;
    gui::enums::state::Status elstopStatus = gui::enums::state::Status::None;

    QMap<api::key_t, gui::enums::state::Status> m_parameterStatus;
    QMap<QLabel*, gui::enums::state::Status> m_potentialStatus;

  public slots:
    void updatePotentialInteractions();

  private slots:
    void applySelectedPotentialToSupportedPairs();

    void updateElstopList();

    void createFiles();
    void changeElstopPlot(const QString& elstopFileName);

    void updatePotentialInteractionStatus();
    void updateElstopInteractionStatus();

    void on_filePB_clicked();

    //  signals:
    //    void elstopFilesCreated(const QString& path);
};

#endif  // INTERACTIONWIDGET_H
