/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef WHEELGUARD_H
#define WHEELGUARD_H

#include <QEvent>
#include <QObject>

/*!
 * \brief Interrupts the wheel event from entering QWidgets.
 *
 * If set as an event filter for a QWidget, wheel events wont't enter the said QWidget.
 */
class WheelGuard : public QObject {

  public:
    explicit WheelGuard(QObject* parent) : QObject(parent) {}

  protected:
    bool eventFilter(QObject* o, QEvent* e) override {
        const QWidget* widget = dynamic_cast<QWidget*>(o);
        if (e->type() == QEvent::Wheel && widget && !widget->hasFocus()) {
            e->ignore();
            return true;
        }

        return QObject::eventFilter(o, e);
    }
};

#endif  // WHEELGUARD_H
