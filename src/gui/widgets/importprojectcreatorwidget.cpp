/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "importprojectcreatorwidget.h"
#include "ui_importprojectcreatorwidget.h"

#include "api/programsettings.h"

#include <QDebug>


ImportProjectCreatorWidget::ImportProjectCreatorWidget(QWidget* parent)
        : AbstractProjectCreatorWidget(parent), ui(new Ui::ImportProjectCreatorWidget) {
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);

    connect(ui->widget_readFile, &FileReadWidget::readyToRead, [this](bool ready) {
        if (ui->stackedWidget->currentWidget() == ui->page_read) emit enableNext(ready);
    });

    connect(ui->widget_types, &TypeConflictSolverWidget::conflictsResolved, [this](bool ready) {
        if (ui->stackedWidget->currentWidget() == ui->page_show) emit enableNext(ready);
    });

    connect(ui->widget_types, &TypeConflictSolverWidget::entryChanged, ui->plotCellWidget, &PlotCell::entryUpdated);

    connect(ui->dimensionSelectorWidget, &DimensionSelector::dimensionsEdited, [this] {
        m_entry.frame.cellLims = ui->dimensionSelectorWidget->getDimensions();
        ui->plotCellWidget->bbUpdated(m_entry.frame.cellLims);
    });

    connect(ui->widget_types, &TypeConflictSolverWidget::tableWritten,
            [this](QWidget* last) { setTabOrder(last, ui->dimensionSelectorWidget); });
}

ImportProjectCreatorWidget::~ImportProjectCreatorWidget() {
    delete ui;
}

bool ImportProjectCreatorWidget::next() {
    bool ret = false;
    if (ui->stackedWidget->currentWidget() == ui->page_read) {
        if (ui->widget_readFile->readEntry()) {
            m_entry = ui->widget_readFile->getEntry();
            updateEntry();
            ui->stackedWidget->setCurrentWidget(ui->page_show);
        }
        ret = true;
    } else if (ui->stackedWidget->currentWidget() == ui->page_show) {
        ret = false;
    }
    emit enableNext(nextEnabled());
    return ret;
}

bool ImportProjectCreatorWidget::previous() {
    if (ui->stackedWidget->currentWidget() == ui->page_read) {
        return false;
    } else if (ui->stackedWidget->currentWidget() == ui->page_show) {
        ui->stackedWidget->setCurrentWidget(ui->page_read);
        return true;
    }
    return false;
}

void ImportProjectCreatorWidget::enter() {
    ui->stackedWidget->setCurrentWidget(ui->page_read);
    ui->widget_readFile->closeFile();
}

const ExtendedEntryData& ImportProjectCreatorWidget::getEntry() {
    // Deleteing extra colors
    QSignalBlocker b(ProgramSettings::getInstance().visualSettings());
    QHash<int, QColor> remain;
    const QHash<int, QColor>& old = ProgramSettings::getInstance().visualSettings().atomTypeColors();
    for (int type : old.keys()) {
        if (type >= -9 && type <= 9) remain[type] = old[type];
    }
    ProgramSettings::getInstance().visualSettings().changeTypeColors(remain);

    // Deleteing extra types
    QHash<int, AtomType> remainingTypes;
    const QHash<int, AtomType>& oldTypes = m_entry.data.types.typeList;
    for (int type : oldTypes.keys()) {
        remainingTypes[std::abs(type)] = oldTypes[type];
    }
    m_entry.data.types.typeList = remainingTypes;

    m_entry.data.info.type = EntryInfo::Modification;
    m_entry.data.info.description = "Initial state";
    m_entry.data.info.timestamp = QDateTime::currentDateTime();
    m_entry.data.info.info = QString("Imported from file %1").arg(ui->widget_readFile->filePath());

    return m_entry;
}


bool ImportProjectCreatorWidget::nextEnabled() {
    if (ui->stackedWidget->currentWidget() == ui->page_read) {
        return ui->widget_readFile->readyforNext();
    } else if (ui->stackedWidget->currentWidget() == ui->page_show) {
        return ui->widget_types->isResolved();
    } else {
        return false;
    }
}

QString ImportProjectCreatorWidget::projectName() {
    return "import";
}

void ImportProjectCreatorWidget::updateEntry() {
    ui->widget_types->changeEntry(&m_entry);
    ui->plotCellWidget->setEntry(m_entry);
    ui->dimensionSelectorWidget->blockSignals(true);
    ui->dimensionSelectorWidget->setDimensions(m_entry.frame.cellLims);
    ui->dimensionSelectorWidget->blockSignals(false);
}
