/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "parcascontrollerutils.h"

#include "api/simulation/parcas/parameters/atomtypesparameterkeys.h"
#include "api/simulation/parcas/parameters/elstopparameterkeys.h"
#include "api/simulation/parcas/parameters/generalparameterkeys.h"
#include "api/simulation/parcas/parameters/interactionparameterkeys.h"
#include "api/simulation/parcas/parameters/outputparameterkeys.h"
#include "api/simulation/parcas/parameters/pressurecontrolparameterkeys.h"
#include "api/simulation/parcas/parameters/recoilparameterkeys.h"
#include "api/simulation/parcas/parameters/simulationcellparameterkeys.h"
#include "api/simulation/parcas/parameters/speedupparameterkeys.h"
#include "api/simulation/parcas/parameters/temperaturecontrolparameterkeys.h"

#include "api/programsettings.h"
#include "api/state/parameters/parameterkeys.h"

#include "api/state/enums.h"

#include <QtMath>


ParcasControllerUtils::ParcasControllerUtils() {}


/**
 * PUBLIC INTERFACE
 */

bool ParcasControllerUtils::writeRelaxFile(const ExtendedEntryData& entry, const QString& path,
                                           const api::BaseParameters& parameters) {

    // Populate the parameters object
    writeGeneralParameters(entry, parameters);
    writeSimulationCellInteractionParameters(entry, parameters);
    writeTemperatureControlParameters(entry, parameters);
    writePressureControlParameters(parameters);
    writeElstopParameters(parameters);
    writeOutputParameters(parameters);

    // Save the parameters object to a text file
    m_params.write(path);

    return true;
}


bool ParcasControllerUtils::writeCascadeFile(const ExtendedEntryData& entry, const QString& path,
                                             const api::BaseParameters& parameters) {

    // Populate the parameters object
    writeGeneralParameters(entry, parameters);
    writeSimulationCellInteractionParameters(entry, parameters);
    writeTemperatureControlParameters(entry, parameters);
    writePressureControlParameters(parameters);
    writeElstopParameters(parameters);
    writeRecoilParameters(parameters);
    writeSpeedupParameters(parameters);
    writeOutputParameters(parameters);

    // Save the parameters object to a text file
    m_params.write(path);

    return true;
}


/**
 * PRIVATE INTERFACE -- Helper functions to avoid code duplication
 */


bool ParcasControllerUtils::writeGeneralParameters(const ExtendedEntryData& entry,
                                                   const api::BaseParameters& parameters) {
    m_params.setParameter(SEED, ParameterValue(2145138759));
    m_params.setParameter(NSTEPS, ParameterValue(999999999));
    {
        m_params.setParameter(TMAX, ParameterValue(parameters[api::parameters::time::TIME].value().toDouble()));
        // Convert fs to Parcas internal units for the maximum time step
        double minMass = std::numeric_limits<double>::max();
        for (const AtomType& type : entry.data.types.allTypes) {
            if (type.mass > 0) minMass = std::min(minMass, type.mass);
        }
        m_params.setParameter(DELTA, ParameterValue(parameters[api::parameters::time::DELTA].value().toDouble() /
                                                    (10.1805 * qSqrt(minMass))));
    }
    m_params.setParameter(NEISKINR, ParameterValue(1.15));
    m_params.setParameter(NPRTBL, ParameterValue(30000));
    m_params.setParameter(NBALANCE, ParameterValue(0));

    return true;
}


bool ParcasControllerUtils::writeTemperatureControlParameters(const ExtendedEntryData& entry,
                                                              const api::BaseParameters& parameters) {

    api::enums::temperaturecontrol::Mode mode = static_cast<api::enums::temperaturecontrol::Mode>(
        parameters[api::parameters::temperature::MODE].value().toInt());
    bool quench = parameters[api::parameters::temperature::QUENCH_ON].value().toBool();
    double damping = parameters[api::parameters::temperature::DAMPING].value().toDouble();
    double quenchRate = parameters[api::parameters::temperature::QUENCH_RATE].value().toDouble();
    bool xCooling = parameters[api::parameters::temperature::X_COOLING_ON].value().toBool();
    bool yCooling = parameters[api::parameters::temperature::Y_COOLING_ON].value().toBool();
    bool zCooling = parameters[api::parameters::temperature::Z_COOLING_ON].value().toBool();
    double xWidth = parameters[api::parameters::temperature::X_COOLING_WIDTH].value().toDouble();
    double yWidth = parameters[api::parameters::temperature::Y_COOLING_WIDTH].value().toDouble();
    double zLower = parameters[api::parameters::temperature::Z_COOLING_LOWER].value().toDouble();
    double zUpper = parameters[api::parameters::temperature::Z_COOLING_UPPER].value().toDouble();
    double quenchStartTime = parameters[api::parameters::temperature::QUENCH_START_TIME].value().toDouble();
    double temperature = parameters[api::parameters::temperature::TARGET_TEMPERATURE].value().toDouble();
    double quenchTemperature = parameters[api::parameters::temperature::QUENCH_TARGET_TEMPERATURE].value().toDouble();

    switch (mode) {
        case api::enums::temperaturecontrol::None:
            m_params.setParameter(MTEMP, ParameterValue(0));
            break;
        case api::enums::temperaturecontrol::AllAtoms:
            if (quench)
                m_params.setParameter(MTEMP, ParameterValue(6));
            else
                m_params.setParameter(MTEMP, ParameterValue(1));
            break;
        case api::enums::temperaturecontrol::Borders:
            if (quench)
                m_params.setParameter(MTEMP, ParameterValue(8));
            else
                m_params.setParameter(MTEMP, ParameterValue(7));
            break;
        default:
            m_params.setParameter(MTEMP, ParameterValue(0));
            break;
    }
    if (quench) {
        m_params.setParameter(TEMP, ParameterValue(quenchTemperature));
        m_params.setParameter(TEMP0, ParameterValue(temperature));
        m_params.setParameter(TRATE, ParameterValue(quenchRate));
        m_params.setParameter(TIMEINI, ParameterValue(quenchStartTime));
    } else {
        m_params.setParameter(TEMP, ParameterValue(temperature));
        m_params.removeParameter(TEMP0);
        m_params.removeParameter(TRATE);
        m_params.removeParameter(TIMEINI);
    }
    if (m_params[LATFLAG].value.toInt() == 1) {
        m_params.setParameter(INITEMP, ParameterValue(temperature));
    }
    m_params.setParameter(BTCTAU, ParameterValue(damping));
    m_params.setParameter(TSCALETH, ParameterValue(0));
    if (xCooling) {
        double value = (entry.frame.cellLims.x2 - entry.frame.cellLims.x1) / 2.0 - xWidth;
        m_params.setParameter(TSCALXOUT, ParameterValue(value));
    } else {
        m_params.removeParameter(TSCALXOUT);
    }
    if (yCooling) {
        double value = (entry.frame.cellLims.y2 - entry.frame.cellLims.y1) / 2.0 - yWidth;
        m_params.setParameter(TSCALYOUT, ParameterValue(value));
    } else {
        m_params.removeParameter(TSCALYOUT);
    }
    if (zCooling) {
        m_params.setParameter(TSCALZMIN, ParameterValue(zLower));
        m_params.setParameter(TSCALZMAX, ParameterValue(zUpper));
    } else {
        m_params.removeParameter(TSCALZMIN);
        m_params.removeParameter(TSCALZMAX);
    }

    return true;
}


bool ParcasControllerUtils::writePressureControlParameters(const api::BaseParameters& parameters) {

    int mode = parameters[api::parameters::pressure::MODE].value().toInt();
    double pressure = parameters[api::parameters::pressure::PRESSURE].value().toDouble();
    double damping = parameters[api::parameters::pressure::DAMPING].value().toDouble();
    double beta = parameters[api::parameters::pressure::BETA].value().toDouble();

    //! \todo check that the mode is set correctly!
    m_params.setParameter(BPCMODE, ParameterValue(mode));
    m_params.setParameter(BPCP0X, ParameterValue(pressure));
    m_params.setParameter(BPCP0Y, ParameterValue(pressure));
    m_params.setParameter(BPCP0Z, ParameterValue(pressure));
    m_params.setParameter(BPCTAU, ParameterValue(damping));
    m_params.setParameter(BPCBETA, ParameterValue(beta));

    return true;
}


bool ParcasControllerUtils::writeSimulationCellInteractionParameters(const ExtendedEntryData& entry,
                                                                     const api::BaseParameters& parameters) {

    int potMode = parameters[api::parameters::interaction::POTENTIAL_MODE].value().toInt();
    QString atomPairInteractions = parameters[api::parameters::interaction::ATOM_PAIR_INTERACTIONS].value().toString();

    QHash<TypeNumberPair, int> interactions;
    QStringList pairwiseInteractions = atomPairInteractions.split(";", QString::SkipEmptyParts);
    for (const QString& pairInteration : pairwiseInteractions) {
        QStringList pair = pairInteration.split(" ", QString::SkipEmptyParts);
        int atomType1 = pair[0].toInt();
        int atomType2 = pair[1].toInt();
        int atomTypeInteraction = pair[2].toInt();
        interactions.insert(TypeNumberPair(atomType1, atomType2), atomTypeInteraction);
    }


    QHash<int, AtomType> allTypes = entry.data.types.allTypes;
    int ntype = 0;
    for (const AtomType& type : allTypes) {
        ntype = std::max(type.typeNumber + 1, ntype);
    }
    ntype = std::max(ntype, 2);

    QList<QVariant> names;
    QList<QVariant> masses;
    for (int i = 0; i < ntype; ++i) {
        names << allTypes[i].symbol;
        masses << allTypes[i].mass;
    }

    QList<QList<QVariant>> iac;
    int current = -1;
    for (int t1 = 0; t1 < ntype; ++t1) {
        for (int t2 = t1; t2 < ntype; ++t2) {
            iac.append(QList<QVariant>());
            current++;

            iac[current].append(t1);
            iac[current].append(t2);
            if (!allTypes.contains(t1) || !allTypes.contains(t2)) {
                iac[current].append(-1);
            } else {
                iac[current].append(interactions[TypeNumberPair(t1, t2)]);
            }
        }
    }

    bool hasPredefinedVelocity = entry.frame.atoms[0].velocity != glm::dvec3(0, 0, 0);
    int latflag = 1;

    bool ionActivated = parameters[api::parameters::ion::ION_TYPE_ATOM_NUMBER].value().toInt() >= 1;
    if (ionActivated) {
        // Cascade simulation
        latflag = 4;
    } else {
        // Relaxation simulation
        if (hasPredefinedVelocity) {
            latflag = 3;
        } else {
            latflag = 1;
        }
    }

    m_params.setParameter(MDLATXYZ, ParameterValue(1));
    m_params.setParameter(LATFLAG, ParameterValue(latflag));
    m_params.setParameter(NATOMS, ParameterValue(entry.frame.atoms.size()));
    m_params.setParameter(BOX_X, ParameterValue(entry.frame.cellLims.x2 - entry.frame.cellLims.x1));
    m_params.setParameter(BOX_Y, ParameterValue(entry.frame.cellLims.y2 - entry.frame.cellLims.y1));
    m_params.setParameter(BOX_Z, ParameterValue(entry.frame.cellLims.z2 - entry.frame.cellLims.z1));
    m_params.setParameter(PB_X, ParameterValue(static_cast<double>(entry.frame.pbc.x)));
    m_params.setParameter(PB_Y, ParameterValue(static_cast<double>(entry.frame.pbc.y)));
    m_params.setParameter(PB_Z, ParameterValue(static_cast<double>(entry.frame.pbc.z)));

    m_params.setParameter(NTYPE, ParameterValue(ntype));
    m_params.setParameter(NAME, ParameterValue(names, true));
    m_params.setParameter(MASS, ParameterValue(masses, true));

    m_params.setParameter(IAC, ParameterValue(iac, true));
    m_params.setParameter(REPPOTCUT, ParameterValue(10.0));
    m_params.setParameter(POTMODE, ParameterValue(potMode));

    return true;
}


bool ParcasControllerUtils::writeElstopParameters(const api::BaseParameters& parameters) {
    QString substrate = parameters[api::parameters::elstop::SUBSTRATE].value().toString();

    m_params.setParameter(SUBSTRATE, ParameterValue(substrate));
    //! \todo add a widget to set these parameters!
    m_params.setParameter(MELSTOP, ParameterValue(5));
    m_params.setParameter(ELSTOPMIN, ParameterValue(10));

    return true;
}


bool ParcasControllerUtils::writeRecoilParameters(const api::BaseParameters& parameters) {
    int ionType = parameters[api::parameters::ion::ION_TYPE].value().toInt();
    double energy = parameters[api::parameters::ion::ENERGY].value().toDouble();

    double x = parameters[api::parameters::ion_transform::X_POSITION].value().toDouble();
    double y = parameters[api::parameters::ion_transform::Y_POSITION].value().toDouble();
    double z = parameters[api::parameters::ion_transform::Z_POSITION].value().toDouble();
    double phi = parameters[api::parameters::ion_transform::PHI].value().toDouble();
    double theta = parameters[api::parameters::ion_transform::THETA].value().toDouble();

    m_params.setParameter(RSMODE, ParameterValue(0));
    //! \todo This could be implemented through the GUI
    m_params.setParameter(IREC, ParameterValue(-2));
    m_params.setParameter(RECATYPE, ParameterValue(ionType));
    m_params.setParameter(XREC, ParameterValue(x));
    m_params.setParameter(YREC, ParameterValue(y));
    m_params.setParameter(ZREC, ParameterValue(z));
    m_params.setParameter(RECEN, ParameterValue(energy));
    m_params.setParameter(RECTHETA, ParameterValue(theta));
    m_params.setParameter(RECPHI, ParameterValue(phi));

    return true;
}


bool ParcasControllerUtils::writeSpeedupParameters(const api::BaseParameters& parameters) {

    double speedup = parameters[api::parameters::speedup::ON].value().toBool();
    double allMoveTime = parameters[api::parameters::speedup::ALL_MOVE_TIME].value().toDouble();
    double forceCriteria = parameters[api::parameters::speedup::FORCE_CRITERIA].value().toDouble();
    double forceCriteriaFactor = parameters[api::parameters::speedup::FORCE_CRITERIA_FACTOR].value().toDouble();
    double energyCriteria = parameters[api::parameters::speedup::ENERGY_CRITERIA].value().toDouble();
    double energyCriteriaFactor = parameters[api::parameters::speedup::ENERGY_CIRTERIA_FACTOR].value().toDouble();
    double overrideCutoffs = parameters[api::parameters::speedup::OVERRIDE_CUTOFFS_ON].value().toBool();
    double hotCutoff = parameters[api::parameters::speedup::HOT_CUTOFF].value().toDouble();
    double coldCutoff = parameters[api::parameters::speedup::COLD_CUTOFF].value().toDouble();
    double fixedCutoff = parameters[api::parameters::speedup::FIXED_CUTOFF].value().toDouble();

    if (speedup) {
        m_params.setParameter(MOVEMODE, ParameterValue(1));
        m_params.setParameter(ALLMOVET, ParameterValue(allMoveTime));
        m_params.setParameter(FCRITA, ParameterValue(forceCriteria));
        m_params.setParameter(FCRITFACT, ParameterValue(forceCriteriaFactor));
        m_params.setParameter(ECRITA, ParameterValue(energyCriteria));
        m_params.setParameter(ECRITFACT, ParameterValue(energyCriteriaFactor));

        QList<QVariant> mcritt;

        QString criteriaTypeMapValue = parameters[api::parameters::speedup::CRITERIA_TYPE_MAP].value().toString();
        QStringList criteriaTypeMap = criteriaTypeMapValue.split(";", QString::SkipEmptyParts);
        for (const QString& typeMapValues : criteriaTypeMap) {
            QStringList typeMap = typeMapValues.split(" ", QString::SkipEmptyParts);
            int keyType = typeMap[0].toInt();
            int valueType = typeMap[1].toInt();
            mcritt << valueType;
        }
        m_params.setParameter(MCRITT, ParameterValue(mcritt, true));

        if (overrideCutoffs) {
            m_params.setParameter(MOVECUTH, ParameterValue(hotCutoff));
            m_params.setParameter(MOVECUTC, ParameterValue(coldCutoff));
            m_params.setParameter(MOVECUTF, ParameterValue(fixedCutoff));
        }
    } else {
        m_params.removeParameter(MOVEMODE);
        m_params.removeParameter(ALLMOVET);
        m_params.removeParameter(FCRITA);
        m_params.removeParameter(FCRITFACT);
        m_params.removeParameter(ECRITA);
        m_params.removeParameter(ECRITFACT);
        m_params.removeParameter(MCRITT);
        m_params.removeParameter(MOVECUTH);
        m_params.removeParameter(MOVECUTC);
        m_params.removeParameter(MOVECUTF);
    }

    return true;
}


bool ParcasControllerUtils::writeOutputParameters(const api::BaseParameters& parameters) {

    int logInterval = parameters[api::parameters::output::LOG_INTERVAL].value().toInt();
    bool movie = parameters[api::parameters::output::MOVIE_ON].value().toBool();
    bool movieUseSteps = parameters[api::parameters::output::MOVIE_STEP_COUNTER_ON].value().toBool();
    int movieStepInterval = parameters[api::parameters::output::MOVIE_STEP_INTERVAL].value().toInt();
    double movieTimeInterval = parameters[api::parameters::output::MOVIE_TIME_INTERVAL].value().toDouble();

    m_params.setParameter(NDUMP, ParameterValue(logInterval));
    m_params.setParameter(NRESTART, ParameterValue(999999999));
    m_params.setParameter(FINALXYZ, ParameterValue("\"out/end.xyz\""));

    if (parameters[api::parameters::speedup::ON].value().toBool()) {
        m_params.setParameter(MOVIEMODE, ParameterValue(20));
    } else {
        m_params.setParameter(MOVIEMODE, ParameterValue(2));
    }
    if (movie) {
        if (movieUseSteps) {
            m_params.setParameter(NMOVIE, ParameterValue(movieStepInterval));
            m_params.removeParameter(DTMOV);
            m_params.removeParameter(TMOV);
        } else {
            m_params.setParameter(NMOVIE, ParameterValue(999999999));
            m_params.setParameter(DTMOV, ParameterValue(QList<QVariant>() << movieTimeInterval));
            m_params.removeParameter(TMOV);
        }
    } else {
        m_params.removeParameter(NMOVIE);
        m_params.removeParameter(DTMOV);
        m_params.removeParameter(TMOV);
    }

    return true;
}
