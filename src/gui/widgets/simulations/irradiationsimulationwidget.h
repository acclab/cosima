/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IRRADIATIONSIMULATIONWIDGET_H
#define IRRADIATIONSIMULATIONWIDGET_H

#include "gui/widgets/simulations/abstractsimulationwidget.h"
#include "gui/widgets/simulations/components/beamwidget.h"
#include "gui/widgets/simulations/components/controlwidget.h"
#include "gui/widgets/simulations/components/generalirradiationwidget.h"
#include "gui/widgets/simulations/components/interactionwidget.h"
#include "gui/widgets/simulations/components/ionwidget.h"

#include <QWidget>


class SpeedupWidget;

namespace Ui {
class IrradiationSimulationWidget;
}

/*!
 * \brief QWidget for modifying the settings of a irradiation simulation
 *
 * Includes QWidgets for modifying the general, beam and cooling settings of an irradiation simulation.
 *
 * \sa GeneralIrradiationSettingsWidget, BeamSettingsWidget, CoolingSettingsWidget
 */
class IrradiationSimulationWidget : public AbstractSimulationWidget {
    Q_OBJECT

  public:
    explicit IrradiationSimulationWidget(QWidget* parent = nullptr);
    ~IrradiationSimulationWidget() override;

    void connectToPlot(PlotCellGLWidget* glWidget) override;
    void disconnectFromPlot(PlotCellGLWidget* glWidget) override;

    void setControllerParametersToWidget(ParcasBaseController* controller) override;
    void setWidgetParametersToController(ParcasBaseController* controller) override;

    //    bool isModified() override;

  private:
    void setComponentEntry(ExtendedEntryData* componentEntry) override;

    enum TabNames { GeneralTab = 0, CascadeTab = 1, RelaxationTab = 2, InteractionsTab = 3, SpeedupTab = 4 };

    Ui::IrradiationSimulationWidget* ui;

    GeneralIrradiationWidget* m_generalIrradiationWidget;
    BeamWidget* m_beamWidget;
    IonWidget* m_ionWidget;
    ControlWidget* m_cascadeControlWidget;
    ControlWidget* m_relaxationControlWidget;
    InteractionWidget* m_interactionWidget;
    SpeedupWidget* m_speedupWidget;

    api::BaseParameters m_unmodifiedCascadeParameters;
    api::BaseParameters m_unmodifiedRelaxParameters;
    api::BaseParameters m_unmodifiedParameters;

  public slots:
    void enableSpeedupModule(bool enabled) override;

  private slots:
    void onChangeTab(int);
    void updateCanvas();
    void setTabStatusIcon(QWidget* tabWidget, const QIcon& icon);

  signals:
    void plottablesChanged();
};

#endif  // IRRADIATIONSIMULATIONWIDGET_H
