/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "testentrychanges.h"
#include "ui_testentrychanges.h"

#include "api/project.h"


TestEntryChanges::TestEntryChanges(QWidget* parent) : QDialog(parent), ui(new Ui::TestEntryChanges) {
    ui->setupUi(this);

    ui->treeView->setHistory(Project::getInstance().history);

    //    connect(ui->treeView, &HistoryTreeView::selectionModified, [this](int entry, int frame) {
    //        //        Project::getInstance().history.setSelectedEntry(entry);
    //        m_entry = Project::getInstance().history.mutableEntry(entry);
    //        qDebug() << "Selected:" << m_entry;
    //    });

    connect(&Project::getInstance().history, &History::entryAdded, [this] {
        qDebug() << "    entryAdded in TestEntryChanges";
        ui->treeView->addEntry(Project::getInstance().history.newestEntry());
    });
}


TestEntryChanges::~TestEntryChanges() {
    delete ui;
}


void TestEntryChanges::on_pushButton_createEntry_clicked() {
    qDebug() << "Creating Entry";

    // Information about the new entry
    EntryInfo info;
    info.type = EntryInfo::Modification;
    info.description = "Test Entry";

    // Make a copy of the current frame (this should be safe as the same data is used to create the runtime directory).
    ExtendedEntryData entryData = Project::getInstance().history.newestEntry()->extendedData();
    entryData.data.info = info;

    try {
        m_entry = Project::getInstance().history.createEntry(entryData, false);
    } catch (HistoryException& e) {
        qDebug() << "Exception:" << e.message;
    }
}


void TestEntryChanges::on_pushButton_addFrame_clicked() {
    qDebug() << "Adding Frame";
    if (m_entry) {
        m_entry->addFrame("/home/fridlund/Development/cosirma-history-private/resources/templates/Si_SiO2_small/history/"
                          "entry_1/frames/frame_0/");
    }
}
