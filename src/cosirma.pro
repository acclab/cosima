#-------------------------------------------------
#
# Project created by QtCreator 2017-06-01T11:25:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += core gui widgets opengl printsupport concurrent svg

# Do not print debug messages to the console when running the release build.
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

TARGET = cosirma
TEMPLATE = app

#QMAKE_CXXFLAGS += -O2

# Copy the executable to the bin directory
#QMAKE_POST_LINK += $$quote(cp -r cosirma $$PWD/../resources/* $$PWD/../bin/)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


# Load the icon file
win32 {
    RC_ICONS = icon.ico
}


SOURCES += \
    api/analysis/cascadelocationpredictor.cpp \
    api/analysis/clusteranalyzer.cpp \
    api/data/generators/elstopgenerator.cpp \
    api/data/generators/reppotgenerator.cpp \
    api/data/generators/zbl96.cpp \
    api/simulation/mdparameters.cpp \
    api/framereaderwriter.cpp \
    api/simulation/irradiationsimulator.cpp \
    api/simulation/parcas/parcasbasecontroller.cpp \
    api/simulation/parcas/parcascascadecontroller.cpp \
    api/simulation/parcas/parcascontrollerutils.cpp \
    api/simulation/parcas/parcasirradiationcontroller.cpp \
    api/simulation/parcas/parcasrelaxationcontroller.cpp \
    api/simulation/relaxationsimulator.cpp \
    api/simulation/simulator.cpp \
    api/simulation/singlecascadesimulator.cpp \
    api/state/parameters/baseparameters.cpp \
    api/state/parameters/ionparameters.cpp \
    api/state/parameters/iontransformparameters.cpp \
    api/state/parameters/outputparameters.cpp \
    api/state/parameters/pressurecontrolparameters.cpp \
    api/state/parameters/temperaturecontrolparameters.cpp \
    api/state/parameters/timeparameters.cpp \
    api/utils/baseparameterutil.cpp \
    api/utils/beamdatautil.cpp \
    api/utils/findinputfiles.cpp \
    gui/dialogs/dialogexportsimulation.cpp \
    gui/dialogs/dialogstartsimulation.cpp \
    gui/dialogs/settingsdialog.cpp \
    gui/rendering/abstractgeometrymodel.cpp \
    gui/rendering/abstractmesh.cpp \
    gui/rendering/arrowmodel.cpp \
    gui/rendering/controlregionmodel.cpp \
    gui/rendering/cubemodel.cpp \
    gui/rendering/cylindermodel.cpp \
    gui/rendering/linemesh.cpp \
    gui/rendering/particlemesh.cpp \
    gui/rendering/particlemodel.cpp \
    gui/rendering/quadmodel.cpp \
    gui/rendering/spheremodel.cpp \
    gui/rendering/trianglemesh.cpp \
    gui/widgets/projectsetup/createscratchwidget.cpp \
    gui/widgets/projectsetup/projectsetupwizard.cpp \
    gui/widgets/simulations/abstractsimulationwidget.cpp \
    gui/widgets/simulations/components/abstractcomponentwidget.cpp \
    gui/widgets/simulations/components/elstopwidget.cpp \
    gui/widgets/simulations/components/iontransformwidget.cpp \
    gui/widgets/simulations/components/speedupwidget.cpp \
    gui/widgets/simulations/singlecascadesimulationwidget.cpp \
    gui/widgets/trackoutputwidget.cpp \
    gui/widgets/typetablesettings.cpp \
    gui/widgets/utils/lineeditdoubledelegate.cpp \
    gui/widgets/utils/lineeditwithicon.cpp \
    main.cpp \
    gui/widgets/simulations/components/temperaturecontrolwidget.cpp \
    gui/dialogs/dialogsetupbackend.cpp \
    gui/widgets/simulations/irradiationsimulationwidget.cpp \
    gui/widgets/ratio.cpp \
    gui/widgets/rangesliderbox.cpp \
    gui/widgets/plotcell.cpp \
    gui/widgets/plotcellglwidget.cpp \
    gui/dialogs/potentialfiledialog.cpp \
    gui/mainwindow.cpp \
    gui/widgets/animatedblock.cpp \
    gui/widgets/dimensionselector.cpp \
    gui/widgets/frameselector.cpp \
    gui/widgets/utils/clickablelabel.cpp \
    gui/widgets/utils/distancefromlimitsb.cpp \
    gui/widgets/utils/graphselector.cpp \
    gui/widgets/utils/interactiveplot.cpp \
    gui/widgets/utils/interactivescatterplot.cpp \
    gui/widgets/utils/jumpslider.cpp \
    gui/widgets/utils/particlegrid.cpp \
    gui/widgets/utils/qxtspanslider/qxtspanslider.cpp \
    gui/widgets/utils/ratiograph.cpp \
    gui/widgets/utils/resizeablegraphicsview.cpp \
    gui/widgets/utils/simulationloopcanvas.cpp \
    api/elementreader.cpp \
    api/framereaderthread.cpp \
    api/potential.cpp \
    api/potentiallist.cpp \
    gui/widgets/utils/collapsiblegroupbox.cpp \
    api/visualsettings.cpp \
    gui/widgets/simulations/relaxationsimulationwidget.cpp \
    api/programsettings.cpp \
    api/fileio.cpp \
    gui/utils/historytreemodel.cpp \
    gui/utils/treeitem.cpp \
    gui/widgets/utils/historytreeview.cpp \
    gui/widgets/boxselectorwidget.cpp \
    gui/widgets/sphereselectorwidget.cpp \
    gui/widgets/cylinderselectorwidget.cpp \
    gui/widgets/utils/typepairwidget.cpp \
    api/binarysettingsio.cpp \
    gui/widgets/templateprojectcreatorwidget.cpp \
    gui/widgets/importprojectcreatorwidget.cpp \
    gui/widgets/filereadwidget.cpp \
    gui/widgets/typeconflictsolverwidget.cpp \
    api/entry.cpp \
    api/entryinfo.cpp \
    api/typedata.cpp \
    api/history.cpp \
    api/util.cpp \
    api/xyzdata.cpp \
    api/potentialfile.cpp \
    api/elstopfile.cpp \
    gui/widgets/simulations/components/pressurecontrolwidget.cpp \
    gui/dialogs/informationpromptdialog.cpp \
    gui/layouts/borderlayout.cpp \
    gui/widgets/simulations/components/beamwidget.cpp \
    gui/widgets/simulations/components/controlwidget.cpp \
    gui/widgets/simulations/components/generalirradiationwidget.cpp \
    gui/widgets/simulations/components/interactionwidget.cpp \
    gui/widgets/simulations/components/ionwidget.cpp \
    gui/widgets/simulations/components/outputwidget.cpp \
    gui/widgets/simulations/components/timewidget.cpp \
    api/simulationlist.cpp \
    api/api.cpp \
    gui/widgets/simulationlistwidget.cpp \
    api/iongenerator.cpp \
    api/project.cpp \
    gui/widgets/atomselectorwidget.cpp \
    gui/widgets/atomselectorarraywidget.cpp \
    gui/widgets/orientationactionwidget.cpp \
    gui/widgets/typeatomselectorwidget.cpp \
    gui/widgets/typetable.cpp \
    ../lib/qcustomplot/qcustomplot.cpp \
    api/atomselections.cpp \
    api/entryeditor.cpp \
    gui/dialogs/chooseelementdialog.cpp \
    gui/dialogs/newprojectdialog.cpp \
    api/abstractstreamable.cpp \
    gui/widgets/entryeditorwidget.cpp \
    tests/gui/testentrychanges.cpp


HEADERS += \
    api/analysis/cascadelocationpredictor.h \
    api/analysis/clusteranalyzer.h \
    api/data/datafilecontent.h \
    api/data/elstopfiledata.h \
    api/data/exportdata.h \
    api/data/generators/elstopgenerator.h \
    api/data/generators/reppotgenerator.h \
    api/data/generators/zbl96.h \
    api/data/iontransformdata.h \
    api/simulation/mdparameterkeys.h \
    api/simulation/mdparameters.h \
    api/simulation/parcas/parameters/atomtypesparameterkeys.h \
    api/simulation/parcas/parameters/elstopparameterkeys.h \
    api/simulation/parcas/parameters/generalparameterkeys.h \
    api/simulation/parcas/parameters/interactionparameterkeys.h \
    api/simulation/parcas/parameters/outputparameterkeys.h \
    api/simulation/parcas/parameters/pressurecontrolparameterkeys.h \
    api/simulation/parcas/parameters/recoilparameterkeys.h \
    api/simulation/parcas/parameters/simulationcellparameterkeys.h \
    api/simulation/parcas/parameters/speedupparameterkeys.h \
    api/simulation/parcas/parameters/temperaturecontrolparameterkeys.h \
    api/data/reppotfiledata.h \
    api/framereaderwriter.h \
    api/simulation/irradiationsimulator.h \
    api/simulation/parcas/parcasbasecontroller.h \
    api/simulation/parcas/parcascascadecontroller.h \
    api/simulation/parcas/parcascontrollerutils.h \
    api/simulation/parcas/parcasirradiationcontroller.h \
    api/simulation/parcas/parcasrelaxationcontroller.h \
    api/simulation/relaxationsimulator.h \
    api/simulation/simulator.h \
    api/simulation/singlecascadesimulator.h \
    api/state/enums.h \
    api/state/parameters/baseparameters.h \
    api/state/parameters/ionparameters.h \
    api/state/parameters/iontransformparameters.h \
    api/state/parameters/outputparameters.h \
    api/state/parameters/parameterkeys.h \
    api/state/parameters/pressurecontrolparameters.h \
    api/state/parameters/temperaturecontrolparameters.h \
    api/state/parameters/timeparameters.h \
    api/data/datafile.h \
    api/utils/baseparameterutil.h \
    api/utils/beamdatautil.h \
    api/utils/findinputfiles.h \
    gui/dialogs/dialogexportsimulation.h \
    gui/dialogs/dialogstartsimulation.h \
    gui/dialogs/settingsdialog.h \
    gui/rendering/abstractgeometrymodel.h \
    gui/rendering/abstractmesh.h \
    gui/rendering/abstractmodel.h \
    gui/rendering/arrowmodel.h \
    gui/rendering/controlregionmodel.h \
    gui/rendering/cubemodel.h \
    gui/rendering/cylindermodel.h \
    gui/rendering/linemesh.h \
    gui/rendering/particlemesh.h \
    gui/rendering/particlemodel.h \
    gui/rendering/quadmodel.h \
    gui/rendering/spheremodel.h \
    gui/rendering/transform.h \
    gui/rendering/trianglemesh.h \
    gui/state/enums.h \
    gui/widgets/projectsetup/createscratchwidget.h \
    gui/widgets/projectsetup/projectsetupwizard.h \
    gui/widgets/simulations/components/abstractcomponentwidget.h \
    gui/widgets/simulations/components/elstopwidget.h \
    gui/widgets/simulations/components/iontransformwidget.h \
    gui/widgets/simulations/components/speedupwidget.h \
    gui/widgets/simulations/components/temperaturecontrolwidget.h \
    gui/dialogs/dialogsetupbackend.h \
    gui/widgets/simulations/irradiationsimulationwidget.h \
    gui/widgets/animatedblock.h \
    gui/widgets/dimensionselector.h \
    gui/dialogs/potentialfiledialog.h \
    gui/widgets/plotcell.h \
    gui/widgets/plotcellglwidget.h \
    gui/mainwindow.h \
    gui/widgets/ratio.h \
    gui/widgets/rangesliderbox.h \
    gui/widgets/frameselector.h \
    gui/widgets/outputmodule.h \
    gui/widgets/simulations/singlecascadesimulationwidget.h \
    gui/widgets/trackoutputwidget.h \
    gui/widgets/typetablesettings.h \
    gui/widgets/utils/distancefromlimitsb.h \
    gui/widgets/utils/clickablelabel.h \
    gui/widgets/utils/graphselector.h \
    gui/widgets/utils/lineeditdoubledelegate.h \
    gui/widgets/utils/lineeditwithicon.h \
    gui/widgets/utils/simulationloopcanvas.h \
    gui/widgets/utils/resizeablegraphicsview.h \
    gui/widgets/utils/ratiograph.h \
    gui/widgets/utils/qxtspanslider/qxtspanslider.h \
    gui/widgets/utils/qxtspanslider/qxtspanslider_p.h \
    gui/widgets/utils/jumpslider.h \
    gui/widgets/utils/interactivescatterplot.h \
    gui/widgets/utils/interactiveplot.h \
    gui/widgets/utils/wheelguard.h \
    api/typepairs.h \
    api/potentiallist.h \
    api/potential.h \
    api/elementreader.h \
    api/framereaderthread.h \
    api/geometries.h \
    api/MDpt.h \
    gui/widgets/utils/particlegrid.h \
    gui/widgets/utils/collapsiblegroupbox.h \
    api/visualsettings.h \
    gui/widgets/simulations/relaxationsimulationwidget.h \
    api/programsettings.h \
    api/fileio.h \
    gui/utils/historytreemodel.h \
    gui/utils/treeitem.h \
    gui/widgets/utils/historytreeview.h \
    gui/widgets/abstractatomselectorwidget.h \
    gui/widgets/boxselectorwidget.h \
    gui/widgets/sphereselectorwidget.h \
    gui/widgets/cylinderselectorwidget.h \
    gui/widgets/simulations/abstractsimulationwidget.h \
    gui/widgets/utils/typepairwidget.h \
    api/binarysettingsio.h \
    gui/widgets/abstractprojectcreatorwidget.h \
    gui/widgets/templateprojectcreatorwidget.h \
    gui/widgets/importprojectcreatorwidget.h \
    gui/widgets/filereadwidget.h \
    gui/widgets/typeconflictsolverwidget.h \
    api/potentialfile.h \
    api/entry.h \
    api/entryinfo.h \
    api/typedata.h \
    api/history.h \
    api/util.h \
    api/xyzdata.h \
    api/elstopfile.h \
    api/backendsettings.h \
    api/locationsettings.h \
    gui/widgets/simulations/components/pressurecontrolwidget.h \
    gui/dialogs/informationpromptdialog.h \
    gui/layouts/borderlayout.h \
    api/data/beamdata.h \
    gui/widgets/simulations/components/beamwidget.h \
    gui/widgets/simulations/components/controlwidget.h \
    gui/widgets/simulations/components/generalirradiationwidget.h \
    gui/widgets/simulations/components/interactionwidget.h \
    gui/widgets/simulations/components/ionwidget.h \
    gui/widgets/simulations/components/outputwidget.h \
    gui/widgets/simulations/components/timewidget.h \
    api/simulationlist.h \
    api/api.h \
    gui/widgets/simulationlistwidget.h \
    api/iongenerator.h \
    api/project.h \
    gui/widgets/atomselectorwidget.h \
    gui/widgets/atomselectorarraywidget.h \
    gui/widgets/allatomselectorwidget.h \
    gui/widgets/orientationactionwidget.h \
    gui/widgets/actionwidgets.h \
    gui/widgets/typeatomselectorwidget.h \
    gui/widgets/typetable.h \
    ../lib/qcustomplot/qcustomplot.h \
    api/glmoperations.h \
    api/atom.h \
    api/atomselections.h \
    api/atomtype.h \
    api/entryeditor.h \
    gui/dialogs/chooseelementdialog.h \
    gui/dialogs/newprojectdialog.h \
    api/abstractstreamable.h \
    gui/widgets/entryeditorwidget.h \
    tests/gui/testentrychanges.h

FORMS += \
    gui/dialogs/dialogexportsimulation.ui \
    gui/dialogs/dialogstartsimulation.ui \
    gui/dialogs/settingsdialog.ui \
    gui/widgets/projectsetup/createscratchwidget.ui \
    gui/widgets/simulations/components/elstopwidget.ui \
    gui/widgets/simulations/components/iontransformwidget.ui \
    gui/widgets/simulations/components/speedupwidget.ui \
    gui/widgets/simulations/components/temperaturecontrolwidget.ui \
    gui/dialogs/dialogsetupbackend.ui \
    gui/widgets/simulations/irradiationsimulationwidget.ui \
    gui/widgets/ratio.ui \
    gui/dialogs/potentialfiledialog.ui \
    gui/widgets/plotcell.ui \
    gui/widgets/graph.ui \
    gui/widgets/frameselector.ui \
    gui/mainwindow.ui \
    gui/widgets/simulations/relaxationsimulationwidget.ui \
    gui/widgets/simulations/singlecascadesimulationwidget.ui \
    gui/widgets/sphereselectorwidget.ui \
    gui/widgets/boxselectorwidget.ui \
    gui/widgets/cylinderselectorwidget.ui \
    gui/widgets/templateprojectcreatorwidget.ui \
    gui/widgets/importprojectcreatorwidget.ui \
    gui/widgets/filereadwidget.ui \
    gui/widgets/trackoutputwidget.ui \
    gui/widgets/typeconflictsolverwidget.ui \
    gui/widgets/simulations/components/pressurecontrolwidget.ui \
    gui/dialogs/informationpromptdialog.ui \
    gui/widgets/simulations/components/beamwidget.ui \
    gui/widgets/simulations/components/generalirradiationwidget.ui \
    gui/widgets/simulations/components/interactionwidget.ui \
    gui/widgets/simulations/components/outputwidget.ui \
    gui/widgets/simulations/components/timewidget.ui \
    gui/widgets/simulationlistwidget.ui \
    gui/widgets/atomselectorwidget.ui \
    gui/widgets/atomselectorarraywidget.ui \
    gui/dialogs/chooseelementdialog.ui \
    gui/dialogs/newprojectdialog.ui \
    gui/widgets/entryeditorwidget.ui \
    tests/gui/testentrychanges.ui

RESOURCES += \
    resources/icons.qrc \
    resources/textures.qrc \
    resources/shaders.qrc \
    resources/logos.qrc \
    resources/files.qrc \
    resources/scripts.qrc \
    resources/tools.qrc \
    resources/zbl96.qrc

INCLUDEPATH += \
    ../lib/glm \
    ../lib/qcustomplot \
    ../lib
