/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "programsettings.h"

#include <QApplication>
#include <QDir>
#include <QSettings>

/*!
 * \brief Access the instance of this class.
 *
 * The instance is created if needed.
 */
ProgramSettings& ProgramSettings::getInstance() {
    static ProgramSettings instance;
    return instance;
}


/*!
 * \brief The size and location of the application window
 * \return The window geometry
 */
QByteArray& ProgramSettings::windowGeometry() {
    return m_windowGeometry;
}

/*!
 * \brief Changes the window geometry of the application window
 * \param The new window geometry
 */
void ProgramSettings::setWindowGeometry(const QByteArray& geomtery) {
    m_windowGeometry = geomtery;
}

/*!
 * \brief The global back end settings
 * \return The back end settings
 */
BackendSettings& ProgramSettings::backendSettings() {
    return m_backendSettings;
}

/*!
 * \brief Changes the back end settings
 * \param The new back
 */
void ProgramSettings::setBackendSettings(const BackendSettings& settings) {
    m_backendSettings = settings;
}


/*!
 * \brief The locations of the direcotries related to COSIRMA
 * \return The location settings
 */
LocationSettings& ProgramSettings::locationSettings() {
    return m_locationSettings;
}

/*!
 * \brief Changes the location settings
 * \param The new location settings
 */
void ProgramSettings::setLocationSettings(const LocationSettings& settings) {
    m_locationSettings = settings;
}


/*!
 * \brief The global visual settings for the plots
 * \return The visual settings
 */
VisualSettings& ProgramSettings::visualSettings() {
    return m_visualSettings;
}

/*!
 * \brief Changes the global visual settings
 *
 * Will send corresponding signals for the visual settings components that changed.
 *
 * \param The new visual settings
 */
void ProgramSettings::setVisualSettings(const VisualSettings& settings) {
    m_visualSettings = settings;
}


/*!
 * \brief Loads the settings from the application settings file.
 *
 * If the settings are not found, tries to locate executables for PARCAS and frame-processor, and sets other settings to
 * default values.
 */
void ProgramSettings::load() {
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    m_windowGeometry = settings.value("geometry", QByteArray()).toByteArray();

    settings.beginGroup("backend");
    QVariant backendSettings = settings.value("backend_settings");
    settings.endGroup();
    if (backendSettings.isValid() && !backendSettings.isNull()) {
        m_backendSettings = backendSettings.value<BackendSettings>();
    } else {
        m_backendSettings = BackendSettings::defaultSettings();
    }

    settings.beginGroup("visual");
    QVariant v = settings.value("visual_settings");
    settings.endGroup();
    if (v.isValid() && !v.isNull()) {
        m_visualSettings = v.value<VisualSettings>();
    } else {
        m_visualSettings = VisualSettings::defaultSettings();
    }

    settings.beginGroup("locations");
    QVariant l = settings.value("location_settings");
    settings.endGroup();
    if (l.isValid() && !l.isNull()) {
        m_locationSettings = l.value<LocationSettings>();
    } else {
        m_locationSettings = LocationSettings::defaultSettings();
    }
}

/*!
 * \brief Saves the settings to the application settings file.
 *
 * Writes the settings so that they can be read with load() when the application is run later
 */
void ProgramSettings::save() {
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings.setValue("geometry", m_windowGeometry);

    settings.beginGroup("backend");
    settings.setValue("backend_settings", QVariant::fromValue(m_backendSettings));
    settings.endGroup();

    settings.beginGroup("visual");
    settings.setValue("visual_settings", QVariant::fromValue(m_visualSettings));
    settings.endGroup();

    settings.beginGroup("locations");
    settings.setValue("location_settings", QVariant::fromValue(m_locationSettings));
    settings.endGroup();
}
