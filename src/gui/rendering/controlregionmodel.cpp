/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "controlregionmodel.h"

#include "api/geometries.h"


ControlRegionModel::ControlRegionModel(QObject* parent) : AbstractModel(parent) {
    qDebug() << this << "::ControlRegionModel()" << this;

    m_show = true;

    QColor color(0, 0, 255, 100);

    // Create all quads, 0-11 are reserved for the 2 blocks on the X-axis, 12-23 are reserved for the 2 blocks on the
    // Y-axis, and 24-29 are reserved for the block on the Z-axis.
    for (std::size_t i = 0; i < 30; i++) {
        quads.push_back(new QuadModel(color, color.rgb()));
        quads[i]->setShowFaces(true);
        quads[i]->setShowLines(true);
    }
}


ControlRegionModel::~ControlRegionModel() {
    for (auto quad : quads) {
        delete quad;
    }
    quads.clear();
}


void ControlRegionModel::draw(const glm::mat4& model, QOpenGLShaderProgram& program) {
    if (!m_show) return;

    for (auto quad : quads) {
        quad->draw(model * modelMatrix(), program);
    }
}


/*!
 * \brief Helper function to update the quads making up the edges of the control regions.
 */
void ControlRegionModel::updateQuad(std::size_t quadId, double x, double y, double z, glm::vec3 rotation, double sizeX,
                                    double sizeY) {
    if (quads.empty()) {
        return;
    }
    quads[quadId]->setTransform(glm::vec3(x, y, z), rotation, glm::vec2(sizeX, sizeY));
}


/*!
 * \brief Master show switch. When this is set to "false" everything will be hidden.
 * \param show
 */
void ControlRegionModel::onShow(bool show) {
    m_show = show;
}


/*!
 * \brief Update the dimensions and loctions of all quads based on these inputs.
 * \param limits
 * \param xWidth
 * \param yWidth
 * \param zLower
 * \param zUpper
 */
void ControlRegionModel::onUpdateValues(const Box& limits, double xWidth, double yWidth, double zLower, double zUpper) {
    std::size_t quadId = 0;

    // Blocks on the X-axis (offset X to avoid depth fighting at overlapping edges)
    double offset = 1e-2;

    updateQuad(quadId++, limits.x1 - offset, 0, 0, ROT_X1, limits.dz(), limits.dy());
    updateQuad(quadId++, limits.x1 + xWidth, 0, 0, ROT_X2, limits.dz(), limits.dy());
    updateQuad(quadId++, limits.x1 + xWidth / 2, 0, limits.z1 - offset, ROT_Z1, xWidth, limits.dy());
    updateQuad(quadId++, limits.x1 + xWidth / 2, 0, limits.z2 + offset, ROT_Z2, xWidth, limits.dy());
    updateQuad(quadId++, limits.x1 + xWidth / 2, limits.y1 - offset, 0, ROT_Y1, xWidth, limits.dz());
    updateQuad(quadId++, limits.x1 + xWidth / 2, limits.y2 + offset, 0, ROT_Y2, xWidth, limits.dz());

    updateQuad(quadId++, limits.x2 + offset, 0, 0, ROT_X2, limits.dz(), limits.dy());
    updateQuad(quadId++, limits.x2 - xWidth, 0, 0, ROT_X1, limits.dz(), limits.dy());
    updateQuad(quadId++, limits.x2 - xWidth / 2, 0, limits.z1 - offset, ROT_Z1, xWidth, limits.dy());
    updateQuad(quadId++, limits.x2 - xWidth / 2, 0, limits.z2 + offset, ROT_Z2, xWidth, limits.dy());
    updateQuad(quadId++, limits.x2 - xWidth / 2, limits.y1 - offset, 0, ROT_Y1, xWidth, limits.dz());
    updateQuad(quadId++, limits.x2 - xWidth / 2, limits.y2 + offset, 0, ROT_Y2, xWidth, limits.dz());


    // Blocks on the Y-axis (offset Y a little bit more than X to avoid overlapping at the corners)
    offset = 2e-2;

    updateQuad(quadId++, 0, limits.y1 - offset, 0, ROT_Y1, limits.dx(), limits.dz());
    updateQuad(quadId++, 0, limits.y1 + yWidth, 0, ROT_Y2, limits.dx(), limits.dz());
    updateQuad(quadId++, 0, limits.y1 + yWidth / 2, limits.z1 - offset, ROT_Z1, limits.dx(), yWidth);
    updateQuad(quadId++, 0, limits.y1 + yWidth / 2, limits.z2 + offset, ROT_Z2, limits.dx(), yWidth);
    updateQuad(quadId++, limits.x1 - offset, limits.y1 + yWidth / 2, 0, ROT_X1, limits.dz(), yWidth);
    updateQuad(quadId++, limits.x2 + offset, limits.y1 + yWidth / 2, 0, ROT_X2, limits.dz(), yWidth);

    updateQuad(quadId++, 0, limits.y2 + offset, 0, ROT_Y2, limits.dx(), limits.dz());
    updateQuad(quadId++, 0, limits.y2 - yWidth, 0, ROT_Y1, limits.dx(), limits.dz());
    updateQuad(quadId++, 0, limits.y2 - yWidth / 2, limits.z1 - offset, ROT_Z1, limits.dx(), yWidth);
    updateQuad(quadId++, 0, limits.y2 - yWidth / 2, limits.z2 + offset, ROT_Z2, limits.dx(), yWidth);
    updateQuad(quadId++, limits.x1 - offset, limits.y2 - yWidth / 2, 0, ROT_X1, limits.dz(), yWidth);
    updateQuad(quadId++, limits.x2 + offset, limits.y2 - yWidth / 2, 0, ROT_X2, limits.dz(), yWidth);

    // Block on the Z-axis (don't offset Z meshes as X and Y already are offset)
    double sz = zUpper - zLower;
    updateQuad(quadId++, 0, 0, zLower, ROT_Z1, limits.dx(), limits.dy());
    updateQuad(quadId++, 0, 0, zUpper, ROT_Z2, limits.dx(), limits.dy());
    updateQuad(quadId++, limits.x1, 0, zLower + sz / 2, ROT_X1, sz, limits.dy());
    updateQuad(quadId++, limits.x2, 0, zLower + sz / 2, ROT_X2, sz, limits.dy());
    updateQuad(quadId++, 0, limits.y1, zLower + sz / 2, ROT_Y1, limits.dx(), sz);
    updateQuad(quadId++, 0, limits.y2, zLower + sz / 2, ROT_Y2, limits.dx(), sz);
}


void ControlRegionModel::onUpdateEnabledRegions(bool x, bool y, bool z) {
    // X-direction
    for (std::size_t i = 0; i < 12; i++) {
        quads[i]->setShowFaces(x);
        quads[i]->setShowLines(x);
    }

    // Y-direction
    for (std::size_t i = 12; i < 24; i++) {
        quads[i]->setShowFaces(y);
        quads[i]->setShowLines(y);
    }

    // Z-direction
    for (std::size_t i = 24; i < 30; i++) {
        quads[i]->setShowFaces(z);
        quads[i]->setShowLines(z);
    }
}
