/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEMPERATURECONTROLWIDGET_H
#define TEMPERATURECONTROLWIDGET_H

#include "abstractcomponentwidget.h"

#include "gui/rendering/controlregionmodel.h"
#include "gui/widgets/plotcellglwidget.h"

#include <QCheckBox>
#include <QPointer>
#include <QWidget>


namespace Ui {
class TemperatureControlWidget;
}

/*!
 * \brief The TemperatureControlWidget class
 *
 * Modify the parameters related to the temperature control model. The class has features for updating the graphical
 * represnation of the atom structure.
 *
 * \todo The system already has a temperature. Implement functionality for showing the current temperature.
 * Additionally, an option for setting the system to some temperature at the start of a simulation should be added.
 */
class TemperatureControlWidget : public AbstractComponentWidget {
    Q_OBJECT

  public:
    explicit TemperatureControlWidget(QWidget* parent = nullptr);
    ~TemperatureControlWidget();

    void setTime(double time);

    void connectToPlot(PlotCellGLWidget* glWidget);
    void disconnectFromPlot(PlotCellGLWidget* glWidget);

    void setEntry(ExtendedEntryData* entry) override;

    void setParameters(api::BaseParameters parameters) override;
    api::BaseParameters parameters() override;

  private:
    Ui::TemperatureControlWidget* ui;

    PlotCellGLWidget* m_glWidget = nullptr;
    QPointer<ControlRegionModel> m_region;

    void setMode(int mode);
    int mode();

    void showNone();
    void showLinear();
    void showBorder();

  private slots:
    void onChangeCoolingRegions();

  public slots:
    void showControlRegion(bool show);
};

#endif  // TEMPERATURECONTROLWIDGET_H
