/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "newprojectdialog.h"
#include "ui_newprojectdialog.h"

#include "api/fileio.h"
#include "api/programsettings.h"
#include "api/util.h"
#include "gui/widgets/importprojectcreatorwidget.h"
#include "gui/widgets/templateprojectcreatorwidget.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>

/*!
 * \brief Constructs a dialog that is ready to be shown.
 * \param parent The parent widget
 */
NewProjectDialog::NewProjectDialog(QWidget* parent) : QDialog(parent), ui(new Ui::NewProjectDialog) {
    ui->setupUi(this);

    //=========================================================================
    // Navigation buttons

    m_pushButton_prev = new QPushButton(tr("Previous"));
    m_pushButton_prev->setAutoDefault(false);

    m_pushButton_next = new QPushButton(tr("Next"));
    m_pushButton_next->setAutoDefault(false);

    ui->buttonBox_navigate->addButton(m_pushButton_prev, QDialogButtonBox::ActionRole);
    ui->buttonBox_navigate->addButton(m_pushButton_next, QDialogButtonBox::ActionRole);

    connect(m_pushButton_next, &QPushButton::clicked, this, &NewProjectDialog::onNextButtonClicked);
    connect(m_pushButton_prev, &QPushButton::clicked, this, &NewProjectDialog::onPrevButtonClicked);

    //=========================================================================
    // Confirmation buttons

    m_pushButton_cancel = new QPushButton(tr("Cancel"));
    m_pushButton_cancel->setAutoDefault(false);

    m_pushButton_create = new QPushButton(tr("Create"));
    m_pushButton_create->setAutoDefault(false);

    ui->buttonBox_confirm->addButton(m_pushButton_cancel, QDialogButtonBox::RejectRole);
    ui->buttonBox_confirm->addButton(m_pushButton_create, QDialogButtonBox::ActionRole);
    connect(m_pushButton_create, &QPushButton::clicked, this, &NewProjectDialog::onCreateButtonClicked);

    //=========================================================================
    // Page: Choose Wizard

    connect(ui->radioButton_template, &QRadioButton::toggled, [this] { m_pushButton_next->setEnabled(true); });
    connect(ui->radioButton_import, &QRadioButton::toggled, [this] { m_pushButton_next->setEnabled(true); });
    connect(ui->radioButton_scratch, &QRadioButton::toggled, [this] { m_pushButton_next->setEnabled(true); });


    //=========================================================================
    // Page: Project Location

    ui->lineEdit_saveLocation->setText(ProgramSettings::getInstance().locationSettings().projectDirPath);

    connect(ui->lineEdit_projectName, &QLineEdit::textChanged, this, &NewProjectDialog::checkIfReadyToCreate);
    connect(ui->lineEdit_saveLocation, &QLineEdit::textChanged, this, &NewProjectDialog::checkIfReadyToCreate);

    // Show the first page by default
    ui->stackedWidget->setCurrentWidget(ui->page_chooseWizard);

    // Disable UI elements by default
    m_pushButton_next->setEnabled(false);
    m_pushButton_prev->setEnabled(false);
    m_pushButton_create->setEnabled(false);
}

NewProjectDialog::~NewProjectDialog() {
    delete ui;
}

/*!
 * \brief Path to the created project directory
 *
 * After successfully executing the dialog, the path of the created directory can be read through this function.
 *
 * \return The project directory path
 */
const QString& NewProjectDialog::getPath() {
    return m_projectPath;
}

/*!
 * Changes the shown page to the previous one. If the current page is the one showing the creator widget, tries to
 * change the page inside the creator widget.
 */
void NewProjectDialog::onPrevButtonClicked() {

    // Currently in a project creator
    if (ui->stackedWidget->currentWidget() == ui->page_projectCreator) {
        if (creator && !creator->previous()) {
            ui->stackedWidget->setCurrentWidget(ui->page_chooseWizard);
            m_pushButton_prev->setEnabled(false);
            m_pushButton_next->setEnabled(true);
        } else {
            m_pushButton_next->setEnabled(creator->nextEnabled());
        }
    }

    // Currently in the last tab
    else if (ui->stackedWidget->currentWidget() == ui->page_projectLocation) {
        if (creator) {
            ui->stackedWidget->setCurrentWidget(ui->page_projectCreator);
            m_pushButton_create->setEnabled(false);
            m_pushButton_next->setEnabled(creator->nextEnabled());
        }
    }
}

/*!
 * Changes the shown page to the next one. If the creator widget page is entered, and the selected creator type has
 * changed, creates a new creator widget instance. If the current page is the one showing the creator widget, tries to
 * change the page inside the creator widget.
 */
void NewProjectDialog::onNextButtonClicked() {
    // Currently in the first tab
    if (ui->stackedWidget->currentWidget() == ui->page_chooseWizard) {
        bool newCreator = false;
        if (ui->radioButton_template->isChecked()) {
            TemplateProjectCreatorWidget* oldCreator = dynamic_cast<TemplateProjectCreatorWidget*>(creator);
            if (!oldCreator) {
                delete creator;
                newCreator = true;
                creator = new TemplateProjectCreatorWidget(this);
            }
        } else if (ui->radioButton_import->isChecked()) {
            ImportProjectCreatorWidget* oldCreator = dynamic_cast<ImportProjectCreatorWidget*>(creator);
            if (!oldCreator) {
                delete creator;
                newCreator = true;
                creator = new ImportProjectCreatorWidget(this);
            }
        }

        if (newCreator) {
            creator->enter();
            if (!ui->page_projectCreator->layout()) ui->page_projectCreator->setLayout(new QHBoxLayout());
            ui->page_projectCreator->layout()->addWidget(creator);
            connect(creator, &AbstractProjectCreatorWidget::enableNext, m_pushButton_next, &QPushButton::setEnabled);
        }
        if (creator) {
            m_pushButton_next->setEnabled(creator->nextEnabled());
            m_pushButton_prev->setEnabled(true);
            ui->stackedWidget->setCurrentWidget(ui->page_projectCreator);
        }
    }

    // Currently in a project creator
    else if (ui->stackedWidget->currentWidget() == ui->page_projectCreator) {
        if (creator) {
            if (!creator->next()) {
                ui->stackedWidget->setCurrentWidget(ui->page_projectLocation);
                updateProjectName(creator->projectName());
                m_pushButton_next->setEnabled(false);
                checkIfReadyToCreate();
            }
        }
    }
}

/*!
 * \brief Writes the new project directory.
 *
 * Gets the entry data from the creator widget and writes it to the selected directory.
 */
void NewProjectDialog::onCreateButtonClicked() {
    bool suc = false;
    QString message;
    QString dest = ui->lineEdit_saveLocation->text() + "/" + ui->lineEdit_projectName->text();

    QDir destDir(dest);

    if (destDir.exists()) {
        message = "The target directory already exists.";
    } else if (!destDir.mkpath(destDir.path())) {
        message = "Creation of the project directory failed.";
    } else if (!creator) {
        message = "Project creation failed.";
    } else {
        QFile projectLabel(destDir.path() + "/.cosirmaproject");
        if (!projectLabel.open(QIODevice::WriteOnly)) qDebug() << "Failed to create project identifier file.";

        ExtendedEntryData tempEntry = creator->getEntry();
        tempEntry.data.info.timestamp = QDateTime::currentDateTime();
        std::pair<bool, QString> writing = Entry(destDir.path() + "/history/entry_1", tempEntry).create();
        suc = writing.first;
        if (!writing.second.isEmpty()) qDebug() << writing.second;
    }

    if (suc) {
        m_projectPath = dest;
        accept();
    } else {
        destDir.removeRecursively();
        QMessageBox::critical(this, "Creation failed!", message, QMessageBox::Ok);
    }
}


void NewProjectDialog::on_toolButton_changeLocation_clicked() {
    QString path =
        QFileDialog::getExistingDirectory(this, tr("Choose save location"), ui->lineEdit_saveLocation->text());
    if (!path.isEmpty()) {
        ui->lineEdit_saveLocation->setText(path);
    }
}

/*!
 * \brief Updates the shown project name
 *
 * If the selected project folder already has a project with the given name, a number is added to the name.
 *
 * \param name  The root of the name, a number might get appended to it
 */
void NewProjectDialog::updateProjectName(const QString& name) {
    QString actualName = name;
    QDir dir(ui->lineEdit_saveLocation->text());
    if (dir.exists()) {
        QSet<QString> projects = dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).toSet();

        int i = 1;
        QString tmpName = name;
        while (projects.contains(tmpName)) {
            tmpName = name + "_" + QString::number(i);
            i++;
        }
        actualName = tmpName;
    }
    ui->lineEdit_projectName->setText(actualName);
}

void NewProjectDialog::checkIfReadyToCreate() {
    bool ready = true;
    if (ui->stackedWidget->currentWidget() != ui->page_projectLocation) ready = false;

    QDir dest(ui->lineEdit_saveLocation->text() + "/" + ui->lineEdit_projectName->text());
    if (dest.exists()) ready = false;

    m_pushButton_create->setEnabled(ready);
}
