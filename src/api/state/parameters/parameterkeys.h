/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef APIPARAMETERKEYS_H
#define APIPARAMETERKEYS_H

#include <QString>


namespace api {

typedef QString key_t;

namespace parameters {

namespace time {
const key_t TIME = "parameters::time::time";
const key_t DELTA = "parameters::time::delta";
}  // namespace time

namespace temperature {
const key_t MODE = "parameters::temperature::control_mode";
const key_t X_COOLING_ON = "parameters::temperature::x_cooling_on";
const key_t Y_COOLING_ON = "parameters::temperature::y_cooling_on";
const key_t Z_COOLING_ON = "parameters::temperature::z_cooling_on";
const key_t X_COOLING_WIDTH = "parameters::temperature::x_cooling_width";
const key_t Y_COOLING_WIDTH = "parameters::temperature::y_cooling_width";
const key_t Z_COOLING_UPPER = "parameters::temperature::z_cooling_upper";
const key_t Z_COOLING_LOWER = "parameters::temperature::z_cooling_lower";
const key_t QUENCH_ON = "parameters::temperature::quench_on";
const key_t QUENCH_START_TIME = "parameters::temperature::quench_start_time";
const key_t QUENCH_TARGET_TEMPERATURE = "parameters::temperature::quench_target_temperature";
const key_t QUENCH_RATE = "parameters::temperature::quench_rate";
const key_t TARGET_TEMPERATURE = "parameters::temperature::target_temperature";
const key_t DAMPING = "parameters::temperature::damping";
}  // namespace temperature

namespace pressure {
const key_t MODE = "parameters::pressure::mode";
const key_t PRESSURE = "parameters::pressure::pressure";
const key_t DAMPING = "parameters::pressure::damping";
const key_t BETA = "parameters::pressure::beta";
}  // namespace pressure

namespace ion {
const key_t BASE_TYPE = "parameters::ion::base_type";
const key_t BASE_TYPE_ATOM_NUMBER = "parameters::ion::base_type_atom_number";
const key_t ION_TYPE = "parameters::ion::ion_type";
const key_t ION_TYPE_ATOM_NUMBER = "parameters::ion::ion_type_atom_number";
const key_t ENERGY = "parameters::ion::energy";
const key_t BASE_TYPE_MERGE_ON = "parameters::ion::base_type_merge_on";
}  // namespace ion

namespace ion_transform {
const key_t X_POSITION = "parameters::ion_transform::x_position";
const key_t Y_POSITION = "parameters::ion_transform::y_position";
const key_t Z_POSITION = "parameters::ion_transform::z_position";
const key_t PHI = "parameters::ion_transform::phi";
const key_t THETA = "parameters::ion_transform::theta";
}  // namespace ion_transform

namespace interaction {
// const key_t INTERACTION_MODE = "parameters::interaction::interaction_mode";
const key_t POTENTIAL_MODE = "parameters::interaction::potential_mode";
const key_t ATOM_PAIR_INTERACTIONS = "parameters::interaction::atom_pair_interactions";
}  // namespace interaction

namespace elstop {
const key_t SUBSTRATE = "parameters::elstop::substrate";
const key_t SUBSTRATE_DENSITY = "parameters::elstop::substrate_density";
}  // namespace elstop

namespace speedup {
const key_t ON = "parameters::speedup::on";
const key_t ALL_MOVE_TIME = "parameters::speedup::all_move_time";
const key_t FORCE_CRITERIA = "parameters::speedup::force_criteria";
const key_t FORCE_CRITERIA_FACTOR = "parameters::speedup::force_criteria_factor";
const key_t ENERGY_CRITERIA = "parameters::speedup::energy_criteria";
const key_t ENERGY_CIRTERIA_FACTOR = "parameters::speedup::energy_criteria_factor";
const key_t CRITERIA_TYPE_MAP = "parameters::speedup::criteria_type_map";  // THIS IS A MAP!!!
const key_t OVERRIDE_CUTOFFS_ON = "parameters::speedup::override_cutoffs";
const key_t HOT_CUTOFF = "parameters::speedup::hot_cutoff";
const key_t COLD_CUTOFF = "parameters::speedup::cold_cutoff";
const key_t FIXED_CUTOFF = "parameters::speedup::fixed_cutoff";
}  // namespace speedup

namespace output {
const key_t LOG_INTERVAL = "parameters::output::log_interval";
const key_t MOVIE_ON = "parameters::output::movie_on";
const key_t MOVIE_STEP_COUNTER_ON = "parameters::output::movie_step_counter_on";
const key_t MOVIE_STEP_INTERVAL = "parameters::output::movie_step_interval";
const key_t MOVIE_TIME_INTERVAL = "parameters::output::movie_time_interval";
}  // namespace output

namespace irradiation {
const key_t SEED = "parameters::irradiation::seed";
const key_t RELAX_ON = "parameters::irradiation::relax_on";
const key_t RELAX_INTERVAL = "parameters::irradiation::relax_interval";
const key_t START_WITH_RELAX_ON = "parameters::irradiation::start_with_relax_on";
const key_t NUMBER_OF_IONS = "parameters::irradiation::number_of_ions";
}  // namespace irradiation

namespace beam {
const key_t TYPE = "parameters::beam::type";
const key_t FACE = "parameters::beam::face";
const key_t SHAPE = "parameters::beam::shape";
const key_t WIDTH = "parameters::beam::width";
const key_t INCLINATION = "parameters::beam::inclination";
const key_t AZIMUTH = "parameters::beam::azimuth";
}  // namespace beam

}  // namespace parameters

}  // namespace api


#endif  // APIPARAMETERKEYS_H
