/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef QUADMODEL_H
#define QUADMODEL_H

#include "api/geometries.h"

#include "gui/rendering/abstractgeometrymodel.h"

#include <QColor>
#include <QObject>


class QuadModel : public AbstractGeometryModel {
    Q_OBJECT

  public:
    explicit QuadModel(const QColor& faceColor, const QColor& lineColor, QObject* parent = nullptr);

  private:
    void setupVertexData();

    QColor m_faceColor;
    QColor m_lineColor;

  public slots:
    void setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec2& size);

    void setFaceColor(const QColor& color);
    void setLineColor(const QColor& color);
};

#endif  // QUADMODEL_H
