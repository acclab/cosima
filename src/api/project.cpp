/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "project.h"

/*!
 * \brief Access the instance of this class.
 *
 * The instance is created if needed.
 */
Project& Project::getInstance() {
    static Project instance;
    return instance;
}


/*!
 * \brief Set the project path. Remember to load the history from the new path with a separate call to the \sa
 * loadHistory() function.
 * \param path The new project path.
 */
void Project::setPath(const QString& path) {
    m_projectDirectoryPath = path;
}

/*!
 * \brief Loads the history of entries located at the project path, previously set through the \sa şetPath() function.
 * \return true if the history was loaded, else false.
 */
bool Project::loadHistory() {
    history.load(m_projectDirectoryPath + "/history");
    if (history.entryCount() == 0) {
        return false;
    }
    return true;
}

bool Project::unloadHistory() {
    history.unload();
    if (history.entryCount() == 0) {
        return true;
    }
    return false;
}

/*!
 * \brief The project directory path
 * \return The path to the currently connected project directory
 */
const QString& Project::path() const {
    return m_projectDirectoryPath;
}

/*!
 * \brief Checks if the project is open
 * \return \c true if the object is connected to a valid projet directory
 */
bool Project::isOpen() const {
    return !m_projectDirectoryPath.isEmpty();
}
