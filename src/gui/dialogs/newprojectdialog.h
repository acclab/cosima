/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include "api/framereaderthread.h"
#include "gui/widgets/abstractprojectcreatorwidget.h"

#include <QDialog>

namespace Ui {
class NewProjectDialog;
}

/*!
 * \brief A dialog for creating new COSIRMA projects
 *
 * Lets the user select between different project creator widgets. After the selected creation procedure has been
 * completed, the user can select the project name and location, after which the new project folder is created.
 *
 * \sa AbstractProjectCreatorWidget
 *
 * \todo Implement project creation in the api and make this widget use it.
 */
class NewProjectDialog : public QDialog {
    Q_OBJECT

  public:
    explicit NewProjectDialog(QWidget* parent = nullptr);
    ~NewProjectDialog();

    const QString& getPath();

  private:
    Ui::NewProjectDialog* ui;

    AbstractProjectCreatorWidget* creator = nullptr;

    QPushButton* m_pushButton_next;
    QPushButton* m_pushButton_prev;
    QPushButton* m_pushButton_create;
    QPushButton* m_pushButton_cancel;

    QString m_projectPath;
    QString m_srcPath;

    void updateProjectName(const QString& name);

  private slots:
    void checkIfReadyToCreate();
    void onNextButtonClicked();
    void onPrevButtonClicked();
    void onCreateButtonClicked();

    void on_toolButton_changeLocation_clicked();
};

#endif  // NEWPROJECTDIALOG_H
