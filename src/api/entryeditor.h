/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ENTRYEDITOR_H
#define ENTRYEDITOR_H

#include "api/atom.h"
#include "api/atomselections.h"
#include "api/data/iontransformdata.h"
#include "api/entry.h"

#include <QObject>


/*!
 * \brief A class for editing entries
 *
 * Used for modifying ExtendedEntryData objects. Modifications are done using Action objects. After each modification
 * operation, signals are emitted based on the changes.
 *
 * Example:
 * \code{.cpp}
 * ExtendedEntryData entry;
 * EntryEditor::CellRotationAction action(EntryEditor::CellRotationAction::xAntiClockwise);
 *
 * editor = EntryEditor();
 * editor.setEntry(entry)
 * editor.edit(action);
 * ExtendedEntryData modifiedEntry = editor.editedEntry();
 * \endcode
 *
 * \sa Action, ExtendedEntryData
 */
class EntryEditor : public QObject {
    Q_OBJECT
  public:
    /*!
     * \brief An abstract class for actions
     *
     * Acts on an ExtendedEntryData object, and after that the changes that were made can be read from the object.
     */
    class Action {
      public:
        Action(const AbstractAtomSelection* selection = nullptr);
        virtual ~Action();

        /*!
         * \brief Applies the operation on \a entry. Modification statuses are saved so that they can be read later.
         * \param entry ExtendedEntryData object being modified
         */
        virtual void act(ExtendedEntryData& entry) const = 0;
        bool atomsChanged() const;
        bool typesChanged() const;
        bool cellLimsChanged() const;
        bool periodicBoundariesChanged() const;
        void setAtomSelector(const AbstractAtomSelection& selection);

      protected:
        const AbstractAtomSelection* m_selection;  //!< \brief Selection of atoms that the action will affect

        mutable bool m_atomsChanged = false;     //!< \brief Modification status of XyzData excluding cell limits
        mutable bool m_typesChanged = false;     //!< \brief Modification status of TypeData
        mutable bool m_cellLimsChanged = false;  //!< \brief Modification status of the cell limits
        mutable bool m_periodicBoundariesChanged = false;  //!< \brief Modification status of the periodic boundaries
    };

    /*!
     * \brief An action for changing the type parameters of selected atoms
     */
    class MergeToTypeAction : public Action {
      public:
        MergeToTypeAction(const AtomType& targetType, bool conserveFix = false,
                          const AbstractAtomSelection* selection = nullptr);
        void act(ExtendedEntryData& entry) const;

      private:
        AtomType m_targetType;
        bool m_conserveFix;
    };


    /*!
     * \brief An action for editing the type data of an entry
     */
    class TypeEditAction : public Action {
      public:
        TypeEditAction(int fromType, const AtomType& targetType);
        void act(ExtendedEntryData& entry) const;

      private:
        int m_fromType;
        AtomType m_targetType;
    };


    /*!
     * \brief An action for adding a new type to an entry
     */
    class TypeAddAction : public Action {
      public:
        TypeAddAction(const AtomType& type);
        void act(ExtendedEntryData& entry) const;

      private:
        AtomType m_type;
    };


    /*!
     * \brief An action for removing an atom type from an entry
     */
    class TypeDeleteAction : public Action {
      public:
        TypeDeleteAction(int typeNumber);
        void act(ExtendedEntryData& entry) const;

      private:
        int m_typeNumber;
    };


    /*!
     * \brief An action for removing atoms from an entry
     */
    class AtomDeleteAction : public Action {
      public:
        AtomDeleteAction(const AbstractAtomSelection* selection = nullptr);
        void act(ExtendedEntryData& entry) const;
    };


    /*!
     * \brief An action for fixing or unfixing atoms in an entry
     */
    class AtomFixAction : public Action {
      public:
        AtomFixAction(bool fix, const AbstractAtomSelection* selection = nullptr);
        void act(ExtendedEntryData& entry) const;

      private:
        bool m_fix;
    };


    /*!
     * \brief An action for changing the cell limits of an entry
     */
    class CellLimitsEditAction : public Action {
      public:
        CellLimitsEditAction(const Box& cellLims);
        void act(ExtendedEntryData& entry) const;

      private:
        Box m_cellLims;
    };


    /*!
     * \brief Action for changing the periodic boundaries of an entry
     */
    class PeriodicBoundaryEditAction : public Action {
      public:
        PeriodicBoundaryEditAction(const glm::bvec3& pbc);
        void act(ExtendedEntryData& entry) const;

      private:
        glm::bvec3 m_pbc;
    };


    /*!
     * \brief An action for rotating the cell of an entry by 90 degrees
     */
    class CellRotationAction : public Action {
      public:
        /*!
         * \brief Rotation options. Two rotations in all directions. Clockwise rotations are -90 degree rotations around
         * a coordinate axis. The anticlockwise rotations have a positive angle.
         */
        enum Rotation { xClockwise, xCounterClockwise, yClockwise, yCounterClockwise, zClockwise, zCounterClockwise };
        CellRotationAction(Rotation r);
        void act(ExtendedEntryData& entry) const;

      private:
        Rotation m_rotation;
    };


    /*!
     * \brief Action fro shifting in any cartesian direction. With periodic boundaries, the shifted atoms will enter on
     * the opposite side.
     */
    class ShiftCellAction : public Action {
      public:
        ShiftCellAction(const glm::dvec3& shift);
        void act(ExtendedEntryData& entry) const override;

      private:
        glm::dvec3 m_shift;
    };


    class RemoveSputteredAction : public Action {
      public:
        RemoveSputteredAction(double cutoff);
        void act(ExtendedEntryData& entry) const override;

      private:
        double m_cutoff;
    };


    class CenterCascadeAction : public Action {
      public:
        CenterCascadeAction(IonTransformData* ionTransform, double cutoff, int recursions);
        ~CenterCascadeAction();

        void act(ExtendedEntryData& entry) const override;

        glm::dvec3 shiftOffset();

      private:
        IonTransformData* m_ionTransform;
        double m_cutoff;
        int m_recursions;
        glm::dvec3* m_shiftOffset;
    };


    /*!
     * \brief An action for applying multiple actions on an entry.
     *
     * Can be used to optimize and simplify cases where many actions have to be applied on an entry. The signals, that
     * correspond to changes in the entry, are emitted only after all the actions have been applied.
     */
    class ArrayAction : public Action {
      public:
        ArrayAction();
        void act(ExtendedEntryData& entry) const;
        void addAction(const Action* action);

      private:
        std::vector<const Action*> m_actions;
    };


    explicit EntryEditor(QObject* parent = nullptr);
    const ExtendedEntryData& modifiedEntry() const;
    void edit(const Action& action);

    void setEntry(const ExtendedEntryData& entry);

    void discardChanges();
    ExtendedEntryData* saveToHistory();
    void saveEntryData(const QString& path);

  signals:
    void atomsChanged();     //!< \brief Emitted when XyzData of the edited entry changes (excluding cell limits).
    void typesChanged();     //!< \brief Emitted when TypeData of the edited entry changes.
    void cellLimsChanged();  //!< \brief Emitted when cell limits of the edited entry changes.
    void entryChanged();     //!< \brief Emitted when a new entry is set with setEntry().
    void periodicBoundariesChanged();  //!< \brief Emitted when the PBCs are changed.
    /*!
     * \brief changed Emitted when the edited entry is modified in any way.
     * \param change \c true if the edited entry differs from the entry that was given with setEntry()
     */
    void changed(bool change);

  private:
    ExtendedEntryData m_entry;
    const ExtendedEntryData* m_unmodifiedEntry = nullptr;
    bool m_atomsChanged = false;
    bool m_cellLimsChanged = false;
    bool m_typesChanged = false;
    bool m_periodicBoundariesChanged = false;
};

#endif  // ENTRYEDITOR_H
