/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEMPLATEPROJECTCREATORWIDGET_H
#define TEMPLATEPROJECTCREATORWIDGET_H

#include "api/framereaderthread.h"
#include "gui/widgets/abstractprojectcreatorwidget.h"

namespace Ui {
class TemplateProjectCreatorWidget;
}

class TemplateProjectCreatorWidget : public AbstractProjectCreatorWidget {
    Q_OBJECT

  public:
    explicit TemplateProjectCreatorWidget(QWidget* parent = nullptr);
    ~TemplateProjectCreatorWidget() override;

    void enter() override;
    bool nextEnabled() override { return m_nextEnabled; }
    const ExtendedEntryData& getEntry() override;
    QString projectName() override;

  private:
    Ui::TemplateProjectCreatorWidget* ui;
    FrameReaderThread m_frameReader;
    QString m_path;
    //    QString m_pathCandidate, m_selectedPath;
    ExtendedEntryData m_entry;
    bool m_nextEnabled;

  private slots:
    //    void frameLoaded();
    //    void frameLoadingFailed();
    void currentTemplateChanged(int);
    void loadTemplateList();

  signals:
    void frameLoading();
    void frameLoadCompleted(bool success);
};

#endif  // TEMPLATEPROJECTCREATORWIDGET_H
