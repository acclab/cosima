/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATOMTYPE_H
#define ATOMTYPE_H

#include <QDataStream>
#include <QDebug>

/*!
 * \brief A struct that includes information about an atom type.
 */
struct AtomType {

    QString symbol = "XX"; /*!< \brief Element symbol of the type */
    int atomNumber = -1;   /*!< \brief Element number (proton count) */
    int typeNumber = 0;    /*!< \brief Type number. PARCAS supports from -9 to 9 */
    double mass = 0.1;     /*!< \brief Mass of an atom belonging to this type */

    /*!
     * \brief Equality operator
     * \param a Type being compared to
     * \return \c true if all the atributes are equal
     */
    bool operator==(const AtomType& a) const {
        return (symbol == a.symbol) && (atomNumber == a.atomNumber) && (typeNumber == a.typeNumber) && (mass == a.mass);
    }

    /*!
     * \brief Inequality operator
     * \param a Type being compared to
     * \return \c true if any of the attributes are inequal
     */
    bool operator!=(const AtomType& a) const { return !operator==(a); }
};

inline QDataStream& operator<<(QDataStream& stream, const AtomType& a) {
    return stream << a.symbol << a.atomNumber << a.typeNumber << a.mass;
}

inline QDataStream& operator>>(QDataStream& stream, AtomType& a) {
    return stream >> a.symbol >> a.atomNumber >> a.typeNumber >> a.mass;
}

inline QDebug operator<<(QDebug d, const AtomType& t) {
    QDebugStateSaver saver(d);
    d << "AtomTypestruct:" << endl;
    d << "  element:      " << t.symbol << endl;
    d << "  atom number:  " << t.atomNumber << endl;
    d << "  type number:  " << t.typeNumber << endl;
    d << "  mass:         " << t.mass << endl;
    return d;
}

#endif  // ATOMTYPE_H
