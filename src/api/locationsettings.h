/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LOCATIONSETTINGS_H
#define LOCATIONSETTINGS_H

#include "abstractstreamable.h"

#include <QDir>
#include <QString>

/*!
 * \brief Program settings struct related to locations that the program uses for storing data and running simulations.
 *
 * \todo At the moment contains also variable "saveBinaryFrames" that does not quite fit under the current scope. The
 * name of the struct should be changed to be more broad or the variable should be moved to a different struct.
 */
struct LocationSettings : AbstractStreamable {
    /*!
     * \brief If \c true, simulation runtime directories are created in a temp direcotry.
     */
    bool useTempRuntimeDir;

    /*!
     * \brief Path to the directory where the runtime directories are created if useTempRuntimeDir = \c false.
     */
    QString runtimeDirPath;

    /*!
     * \brief Path to the directory where the project directories are created.
     */
    QString projectDirPath;

    /*!
     * \brief If \c true, in Frame writes also the binary versions of xyz-files.
     */
    bool saveBinaryFrames;

    /*!
     * \brief Generates a struct that has default settings
     * \return An object initialized with default settings
     */
    static LocationSettings defaultSettings() {
        LocationSettings settings;
        settings.useTempRuntimeDir = true;
        settings.runtimeDirPath = QDir::homePath() + "/cosirma/runtimes";
        settings.projectDirPath = QDir::homePath() + "/cosirma/projects";
        settings.saveBinaryFrames = false;
        return settings;
    }

    QDataStream& save(QDataStream& out) const override {
        out << useTempRuntimeDir << runtimeDirPath << projectDirPath << saveBinaryFrames;
        return out;
    }

    QDataStream& load(QDataStream& in) override {
        in >> useTempRuntimeDir >> runtimeDirPath >> projectDirPath >> saveBinaryFrames;
        return in;
    }

    QDebug debug(QDebug d) const override {
        d << "LocationSettings:" << endl;
        d << "  useTempRuntimeDir: " << useTempRuntimeDir << endl;
        d << "  runtimeDirPath: " << runtimeDirPath << endl;
        d << "  projectDirPath: " << projectDirPath << endl;
        d << "  saveBinaryFrames: " << saveBinaryFrames << endl;
        return d;
    }
};

Q_DECLARE_METATYPE(LocationSettings)

#endif  // LOCATIONSETTINGS_H
