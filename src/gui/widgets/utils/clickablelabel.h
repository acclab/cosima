/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QColor>
#include <QLabel>
#include <QMouseEvent>
#include <QWidget>


/*! \brief QLabel that emits a signal when clicked.
 */
class ClickableLabel : public QLabel {
    Q_OBJECT

  public:
    explicit ClickableLabel(QWidget* parent = nullptr) : QLabel(parent) {}

    void setColor(const QColor& color);
    const QColor& color();

  protected:
    void mousePressEvent(QMouseEvent* event);

  private:
    QColor m_color;

  signals:
    void clicked();
};

#endif  // CLICKABLELABEL_H
