/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "interactionwidget.h"
#include "ui_interactionwidget.h"

#include "api/entry.h"
#include "api/potentiallist.h"
#include "api/utils/findinputfiles.h"

#include "gui/dialogs/potentialfiledialog.h"
#include "gui/widgets/utils/lineeditdoubledelegate.h"
#include "gui/widgets/utils/wheelguard.h"

#include <QComboBox>


InteractionWidget::InteractionWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::InteractionWidget) {
    ui->setupUi(this);

    // ===============================================================
    // Potentials and intraction modes

    ui->ionGB->setVisible(false);

    // Connect to the state
    connect(ui->potmodeCB, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [this] {
        int potmode = ui->potmodeCB->currentData().toInt();

        updatePotentialInteractions();
        emit modified(hasReppotErrors() || hasElstopErrors());
    });
    connect(ui->useForAllPB, &QPushButton::clicked, this, [=] {
        applySelectedPotentialToSupportedPairs();
        emit modified(hasReppotErrors() || hasElstopErrors());
    });

    //! \todo This should be activated once the potential list is a little bit less messy
    ui->checkBox_lambda3->setVisible(false);
    ui->checkBox_lambda3->setChecked(false);

    ui->potmodeCB->setFocusPolicy(Qt::StrongFocus);
    ui->potmodeCB->installEventFilter(new WheelGuard(this));

    setFocusProxy(ui->potmodeCB);


    // ===============================================================
    // Electronic Stopping

    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Min"
                                                             << "Max"
                                                             << "Step");
    ui->tableWidget->setItemDelegate(new LineEditDoubleDelegate(this));
    ui->tableWidget->setItem(0, 0, new QTableWidgetItem("0.0"));
    ui->tableWidget->setItem(0, 1, new QTableWidgetItem("2e+07"));
    ui->tableWidget->setItem(0, 2, new QTableWidgetItem("1e+05"));

    ui->plotWidget->addGraph();
    ui->plotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    ui->plotWidget->xAxis->setLabel("v [m/s]");
    ui->plotWidget->yAxis->setLabel("F [eV/Å]");

    connect(ui->lineEdit_substrate, &QLineEdit::editingFinished, this, [=]() {
        if (ui->lineEdit_substrate->text().isEmpty()) {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE] = gui::enums::state::Error;
        } else {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE] = gui::enums::state::Ok;
            createElstopData();
        }
        updateElstopInteractionStatus();
        emit modified(hasReppotErrors() || hasElstopErrors());
    });
    connect(ui->lineEdit_density, &QLineEdit::editingFinished, this, [=]() {
        if (ui->lineEdit_density->text().toDouble() == 0) {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] = gui::enums::state::Error;
        } else {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] = gui::enums::state::Ok;
            createElstopData();
        }
        updateElstopInteractionStatus();
        emit modified(hasReppotErrors() || hasElstopErrors());
    });

    connect(ui->tableWidget->model(), &QStandardItemModel::dataChanged, this, &InteractionWidget::updateElstopList);
    connect(ui->listWidget_elstopFiles, &QListWidget::currentTextChanged, this, &InteractionWidget::changeElstopPlot);
    connect(ui->pushButton, &QPushButton::clicked, this, &InteractionWidget::createFiles);
}


InteractionWidget::~InteractionWidget() {
    delete ui;
}


void InteractionWidget::setEntry(ExtendedEntryData* entry) {
    AbstractComponentWidget::setEntry(entry);

    // Make sure to set the fields independent of the parameters when a new entry is set.
    updateFieldsBasedOnEntry();
}


void InteractionWidget::setParameters(api::BaseParameters parameters) {
    if (entry() == nullptr) return;

    QSignalBlocker blocker1(ui->potmodeCB);

    int potMode = parameters[api::parameters::interaction::POTENTIAL_MODE].value().toInt();

    ui->potmodeCB->setCurrentIndex(ui->potmodeCB->findData(potMode));
    updatePotentialInformationText(PotentialList::getInstance().getPotential(ui->potmodeCB->currentData().toInt()));

    QString interactionString = parameters[api::parameters::interaction::ATOM_PAIR_INTERACTIONS].value().toString();
    QStringList interactions = interactionString.split(";", QString::SkipEmptyParts);
    for (const QString& interaction : interactions) {
        QStringList parts = interaction.split(" ", QString::SkipEmptyParts);
        int type1 = parts[0].toInt();
        int type2 = parts[1].toInt();
        api::enums::interaction::Mode mode = static_cast<api::enums::interaction::Mode>(parts[2].toInt());

        m_currentInteractions[TypeNumberPair(type1, type2)] = mode;
    }


    QString substrate = ui->lineEdit_substrate->text();
    if (parameters.values().contains(api::parameters::elstop::SUBSTRATE)) {
        substrate = parameters[api::parameters::elstop::SUBSTRATE].value().toString();
        if (substrate.isEmpty()) {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE] = gui::enums::state::Status::Error;
        } else {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE] = gui::enums::state::Status::Ok;
        }
    }
    QString substrateDensity = ui->lineEdit_density->text();
    if (parameters.values().contains(api::parameters::elstop::SUBSTRATE_DENSITY)) {
        substrateDensity = parameters[api::parameters::elstop::SUBSTRATE_DENSITY].value().toString();
        if (substrateDensity.toDouble() == 0) {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] = gui::enums::state::Status::Error;
        } else {
            m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] = gui::enums::state::Status::Ok;
        }
    }
    ui->lineEdit_substrate->setText(substrate);
    ui->lineEdit_density->setText(substrateDensity);


    // Look for input files alread present in the entry folder
    FindInputFiles::readReppotFiles(m_entry);
    FindInputFiles::readElstopFiles(m_entry, ui->lineEdit_substrate->text());

    // Generate the potential interaction widgets with the correct data.
    updatePotentialInteractions();
    updatePotentialInteractionStatus();

    // Generate the elstop interaction widgets with the correct data.
    updateElstopList();
    updateElstopInteractionStatus();

    createElstopData();
}


api::BaseParameters InteractionWidget::parameters() {

    QString atomPairInteractions = "";

    if (entry() != nullptr) {
        for (const AtomType& type1 : entry()->data.types.allTypes) {
            if (type1.atomNumber < 1) continue;

            for (const AtomType& type2 : entry()->data.types.allTypes) {
                if (type2.atomNumber < 1) continue;
                if (type2.typeNumber < type1.typeNumber) continue;

                TypeNumberPair pair(type1.typeNumber, type2.typeNumber);
                if (m_currentInteractions[pair]) {
                    atomPairInteractions += QString("%1 %2 %3;")
                                                .arg(type1.typeNumber)
                                                .arg(type2.typeNumber)
                                                .arg(m_currentInteractions[pair]);
                }
            }
        }
    }

    api::BaseParameters params;

    params.set(api::parameters::interaction::POTENTIAL_MODE, api::value_t(ui->potmodeCB->currentData().toInt()));
    params.set(api::parameters::interaction::ATOM_PAIR_INTERACTIONS, api::value_t(atomPairInteractions));


    params.set(api::parameters::elstop::SUBSTRATE, api::value_t(ui->lineEdit_substrate->text()));
    params.set(api::parameters::elstop::SUBSTRATE_DENSITY, api::value_t(ui->lineEdit_density->text().toDouble()));

    return params;
}


void InteractionWidget::updatePotentialInformationText(const Potential& potential) {

    if (potential.mode() == 0) {
        ui->textEdit->setVisible(false);
        return;
    } else {
        ui->textEdit->setVisible(true);
    }

    QStringList typeNames;
    QHash<int, AtomType> types = m_entry->data.types.typeList;
    for (AtomType at : types.values()) {
        if (at.atomNumber < 0) {
            continue;
        }
        typeNames.append(at.symbol);
    }

    QStringList supportedPairs;
    QStringList unSupportedPairs;
    for (int i = 0; i < typeNames.length(); i++) {
        QString element1 = typeNames[i];
        for (int j = i; j < typeNames.length(); j++) {
            QString element2 = typeNames[j];
            if (potential.supportsPair(element1, element2)) {
                supportedPairs << element1 + "\t" + element2;
            } else {
                unSupportedPairs << element1 + "\t" + element2;
            }
        }
    }

    QStringList lines = potential.information().split("\n");
    QString htmlText = "";
    htmlText += "<h2>Potmode: " + QString::number(potential.mode()) + ", " + lines[0] + "</h2>";

    if (!unSupportedPairs.isEmpty()) {
        htmlText += "<h3>Unsupported atom pairs</h3>";

        htmlText += "<table style=\"width:100%\">";
        htmlText += "<tr>";
        htmlText += "<th align=\"left\">Element 1</th>";
        htmlText += "<th align=\"left\">Element 2</th>";
        htmlText += "</tr>";
        for (int i = 0; i < unSupportedPairs.length(); i++) {
            if (unSupportedPairs[i].isEmpty()) continue;
            QStringList parts = unSupportedPairs[i].split("\t");
            htmlText += "<tr>";
            for (int j = 0; j < parts.length(); j++) {
                htmlText += "<td>";
                htmlText += parts[j];
                htmlText += "</td>";
            }
            htmlText += "</tr>";
        }
        htmlText += "</table>";
    }

    htmlText += "<h3>Supported atom pairs</h3>";

    htmlText += "<table style=\"width:100%\">";
    htmlText += "<tr>";
    htmlText += "<th align=\"left\">Element 1</th>";
    htmlText += "<th align=\"left\">Element 2</th>";
    htmlText += "<th align=\"left\">Comment</th>";
    htmlText += "</tr>";
    for (int i = 1; i < lines.length(); i++) {
        if (lines[i].isEmpty()) continue;
        QStringList parts = lines[i].split("\t");
        if (parts.length() >= 2) {
            QString element1 = parts[0];
            QString element2 = parts[1];
            QString comment = "";
            if (parts.length() >= 3) {
                comment = parts[2];
            }

            QString stylingStart = "";
            QString stylingEnd = "";
            if (supportedPairs.contains(element1 + "\t" + element2) ||
                supportedPairs.contains(element2 + "\t" + element1)) {

                stylingStart = "<b style=\"color:blue;\">";
                stylingEnd = "</b>";
            }
            htmlText += "<tr>";
            htmlText += "<td>" + stylingStart + element1 + stylingEnd + "</td>";
            htmlText += "<td>" + stylingStart + element2 + stylingEnd + "</td>";
            htmlText += "<td>" + stylingStart + comment + stylingEnd + "</td>";
            htmlText += "</tr>";
        }
    }
    htmlText += "</table>";
    ui->textEdit->setHtml(htmlText);
}


void InteractionWidget::createFiles() {
    createElstopData();
}


void InteractionWidget::updateElstopList() {
    if (m_entry == nullptr) return;

    QString substrateText = ui->lineEdit_substrate->text();


    QStringList elstopFileList;
    for (const QString& symbol : m_entry->data.elstopFiles.keys()) {
        if (m_entry->data.elstopFiles[symbol].hasData()) {
            elstopFileList << QString("elstop.%2.%3.in").arg(symbol).arg(substrateText);
        }
    }

    // Remember the currently selected index to select it again after the list has been regenerated.
    int currentRow = ui->listWidget_elstopFiles->currentRow();

    ui->listWidget_elstopFiles->clear();
    ui->listWidget_elstopFiles->addItems(elstopFileList);

    // Select the first entry if none has been selected and data is found
    if (currentRow == -1 && ui->listWidget_elstopFiles->count() > 0) {
        currentRow = 0;
    }
    // Reselect the same index after the new data has been generated.
    ui->listWidget_elstopFiles->setCurrentRow(currentRow);
}


void InteractionWidget::changeElstopPlot(const QString& elstopFileName) {
    QStringList filenameParts = elstopFileName.split(".");
    if (filenameParts.size() < 2) return;

    // Elstop files have the pattern "elstop.X.Substrate.in", by extracting "X" one can find the atom currently being
    // slowed down by the substrate.
    QString elementSymbol = filenameParts[1];

    //    const QMap<QString, QList<QPair<double, double>>> elstopMap = m_generator.elstops();
    const QMap<QString, DataFileContent<ElstopFileData>> elstopMap = m_generator.data();

    QVector<double> x, y;

    double xmin = std::numeric_limits<double>::max();
    double xmax = std::numeric_limits<double>::min();
    double ymin = std::numeric_limits<double>::max();
    double ymax = std::numeric_limits<double>::min();

    for (const ElstopFileData& elstopDataPoint : elstopMap[elementSymbol].values()) {
        double xData = elstopDataPoint.v;
        double yData = elstopDataPoint.F;
        x.push_back(xData);
        y.push_back(yData);

        if (xData < xmin) xmin = xData;
        if (xData > xmax) xmax = xData;
        if (yData < ymin) ymin = yData;
        if (yData > ymax) ymax = yData;
    }

    ui->plotWidget->graph(0)->setData(x, y);
    ui->plotWidget->xAxis->setRange(xmin, xmax);
    ui->plotWidget->yAxis->setRange(ymin, ymax);
    ui->plotWidget->replot();
}


void InteractionWidget::updateFieldsBasedOnEntry() {
    if (entry() == nullptr) return;

    //============================
    //   POTENTIAL INTERACTIONS
    //============================

    // Update potential mode combobox based on the atom types in the entry data

    QStringList typeNames;
    QHash<int, AtomType> types = entry()->data.types.allTypes;
    for (AtomType at : types.values()) {
        if (at.atomNumber < 1) continue;   // Do not include fake ions
        if (at.typeNumber == 0) continue;  // Do not include real ions
        typeNames.append(at.symbol);
    }

    ui->potmodeCB->blockSignals(true);
    int selectedIndex = ui->potmodeCB->currentIndex();
    ui->potmodeCB->clear();
    ui->potmodeCB->addItem("Only Pairs", 0);
    QList<Potential> potentials = PotentialList::getInstance().filteredPotentials(typeNames);
    for (const Potential& p : potentials) {
        QString text = p.name() + " (" + QString::number(p.mode()) + ")";
        ui->potmodeCB->addItem(text, p.mode());
        int itemId = ui->potmodeCB->count() - 1;
        ui->potmodeCB->setItemData(itemId, p.information(), Qt::ToolTipRole);
    }
    ui->potmodeCB->setCurrentIndex(selectedIndex);
    ui->potmodeCB->blockSignals(false);


    //=========================
    //   ELSTOP INTERACTIONS
    //=========================

    // Update the substrate text based on the atom types in the entry data

    // Remember the previously chosen value and re-apply it if nothing is in the field, otherwise generate the value.
    QString substrate = ui->lineEdit_substrate->text();
    if (substrate.isEmpty()) {
        for (const QString& atomName : typeNames) {
            substrate += atomName;
        }
        m_parameterStatus[api::parameters::elstop::SUBSTRATE] = gui::enums::state::Status::Attention;
    }

    ui->lineEdit_substrate->setText(substrate);

    // Compute the density of the entire structure cell. This is just an artihmetic average of all atoms in the
    // structure. The user should be suggested to update the value to better reflect the chosen substrate.

    // Remember the previously chosen value, if there already was something in the field, reapply that value.
    //! \todo this logic doesn't work if a new project is created and populated into the widgets. In that case there
    //! should be a way to clear the current widget.
    QString densityText = ui->lineEdit_density->text();
    if (densityText.isEmpty()) {
        Box atomLims = entry()->frame.atomLims;
        double volume = atomLims.dx() * atomLims.dy() * atomLims.dz();

        double totalMass = 0;
        for (const Atom& atom : entry()->frame.atoms) {
            totalMass += entry()->data.types.typeList[atom.type].mass;
        }
        // Convert from u to g
        totalMass = totalMass * 1.6605390e-24;

        // g/cm^3
        double density = totalMass / volume * (1e8 * 1e8 * 1e8);
        densityText = QString::number(density);

        m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] = gui::enums::state::Status::Attention;
    }

    ui->lineEdit_density->setText(densityText);

    updateElstopInteractionStatus();
}


void InteractionWidget::createElstopData() {
    if (hasElstopErrors()) return;

    // Create the data and add it to the entry
    double rho = ui->lineEdit_density->text().toDouble();
    double xmin = ui->tableWidget->item(0, 0)->text().toDouble();
    double xmax = ui->tableWidget->item(0, 1)->text().toDouble();
    double xstep = ui->tableWidget->item(0, 2)->text().toDouble();

    // Find all occurences that starts with a capital letter until the next capital letter. This will include any
    // numbers to the preceeding letter combination.
    QRegularExpression regexp("[A-Z][^A-Z]*");
    QRegularExpression regexpNumber("[0-9]*");
    QRegularExpressionMatchIterator match = regexp.globalMatch(ui->lineEdit_substrate->text());

    QList<ElstopGenerator::SubstratePart> substrate;
    while (match.hasNext()) {
        // Split on text and numbers. The strings should start with a uppercase letter and have zero or one lowercase
        // letters following. At the end there is either a number or not. The number specifies the component count of
        // that atom species in the compound.
        QRegularExpression textexp("^[A-Z][a-z]*");
        QRegularExpression numberexp("[1-9][0-9]*$");

        QStringList elements = match.next().capturedTexts();
        for (const QString& element : elements) {
            QString symbol = textexp.match(element).captured();
            int amount = qMax(1, numberexp.match(element).captured().toInt());  // The amount should be at least 1.
            substrate << ElstopGenerator::SubstratePart(symbol, amount);
        }
    }

    m_generator.generate(substrate, *m_entry, rho, xmin, xmax, xstep);

    bool anyElstopDataUpdated = false;
    // Only update elstop files without data.
    //! \todo implement a similar "replace" logic as found in the potentials
    for (const QString& key : m_entry->data.elstopFiles.keys()) {
        if (!m_entry->data.elstopFiles[key].hasData()) {
            m_entry->data.elstopFiles[key].setData(m_generator.data()[key]);
            anyElstopDataUpdated = true;
        }
    }

    updateElstopList();

    if (anyElstopDataUpdated) {
        emit modified(hasReppotErrors() || hasElstopErrors());
    }
}


void InteractionWidget::updatePotentialInteractions() {
    if (entry() == nullptr) return;

    // Updating the interaction mapping widget

    // Refilling the grids
    for (QWidget* w : m_gridWidgets) w->deleteLater();
    m_gridWidgets.clear();

    QString potentialText = ui->potmodeCB->currentText();
    int potMode = ui->potmodeCB->currentData().toInt();
    Potential activePotential = PotentialList::getInstance().getPotential(potMode);
    updatePotentialInformationText(activePotential);

    // Remember the previous values and reapply them if they exist
    QMap<TypeNumberPair, api::enums::interaction::Mode> previousInteractions = m_currentInteractions;
    m_currentInteractions.clear();


    //    potentialStatus = gui::enums::state::Status::Ok;
    m_potentialStatus.clear();

    QVector<QComboBox*> ion_cbs, entry_cbs;

    int ionPairsCount = 0;
    int entryPairsCount = 0;


    // Sort the entries to make the listing always appear in the same order
    QList<int> typeKeys = entry()->data.types.allTypes.keys();
    std::sort(typeKeys.begin(), typeKeys.end());

    for (int key1 : typeKeys) {
        AtomType atomType1 = entry()->data.types.allTypes[key1];
        if (atomType1.atomNumber < 1) continue;

        for (int key2 : typeKeys) {

            // Only list every interaction once. E.g. only 1-2, and no 2-1...
            if (key2 < key1) continue;

            AtomType atomType2 = entry()->data.types.allTypes[key2];
            if (atomType2.atomNumber < 1) continue;

            TypeNumberPair pair(atomType1.typeNumber, atomType2.typeNumber);
            TypePairWidget* pairWidget = new TypePairWidget(atomType1, atomType2, this);

            QComboBox* comboBox = new QComboBox(this);
            comboBox->setFocusPolicy(Qt::StrongFocus);
            comboBox->installEventFilter(new WheelGuard(comboBox));

            comboBox->addItem("None", api::enums::interaction::None);
            comboBox->addItem("Pair", api::enums::interaction::Pair);
            if (activePotential.supportsPair(atomType1.symbol, atomType2.symbol)) {
                if (potentialText != "Only Pairs") {
                    comboBox->addItem(potentialText, api::enums::interaction::Potmode);
                }
            }

            ClickableLabel* iconLabel = new ClickableLabel(this);
            ClickableLabel* errorLabel = new ClickableLabel(this);

            // Helper function to update the labels indicating missing potential files.
            auto updateLabel = [=](api::enums::interaction::Mode mode) {
                QSize size(15, 15);
                QIcon icon;
                if (mode == api::enums::interaction::None) {
                    icon = loadIcon(gui::enums::state::Status::Attention);
                    errorLabel->setStyleSheet("color: blue");
                    errorLabel->setText("No interaction between the types!");
                    m_potentialStatus[iconLabel] = gui::enums::state::Status::Attention;
                    emit needsAttention(hasReppotErrors() || hasElstopErrors());
                } else {
                    QString errorMessage = potentialFileError(pair, activePotential, mode);
                    if (!errorMessage.isEmpty()) {
                        icon = loadIcon(gui::enums::state::Error);
                        errorLabel->setStyleSheet("color: red");
                        errorLabel->setText(errorMessage);
                        m_potentialStatus[iconLabel] = gui::enums::state::Status::Error;
                        emit needsAttention(hasReppotErrors() || hasElstopErrors());
                    } else {
                        icon = loadIcon(gui::enums::state::Ok);
                        errorLabel->setStyleSheet("color: green");
                        errorLabel->setText("");
                        //! \todo figure out a way to set the Ok value... For now it is set when saving.
                        // potentialStatus = qMax(potentialStatus, gui::enums::state::Status::Ok);
                        m_potentialStatus[iconLabel] = gui::enums::state::Status::Ok;
                    }
                }
                iconLabel->setPixmap(icon.pixmap(size));
                updatePotentialInteractionStatus();
            };

            connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int i) {
                api::enums::interaction::Mode newMode =
                    static_cast<api::enums::interaction::Mode>(comboBox->itemData(i).toInt());
                m_currentInteractions[pair] = newMode;
                updateLabel(newMode);
                emit modified(hasReppotErrors() || hasElstopErrors());
            });

            api::enums::interaction::Mode mode = previousInteractions[pair];
            if (mode == api::enums::interaction::Potmode &&
                !activePotential.supportsPair(atomType1.symbol, atomType2.symbol)) {
                mode = api::enums::interaction::None;
            }

            connect(iconLabel, &ClickableLabel::clicked, this, &InteractionWidget::on_filePB_clicked);
            connect(errorLabel, &ClickableLabel::clicked, this, &InteractionWidget::on_filePB_clicked);


            {  // Block the initial signals from the combobox
                QSignalBlocker tempBlocker(comboBox);
                m_currentInteractions[pair] = mode;
                updateLabel(mode);
                comboBox->setCurrentIndex(comboBox->findData(mode));
            }

            if (atomType1.typeNumber == 0 || atomType2.typeNumber == 0) {
                ui->ionGrid->addWidget(pairWidget, ionPairsCount, 0);
                ui->ionGrid->addWidget(comboBox, ionPairsCount, 1);
                ui->ionGrid->addWidget(iconLabel, ionPairsCount, 2);
                ui->ionGrid->addWidget(errorLabel, ionPairsCount, 3);
                ionPairsCount++;
                ion_cbs.push_back(comboBox);
            } else {
                ui->existingGrid->addWidget(pairWidget, entryPairsCount, 0);
                ui->existingGrid->addWidget(comboBox, entryPairsCount, 1);
                ui->existingGrid->addWidget(iconLabel, entryPairsCount, 2);
                ui->existingGrid->addWidget(errorLabel, entryPairsCount, 3);
                entryPairsCount++;
                entry_cbs.push_back(comboBox);
            }

            m_gridWidgets << pairWidget << comboBox << iconLabel << errorLabel;

            m_currentInteractions.insert(pair, mode);
        }
    }


    // Set the visibility of the grids
    AtomType ionType = m_entry->data.types.allTypes[0];
    bool ionActivated = ionType.atomNumber >= 1;
    ui->ionGB->setVisible(ionActivated);

    // Set the tab order of the widgets manually
    QWidget* last = ui->filePB;

    for (QComboBox* cb : entry_cbs) {
        setTabOrder(last, cb);
        last = cb;
    }

    for (QComboBox* cb : ion_cbs) {
        setTabOrder(last, cb);
        last = cb;
    }

    setTabOrder(last, ui->ionGB);
    last = ui->ionGB;

    updatePotentialInteractionStatus();
}


void InteractionWidget::updateElstopInteractionStatus() {
    elstopStatus = gui::enums::state::Status::None;

    auto setLabelStatus = [=](QLabel* label, gui::enums::state::Status status) {
        elstopStatus = qMax(elstopStatus, status);

        QString tooltip = "";
        switch (status) {
            case gui::enums::state::Error:
                tooltip = "Can't generate elstop with given value";
                break;
            case gui::enums::state::Attention:
                tooltip = "Autogenerated value, consider changing this.";
                break;
            default:
                tooltip = "";
        }

        QIcon icon = loadIcon(status);
        QPixmap pixmap = icon.pixmap(icon.actualSize(QSize(15, 15)));
        label->setPixmap(pixmap);
        label->setToolTip(tooltip);
    };

    setLabelStatus(ui->label_defaultSubstrate, m_parameterStatus[api::parameters::elstop::SUBSTRATE]);
    setLabelStatus(ui->label_defaultDensity, m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY]);

    QIcon icon = loadIcon(elstopStatus);
    ui->tabWidget->setTabIcon(1, icon);
}


void InteractionWidget::updatePotentialInteractionStatus() {

    potentialStatus = gui::enums::state::None;
    for (const gui::enums::state::Status& status : m_potentialStatus) {
        potentialStatus = qMax(potentialStatus, status);
    }

    ui->tabWidget->setTabIcon(0, loadIcon(potentialStatus));
}


void InteractionWidget::applySelectedPotentialToSupportedPairs() {
    int potMode = ui->potmodeCB->currentData().toInt();
    if (potMode == 0) {
        for (const AtomType& type1 : entry()->data.types.allTypes) {
            if (type1.atomNumber < 1) continue;

            for (const AtomType& type2 : entry()->data.types.allTypes) {
                if (type2.atomNumber < 1) continue;
                if (type2.typeNumber < type1.typeNumber) continue;

                m_currentInteractions[TypeNumberPair(type1.typeNumber, type2.typeNumber)] =
                    api::enums::interaction::Pair;
            }
        }
    } else {
        Potential activePotential = PotentialList::getInstance().getPotential(potMode);
        for (const AtomType& type1 : entry()->data.types.allTypes) {
            if (type1.atomNumber < 1) continue;

            for (const AtomType& type2 : entry()->data.types.allTypes) {
                if (type2.atomNumber < 1) continue;
                if (type2.typeNumber < type1.typeNumber) continue;

                api::enums::interaction::Mode mode = api::enums::interaction::None;
                if (activePotential.supportsPair(type1.symbol, type2.symbol)) {
                    mode = api::enums::interaction::Potmode;
                }
                m_currentInteractions[TypeNumberPair(type1.typeNumber, type2.typeNumber)] = mode;
            }
        }
    }

    updatePotentialInteractions();
}


void InteractionWidget::on_filePB_clicked() {
    qDebug() << "Load potential files";
    PotentialFileDialog fileDialog(entry()->data.potentialFiles, this);
    if (fileDialog.exec()) {
        // Copy over the selected potential files to the entry when accepting the output.
        entry()->data.potentialFiles = fileDialog.potentialFiles();

        updatePotentialInteractions();
        emit modified(hasReppotErrors() || hasElstopErrors());
    }
}


QString InteractionWidget::potentialFileError(const TypeNumberPair& pair, const Potential& selectedPotential,
                                              api::enums::interaction::Mode mode) {
    QHash<int, AtomType> allTypes = entry()->data.types.allTypes;
    const AtomType& t1 = allTypes[pair.type1()];
    const AtomType& t2 = allTypes[pair.type2()];
    ElementPair elementPair(t1.symbol, t2.symbol);

    bool pairFileMissing = false;
    bool eamFileMissing = false;
    if (entry()->data.potentialFiles.contains(elementPair)) {
        pairFileMissing = !entry()->data.potentialFiles[elementPair].hasData();
        eamFileMissing = !entry()->data.potentialFiles[elementPair].hasData();
    }

    bool pairFileRequired =
        (mode == api::enums::interaction::Pair) ||
        (mode == api::enums::interaction::Potmode && selectedPotential.fileType() == Potential::Reppot);
    bool eamFileRequired = (mode == api::enums::interaction::Potmode && selectedPotential.fileType() == Potential::EAM);
    bool pairFileError = pairFileMissing && pairFileRequired;
    bool eamFileError = eamFileMissing && eamFileRequired;

    if (pairFileError && eamFileError) {
        return "Pair and EAM potential files required!";
    } else if (pairFileError) {
        return "Pair potential file required!";
    } else if (eamFileError) {
        return "EAM potential file required!";
    }

    return "";
}


bool InteractionWidget::hasElstopErrors() {
    if (m_parameterStatus[api::parameters::elstop::SUBSTRATE] == gui::enums::state::Error) {
        return true;
    }
    if (m_parameterStatus[api::parameters::elstop::SUBSTRATE_DENSITY] == gui::enums::state::Error) {
        return true;
    }
    return false;
}


bool InteractionWidget::hasReppotErrors() {
    return potentialStatus == gui::enums::state::Status::Error;
}
