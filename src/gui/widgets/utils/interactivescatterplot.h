/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef INTERACTIVESCATTERPLOT_H
#define INTERACTIVESCATTERPLOT_H

#include "qcustomplot.h"

#include <glm/vec2.hpp>

#include <vector>


/*! \brief Interactive scatter plot.
 *
 *  Inherits QCustomPlot and adds support for an interactive scatter plot that allows grabbing and dragging the points.
 *
 * \todo Inherit InteractivePlot insted of QCustomplot.
 */
class InteractiveScatterPlot : public QCustomPlot {
    Q_OBJECT

  public:
    explicit InteractiveScatterPlot(QWidget* parent = nullptr);
    QList<glm::dvec2> getPoints();
    void setPoints(QList<glm::dvec2>);
    void addPoint(double, double);
    void setHighlightedSection(int);
    void modifySection(int, double, double, double, double);
    bool removePoint(int);
    int getSelectedPoint();

  protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);

  private:
    QList<glm::dvec2> points;
    glm::dvec2 grabbedCoordinate;
    int pointSize, grabbedPoint, highlightedSection, selectedPoint;
    QTimer* rescalingTimer;
    QPoint lastCursorPos;
    double axisMinx, axisMiny;
    QCPItemRect* zoomRect;

  public slots:
    void fitToData();

  private slots:
    void plotScatter();
    void cursorOutside();
    void movePointToPos(int, QPoint);

  signals:
    void pointsChanged();
    void sectionSplitted(int);
    void sectionAdded();
    void sectionChanged(int);
};

#endif  // INTERACTIVESCATTERPLOT_H
