/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BINARYSETTINGSIO_H
#define BINARYSETTINGSIO_H

#include "abstractstreamable.h"

/*!
 * \brief A class for writing AbstractStreamable objects into binary files
 *
 * Can be used to save and load AbstractStreamable objects using binary format.
 */
class BinarySettingsIO {
  public:
    BinarySettingsIO(const QString& path);

    bool save(const AbstractStreamable& settings);
    bool load(AbstractStreamable& settings);

    bool hasContent();

  private:
    QString m_path;
};

#endif  // BINARYSETTINGSIO_H
