/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BEAMDATAUTIL_H
#define BEAMDATAUTIL_H

#include "api/geometries.h"
#include "api/state/enums.h"

#include <glm/vec3.hpp>


class BroadBeamUtil {
  public:
    static glm::dvec3 position(const Box& cellLims, api::enums::beam::Face face);
    static glm::vec3 rotation(double inclination, double azimuth, api::enums::beam::Face face);
    static glm::vec3 direction(const glm::vec3& rotation);
    static glm::vec3 direction(double inclination, double azimuth, api::enums::beam::Face face);
};

#endif  // BEAMDATAUTIL_H
