/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ENUMS_H
#define ENUMS_H

namespace api {
namespace enums {

namespace temperaturecontrol {
enum Mode {
    None,      //!< \brief No temperature scaling
    AllAtoms,  //!< \brief All atoms are controlled
    /*!
     * \brief Atoms at the "border" regions are controlled. Region dimensions specified by #xCoolingWidth,
     * #yCoolingWidth, #zCoolingLower, and #zCoolingUpper.
     */
    Borders
};
}

namespace pressurecontrol {
enum Mode {
    None,  //!< \brief Don't use the pressure control
    /*!
     * \brief Scale the cell dimensions individually, trying to reach the same pressure in all directions.
     */
    Isobaric,
    /*!
     * \brief Scale the cell dimensions with the same multipliers, conserving the shape of the cell.
     */
    Isotropic
};
}

namespace interaction {
enum Mode { Kill = -1, None = 0, Potmode = 1, Pair = 2 };
}  // namespace interaction

namespace beam {
enum Type {
    Broad,   //!< \brief Broad beam. Ions are introduced homogeniously on a face of the simulation cell.
    Focused  //!< \brief Focused beam. Ions are introduced on a specified area.
};
enum Shape {
    Circular,  //!< \brief Circular beam shape
    Square     //!< \brief Square beam shape
};
enum Face {
    X_lower,  //!< \brief The face parallel to the yz-plane with the higher x-coordinate value
    X_upper,  //!< \brief The face parallel to the yz-plane with the higher x-coordinate value
    Y_lower,  //!< \brief The face parallel to the xz-plane with the higher y-coordinate value
    Y_upper,  //!< \brief The face parallel to the xz-plane with the higher y-coordinate value
    Z_lower,  //!< \brief The face parallel to the xy-plane with the higher z-coordinate value
    Z_upper   //!< \brief The face parallel to the xy-plane with the higher z-coordinate value
};
}  // namespace beam

namespace filetype {
enum Type { Reppot, EAM, Elstop };
}

}  // namespace enums
}  // namespace api

#endif  // ENUMS_H
