/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "filereadwidget.h"
#include "ui_filereadwidget.h"

#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>

#include <glm/vec3.hpp>

FileReadWidget::FileReadWidget(QWidget* parent) : QWidget(parent), ui(new Ui::FileReadWidget) {
    ui->setupUi(this);

    ui->textEdit->setReadOnly(true);

    units["m"] = Unit("m", 1e10);
    units["nm"] = Unit("nm", 10);
    units["Å"] = Unit("nm", 1);
    units["m/s"] = Unit("nm", 1e-5);
    units["Å/fs"] = Unit("nm", 1);

    QVector<QString> positionUnits = {"m", "nm", "Å", "Other"};
    QVector<QString> velocityUnits = {"m/s", "Å/fs", "Other"};

    variables[Variable::None] = Variable(Variable::None, "");
    variables[Variable::Position] =
        Variable(Variable::Position, "Position", {Component::X, Component::Y, Component::Z}, positionUnits);
    variables[Variable::Velocity] =
        Variable(Variable::Velocity, "Velocity", {Component::X, Component::Y, Component::Z}, velocityUnits);
    variables[Variable::Type] = Variable(Variable::Type, "Particle Type");
    variables[Variable::Symbol] = Variable(Variable::Symbol, "Element Symbol");
    variables[Variable::Id] = Variable(Variable::Id, "Particle Id");

    availableVariables[FileType::XYZ] = {Variable::None, Variable::Position, Variable::Velocity,
                                         Variable::Type, Variable::Symbol,   Variable::Id};
    availableVariables[FileType::Lammps] = {Variable::None, Variable::Position, Variable::Type, Variable::Id};
    availableVariables[FileType::Other] = {Variable::None, Variable::Position, Variable::Velocity,
                                           Variable::Type, Variable::Symbol,   Variable::Id};

    ui->gridLayout->setColumnMinimumWidth(2, 30);
    ui->gridLayout->setColumnMinimumWidth(3, 30);

    ui->firstLineWidget->hide();
    ui->columnsGB->hide();

    connect(ui->browsePB, &QPushButton::clicked, this, &FileReadWidget::browseFile);
    connect(ui->filepathLE, &QLineEdit::textChanged, this, &FileReadWidget::changeFilePath);
    connect(ui->filepathLE, &QLineEdit::editingFinished, this, &FileReadWidget::openFile);
    connect(ui->firstLineSB, QOverload<int>::of(&QSpinBox::valueChanged), [this] {
        ui->firstLineSB->setValue(std::min(ui->firstLineSB->value(), selectedFile.lines().count()));
        int val = ui->firstLineSB->value();
        int oldColumns = selectedFile.columns();
        selectedFile.setAtomsLine(val - 1);
        ui->atomNumSB->setValue(std::min(ui->atomNumSB->value(), selectedFile.lines().count() - val + 1));
        updateText();
        if (oldColumns < selectedFile.columns()) writeTable();
    });

    connect(ui->atomNumSB, QOverload<int>::of(&QSpinBox::valueChanged), [this](int i) {
        ui->atomNumSB->setValue(std::min(i, selectedFile.lines().count() - ui->firstLineSB->value() + 1));
        int oldColumns = selectedFile.columns();
        selectedFile.setAtoms(ui->atomNumSB->value());
        updateText();
        if (oldColumns < selectedFile.columns()) writeTable();
    });


    ui->velocitiesGB->setVisible(false);
    ui->velocityUnitsLE->setVisible(false);
    ui->velocityUnitsLabel->setVisible(false);

    ui->velocityUnitsCB->addItems(QStringList(velocityUnits.toList()));
    ui->velocityUnitsLabel->setText("m/s");

    connect(ui->velocityUnitsCB, &QComboBox::currentTextChanged, [this](QString s) {
        ui->velocityUnitsLE->setVisible(s == "Other");
        ui->velocityUnitsLabel->setVisible(s == "Other");
    });

    connect(ui->velocityUnitsLE, &QLineEdit::editingFinished, [this] {
        if (ui->velocityUnitsLE->text().toDouble() == 0) ui->velocityUnitsLE->setText("0.0");
    });
}

FileReadWidget::~FileReadWidget() {
    delete ui;
}

QString FileReadWidget::filePath() {
    return ui->filepathLE->text();
}

void FileReadWidget::browseFile() {
    QString path = QFileDialog::getOpenFileName(this, tr("Choose a file"), QDir::homePath());
    if (path.length() != 0) {
        changeFilePath(path);
        openFile();
    }
}

void FileReadWidget::changeFilePath(QString path) {
    if (ui->filepathLE->text() != path) {
        ui->filepathLE->blockSignals(true);
        ui->filepathLE->setText(path);
        ui->filepathLE->blockSignals(false);
    }

    if (File(path).exists()) {
        ui->filepathLE->setStyleSheet("color : black");
    } else {
        ui->filepathLE->setStyleSheet("color : red");
    }

    emit readyToRead(false);
    ui->columnsGB->setEnabled(false);
}


void FileReadWidget::openFile() {
    selectedFile = File(ui->filepathLE->text());
    updateText();
    writeTable();

    ui->atomNumSB->blockSignals(true);
    ui->firstLineSB->blockSignals(true);
    bool showFirstLine = (selectedFile.type() == Other) && (selectedFile.lines().count() != 0);
    if (showFirstLine) {
        ui->atomNumSB->setMaximum(selectedFile.lines().count() * 10);
        ui->atomNumSB->setValue(selectedFile.atoms());
        ui->firstLineSB->setMaximum(selectedFile.lines().count() * 10);
        ui->firstLineSB->setValue(std::max(selectedFile.atomsLine(), 1));
    }
    ui->firstLineWidget->setVisible(showFirstLine);
    ui->atomNumSB->blockSignals(!showFirstLine);
    ui->firstLineSB->blockSignals(!showFirstLine);

    ui->columnsGB->setEnabled(true);

    ui->velocitiesGB->setVisible(selectedFile.velocitiesLine() != -1);

    emit readyToRead(selectedFile.isOpen());
}

/*!
 * Reinitializes the dialog.
 */
void FileReadWidget::closeFile() {
    emit readyToRead(false);
    ui->textEdit->setText("");
    qDeleteAll(tableWidgets);
    tableWidgets.clear();
    columns.clear();
    ui->columnsGB->hide();
    ui->firstLineWidget->hide();
}

/*!
 * Changes the color of the lines in ui->textEdit that are not part of the atom data.
 */
void FileReadWidget::updateText() {
    ui->textEdit->clear();

    if (!selectedFile.exists()) return;

    int atoms, atomsLine;

    if (selectedFile.type() == FileType::Other) {
        atoms = ui->atomNumSB->value();
        atomsLine = ui->firstLineSB->value() - 1;
    } else {
        atoms = selectedFile.atoms();
        atomsLine = selectedFile.atomsLine();
    }

    const QStringList& lines = selectedFile.lines();
    int line = 0;
    int atomsBlock = 0;

    QString text = "";
    ui->textEdit->setTextColor(Qt::gray);
    for (int i = std::max(0, atomsLine - 1000);; i++) {
        if (i == atomsLine) {
            ui->textEdit->append(text);
            if (atoms != 0) {
                ui->textEdit->setTextColor(Qt::black);
            }
            atomsBlock = line;
            text = "";
        } else if (i == atomsLine + 100) {
            ui->textEdit->append(text);
            ui->textEdit->setTextColor(Qt::gray);
            ui->textEdit->append("...");
            break;
        } else if (i >= lines.length()) {
            ui->textEdit->append(text);
            break;
        } else if (i == atomsLine + atoms) {
            ui->textEdit->append(text);
            ui->textEdit->setTextColor(Qt::gray);
            text = "";
        } else if (i != 0) {
            text += "\n";
        }
        text += lines[i];
        line++;
    }

    // Change the view to where the atom data begins.
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start);
    cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, std::max(0, atomsBlock - 2));
    ui->textEdit->setTextCursor(cursor);
}

void FileReadWidget::writeTable() {
    qDeleteAll(tableWidgets);
    tableWidgets.clear();
    columns.clear();

    if (!selectedFile.exists()) return;

    QVector<ColumnData> initData = defaultColumnData();

    for (int i = 0; i < initData.count(); ++i) {
        Column c;
        const ColumnData& data = initData[i];
        c.variable = new QComboBox(this);
        for (const Variable::VariableType t : availableVariables[selectedFile.type()]) {
            const Variable& v = variables[t];
            c.variable->addItem(v.name, v.type);
        }

        c.component = new QComboBox(this);

        c.unit = new QComboBox(this);
        QLineEdit* customFactor = new QLineEdit(this);
        c.customFactor = customFactor;
        c.customUnit = new QLabel(this);

        ui->gridLayout->addWidget(c.variable, i, 0);
        ui->gridLayout->addWidget(c.component, i, 1);
        ui->gridLayout->addWidget(c.unit, i, 2);
        ui->gridLayout->addWidget(c.customFactor, i, 3);
        ui->gridLayout->addWidget(c.customUnit, i, 4);

        columns.push_back(c);
        c.variable->setCurrentIndex(c.variable->findData(data.variable));
        updateColumnVariable(i);
        c.component->setCurrentIndex(c.component->findData(data.component));
        c.unit->setCurrentText(data.unit);
        updateColumnUnit(i);

        connect(c.variable, QOverload<int>::of(&QComboBox::currentIndexChanged),
                [this, i] { updateColumnVariable(i); });
        connect(c.unit, QOverload<int>::of(&QComboBox::currentIndexChanged), [this, i] { updateColumnUnit(i); });
        connect(customFactor, &QLineEdit::editingFinished, [customFactor] {
            double d = customFactor->text().toDouble();
            if (d == 0) customFactor->setText("0.0");
        });

        tableWidgets << c.variable << c.component << c.unit << c.customFactor << c.customUnit;
    }
    ui->columnsGB->setVisible(initData.count() > 0);
}

QVector<FileReadWidget::ColumnData> FileReadWidget::defaultColumnData() {
    int cols = selectedFile.columns();
    QVector<ColumnData> def;
    for (int i = 0; i < cols; ++i) {
        def.push_back(ColumnData());
    }

    if (selectedFile.type() == XYZ) {
        if (cols >= 1) def[0] = ColumnData(Variable::Symbol);

        if (cols >= 4) {
            def[1] = ColumnData(Variable::Position, Component::X, "Å");
            def[2] = ColumnData(Variable::Position, Component::Y, "Å");
            def[3] = ColumnData(Variable::Position, Component::Z, "Å");
        }

        if (cols >= 5) def[4] = ColumnData(Variable::Type);

        if (cols >= 6) def[5] = ColumnData(Variable::Id);

        if (cols >= 9) {
            def[6] = ColumnData(Variable::Velocity, Component::X, "Å/fs");
            def[7] = ColumnData(Variable::Velocity, Component::Y, "Å/fs");
            def[8] = ColumnData(Variable::Velocity, Component::Z, "Å/fs");
        }
    }

    else if (selectedFile.type() == Lammps) {
        if (cols >= 1) def[0] = ColumnData(Variable::Id);
        if (cols >= 2) def[1] = ColumnData(Variable::Type);
        if (cols >= 5) {
            def[2] = ColumnData(Variable::Position, Component::X, "Å");
            def[3] = ColumnData(Variable::Position, Component::Y, "Å");
            def[4] = ColumnData(Variable::Position, Component::Z, "Å");
        }
    }

    return def;
}

QVector<FileReadWidget::ColumnData> FileReadWidget::currentColumnData() {
    QVector<ColumnData> cur;
    for (const Column& c : columns) {
        ColumnData data;
        data.variable = static_cast<Variable::VariableType>(c.variable->currentData().toInt());
        if (c.component->count() == 0)
            data.component = Component::None;
        else
            data.component = static_cast<Component::ComponentType>(c.component->currentData().toInt());
        data.unit = c.unit->currentText();
        data.customUnitFactor = c.customFactor->text().toDouble();
        data.customUnitBasis = c.customUnit->text();
        cur.push_back(data);
    }

    return cur;
}

void FileReadWidget::updateColumnVariable(int i) {
    Column c = columns[i];
    if (c.variable->currentText().isEmpty()) {
        c.component->setVisible(false);
        c.unit->setVisible(false);
        c.customUnit->setVisible(false);
        c.customFactor->setVisible(false);
        return;
    }

    const Variable& v = variables[static_cast<Variable::VariableType>(c.variable->currentData().toInt())];
    c.component->setVisible(v.components.size() != 0);
    c.component->clear();
    for (const Component::ComponentType& com : v.components) {
        c.component->addItem(Component(com).toString(), com);
    }

    c.unit->setVisible(v.units.count() != 0);
    c.unit->blockSignals(true);
    c.unit->clear();
    for (const Unit& u : v.units) {
        c.unit->addItem(u.text);
    }
    c.unit->blockSignals(false);
    updateColumnUnit(i);
}

void FileReadWidget::updateColumnUnit(int i) {
    Column c = columns[i];
    if (c.unit->currentText() == "Other") {
        const Variable& v = variables[static_cast<Variable::VariableType>(c.variable->currentData().toInt())];
        c.customUnit->setText(v.units.front());
        c.customFactor->setText("1.0");
        c.customUnit->show();
        c.customFactor->show();
    } else {
        c.customUnit->hide();
        c.customFactor->hide();
    }
}

/*!
 * Finds the column indices of the variable-component pairs
 */
QHash<std::pair<FileReadWidget::Variable::VariableType, FileReadWidget::Component::ComponentType>, int>
FileReadWidget::indices(const QVector<FileReadWidget::ColumnData>& data) {

    QHash<std::pair<Variable::VariableType, Component::ComponentType>, int> mapping;

    int i = 0;
    for (const ColumnData& c : data) {
        std::pair<Variable::VariableType, Component::ComponentType> pair(c.variable, c.component);
        mapping[pair] = i;

        ++i;
    }

    return mapping;
}

QHash<std::pair<FileReadWidget::Variable::VariableType, FileReadWidget::Component::ComponentType>, double>
FileReadWidget::unitFactors(const QVector<FileReadWidget::ColumnData>& data) {
    QHash<std::pair<Variable::VariableType, Component::ComponentType>, double> fac;

    for (const ColumnData& c : data) {
        std::pair<Variable::VariableType, Component::ComponentType> pair(c.variable, c.component);
        if (units.contains(c.unit))
            fac[pair] = units[c.unit].factor;
        else if (c.unit == "Other")
            fac[pair] = c.customUnitFactor * units[c.customUnitBasis].factor;
    }

    return fac;
}

bool FileReadWidget::readEntry() {
    entry = ExtendedEntryData();
    XyzData& xyzData = entry.frame;
    TypeData& types = entry.data.types;

    QVector<ColumnData> data = currentColumnData();
    QHash<std::pair<Variable::VariableType, Component::ComponentType>, int> ind = indices(data);
    QHash<std::pair<Variable::VariableType, Component::ComponentType>, double> fac = unitFactors(data);

    int cols = 0;
    for (int i : ind.values()) cols = std::max(cols, i);

    const QStringList& lines = selectedFile.lines();
    int start = std::max(0, selectedFile.atomsLine());
    int end = std::min(lines.count(), selectedFile.atomsLine() + selectedFile.atoms());

    std::pair<Variable::VariableType, Component::ComponentType> symbol(Variable::Symbol, Component::None);
    std::pair<Variable::VariableType, Component::ComponentType> posX(Variable::Position, Component::X);
    std::pair<Variable::VariableType, Component::ComponentType> posY(Variable::Position, Component::Y);
    std::pair<Variable::VariableType, Component::ComponentType> posZ(Variable::Position, Component::Z);
    std::pair<Variable::VariableType, Component::ComponentType> type(Variable::Type, Component::None);
    std::pair<Variable::VariableType, Component::ComponentType> id(Variable::Id, Component::None);
    std::pair<Variable::VariableType, Component::ComponentType> vX(Variable::Velocity, Component::X);
    std::pair<Variable::VariableType, Component::ComponentType> vY(Variable::Velocity, Component::Y);
    std::pair<Variable::VariableType, Component::ComponentType> vZ(Variable::Velocity, Component::Z);

    bool readAllAsOneType = (!ind.contains(symbol) && !ind.contains(type));
    bool readSymbolAsType = (ind.contains(symbol) && !ind.contains(type));
    bool readTypeAsSymbol = (!ind.contains(symbol) && ind.contains(type));

    QHash<QString, int> symbolToType;

    // Reading atom data
    for (int i = start; i < end; ++i) {
        QStringList list = lines[i].split(QRegExp("\\s+"), QString::SkipEmptyParts);
        if (lines.count() < cols) {
            qDebug() << "Not enough columns on line" << (i + 1) << "of file" << selectedFile.path();
            xyzData = XyzData();
            return false;
        }

        Atom a;

        if (ind.contains(symbol))
            a.symbol = list[ind[symbol]];
        else if (readAllAsOneType)
            a.symbol = "";
        else if (readTypeAsSymbol)
            a.symbol = QString::number(abs(list[ind[type]].toInt()));

        if (ind.contains(posX))
            a.position.x = list[ind[posX]].toDouble() * fac[posX];
        else
            a.position.x = 0;

        if (ind.contains(posY))
            a.position.y = list[ind[posY]].toDouble() * fac[posY];
        else
            a.position.y = 0;

        if (ind.contains(posZ))
            a.position.z = list[ind[posZ]].toDouble() * fac[posZ];
        else
            a.position.z = 0;

        if (ind.contains(type))
            a.type = list[ind[type]].toInt();
        else if (readSymbolAsType) {
            if (!symbolToType.contains(a.symbol)) symbolToType[a.symbol] = symbolToType.count();
            a.type = symbolToType[a.symbol];
        } else if (readAllAsOneType) {
            a.type = 1;
        }

        if (ind.contains(id))
            a.identifier = list[ind[id]].toInt();
        else
            a.identifier = xyzData.atoms.size() + 1;

        if (ind.contains(vX))
            a.velocity.x = list[ind[vX]].toDouble() * fac[vX];
        else
            a.velocity.x = 0;

        if (ind.contains(vY))
            a.velocity.y = list[ind[vY]].toDouble() * fac[vY];
        else
            a.velocity.y = 0;

        if (ind.contains(vZ))
            a.velocity.z = list[ind[vZ]].toDouble() * fac[vZ];
        else
            a.velocity.z = 0;


        xyzData.atoms.push_back(a);
        xyzData.amounts[a.type] += 1;
        if (i == start)
            xyzData.atomLims = Box(a.position.x, a.position.x, a.position.y, a.position.y, a.position.z, a.position.z);
        else {
            xyzData.atomLims.x1 = std::min(xyzData.atomLims.x1, a.position.x);
            xyzData.atomLims.x2 = std::max(xyzData.atomLims.x2, a.position.x);
            xyzData.atomLims.y1 = std::min(xyzData.atomLims.y1, a.position.y);
            xyzData.atomLims.y2 = std::max(xyzData.atomLims.y2, a.position.y);
            xyzData.atomLims.z1 = std::min(xyzData.atomLims.z1, a.position.z);
            xyzData.atomLims.z2 = std::max(xyzData.atomLims.z2, a.position.z);
        }

        if (!types.typeList.contains(a.type)) {
            AtomType t;
            t.typeNumber = a.type;
            ElementReader elementReader;
            t.atomNumber = elementReader.zFromSymbol(a.symbol);
            t.symbol = a.symbol;
            types.typeList.insert(a.type, t);
        }
    }


    // Calculating the conversion factor for the cell dimensions
    double factor = 1;
    if (fac.contains(posX))
        factor = fac[posX];
    else if (fac.contains(posY))
        factor = fac[posY];
    else if (fac.contains(posZ))
        factor = fac[posZ];

    setCellLimits(factor);

    setMasses();

    readVelocities();  // Reading  the velocities if they were found from the Lammps data file

    return true;
}


void FileReadWidget::setMasses() {
    if (!selectedFile.isOpen()) return;

    QSet<int> foundFromFile;

    if (selectedFile.type() == Lammps) {
        int massesLine = -1;
        const QStringList& lines = selectedFile.lines();
        for (int i = 0; i < lines.count(); ++i) {
            QStringList words = lines[i].split(QRegExp("\\s+"), QString::SkipEmptyParts);
            if ((words.count() >= 1) && (words[0].compare("Masses", Qt::CaseInsensitive) == 0)) {
                massesLine = i;
                break;
            }
        }

        if (massesLine != -1) {
            for (int i = massesLine + 2; i < lines.count(); ++i) {
                QStringList words = lines[i].split(QRegExp("\\s+"), QString::SkipEmptyParts);
                if (words.count() < 2) break;
                int type = words[0].toInt();
                double mass = words[1].toDouble();
                if (type == 0) break;
                if (entry.data.types.typeList.contains(type)) {
                    entry.data.types.typeList[type].mass = mass;
                    foundFromFile.insert(type);
                }
            }
        }
    }

    ElementReader reader;
    for (auto i = entry.data.types.typeList.begin(); i != entry.data.types.typeList.end(); ++i) {
        if (!foundFromFile.contains(i.value().typeNumber)) {
            i.value().mass = reader.getMass(reader.zFromSymbol(i.value().symbol));
        }
    }
}

void FileReadWidget::setCellLimits(double factor) {
    XyzData& xyzData = entry.frame;

    bool foundFromFile = false;
    if (selectedFile.type() == XYZ) {
        if (selectedFile.lines().count() >= 2) {
            QStringList words = selectedFile.lines()[1].split(QRegExp("\\s+"), QString::SkipEmptyParts);
            for (int i = 0; i < words.count() - 3; ++i) {
                if (words[i].compare("boxsize", Qt::CaseInsensitive) == 0) {
                    double dx = words[i + 1].toDouble() * factor;
                    double dy = words[i + 2].toDouble() * factor;
                    double dz = words[i + 3].toDouble() * factor;
                    xyzData.cellLims = Box(-dx / 2, dx / 2, -dy / 2, dy / 2, -dz / 2, dz / 2);
                    foundFromFile = true;
                }
            }
        }
    } else if (selectedFile.type() == Lammps) {
        bool xFound = false;
        bool yFound = false;
        bool zFound = false;
        xyzData.cellLims = Box(0, 0, 0, 0, 0, 0);
        for (const QString& line : selectedFile.lines()) {
            QStringList words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
            if (words.count() >= 4) {
                if ((words[2].compare("xlo", Qt::CaseInsensitive) == 0) &&
                    (words[3].compare("xhi", Qt::CaseInsensitive) == 0)) {
                    xyzData.cellLims.x1 = words[0].toDouble() * factor;
                    xyzData.cellLims.x2 = words[1].toDouble() * factor;
                    xFound = true;
                } else if ((words[2].compare("ylo", Qt::CaseInsensitive) == 0) &&
                           (words[3].compare("yhi", Qt::CaseInsensitive) == 0)) {
                    xyzData.cellLims.y1 = words[0].toDouble() * factor;
                    xyzData.cellLims.y2 = words[1].toDouble() * factor;
                    yFound = true;
                } else if ((words[2].compare("zlo", Qt::CaseInsensitive) == 0) &&
                           (words[3].compare("zhi", Qt::CaseInsensitive) == 0)) {
                    xyzData.cellLims.z1 = words[0].toDouble() * factor;
                    xyzData.cellLims.z2 = words[1].toDouble() * factor;
                    zFound = true;
                }
                if (xFound && yFound && zFound) break;
            }
            foundFromFile = xFound || yFound || zFound;
        }
    }

    if (!foundFromFile) xyzData.cellLims = xyzData.atomLims;
}

void FileReadWidget::readVelocities() {
    if (!selectedFile.isOpen()) return;

    if (selectedFile.type() == Lammps && selectedFile.velocitiesLine() != -1) {
        double factor = 1;
        if (units.contains(ui->velocityUnitsCB->currentText())) {
            factor = units[ui->velocityUnitsCB->currentText()].factor;
        } else {
            factor = ui->velocityUnitsLE->text().toDouble() * units[ui->velocityUnitsLabel->text()].factor;
        }

        QHash<int, glm::dvec3> velocities;
        const QStringList& lines = selectedFile.lines();
        for (int i = selectedFile.velocitiesLine(); i < lines.count(); ++i) {
            QStringList words = lines[i].split(QRegExp("\\s+"), QString::SkipEmptyParts);
            if (words.count() < 4) break;
            int id = words[0].toInt();
            if (id == 0) break;
            velocities[id] =
                glm::dvec3(words[1].toDouble() * factor, words[2].toDouble() * factor, words[3].toDouble() * factor);
        }

        for (Atom& a : entry.frame.atoms) {
            if (velocities.contains(a.identifier)) a.velocity = velocities[a.identifier];
        }
    }
}


bool FileReadWidget::readyforNext() {
    return selectedFile.isOpen();
}


// Functions of the member class File

/*!
 * Counts the colums of the file
 */
void FileReadWidget::File::countColumns() {
    columns_m = 0;
    if (!fileParsed) parseFile();

    if (lines_m.length() < 3) {
        return;
    }

    if (type_m != Other) {
        if (atomsLine_m >= 0 && atomsLine_m < lines_m.length())
            columns_m = lines_m[atomsLine_m].split(QRegExp("\\s+"), QString::SkipEmptyParts).count();
    } else {
        for (int i = 0; i < std::min(50, lines_m.length()); ++i) {
            columns_m = std::max(columns_m, lines_m[i].split(QRegExp("\\s+"), QString::SkipEmptyParts).count());
        }
    }
}

/*!
 * Finds the type of the file.
 */
void FileReadWidget::File::findType() {
    if (QFile(path_m).fileName().contains(".xyz")) {
        type_m = FileType::XYZ;
    } else if (isLammps()) {
        type_m = FileType::Lammps;
    } else {
        type_m = FileType::Other;
    }
    typeFound = true;
}

/*!
 * Checks if the file is (maybe) LAMMPS data file.
 * Makes use of the fact that the 3rd line of a LAMMPS data file has to be "<integer> atoms".
 */
bool FileReadWidget::File::isLammps() {
    if (!linesRead) readLines();

    if (lines_m.length() < 3) return false;
    QStringList words = lines_m[2].split(QRegExp("\\s+"), QString::SkipEmptyParts);
    if (words.length() >= 2 && words[1].toLower() == "atoms" && words[0].toInt() != 0) {
        return true;
    }
    return false;
}

/*!
 * Reads the lines in the file.
 */
void FileReadWidget::File::readLines() {
    QFile inputFile(path_m);
    lines_m.clear();
    if (exists() && inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        while (!in.atEnd()) {
            QString line = in.readLine();
            lines_m << line;
        }
    }
    linesRead = true;
}

/*!
 * Finds the number of atoms and the line on which the atom data starts.
 */
void FileReadWidget::File::parseFile() {
    if (!typeFound) findType();
    if (!linesRead) readLines();

    atomsLine_m = -1;
    velocitiesLine_m = -1;

    if (lines_m.length() < 3) return;
    if (type_m == FileType::XYZ) {
        atoms_m = lines_m[0].trimmed().toInt();
        atomsLine_m = 2;
    } else if (type_m == FileType::Lammps) {
        atoms_m = lines_m[2].split(QRegExp("\\s+"), QString::SkipEmptyParts)[0].toInt();
        atoms_m = lines_m[2].split(QRegExp("\\s+"), QString::SkipEmptyParts)[0].toInt();
        for (int i = 0; i < lines_m.length() - 2; i++) {
            if (lines_m[i] == "") continue;
            QString word = lines_m[i].split(QRegExp("\\s+"), QString::SkipEmptyParts)[0];
            bool nextRowEmpty = lines_m[i + 1].isEmpty();
            if (word.compare("atoms", Qt::CaseInsensitive) == 0 && nextRowEmpty) {
                atomsLine_m = i + 2;
                i += atoms_m;
            } else if (word.compare("velocities", Qt::CaseInsensitive) == 0 && nextRowEmpty) {
                velocitiesLine_m = i + 2;
            }
            if (atomsLine_m != -1 && velocitiesLine_m != -1) break;
        }
    }
    fileParsed = true;
}
