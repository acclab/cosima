/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PROGRAMSETTINGS_H
#define PROGRAMSETTINGS_H

#include "backendsettings.h"
#include "locationsettings.h"
#include "visualsettings.h"

/*!
 * \brief A singleton class for program level settings.
 *
 * Contains settings that are common for all projects, such as the visual settings of the plots, back end settings, and
 * locations of directories related to COSIRMA.
 */
class ProgramSettings {
  public:
    static ProgramSettings& getInstance();

    QByteArray& windowGeometry();
    void setWindowGeometry(const QByteArray& geomtery);

    BackendSettings& backendSettings();
    void setBackendSettings(const BackendSettings& settings);

    LocationSettings& locationSettings();
    void setLocationSettings(const LocationSettings& settings);

    VisualSettings& visualSettings();
    void setVisualSettings(const VisualSettings& s);

    void load();
    void save();

  private:
    ProgramSettings() {
        qRegisterMetaTypeStreamOperators<BackendSettings>("BackendSettings");
        qRegisterMetaTypeStreamOperators<VisualSettings>("VisualSettings");
        qRegisterMetaTypeStreamOperators<LocationSettings>("LocationSettings");
    }

    BackendSettings m_backendSettings;
    VisualSettings m_visualSettings;
    LocationSettings m_locationSettings;

    QByteArray m_windowGeometry;
};

#endif  // PROGRAMSETTINGS_H
