/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ANIMATEDBLOCK_H
#define ANIMATEDBLOCK_H

#include "api/entry.h"
#include "api/geometries.h"
#include "api/visualsettings.h"

#include "gui/widgets/plotcellglwidget.h"

#include <QTimer>
#include <QWidget>


// Example:
//  AnimatedBlock *block = new AnimatedBlock();
//  block->draw(atoms);

/*!
 * \brief QWidget that shows a rotating plot of atoms using PlotCellGLWidget.
 *
 * \sa PlotCellGLWidget
 * \todo Inherit PlotCell and manipulate rotation through the GL widget.
 */
class AnimatedBlock : public QWidget {
    Q_OBJECT

  public:
    explicit AnimatedBlock(QWidget* parent = nullptr);
    ~AnimatedBlock() {}
    void draw(const XyzData&, const VisualSettings&);

  private:
    PlotCellGLWidget* m_plotWidget;

    bool firstPlot = true;
    QTimer* timer;

  private slots:
    void rotateCamera();
};

#endif  // ANIMATEDBLOCK_H
