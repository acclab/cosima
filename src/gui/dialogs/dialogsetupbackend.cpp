/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "dialogsetupbackend.h"
#include "ui_dialogsetupbackend.h"

#include <QDebug>
#include <QFileDialog>


DialogSetupBackend::DialogSetupBackend(QWidget* parent) : DialogSetupBackend(parent, "") {}

DialogSetupBackend::DialogSetupBackend(QWidget* parent, QString parcasPath)
        : QDialog(parent), ui(new Ui::DialogSetupBackend) {
    ui->setupUi(this);

    if (parcasPath.isEmpty()) {
        tryLocateParcasFile();
    } else {
        setParcasPath(parcasPath);
    }

    connect(ui->pushButton_parcasExecutableLocate, &QPushButton::clicked, this,
            &DialogSetupBackend::locateParcasDialog);
}

DialogSetupBackend::~DialogSetupBackend() {
    delete ui;
}

void DialogSetupBackend::locateParcasDialog() {
    QFileDialog dialog(this);
    dialog.setOption(QFileDialog::DontUseNativeDialog, true);
    dialog.setFileMode(QFileDialog::ExistingFile);

    QStringList fileNames;
    if (dialog.exec()) fileNames = dialog.selectedFiles();

    if (!fileNames.isEmpty()) {
        qDebug() << fileNames[0];
        setParcasPath(fileNames[0]);
    }
}


bool DialogSetupBackend::checkParcasFile(QString path) {
    QFile parcasFile(path);
    return parcasFile.exists();
}


void DialogSetupBackend::tryLocateParcasFile() {
    qDebug() << "Look for PARCAS";

    // Check the default location for an installation of PARCAS
    QString checkPath = QCoreApplication::applicationDirPath() + "/bin/parcas";
    bool found = checkParcasFile(checkPath);

    if (!found) {
        // Loop over these locations, if the default location doesn't have a link to PARCAS
        QStringList dirs;
        dirs << "bin"
             << "bin/parcas"
             << "parcas"
             << "Parcas"
             << "PARCAS";

        for (QString dir : dirs) {
            checkPath = QDir::homePath() + "/" + dir + "/parcas";
            qDebug() << "    Looking at: " << checkPath;
            if (checkParcasFile(checkPath)) {
                found = true;
                break;
            }
        }
    }

    if (found) {
        qDebug() << "found parcas in " << checkPath;
        setParcasPath(checkPath);
    }
}


void DialogSetupBackend::setParcasPath(QString path) {
    ui->lineEdit_parcasExecutable->setText(path);
}

QString DialogSetupBackend::parcasPath() {
    return ui->lineEdit_parcasExecutable->text();
}
