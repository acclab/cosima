/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "potential.h"

/*!
 * \brief Constructs a potential with given attributes.
 * \param name Name of the potential (e.g. "Watanbe")
 * \param mode Mode of the potetial. The PARCAS "potemode" parameter of the potential.
 * \param supportedPairs Element pairs supported by the potential
 * \param type Type of the potential. Tells what kind of potential files are needed for the potential.
 * \param information Information on the potential (e.g citation)
 */
Potential::Potential(const QString& name, int mode, const QList<AtomPair>& supportedPairs, Formalism formalism,
                     InputType file, const QString& information) {
    m_potentialName = name;
    m_potmode = mode;
    m_pairs = supportedPairs;
    m_formalism = formalism;
    m_potFile = file;

    m_info = information + "\n";
    for (AtomPair a : m_pairs) {
        m_info += a.element1 + "\t" + a.element2;
        if (a.information.isEmpty()) {
            m_info += "\n";
        } else {
            m_info += "\t" + a.information + "\n";
        }
    }

    QStringList supportedElements;
    for (AtomPair pair : m_pairs) {
        supportedElements << pair.element1 << pair.element2;
    }
    m_elements = supportedElements.toSet();
}

Potential::Potential(int mode, const Potential& other, const QString& info) {
    m_potentialName = other.m_potentialName;
    m_potmode = mode;  //! Set different mode.
    m_pairs = other.m_pairs;
    m_formalism = other.m_formalism;
    m_potFile = other.m_potFile;

    if (info.isNull()) {
        m_info = other.m_info;
    } else {
        m_info = info + "\n";
        for (AtomPair a : m_pairs) {
            m_info += a.element1 + "\t" + a.element2;
            if (a.information.isEmpty()) {
                m_info += "\n";
            } else {
                m_info += "\t" + a.information + "\n";
            }
        }
    }

    m_elements = other.m_elements;
}

/*!
 * \brief Potential::supportsPair
 * \param element1 The symbol of the first element (e.g. "Si")
 * \param element2 The symbol of the second element (e.g. "O")
 * \return \c true if the potential supports both elements
 */
bool Potential::supportsPair(const QString& element1, const QString& element2) const {
    if (m_pairs.empty()) {
        return true;
    }
    return m_pairs.contains(AtomPair(element1, element2));
}

/*!
 * \brief Name of the potential (e.g. "Watanabe")
 * \return Name string
 */
const QString& Potential::name() const {
    return m_potentialName;
}

/*!
 * \brief Mode of the potetial
 *
 * The PARCAS "potemode" parameter of the potential
 *
 * \return Potential mode integer
 */
int Potential::mode() const {
    return m_potmode;
}

/*!
 * \brief Elements supported by the potential
 * \return Set of element symbols
 */
const QSet<QString>& Potential::supportedElements() const {
    return m_elements;
}


/*!
 * \brief Potential::formalism
 *
 * If a formalism is assigned to the potential, it can potentially alter the visuals of some settings when the potential
 * is used.
 *
 * \return The formalism of the current potential.
 */
Potential::Formalism Potential::formalism() const {
    return m_formalism;
}

/*!
 * \brief File type of the potential
 *
 * FileType tells what kind of potential files are needed for the potential.
 *
 * \return
 */
Potential::InputType Potential::fileType() const {
    return m_potFile;
}

QList<Potential::AtomPair> Potential::pairs() {
    return m_pairs;
}

/*!
 * \brief Information on the potential (e.g citation)
 * \return String of information
 */
const QString& Potential::information() const {
    return m_info;
}
