/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "temperaturecontrolparameters.h"

#include "api/state/parameters/parameterkeys.h"


api::TemperatureControlParameters::TemperatureControlParameters() {
    setup();
}

void api::TemperatureControlParameters::setupInitialValues() {
    using namespace api::parameters::temperature;

    set(MODE, api::value_t(0, true));
    set(X_COOLING_ON, api::value_t(false, true));
    set(Y_COOLING_ON, api::value_t(false, true));
    set(Z_COOLING_ON, api::value_t(false, true));
    set(X_COOLING_WIDTH, api::value_t(0.0, true));
    set(Y_COOLING_WIDTH, api::value_t(0.0, true));
    set(Z_COOLING_UPPER, api::value_t(0.0, true));
    set(Z_COOLING_LOWER, api::value_t(0.0, true));
    set(QUENCH_ON, api::value_t(false, true));
    set(QUENCH_START_TIME, api::value_t(0.0, true));
    set(QUENCH_TARGET_TEMPERATURE, api::value_t(0.0, true));
    set(QUENCH_RATE, api::value_t(0.1, true));
    set(TARGET_TEMPERATURE, api::value_t(0.0, true));
    set(DAMPING, api::value_t(200.0, true));
}
