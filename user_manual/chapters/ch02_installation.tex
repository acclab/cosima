\chapter{Requirements}

PARCAS v5.23 from the \texttt{speedup} branch\footnote{\protect\url{https://gitlab.com/acclab/parcas/-/tree/speedup}} is used as backend for the MD simulations, and \programname{} cannot run any simulations without PARCAS. Please, refer to the building instructions in the PARCAS package for how to compile and set up PARCAS.

A reference to the PARCAS executable can be set in the backend settings (\cref{sec:backend-settings}) to run the simulations locally. Alternatively one can export the simulation environment as a standalone simulation, and set the PARCAS executable path in the run script.

\programname{} uses the OpenGL 3.0 core profile to visualise the atom structures with the \emph{inspector} (\cref{sec:inspector}). All modern computers (2010+) should have support for the OpenGL 3.0 core profile without any extra actions needed.


\section{Installation of \programname{}}


\subsection{Pre-built executable}

Download the latest version from \url{https://gitlab.com/acclab/cosirma/-/releases}.

If the pre-built binaries doesn't work for you, feel free to follow the instructions below to download the source code with git and compile your own version of the code.


\subsection{Compile from source}

The newest, stable, release of the sources can be accessed from the GIT-repository on GitLab\footnote{\url{https://gitlab.com/acclab/cosirma}}. Make sure to install all the necessary development tools for building C++ applications with Qt on your system (e.g. compilers, git, make, etc). The lists below will guide you in the right direction.


\textbf{Compilation}

The easiest way to compile \programname{}, is by opening the project in QtCreator, setting up a build profile (kit) containing links to the installed development tools (g++ and make), and use the QtCreator hot-keys.

Alternatively, it is possible to run \texttt{qmake} (included in the Qt 5.7+ package) in the \texttt{src} folder, containing the \texttt{cosirma.pro} file. Followed by \texttt{make} to generate the executable.

\Cref{sec:compilation-linux-support,sec:compilation-windows-support} lists necessary packages for Linux and Windows, respectively.


% To get more control over the simulation we provide a \emph{frame processor utility tool}. This tool is a small standalone application that is used by the process running the simulation. PARCAS is not integrated into the graphical interface, meaning it is only controlled through input files and launched as a separate process. We have plans on allowing running the simulations remotely, hence, it wouldn't make sense to tie PARCAS into the locally running \programname{}. For this reason we need a small utility tool to handle the intermediate steps between the different executions of PARCAS. The utility tool can be built in the same manner as the main program of \programname{}.


\pagebreak
\subsubsection{Linux support}
\label{sec:compilation-linux-support}

\textbf{Software needed to compile the project:}

\renewcommand{\arraystretch}{1.2}

\hfill\begin{tabularx}{0.95\textwidth}{p{3cm} X}
  \textbf{Qt 5.7}\footnote{\url{https://www.qt.io/}} or newer      & Contains the Qt libraries and headers for making the Graphical User Interface. \\
  \textbf{qMake}                                                   & Bundled with Qt and used to auto-generate C++ classes from the *.ui files. \\
  \textbf{Make}\footnote{\url{https://www.gnu.org/software/make/}} & The framework for compiling the sources with the auto-generated Makefile. \\
  \textbf{g++}\footnote{\url{https://gcc.gnu.org/}}                & The GNU C++ compiler.
\end{tabularx}


\textbf{Optional software:}

\hfill\begin{tabularx}{0.95\textwidth}{p{3cm} X}
  \textbf{QtCreator}\footnote{\url{https://www.qt.io/qt-features-libraries-apis-tools-and-ide/\#ide}} & IDE for developing Qt projects. \\
  \textbf{Doxygen}\footnote{\url{http://www.doxygen.nl/}} & Auto-generates technical documentation from the comments in the code.
\end{tabularx}



\subsubsection{Windows support}
\label{sec:compilation-windows-support}


We recommend to use build tools that generate native Windows code, as emulated posix layers usually cause a small degradation in performance. This is not noticable on the graphical interface, however, when running a longer task with PARCAS this efficiency difference will definitiely be noticable.

We used the \texttt{MSYS2}\footnote{\url{https://www.msys2.org/}} package to set up the development environment. MSYS2 provides a unix-like system that lets you install all necessary packages for Qt and QtCreator. Just choose the MSYS2 package suitable for your system (if you are on a 64 bit operating system, choose the 64 bit variant, etc). The 64 bit version can be used to build applications for 32 bit operating systems. These instructions are taken from \url{https://wiki.qt.io/MSYS2} and put here for convenience.

Install the packages for Qt and QtCreator (the \texttt{i686} is build tools for 32 bit software and the \texttt{x86\_64} is for 64 bit software). \texttt{pacman} is the a package manager that lets you install the chosen packages. Please, read up on the command flags from the documentation of \texttt{pacman} in the MSYS2 package.


\begin{enumerate}

  \item Install the necessary packages for setting up the MSYS2 development environment (restart the MSYS2 shell after the commands):
\begin{verbatim}
  pacman -Sy
  pacman --needed -S bash pacman pacman-mirrors msys2-runtime
\end{verbatim}

\item Update packages if there are any updates available (restart the MSYS2 shell after the command):
\begin{verbatim}
  pacman -Su
\end{verbatim}

\item Install the QtCreator editor:
\begin{verbatim}
  pacman -S mingw-w64-i686-qt-creator mingw-w64-x86_64-qt-creator
\end{verbatim}

\item Install the Qt libraries. This command will install the latest version of Qt.
\begin{verbatim}
  pacman -S mingw-w64-i686-qt5-static mingw-w64-x86_64-qt5-static
\end{verbatim}

\item Install \texttt{clang} and \texttt{gdb} for debugging and formatting (you might also need the \texttt{i686} versions as well, just follow the convention shown in the commands above).
\begin{verbatim}
  pacman -S mingw-w64-x86_64-clang
  pacman -S mingw-w64-x86_64-gdb
\end{verbatim}

\end{enumerate}


After you have installed the Qt dependencies QtCreator should find the compiler tools automatically and allow you to select the compiler kits when building \programname{}.

\texttt{pacman} will install the latest stable versions of Qt5 (when writing this it is 5.15). On the linux side, Qt 5.12 has been used for main development, and some of the functions used there throws deprecation warnings in later versions, just ignore these if they doesn't cause compilation errors.




\section{Installation of PARCAS}

Currently there is no ``safe'' way of providing PARCAS pre-built, depending on the operating system, a transfer of pre-built version might work, but there are no guarantees. It is also better to get used to the compilation process as this is the only way of getting PARCAS to a high-performance computing (HPC) cluster. The compilation process, doesn't take too long, and should be fairly straight forward with the instructions provided in the documentation coming with the PARCAS sources.

The sources of PARCAS is accessible from GitLab\footnote{\url{https://gitlab.com/acclab/parcas}}. Follow the documentation included with the sources.

\subsection{Linux support:}

\hfill\begin{tabularx}{0.95\textwidth}{p{3cm} X}
  \textbf{OpenMPI}\footnote{\url{https://www.open-mpi.org/}}       & Parallelisation library. \\
  \textbf{GFortran}\footnote{\url{https://gcc.gnu.org/}}           & The GNU fortran compiler. \\
  \textbf{Make}\footnote{\url{https://www.gnu.org/software/make/}} & The framework for compiling the sources with Makefiles.
\end{tabularx}


Note: PARCAS has build profiles for Cray and Intel as well, but these compilers are not free, and most people will not have access to them on local computers. We recommend using the Intel version if running on a cluster with Intel compilers as these might perform a bit better than GNU in certain scenarios.


\subsection{Windows support:}

PARCAS is currently Linux only, but we are working on a port for Windows.
