/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARCASBASECONTROLLER_H
#define PARCASBASECONTROLLER_H

#include "api/data/exportdata.h"
#include "api/entry.h"
#include "api/state/parameters/baseparameters.h"

#include <QString>


class ParcasBaseController : public AbstractStreamable {

  public:
    ParcasBaseController();
    virtual ~ParcasBaseController(){};

    virtual bool createLocalSimulation(const QString& path, const ExtendedEntryData& entry);
    virtual bool writeLocalScript(const QString& path, const ExtendedEntryData& entry) = 0;

    virtual bool createStandaloneSimulation(const QString& path, const ExtendedEntryData& entry,
                                            const ExportData& data);
    virtual bool writeStandaloneScript(const QString& path, const ExtendedEntryData& entry, const ExportData& data) = 0;

    virtual bool createSimulationEnvironment(const QString& path, const ExtendedEntryData& entry);

    virtual bool writeInputFile(const QString& path, const ExtendedEntryData& entry) = 0;

    void insertParameters(api::BaseParameters parameters);
    api::BaseParameters parameters();

    void setEntry(const ExtendedEntryData& entry);
    const ExtendedEntryData& entry();

    void setSaveMovieFile(bool save);
    bool saveMovieFile();

    virtual void setupDefaultState() { qWarning() << "This function should be overloaded!"; }

    virtual QString name() = 0;

    virtual QDataStream& save(QDataStream& out) const override;
    virtual QDataStream& load(QDataStream& in) override;


  protected:
    bool writeTemplateFile(const QString& templatePath, const QString& path,
                           const QMap<QString, QString>& parameterMap);
    bool writeFiles(const QString& path, const QMap<QString, QString>& files);

    ExtendedEntryData m_entry;
    api::BaseParameters m_parameters;
};

#endif  // PARCASBASECONTROLLER_H
