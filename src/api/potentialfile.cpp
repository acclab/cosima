/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "potentialfile.h"

/*!
 * \brief If a file path is given, checks if the file name follows PARCAS naming conventions, and if it does, saves the
 * element pair and the file type into the corresponding attribute. The validity of the name can later be checked with
 * isValid(). If no path is given, constructs a blank object.
 * \param pathToFile Path to the file
 */
PotentialFile::PotentialFile(const QString& pathToFile) {
    m_path = pathToFile;
    QStringList list = QDir(m_path).dirName().split(".");

    if (list.count() < 3) {
        m_isValid = false;
        return;
    }

    pairAndType.pair = ElementPair(list[1], list[2]);

    if (list[0] == "reppot") {
        pairAndType.type = FileType::Reppot;
    } else if (list[0] == "eam") {
        pairAndType.type = FileType::EAM;
    } else {
        pairAndType.pair = ElementPair();
        m_isValid = false;
        return;
    }

    m_isValid = true;
}

/*!
 * \brief Path to the file given in the constructor.
 * \return File path string
 */
const QString& PotentialFile::path() const {
    return m_path;
}

/*!
 * \brief Auxillary function for accessing the element pair
 * \return Element pair
 */
const ElementPair& PotentialFile::pair() const {
    return pairAndType.pair;
}

/*!
 * \brief Auxillary function for accessing the file type
 * \return File type
 */
PotentialFile::FileType PotentialFile::type() const {
    return pairAndType.type;
}

/*!
 * \brief Checks if the file specified in the constructor exists.
 * \return \c true if the file exists
 */
bool PotentialFile::exists() const {
    QFile file(m_path);
    return file.exists();
}

/*!
 * \brief Returns the validity of the file path that was given in the constructor.
 * \return \c true if the given fileName follows PARCAS naming conventions
 */
bool PotentialFile::isValid() const {
    return m_isValid;
}

/*!
 * \brief Generates a filename that follows PARCAS naming conventions based on the element pair and the file type.
 * \return Generated file name
 */
QString PotentialFile::name() const {
    switch (pairAndType.type) {
        case Reppot:
            return QString("reppot.%1.%2.in").arg(pairAndType.pair.element1, pairAndType.pair.element2);
        case EAM:
            return QString("eam.%1.%2.in").arg(pairAndType.pair.element1, pairAndType.pair.element2);
        default:
            return "";
    }
}

QDataStream& PotentialFile::save(QDataStream& out) const {
    out << m_path << pairAndType.pair << pairAndType.type << m_isValid;
    return out;
}

QDataStream& PotentialFile::load(QDataStream& in) {
    in >> m_path >> pairAndType.pair >> pairAndType.type >> m_isValid;
    return in;
}
