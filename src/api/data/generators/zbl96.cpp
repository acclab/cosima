/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "zbl96.h"

#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QtMath>


Zbl96::Zbl96() {
    readscoef();
}


void Zbl96::generate(int z1, int z2, double m1, double m2, double min, double max, double step, unsigned int flag) {

    // Extract the density (g/cm^3) and atomic density of the target element
    m_density = m_scoef[z2][5];

    double E, x;
    double sunit = NA * m_density / (m2 * 1.0e25);
    double xunit = 1.0;
    int i, n;

    switch (flag & SUNIT) {
        case EV_A:
            sunit = 100.0 * NA * m_density / (m2 * 1.0e25);
            break;
        case KEV_NM:
            sunit = NA * m_density / (m2 * 1.0e25);
            break;
        case KEV_UM:
            sunit = 1000.0 * NA * m_density / (m2 * 1.0e25);
            break;
        case MEV_MM:
            sunit = 1000.0 * NA * m_density / (m2 * 1.0e25);
            break;
        case KEV_UG_CM2:
            sunit = NA / (m2 * 1e24);
            break;
        case MEV_MG_CM2:
            sunit = NA / (m2 * 1e24);
            break;
        case KEV_MG_CM2:
            sunit = 1000.0 * NA / (m2 * 1e24);
            break;
        case EV_1E15ATOMS_CM2:
            sunit = 1.0;
            break;
    }
    switch (flag & XUNIT) {
        case EV:
            xunit = 1000.0;
            break;
        case KEV:
            xunit = 1.0;
            break;
        case MEV:
            xunit = 0.001;
            break;
        case V0:
            xunit = 1.0;
            break;
        case BETA:
            xunit = 0.0072974;
            break;
        case M_S:
            xunit = 2187673.0;
            break;
        case CM_S:
            xunit = 218767300.0;
            break;
    }


    for (x = min, i = 0; x <= max; x += step, i++)
        ;
    n = i;

    m_values.reserve(n);
    m_values.clear();

    for (x = min, i = 0; x <= max; x += step, i++) {
        //! \todo figure out what the Energy flag does!
        switch (flag & ENERGY) {
            case FALSE:
                E = 25.0 * x * x / (xunit * xunit);
                break;
            default:
                E = x / (xunit * m1);
                break;
        }

        // Check the atom type and treat it a little bit differently depending on the atom number.
        switch (z1) {
            case 1:
                // Proton
                m_values.append(QPair<double, double>(x, pstop(z2, E)));
                break;
            case 2:
                // Helium
                m_values.append(QPair<double, double>(x, hestop(z2, E)));
                break;
            default:
                // All other atoms
                m_values.append(QPair<double, double>(x, histop(z1, z2, E)));
                break;
        }

        // If using nuclear stopping. At this point the data should already have been added to the list, and it is safe
        // to use the index to access it.
        switch (flag & NUCLEAR) {
            case N_ONLY:
                m_values[i].second = nuclear(z1, z2, m1, m2, E * m1);
                break;
            case N_BOTH:
                m_values[i].second += nuclear(z1, z2, m1, m2, E * m1);
                break;
            case N_NO:
                break;
        }

        // Rescale with the chosen unit.
        m_values[i].second *= sunit;
    }
}


const QList<QPair<double, double>>& Zbl96::values() const {
    return m_values;
}

double Zbl96::density() const {
    return m_density;
}


/*!
 * \brief Combines the two tables into one table
 */
void Zbl96::readscoef() {
    QFile fileScoefA(":/zbl96/scoef.95a");
    if (!fileScoefA.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't find the file...";
    }

    QTextStream instreamA(&fileScoefA);
    QString row;
    instreamA.readLine();  // Read the header lines beforehand
    instreamA.readLine();  // Read the header lines beforehand
    for (int i = 0; i < ROWS; ++i) {
        row = instreamA.readLine();
        QStringList columns = row.split(" ", QString::SkipEmptyParts);
        if (columns.isEmpty()) break;
        for (int j = 0; j < ACOLS; ++j) {
            m_scoef[i + 1][j + 1] = columns.at(j).toDouble();
        }
    }

    fileScoefA.close();


    QFile fileScoefB(":/zbl96/scoef.95b");
    if (!fileScoefB.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't find the file...";
    }

    QTextStream instreamB(&fileScoefB);
    instreamB.readLine();  // Read the header lines beforehand
    instreamB.readLine();  // Read the header lines beforehand
    for (int i = 0; i < ROWS; ++i) {
        row = instreamB.readLine();
        QStringList columns = row.split(" ", QString::SkipEmptyParts);
        if (columns.isEmpty()) break;
        for (int j = 0; j < BCOLS; ++j) {
            m_scoef[i + 1][j + 1 + ACOLS] = columns.at(j).toDouble();
        }
    }

    fileScoefB.close();
}


double Zbl96::pstop(int z2, double E) {
    double sp, x, pe0, pe, sl, sh, ppow;

    pe0 = 10.0;

    if (E > 1.0e4) {
        x = qLn(E) / E;
        sp = m_scoef[z2][17] + (m_scoef[z2][18] * x) + (m_scoef[z2][19] * x * x) + (m_scoef[z2][20] / x);
    } else {
        pe = qMax(pe0, E);
        sl = m_scoef[z2][9] * pow((double)pe, (double)m_scoef[z2][10]) +
             m_scoef[z2][11] * pow((double)pe, (double)m_scoef[z2][12]);
        sh = m_scoef[z2][13] / pow((double)pe, (double)m_scoef[z2][14]) *
             qLn((m_scoef[z2][15] / pe) + m_scoef[z2][16] * pe);
        sp = sl * sh / (sl + sh);
        if (E <= pe0) {
            ppow = 0.45;
            if (z2 < 7) ppow = 0.35;
            sp *= qPow((double)E / pe0, (double)ppow);
        }
    }

    return (sp);
}


double Zbl96::hestop(int z2, double E) {
    double se, he0, he, a, b, heh, sp;

    he0 = 1.0;

    he = qMax(he0, E);

    b = qLn(he);
    a = 0.2865 + 0.1266 * b - 0.001429 * b * b + 0.02402 * b * b * b - 0.01135 * b * b * b * b +
        0.001475 * b * b * b * b * b;
    a = qMin(a, 30.0);
    heh = 1.0 - qExp(-a);

    he = qMax(he, 1.0);
    a = 1.0 + (0.007 + 0.00005 * z2) * exp(-pow((double)7.6 - qLn(he), (double)2));
    heh *= a * a;
    sp = pstop(z2, he);
    se = sp * heh * 4.0;

    if (E <= he0) se *= qSqrt(E / he0);

    return (se);
}


double Zbl96::histop(int z1, int z2, double E) {
    double yrmin, vrmin, v, vfermi, vr, yr, a, q, lambda0, lambda1, zeta0, zeta;
    double sp, se, hipower, eee, eion, l, vfcorr0, vfcorr1, vmin;
    int j;

    yrmin = 0.13;
    vrmin = 1.0;
    vfermi = m_scoef[z2][7];

    v = qSqrt(E / 25.0) / vfermi;
    if (v < 1.0)
        vr = (3.0 * vfermi / 4.0) * (1.0 + (2.0 * v * v / 3.0) - (pow((double)v, (double)4) / 15.0));
    else
        vr = v * vfermi * (1.0 + 1.0 / (5.0 * v * v));

    yr = qMax(vr / qPow((double)z1, (double)0.6667), yrmin);
    yr = qMax(yr, vrmin / qPow((double)z1, (double)0.6667));

    a = -0.803 * qPow((double)yr, (double)0.3) + 1.3167 * qPow((double)yr, (double)0.6) + 0.38157 * yr +
        0.008983 * yr * yr;

    a = qMin(a, 50.0);

    q = 1.0 - qExp(-a);

    q = qMax(q, 0.0);
    q = qMin(q, 1.0);

    for (j = 22; j <= 39 && q > m_scoef[93][j]; j++)
        ;
    j--;

    j = qMax(j, 22);
    j = qMin(j, 38);

    lambda0 = m_scoef[z1][j];
    lambda1 = (q - m_scoef[93][j]) * (m_scoef[z1][j + 1] - m_scoef[z1][j]) / (m_scoef[93][j + 1] - m_scoef[93][j]);
    l = (lambda0 + lambda1) / pow((double)z1, (double)0.33333);
    zeta0 = q + (1. / (2.0 * pow((double)vfermi, (double)2))) * (1.0 - q) *
                    qLn(1.0 + pow((double)4.0 * l * vfermi / 1.919, (double)2));
    a = qLn(E);
    a = qMax(a, 0.0);

    zeta = zeta0 * (1.0 + (1.0 / (z1 * z1)) * (0.08 + 0.0015 * z2) * qExp(-qPow((double)7.6 - a, (double)2)));
    a = vrmin / qPow((double)z1, (double)0.6667);
    a = qMax(a, yrmin);

    if (yr > a) {
        sp = pstop(z2, E);
        se = sp * qPow((double)zeta * z1, (double)2);
        eion = qMin(E, 9999.0);
        for (j = 41; j <= 53 && eion >= m_scoef[93][j]; j++)
            ;
        j--;
        j = qMax(j, 41);
        j = qMin(j, 53);

        vfcorr0 = m_scoef[z2][j];
        vfcorr1 =
            (eion - m_scoef[93][j]) * (m_scoef[z2][j + 1] - m_scoef[z2][j]) / (m_scoef[93][j + 1] - m_scoef[93][j]);
        se *= (vfcorr0 + vfcorr1);
    } else {
        vrmin = qMax(vrmin, yrmin * qPow((double)z1, (double)0.6667));
        a = qPow((double)vrmin, (double)2) - 0.8 * qPow((double)vfermi, (double)2);
        a = qMax(a, 0.0);

        vmin = 0.5 * (vrmin + qSqrt(a));
        eee = 25.0 * vmin * vmin;
        sp = pstop(z2, eee);
        eion = qMin(eee, 9999.0);

        for (j = 41; j <= 53 && eion >= m_scoef[93][j]; j++)
            ;
        j--;

        j = qMax(j, 41);
        j = qMin(j, 53);

        vfcorr0 = m_scoef[z2][j];
        vfcorr1 =
            (eion - m_scoef[93][j]) * (m_scoef[z2][j + 1] - m_scoef[z2][j]) / (m_scoef[93][j + 1] - m_scoef[93][j]);
        sp = sp * (vfcorr0 + vfcorr1);

        hipower = 0.47;
        if (z1 == 3)
            hipower = 0.55;
        else if (z2 < 7)
            hipower = 0.375;
        else if (z1 < 18 && (z2 == 14 || z2 == 32))
            hipower = 0.375;
        se = (sp * qPow((double)zeta * z1, (double)2)) * qPow((double)E / eee, (double)hipower);
    }

    return (se);
}


double Zbl96::nuclear(int z1, int z2, double m1, double m2, double E) {
    double eps, a, sn;

    if (E == 0.0) return (0.0);

    eps = 32.53 * m2 * E / (z1 * z2 * (m1 + m2) * (pow((double)z1, (double)0.23) + pow((double)z2, (double)0.23)));

    if (eps < 30) {
        a = (0.01321 * pow((double)eps, (double)0.21226) + (0.19593 * pow((double)eps, (double)0.5)));
        sn = 0.5 * log(1.0 + 1.1383 * eps) / (eps + a);
    } else {
        sn = log(eps) / (2.0 * eps);
    }
    sn *= z1 * z2 * m1 * 8.462 / ((m1 + m2) * (pow((double)z1, (double)0.23) + pow((double)z2, (double)0.23)));

    return (sn);
}
