/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "api/api.h"
#include "gui/widgets/entryeditorwidget.h"
#include "gui/widgets/simulationlistwidget.h"
#include "gui/widgets/trackoutputwidget.h"

#include <QCloseEvent>
#include <QFileDialog>
#include <QMainWindow>

// Forward declarations of classes
class GraphSelector;


namespace Ui {
class MainWindow;
}

/*!
 * \brief Core of the GUI
 *
 * Connects to an instance of Api and makes use of its functionalities. The main component of the GUI is a tab widget
 * with four tabs. The first tab is used for opening and creating COSIRMA projects. The second tab includes
 * EntryEditorWidget that uses Api#entryEditor for editing the latest entry of the project that is currently open. The
 * third tab (SimulationListWidget) handles COSIRMA simulations and their settings using Api#simulationList. The fourth
 * and final tab (OutputTab) is for visualizing and analyzing the history of the project that is currently open.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    explicit MainWindow(Api* a, QWidget* parent = nullptr);
    ~MainWindow();

  protected:
    void closeEvent(QCloseEvent* event);

  private:
    std::pair<bool, QString> readyToStartSimulation();
    void saveChanges();
    void discardChanges();
    bool isModified();

    Ui::MainWindow* ui;
    Api* m_api;

    EntryEditorWidget* m_entryEditorWidget = nullptr;
    SimulationListWidget* m_simulationListWidget = nullptr;
    TrackOutputWidget* m_trackOutputWidget = nullptr;

    ExtendedEntryData* simulationEntryData = nullptr;

  private slots:
    void setupTabIcon(int tab, QString path1, QString path2, int height);
    void setupActionIcon(QAction* action, QString path, int height);
    void initIcons();
    void openProject();
    void createProject();
    void showSettings();
    void showUserManual();
    void showLicenseFile();
    void showAbout();

    void handleStartSimulation();
    void handleStopSimulation();

    void onProjectClosing();
    void onProjectClose();
    void onProjectOpen();

    void updateStartSimulationButton(bool enabled, bool checked, const QString& tooltip);
    void handleSimulationStatus(Api::SimulationStatus status);
    void handleSimulationStatus(Api::SimulationStatus status, const QString& message);

    void handleEnableSimulationButtons(bool enable);

    void on_pushButton_exportSimulation_clicked();
};

#endif  // MAINWINDOW_H
