/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ui_boxselectorwidget.h"

#include "api/atomselections.h"
#include "gui/rendering/cubemodel.h"
#include "gui/widgets/boxselectorwidget.h"

#include <QColorDialog>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>


BoxSelectorWidget::BoxSelectorWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent), ui(new Ui::BoxSelectorWidget) {
    qDebug() << this << "::BoxSelectorWidget()";

    ui->setupUi(this);

    connect(ui->xSlider, &RangeSliderBox::rangeEdited, this, &BoxSelectorWidget::xDimensionChanged);
    connect(ui->ySlider, &RangeSliderBox::rangeEdited, this, &BoxSelectorWidget::yDimensionChanged);
    connect(ui->zSlider, &RangeSliderBox::rangeEdited, this, &BoxSelectorWidget::zDimensionChanged);

    connect(this, &BoxSelectorWidget::xDimensionChanged, this, &BoxSelectorWidget::updateDimensions);
    connect(this, &BoxSelectorWidget::yDimensionChanged, this, &BoxSelectorWidget::updateDimensions);
    connect(this, &BoxSelectorWidget::zDimensionChanged, this, &BoxSelectorWidget::updateDimensions);

    connect(ui->colorLabel, &ClickableLabel::clicked, [this] {
        QColorDialog dialog(this);
        dialog.setOption(QColorDialog::DontUseNativeDialog, true);
        dialog.setOption(QColorDialog::ShowAlphaChannel, true);
        dialog.setWindowTitle("Color Picker");
        dialog.setCurrentColor(m_color);

        if (dialog.exec() == QDialog::Accepted && m_color != dialog.currentColor()) {
            setColor(dialog.currentColor());
        }
    });

    setFocusProxy(ui->xSlider);
}


BoxSelectorWidget::~BoxSelectorWidget() {
    qDebug() << this << "::~BoxSelectorWidget()";

    if (m_context) {
        m_context->removeModel(m_cube);
        m_context->removeModel(m_wireframeCube);
    }

    if (m_cube) delete m_cube;
    if (m_wireframeCube) delete m_wireframeCube;

    delete ui;
}


const AbstractAtomSelection& BoxSelectorWidget::selector() {
    return m_selector;
}


/*!
 * Sets the limits of the RangeSliderBoxes.
 */
void BoxSelectorWidget::setLimits(const Box& b) {
    ui->xSlider->setLimits(b.x1, b.x2);
    ui->ySlider->setLimits(b.y1, b.y2);
    ui->zSlider->setLimits(b.z1, b.z2);
}


/*!
 * Returns a box given by the values of the RangeSliderBoxes.
 */
Box BoxSelectorWidget::getDimensions() {
    qDebug() << "BoxSelectorWidget::getDimensions()";

    double x1 = ui->xSlider->lowerValue();
    double x2 = ui->xSlider->upperValue();
    double y1 = ui->ySlider->lowerValue();
    double y2 = ui->ySlider->upperValue();
    double z1 = ui->zSlider->lowerValue();
    double z2 = ui->zSlider->upperValue();

    return Box(x1, x2, y1, y2, z1, z2);
}


/*!
 * Sets the values of the RangeSliderBoxes based on the given box.
 */
void BoxSelectorWidget::setDimensions(const Box& dim) {
    qDebug() << "BoxSelectorWidget::setDimensions()";

    ui->xSlider->setRange(dim.x1, dim.x2);
    ui->ySlider->setRange(dim.y1, dim.y2);
    ui->zSlider->setRange(dim.z1, dim.z2);
}


void BoxSelectorWidget::blockSignalsFromComponents(bool b) {
    ui->xSlider->blockSignals(b);
    ui->ySlider->blockSignals(b);
    ui->zSlider->blockSignals(b);
    ui->wireframeCB->blockSignals(b);
    ui->surfaceCB->blockSignals(b);
}


void BoxSelectorWidget::setColor(const QColor& color) {
    qDebug() << "BoxSelectorWidget::setColor()";

    m_color = color;
    ui->colorLabel->setStyleSheet(QString("background-color: rgba(%1,%2,%3,%4);border: 1px solid black;")
                                      .arg(color.red())
                                      .arg(color.green())
                                      .arg(color.blue())
                                      .arg(color.alpha()));
    m_cube->setFaceColor(color);
    m_cube->setLineColor(color.rgb());
    m_wireframeCube->setLineColor(color.rgb());
}


Box BoxSelectorWidget::calculateLimits() {
    if (!m_entry) return Box();

    const Box b1 = m_entry->frame.atomLims;
    const Box b2 = m_entry->frame.cellLims;
    double margin = 1e-5;
    Box bounding(std::min(b1.x1 - margin, b2.x1), std::max(b1.x2 + margin, b2.x2), std::min(b1.y1 - margin, b2.y1),
                 std::max(b1.y2 + margin, b2.y2), std::min(b1.z1 - margin, b2.z1), std::max(b1.z2 + margin, b2.z2));

    return bounding;
}


void BoxSelectorWidget::init() {
    if (!m_entry) return;
    //    blockSignalsFromComponents(true);

    Box b = calculateLimits();
    setLimits(b);
    setDimensions(b);

    m_color = QColor(0, 0, 255, 100);

    m_cube = new CubeModel(getDimensions(), m_color, m_color.rgb(), 0);
    m_wireframeCube = new CubeModel(getDimensions(), m_color, m_color.rgb(), 50);

    m_cube->setShowFaces(true);
    m_cube->setShowLines(false);
    m_wireframeCube->setShowFaces(false);
    m_wireframeCube->setShowLines(false);

    connect(ui->surfaceCB, &QCheckBox::clicked, [this](bool checked) {
        m_cube->setShowFaces(checked);
        m_cube->setShowLines(checked);
        m_context->update();
    });
    connect(ui->wireframeCB, &QCheckBox::clicked, [this](bool checked) {
        m_wireframeCube->setShowLines(checked);
        m_context->update();
    });

    setColor(m_color);

    ui->surfaceCB->setCheckState(Qt::Checked);
    ui->wireframeCB->setCheckState(Qt::Unchecked);

    //    blockSignalsFromComponents(false);

    updateDimensions();
}


void BoxSelectorWidget::updateAtoms() {
    if (!m_entry) return;
    blockSignalsFromComponents(true);

    setLimits(calculateLimits());

    blockSignalsFromComponents(false);

    updateDimensions();
}


void BoxSelectorWidget::updateCellLims() {
    if (!m_entry) return;
    blockSignalsFromComponents(true);

    setLimits(calculateLimits());

    blockSignalsFromComponents(false);

    updateDimensions();
}


void BoxSelectorWidget::connectToPlot() {
    qDebug() << "BoxSelectorWidget::connectToPlot()";
    //! \todo Only allow this to be connected once...

    m_context->addModel(m_cube);
    m_context->addModel(m_wireframeCube);
}


void BoxSelectorWidget::hidePlottables() {
    QSignalBlocker b1(ui->wireframeCB);
    QSignalBlocker b2(ui->surfaceCB);

    ui->wireframeCB->setChecked(false);
    ui->surfaceCB->setChecked(false);
}


void BoxSelectorWidget::updateDimensions() {
    Box box = getDimensions();
    m_selector = BoxAtomSelection(box);

    m_cube->setBox(box);
    m_wireframeCube->setBox(box);

    m_context->update();
}
