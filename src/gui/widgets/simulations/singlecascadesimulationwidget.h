/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SINGLECASCADESIMULATIONWIDGET_H
#define SINGLECASCADESIMULATIONWIDGET_H

#include "gui/widgets/simulations/abstractsimulationwidget.h"
#include "gui/widgets/simulations/components/controlwidget.h"
#include "gui/widgets/simulations/components/interactionwidget.h"
#include "gui/widgets/simulations/components/iontransformwidget.h"
#include "gui/widgets/simulations/components/ionwidget.h"
#include "gui/widgets/simulations/components/speedupwidget.h"

#include <QWidget>


namespace Ui {
class SingleCascadeSimulationWidget;
}

class SingleCascadeSimulationWidget : public AbstractSimulationWidget {
    Q_OBJECT

  public:
    explicit SingleCascadeSimulationWidget(QWidget* parent = nullptr);
    ~SingleCascadeSimulationWidget() override;

    void connectToPlot(PlotCellGLWidget* glWidget) override;
    void disconnectFromPlot(PlotCellGLWidget* glWidget) override;

    void setControllerParametersToWidget(ParcasBaseController* controller) override;
    void setWidgetParametersToController(ParcasBaseController* controller) override;

    //    bool isModified() override;

  private:
    void setComponentEntry(ExtendedEntryData* componentEntry) override;

    enum TabNames { CascadeTab = 0, InteractionsTab = 1, SpeedupTab = 2 };

    Ui::SingleCascadeSimulationWidget* ui;

    IonWidget* m_ionWidget;
    IonTransformWidget* m_ionTransformWidget;
    ControlWidget* m_controlWidget;
    InteractionWidget* m_interactionWidget;
    SpeedupWidget* m_speedupWidget;

    api::BaseParameters m_unmodifiedParameters;

  public slots:
    void enableSpeedupModule(bool enabled) override;

  private slots:
    void onChangeTab(int);
    void setTabStatusIcon(QWidget* tabWidget, const QIcon& icon);

  signals:
    void plottablesChanged();
};

#endif  // SINGLECASCADESIMULATIONWIDGET_H
