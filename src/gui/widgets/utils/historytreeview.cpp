/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "historytreeview.h"

#include "api/project.h"

#include <QDebug>


HistoryTreeView::HistoryTreeView(QWidget* parent) : QTreeView(parent) {
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setFocusPolicy(Qt::NoFocus);
    setStyleSheet("QTreeView::branch:!has-children {background-color: white;}");

    //    // Initialize the view with an empty data object
    //    setHistory(History());


    connect(&Project::getInstance().history, &History::frameAdded, this,
            [this](const QString& frameInfo, int entryIndex, int frameIndex) {
                qDebug() << entryIndex << frameIndex;
                m_historyModel->addFrameToEntry(frameInfo, entryIndex, frameIndex);
            });

    connect(&Project::getInstance().history, &History::entryRemoved, this, [this](int entryIndex) {
        qDebug() << "Remove: " << entryIndex;
        m_historyModel->removeEntry(entryIndex);
    });
}


HistoryTreeView::~HistoryTreeView() {
    qDebug() << this << "::~HistoryTreeView()";
    delete m_historyModel;
}


int HistoryTreeView::entry() {
    return selectedEntry.row();
}


int HistoryTreeView::frame() {
    return selectedFrame.row();
}


void HistoryTreeView::addEntry(const Entry* entry) {
    m_historyModel->addEntry(entry);
}


void HistoryTreeView::setHistory(const History& history) {
    qDebug() << "HistoryTreeView::setHistory()";

    if (m_historyModel) delete m_historyModel;
    m_historyModel = new HistoryTreeModel(history);
    setModel(m_historyModel);
    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &HistoryTreeView::handleSelection);
}


void HistoryTreeView::setSelectedFrame(int entry, int frame) {
    QModelIndex newEntry = m_historyModel->index(entry, 0);

    QModelIndex newFrame = m_historyModel->index(frame, 0, newEntry);

    if (!newFrame.isValid()) return;

    if (selectedFrame != newFrame) {
        selectedFrame = newFrame;
        selectedEntry = newEntry;
        selectSelected();
        emit selectionModified(selectedEntry.row(), selectedFrame.row());
    }
}


void HistoryTreeView::selectSelected() {
    disconnect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &HistoryTreeView::handleSelection);

    clearSelection();
    selectionModel()->select(selectedEntry, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    selectionModel()->select(selectedFrame, QItemSelectionModel::Select | QItemSelectionModel::Rows);

    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &HistoryTreeView::handleSelection);
}


void HistoryTreeView::handleSelection(const QItemSelection& selected, const QItemSelection& /*deselected*/) {

    // Disconnecting this slot so that it will not be called recursively
    disconnect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &HistoryTreeView::handleSelection);

    QModelIndexList rows = selectionModel()->selectedRows();
    QModelIndex oldSelectedEntry = selectedEntry;
    QModelIndex oldSelectedFrame = selectedFrame;

    // User clicks on one row
    if (rows.count() == 1) {
        selectedEntry = rows.first();
        if (selectedEntry.parent().isValid()) {
            selectedFrame = selectedEntry;
            selectedEntry = selectedEntry.parent();
        }
    }
    // Handling multiple selection
    else if (rows.count() > 1) {
        if (selected.indexes().count() == selectionModel()->selectedIndexes().count()) {
            selectedEntry = rows.first();
            if (selectedEntry.parent().isValid()) {
                selectedFrame = selectedEntry;
                selectedEntry = selectedEntry.parent();
            }
        }

        for (const QModelIndex& s : selected.indexes()) {
            QModelIndex entry = m_historyModel->parent(s);
            if (!entry.isValid()) {
                entry = s;
            }
            if (entry.internalPointer() != selectedEntry.internalPointer()) {
                selectionModel()->select(s, QItemSelectionModel::Deselect);
            }
        }
    }


    if (selectedEntry != oldSelectedEntry) {
        if (selectedFrame == oldSelectedFrame) {
            selectedFrame = m_historyModel->index(m_historyModel->rowCount(selectedEntry) - 1, 0, selectedEntry);
        }
    }

    // Select the row of the selected entry and frame
    selectionModel()->select(selectedEntry, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    selectionModel()->select(selectedFrame, QItemSelectionModel::Select | QItemSelectionModel::Rows);

    connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &HistoryTreeView::handleSelection);

    if (selectedFrame != oldSelectedFrame) {
        emit selectionModified(selectedEntry.row(), selectedFrame.row());
        qDebug() << "Selection:" << selectedEntry.row() << selectedFrame.row();
    }
}
