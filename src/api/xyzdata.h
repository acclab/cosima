/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef XYZDATA_H
#define XYZDATA_H

#include "abstractstreamable.h"
#include "atom.h"
#include "geometries.h"

#include <QDebug>
#include <QMap>

/*!
 * \brief %Atom data struct
 *
 * Can be read from ".xyz" file.
 */
struct XyzData : AbstractStreamable {
    QVector<Atom> atoms;                              /*!< \brief Vector of atoms*/
    Box atomLims;                                     /*!< \brief Minimum and maximum atom coordinates*/
    Box cellLims;                                     /*!< \brief Simulation cell dimensions*/
    glm::bvec3 pbc = glm::bvec3(false, false, false); /*!< \brief Periodic boundary condition*/
    QMap<int, int> amounts; /*!< \brief Atom type numbers mapped to the amounts of said types in atoms vector*/

    bool read(const QString& path);
    bool write(const QString& path) const;
    void center();
    void updateAmounts();
    void updateAtomLims();

    QDataStream& save(QDataStream& out) const override;
    QDataStream& load(QDataStream& in) override;
};

#endif  // XYZDATA_H
