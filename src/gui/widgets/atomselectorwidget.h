/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATOMSELECTORWIDGET_H
#define ATOMSELECTORWIDGET_H

#include "abstractatomselectorwidget.h"

namespace Ui {
class AtomSelectorWidget;
}

class AtomSelectorWidget : public AbstractAtomSelectorWidget {
    Q_OBJECT

  public:
    explicit AtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr);
    ~AtomSelectorWidget() override;

    const AbstractAtomSelection& selector() override;
    void setEntry(const ExtendedEntryData* entry) override;
    void init() override;
    void updateAtoms() override;
    void updateCellLims() override;
    void updateTypes() override;

    void connectToPlot() override;
    void hidePlottables() override;

    void zero();

    void setClosable(bool closable);

  signals:
    void closeRequested();
    void modeChanged();

  private:
    enum AtomSelectorGeneralType { All, Volume, Combination, Type };
    enum AtomSelectorType {
        AllSelector,
        BoxSelector,
        SphereSelector,
        CylinderSelector,
        UnionArray,
        IntersectionArray,
        TypeSelector
    };

    Ui::AtomSelectorWidget* ui;

    AbstractAtomSelectorWidget* m_selectedWidget = nullptr;

    NullAtomSelection m_nullSelector;
    InverseAtomSelection m_invertedSelector;

  private slots:
    AtomSelectorType currentType();
    void option1Changed();
    void option2Changed();
};

#endif  // ATOMSELECTORWIDGET_H
