/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ELSTOPGENERATOR_H
#define ELSTOPGENERATOR_H

#include "api/entry.h"

#include <QList>


class ElstopGenerator {
  public:
    // Helper objecst for generating the elstop files
    struct SubstratePart {
        int amount = 1;
        QString symbol;

        SubstratePart(const QString& _symbol, int _amount = 1) {
            symbol = _symbol;
            amount = _amount;
        }
    };

    void generate(const QList<SubstratePart>& substrate, const ExtendedEntryData& entry, double substrateDensity,
                  double xmin = 0, double xmax = 2e7, double xstep = 1e5);

    //    const QMap<QString, DataFile<ElstopFileData>>& data();
    const QMap<QString, DataFileContent<ElstopFileData>>& data();

  private:
    // Helper objects for generating the elstop files
    struct Elstop {
        double density = -1;
        QList<QPair<double, double>> values;
    };

    QString m_substrateName = "";
    //    QMap<QString, DataFile<ElstopFileData>> m_data;
    QMap<QString, DataFileContent<ElstopFileData>> m_data;
};

#endif  // ELSTOPGENERATOR_H
