/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TYPEDATA_H
#define TYPEDATA_H

#include "abstractstreamable.h"
#include "atomtype.h"
#include "elementreader.h"
#include "framereaderwriter.h"

/*!
 * \brief An entry level struct for atom types
 */
struct TypeData : AbstractStreamable {
    QHash<int, AtomType> typeList;         /*!< \brief Map from atom type numbers to atom types*/
    QHash<int, AtomType> injectedTypeList; /*!< \brief Map from atom type numbers to atom types*/
    QHash<int, AtomType> allTypes;

    TypeData();
    TypeData(const XyzData& frame);

    bool read(const QString& path);
    bool write(const QString& path);

    void injectAtomType(const AtomType& type);
    void removeInjectedAtomType(const AtomType& type);
    void removeInjectedTypes();

    QDataStream& save(QDataStream& out) const;
    QDataStream& load(QDataStream& in);

  private:
    void mergeAllTypes();
};

#endif  // TYPEDATA_H
