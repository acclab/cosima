/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "ratiograph.h"
#include "ui_graph.h"

#include <QDebug>
#include <QString>
#include <QVector>

#include <algorithm>


using std::vector;

RatioGraph::RatioGraph(QWidget* parent) : QWidget(parent), ui(new Ui::Graph) {
    ui->setupUi(this);
    firstPlot = true;
    typeNumber1 = 1;
    typeNumber2 = 2;
    connect(ui->homePB, &QPushButton::clicked, this, &RatioGraph::setDefaultView);

    ui->widget->addGraph();
    ui->widget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

RatioGraph::~RatioGraph() {
    delete ui;
}

/*!
 * Plots the ratio of atom types 1 and 2 as a function of z.
 * Plot is done for the atoms in a bin determined by the indices boxi and boxj.
 */
void RatioGraph::plotRatio(int boxi, int boxj) {
    indexI = boxi;
    indexJ = boxj;
    double dz = limits.z2 - limits.z1;

    vector<int> histType1;
    histType1.resize(resz);
    vector<int> histType2;
    histType2.resize(resz);

    for (int k = 0; k < resz; ++k) {
        histType1[k] = 0;
        histType2[k] = 0;
    }

    for (Atom a : pillars[boxi][boxj]) {
        double z = a.position.z;
        int zi = static_cast<int>((z - limits.z1) / dz * resz);
        if (zi == resz) zi = resz - 1;
        int type = a.type;

        if (useSymbols) {
            if (a.symbol.compare(typeSymbol1) == 0) {
                typeNumber1 = abs(a.type);
                histType1[zi]++;
            }
            if (a.symbol.compare(typeSymbol2) == 0) {
                typeNumber2 = abs(a.type);
                histType2[zi]++;
            }
        } else {
            if (abs(type) == typeNumber1) {
                typeSymbol1 = a.symbol;
                histType1[zi]++;
            }
            if (abs(type) == typeNumber2) {
                typeSymbol2 = a.symbol;
                histType2[zi]++;
            }
        }
    }

    QVector<double> z(resz), ratio(resz);
    for (int k = 0; k < resz; ++k) {
        int type1 = histType1[k];
        int type2 = histType2[k];

        double r = 0;
        if (type1 != 0) {
            r = static_cast<double>(type2) / type1;
        }
        ratio[k] = r;
        z[k] = limits.z1 + k * dz / resz;
    }

    ui->widget->graph(0)->setData(z, ratio);

    if (firstPlot) {
        ui->widget->rescaleAxes();
        ui->widget->xAxis->scaleRange(1.1, ui->widget->xAxis->range().center());
        ui->widget->yAxis->scaleRange(1.1, ui->widget->yAxis->range().center());
        firstPlot = false;
    }
    ui->widget->xAxis->setLabel("z [Å]");
    ui->widget->yAxis->setLabel("ratio " + typeSymbol2 + "/" + typeSymbol1);
    ui->widget->replot();
}


/*!
 * Slices atoms into this->pillars based on their x- and y-coordinates.
 */
void RatioGraph::readAtoms(const XyzData* cell) {
    limits = cell->cellLims;
    double dx = limits.x2 - limits.x1;
    double dy = limits.y2 - limits.y1;

    pillars.clear();
    pillars.resize(res);
    for (int i = 0; i < res; ++i) pillars[i].resize(res);

    for (Atom a : cell->atoms) {
        double x = a.position.x - limits.x1;
        double y = a.position.y - limits.y1;

        int xi = static_cast<int>(res * x / dx);
        if (xi == res) xi = res - 1;
        int yi = int(res * y / dy);
        if (yi == res) yi = res - 1;

        pillars[xi][yi].push_back(a);
    }
}

/*!
 * Returns the amount of bins in x and y directions.
 */
int RatioGraph::getRes() {
    return res;
}

/*!
 * Returns the plot resolution in z direction.
 */
int RatioGraph::getResZ() {
    return resz;
}

/*!
 * Sets the amount of bins in x and y directions.
 */
void RatioGraph::setRes(int resolution) {
    res = resolution;
}

/*!
 * Sets the plot resolution in z direction.
 */
void RatioGraph::setResZ(int resolutionZ) {
    resz = resolutionZ;
}

/*!
 * Fits the plot into the data.
 */
void RatioGraph::setDefaultView() {
    firstPlot = true;
    plotRatio(indexI, indexJ);
}

/*!
 * Sets the atom types that will be used to calculate the ratio.
 * Takes element symbols as arguments.
 */
void RatioGraph::setTypes(QString t1, QString t2) {
    typeSymbol1 = t1;
    typeSymbol2 = t2;

    useSymbols = true;
}


/*!
 * Sets the atom types that will be used to calculate the ratio.
 * Takes type numbers as arguments.
 */
void RatioGraph::setTypes(int t1, int t2) {
    typeNumber1 = t1;
    typeNumber2 = t2;

    typeSymbol1 = "";
    typeSymbol2 = "";

    useSymbols = false;
}


/*!
 * Returns the element symbol of the first atom type.
 */
QString RatioGraph::getTypeSymbol1() {
    return typeSymbol1;
}

/*!
 * Returns the element symbol of the second atom type.
 */
QString RatioGraph::getTypeSymbol2() {
    return typeSymbol2;
}
