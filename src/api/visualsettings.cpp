/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "visualsettings.h"

/*!
 * \brief Constructs a blank object.
 *
 * An object constructed with this function has no atom sizes or colors. The default visual settings can be accessed
 * through defaultSettings().
 *
 * \param parent Parent object
 */
VisualSettings::VisualSettings(QObject* parent) : QObject(parent) {}

/*!
 * \brief Copy constructor
 * \param v VisualSettings object being copied
 */
VisualSettings::VisualSettings(const VisualSettings& v) : QObject(v.parent()) {
    m_atomTypeColors = v.atomTypeColors();
    m_atomTypeSizes = v.atomTypeSizes();
    m_backgroundColor = v.backgroundColor();
    m_cellBoundaryColor = v.cellBoundaryColor();
    m_cellBoundaryEnabled = v.cellBoundaryEnabled();
    m_depthPeels = v.depthPeelCount();
}

/*!
 * \brief Default settings
 *
 * Default settings are atom size 1.0, black cell boundary, white background, and different atom colors for all types.
 *
 * \return Default VisualSettings object
 */
VisualSettings VisualSettings::defaultSettings() {

    VisualSettings def;
    QHash<int, QColor> colors;
    QHash<int, float> sizes;

    colors.insert(-9, QColor("#8c0606"));
    colors.insert(-8, QColor("#f28974"));
    colors.insert(-7, QColor("#e5aa6e"));
    colors.insert(-6, QColor("#998c49"));
    colors.insert(-5, QColor("#1dbf6e"));
    colors.insert(-4, QColor("#348c8c"));
    colors.insert(-3, QColor("#216080"));
    colors.insert(-2, QColor("#330aff"));
    colors.insert(-1, QColor("#0000ff"));
    colors.insert(0, QColor("#b21b4d"));
    colors.insert(1, QColor("#ffcda0"));
    colors.insert(2, QColor("#ff0000"));
    colors.insert(3, QColor("#ffdb26"));
    colors.insert(4, QColor("#84e622"));
    colors.insert(5, QColor("#0affff"));
    colors.insert(6, QColor("#0aa5f2"));
    colors.insert(7, QColor("#6274cc"));
    colors.insert(8, QColor("#ff5500"));
    colors.insert(9, QColor("#99498c"));

    for (int i = -9; i <= 9; ++i) {
        sizes[i] = 1.0;
    }

    def.changeTypeColors(colors);
    def.changeTypeSizes(sizes);
    def.changeCellBoundaryColor(QColor(0, 0, 0));
    def.changeBackgroundColor(QColor(255, 255, 255));
    def.changeCellBoundaryEnabled(true);
    def.changeDepthPeelCount(4);

    return def;
}

/*!
 * \brief %Atom type colors
 * \return Map from atom type number to color
 */
const QHash<int, QColor>& VisualSettings::atomTypeColors() const {
    return m_atomTypeColors;
}

/*!
 * \brief %Atom type sizes
 * \return Map from atom type number to size in Angstroms
 */
const QHash<int, float>& VisualSettings::atomTypeSizes() const {
    return m_atomTypeSizes;
}

/*!
 * \brief Background color of a plot
 * \return Background color
 */
const QColor& VisualSettings::backgroundColor() const {
    return m_backgroundColor;
}

/*!
 * \brief Cell boundary color
 * \return Cell boundary color
 */
const QColor& VisualSettings::cellBoundaryColor() const {
    return m_cellBoundaryColor;
}

/*!
 * \brief If the outline of the simulation box should be drawn or not.
 * \return If the cell boundary is shown
 */
bool VisualSettings::cellBoundaryEnabled() const {
    return m_cellBoundaryEnabled;
}

/*!
 * \brief How many depth peels are performed by the rendering pipeline.
 * \return the number of depth peels
 */
int VisualSettings::depthPeelCount() const {
    return m_depthPeels;
}

/*!
 * \brief Changes the atom type colors.
 *
 * If the colors are changed, emits atomTypeColorsChanged().
 *
 * \param colors The new colors
 */
void VisualSettings::changeTypeColors(const QHash<int, QColor>& colors) {
    if (m_atomTypeColors != colors) {
        m_atomTypeColors = colors;
        emit atomTypeColorsChanged(m_atomTypeColors);
    }
}

/*!
 * \brief Changes the atom type sizes.
 *
 * If the sizes change, emits atomTypeSizesChanged().
 *
 * \param sizes The new sizes
 */
void VisualSettings::changeTypeSizes(const QHash<int, float>& sizes) {
    if (m_atomTypeSizes != sizes) {
        m_atomTypeSizes = sizes;
        emit atomTypeSizesChanged(m_atomTypeSizes);
    }
}

/*!
 * \brief Changes the backgroud color.
 *
 * If the color changes, emits backgroundColorChanged().
 *
 * \param color The new color
 */
void VisualSettings::changeBackgroundColor(const QColor& color) {
    if (m_backgroundColor != color) {
        m_backgroundColor = color;
        emit backgroundColorChanged(color);
    }
}

/*!
 * \brief Changes the cell boundary color
 *
 * If the color changes, emits cellBoundaryColorChanged().
 *
 * \param color The new color
 */
void VisualSettings::changeCellBoundaryColor(const QColor& color) {
    if (m_cellBoundaryColor != color) {
        m_cellBoundaryColor = color;
        emit cellBoundaryColorChanged(color);
    }
}

/*!
 * \brief Updates the state of the drawing of the simulation box outline.
 * \param enabled
 */
void VisualSettings::changeCellBoundaryEnabled(bool enabled) {
    if (m_cellBoundaryEnabled != enabled) {
        m_cellBoundaryEnabled = enabled;
        emit cellBoundaryEnabledChanged(enabled);
    }
}

/*!
 * \brief Update the depth peel count.
 * \param count
 */
void VisualSettings::changeDepthPeelCount(int count) {
    if (m_depthPeels != count) {
        m_depthPeels = count;
        emit depthPeelCountChanged(count);
    }
}

/*!
 * \brief Streaming operator for streaming VisualSettings objects into QDataStream
 * \param stream The Stream
 * \param settings The object being streamed
 * \return The stream afterwards
 */
QDataStream& operator<<(QDataStream& stream, const VisualSettings& settings) {
    stream << settings.atomTypeColors() << settings.atomTypeSizes() << settings.backgroundColor()
           << settings.cellBoundaryColor() << settings.cellBoundaryEnabled() << settings.depthPeelCount();
    return stream;
}

/*!
 * \brief Streaming operator for streaming VisualSettings objects from QDataStream
 * \param stream The Stream
 * \param settings The object being streamed
 * \return The stream afterwards
 */
QDataStream& operator>>(QDataStream& stream, VisualSettings& settings) {
    QHash<int, QColor> tempColors;
    QHash<int, float> tempSizes;
    QColor tempBgColor, tempOlColor;
    bool tempCellBoundaryEnabled;
    int tempDepthPeels;

    stream >> tempColors >> tempSizes >> tempBgColor >> tempOlColor >> tempCellBoundaryEnabled >> tempDepthPeels;

    settings.changeTypeColors(tempColors);
    settings.changeTypeSizes(tempSizes);
    settings.changeBackgroundColor(tempBgColor);
    settings.changeCellBoundaryColor(tempOlColor);
    settings.changeCellBoundaryEnabled(tempCellBoundaryEnabled);
    settings.changeDepthPeelCount(tempDepthPeels);

    return stream;
}

/*!
 * \brief Streaming operator for streaming VisualSettings objects into QDebug
 * \param stream The Stream
 * \param settings The object being streamed
 * \return The stream afterwards
 */
QDebug operator<<(QDebug d, const VisualSettings& settings) {
    d << settings.atomTypeColors();
    d << settings.atomTypeSizes();
    d << settings.backgroundColor();
    d << settings.cellBoundaryColor();
    return d;
}
