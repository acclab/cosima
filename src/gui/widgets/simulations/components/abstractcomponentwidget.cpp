/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "abstractcomponentwidget.h"

#include <QIcon>


AbstractComponentWidget::AbstractComponentWidget(QWidget* parent) : QWidget(parent) {}


void AbstractComponentWidget::setEntry(ExtendedEntryData* entry) {
    m_entry = entry;
}


ExtendedEntryData* AbstractComponentWidget::entry() {
    return m_entry;
}


void AbstractComponentWidget::setParameters(api::BaseParameters parameters) {
    m_original = parameters;
}


void AbstractComponentWidget::setReturnParameter(api::BaseParameters& returnParams, const api::key_t& key,
                                                 const QVariant& newValue) {
    if (m_original[key].value() == newValue) {
        returnParams.set(key, api::value_t(newValue, false, true));
    } else {
        returnParams.set(key, api::value_t(newValue, true, false));
    }
}


QIcon AbstractComponentWidget::loadIcon(gui::enums::state::Status status) {
    QIcon icon;
    switch (status) {
        case gui::enums::state::Error:
            icon.addFile(":/icons/times-circle-red", QSize(15, 15));
            break;

        case gui::enums::state::Warning:
            icon.addFile(":/icons/exclamation-triangle-orange", QSize(15, 15));
            break;

        case gui::enums::state::Modified:
            icon.addFile(":/icons/save", QSize(15, 15));
            break;

        case gui::enums::state::Attention:
            icon.addFile(":/icons/info-circle-blue", QSize(15, 15));
            break;

        case gui::enums::state::Ok:
            icon.addFile(":/icons/check-circle-green", QSize(15, 15));
            break;

        default:
            break;
    }
    return icon;
}
