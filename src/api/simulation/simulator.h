/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "api/simulation/parcas/parcasbasecontroller.h"

#include <QObject>
#include <QPointer>
#include <QProcess>
#include <QThread>

class QTemporaryDir;
class MDParameters;


class Simulator : public QObject {
    Q_OBJECT

  public:
    enum Status { Started, Stopping, Finished, Error };
    Q_ENUM(Status)
    enum SimulationStep { PreSimulation, Simulation, PostSimulation };

    explicit Simulator(ParcasBaseController* controller, QObject* parent = nullptr);
    ~Simulator();

    ParcasBaseController* controller();

    const QString& runtimePath() const;
    bool isRunning();

    std::pair<bool, QString> initRuntime(const ExtendedEntryData& entry);
    std::pair<bool, QString> initRuntime(const ExtendedEntryData& entry, bool useTemp, const QString& path = "");

    void updateInputFile(const ExtendedEntryData& entry);

    void setCurrentEntry(ExtendedEntryData entry);
    ExtendedEntryData currentEntry();

  protected:
    virtual void preSimulationStep();
    virtual void simulationStep();
    virtual void postSimulationStep();

    bool m_running = false;
    bool m_stepDone = true;


  private:
    QString nextRuntimePath(const QString& dir);

    void clearParcasProcess();

    void setSimulationStep(SimulationStep state);

    ParcasBaseController* m_controller = nullptr;
    // Initial step starts at the "end" of the POST_SIMULATION step in the iteration.
    SimulationStep m_step = PostSimulation;

    QPointer<QProcess> m_parcasProcess = nullptr;

    QTemporaryDir* m_tempDir = nullptr;
    QString m_runtimePath = "";

    ExtendedEntryData m_currentEntry;

  public slots:
    void simulate();
    void stop();


  private slots:
    void simulationLoop();

    void handleProcessErrorOccured(QProcess::ProcessError error);
    void handleProcessStandardError();
    void handleProcessStandardOutput();
    void handleProcessStarted();
    void handleProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void handleProcessStateChanged(QProcess::ProcessState newState);


  signals:
    void statusUpdate(Simulator::Status status, const QString& message = nullptr);

    void finished();
    void newFrame(const QString& path);

    void preSimulationStepDone();
    void simulationStepDone();
    void postSimulationStepDone();
};

Q_DECLARE_METATYPE(Simulator::Status)

#endif  // SIMULATOR_H
