/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "dialogstartsimulation.h"
#include "ui_dialogstartsimulation.h"

#include "api/programsettings.h"

#include <QDebug>
#include <QFileDialog>
#include <QThread>
#include <QMessageBox>


DialogStartSimulation::DialogStartSimulation(QWidget* parent) : QDialog(parent), ui(new Ui::DialogStartSimulation) {
    ui->setupUi(this);
    load();
}

DialogStartSimulation::~DialogStartSimulation() {
    delete ui;
}

void DialogStartSimulation::load() {
    // Backend Settings
    ui->lineEdit_parcasPath->setText(ProgramSettings::getInstance().backendSettings().parcasPath);
    ui->groupBox_mpi->setChecked(ProgramSettings::getInstance().backendSettings().useMpi);
    ui->spinBox_mpiCores->setValue(ProgramSettings::getInstance().backendSettings().mpiCores);
}

void DialogStartSimulation::save() {
    // Backend Settings
    ProgramSettings::getInstance().backendSettings().parcasPath = ui->lineEdit_parcasPath->text();
    ProgramSettings::getInstance().backendSettings().useMpi = ui->groupBox_mpi->isChecked();
    ProgramSettings::getInstance().backendSettings().mpiCores = ui->spinBox_mpiCores->value();
    ProgramSettings::getInstance().backendSettings().showSimulationStartDialog =
        !ui->checkBox_doNotShowAgain->isChecked();
}

void DialogStartSimulation::on_pushButton_start_clicked() {
    // Validate if the path to the PARCAS executable points to a file
    if (!QFile(ui->lineEdit_parcasPath->text()).exists()) {
        QMessageBox::warning(this, "PARCAS not found", "Please, make sure to set the path to PARCAS correctly.", QMessageBox::Ok);
    } else {
        accept();
    }
}

void DialogStartSimulation::on_pushButton_openParcasPath_clicked() {
    QString currentPath = ui->lineEdit_parcasPath->text();
    QString path = QDir::currentPath();
    if (QFile(currentPath).exists()) {
        path = currentPath;
    }
    QString filename = QFileDialog::getOpenFileName(this, "Locate PARCAS", path);
    if (!filename.isEmpty()) {
        ui->lineEdit_parcasPath->setText(filename);
    }
}
