/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IONWIDGET_H
#define IONWIDGET_H

#include "abstractcomponentwidget.h"

#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QWidget>


class IonWidget : public AbstractComponentWidget {
    Q_OBJECT

  public:
    explicit IonWidget(QWidget* parent = nullptr);
    ~IonWidget() {}

    void setEntry(ExtendedEntryData* entry) override;

    void setParameters(api::BaseParameters parameters) override;
    api::BaseParameters parameters() override;

  private:
    enum EnergyUnit { eV, keV, MeV };

    QComboBox* m_comboBox_type;
    QLineEdit* m_lineEdit_element;
    QDoubleSpinBox* m_doubleSpinBox_mass;
    QDoubleSpinBox* m_doubleSpinBox_energy;
    QComboBox* m_comboBox_energyUnit;
    QCheckBox* m_checkBox_mergeWithBaseType;

  private slots:
    void updateEntryTypes();

  signals:
    void entryTypesUpdated();
};

#endif  // IONWIDGET_H
