/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GRAPH_H
#define GRAPH_H

#include "api/atom.h"
#include "api/entry.h"
#include "qcustomplot.h"

#include <QWidget>

#include <vector>


namespace Ui {
class Graph;
}

// Example:
//  RatioGraph *graph = new RatioGraph(this);
//  ui->qLayout->addWidget(graph);
//  graph->setRes(3);
//  graph->setResZ(50);
//  graph->readAtoms(atoms);
//  graph->setTypes(1,2);
//  graph->plotRatio(1, 1);


/*!
 * \brief QWidget that includes a graph of ratios between atom types.
 *
 * Draws a 2D-plot of the ratio of the atom types 1 and 2 as a function of z using qCustomPlot.
 * The atom block can be sliced into bins with a xy-resolution "res" and
 * values of ratio within given bin are plotted with a resolution of "resZ".
 * Addiotionally QWidget (this) inludes qButtons  for controlling the plotting actions.
 *
 * \sa Ratio
 */
class RatioGraph : public QWidget {
    Q_OBJECT

  public:
    explicit RatioGraph(QWidget* parent = nullptr);
    ~RatioGraph();
    void plotRatio(int, int);
    void readAtoms(const XyzData*);
    void setResZ(int);
    void setRes(int);
    int getResZ();
    int getRes();
    void setTypes(int, int);
    void setTypes(QString, QString);
    QString getTypeSymbol1();
    QString getTypeSymbol2();

  private:
    Ui::Graph* ui;
    std::vector<std::vector<std::vector<Atom> > > pillars;
    int res, resz, indexI, indexJ, typeNumber1, typeNumber2;
    bool firstPlot, useSymbols;
    QString typeSymbol1, typeSymbol2;
    Box limits;

  private slots:
    void setDefaultView();
};

#endif  // GRAPH_H
