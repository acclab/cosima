/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "typedata.h"

#include "binarysettingsio.h"

/*!
 * \brief Constructs a blank object
 */

TypeData::TypeData() {}

/*!
 * \brief Constructs an object based on given atom data.
 *
 * Goes through the atoms in the data and adds a new type for each type number that it finds. The element symbol of the
 * type is taken from the first occurance, and the mass is taken from the periodic table for element.
 *
 * \param xyzData Atom data
 */
TypeData::TypeData(const XyzData& xyzData) {
    QSet<int> required;
    for (int i : xyzData.amounts.keys()) required.insert(std::abs(i));

    ElementReader reader;
    for (const Atom& a : xyzData.atoms) {
        int typeNumber = abs(a.type);
        if (!typeList.contains(typeNumber)) {
            AtomType type;
            type.typeNumber = typeNumber;
            type.symbol = a.symbol;
            type.atomNumber = reader.zFromSymbol(a.symbol);
            type.mass = reader.getMass(type.atomNumber);

            typeList.insert(typeNumber, type);
            required.remove(typeNumber);
            if (required.isEmpty()) break;
        }
    }

    mergeAllTypes();
}

/*!
 * \brief Reads the type data from a binary formatted file.
 * \param path Path to the file
 * \return \c true if the reading is successful
 */
bool TypeData::read(const QString& path) {
    TypeData data;
    if (BinarySettingsIO(path).load(data)) {
        typeList = data.typeList;
        if (!typeList.isEmpty()) {
            mergeAllTypes();
            return true;
        }
    }

    typeList.clear();
    return false;
}

/*!
 * \brief Writes the type data into a binary formatted file.
 * \param path Path to the file
 * \return \c true if the wrintig is successful
 */
bool TypeData::write(const QString& path) {
    if (path.isEmpty()) return false;
    return BinarySettingsIO(path).save(*this);
}


/**
 * @brief Insert or update the chosen atom type into the injected list.
 * @param type
 */
void TypeData::injectAtomType(const AtomType& type) {
    if (type.atomNumber < 1) return;

    if (injectedTypeList.contains(type.typeNumber)) {
        injectedTypeList[type.typeNumber] = type;
    } else {
        injectedTypeList.insert(type.typeNumber, type);
    }

    mergeAllTypes();
}


void TypeData::removeInjectedAtomType(const AtomType& type) {
    if (type.atomNumber == -1) return;

    if (injectedTypeList.contains(type.typeNumber)) {
        injectedTypeList.remove(type.typeNumber);
    }

    mergeAllTypes();
}


void TypeData::removeInjectedTypes() {
    injectedTypeList.clear();
    mergeAllTypes();
}


QDataStream& TypeData::save(QDataStream& out) const {
    out << typeList;
    return out;
}


QDataStream& TypeData::load(QDataStream& in) {
    in >> typeList;
    return in;
}


/**
 * @brief Utility function to merge the type list from the atom data and the injected types.
 */
void TypeData::mergeAllTypes() {
    allTypes.clear();
    allTypes.unite(typeList);
    for (int key : injectedTypeList.keys()) {
        if (!allTypes.contains(key)) {
            allTypes.insert(key, injectedTypeList[key]);
        }
    }
}
