/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "projectsetupwizard.h"

#include "api/entryeditor.h"
#include "api/programsettings.h"

#include "gui/widgets/entryeditorwidget.h"
#include "gui/widgets/filereadwidget.h"
#include "gui/widgets/importprojectcreatorwidget.h"
#include "gui/widgets/projectsetup/createscratchwidget.h"
#include "gui/widgets/templateprojectcreatorwidget.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QVBoxLayout>


ProjectSetupWizard::ProjectSetupWizard(QWidget* parent) : QWizard(parent) {

    int nWidth = 800;
    int nHeight = 600;
    if (parent != nullptr)
        setGeometry(parent->x() + parent->width() / 2 - nWidth / 2, parent->y() + parent->height() / 2 - nHeight / 2,
                    nWidth, nHeight);
    else
        resize(nWidth, nHeight);


//    QPixmap pixmap(":/logos/cosirma-logo");
//    setPixmap(QWizard::LogoPixmap, pixmap.scaled(100, 100, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    //    setPixmap(QWizard::LogoPixmap, QPixmap(":/logos/cosirma-logo.png"));
    // setPixmap(QWizard::WatermarkPixmap, QPixmap(":/logos/cosirma-logo.png"));

    m_templatePage = new TemplatePage(this);
    m_importPage = new ImportPage(this);
    m_importEditorPage = new ImportEditorPage(this);
    m_scratchPage = new ScratchPage(this);
    m_lastPage = new LastPage(this);

    setPage(Page_First, new FirstPage(this));
    setPage(Page_Template, m_templatePage);
    setPage(Page_Import, m_importPage);
    setPage(Page_ImportEditor, m_importEditorPage);
    setPage(Page_Scratch, m_scratchPage);
    setPage(Page_Last, m_lastPage);

    setStartId(Page_First);

    setWindowTitle(tr("New Project"));

    connect(this, &QWizard::currentIdChanged, [this](int currentId) {
        switch (currentId) {
            case Page_Last:
                m_lastPage->setProjectName(m_projectName);
        }
    });

    connect(m_templatePage, &TemplatePage::newProjectEntry, [this] {
        m_projectName = m_templatePage->projectName();
        m_entry = m_templatePage->entry();
    });

    connect(m_importPage, &ImportPage::newEntryCreated,
            [this] { m_importEditorPage->setEntry(m_importPage->entry()); });

    connect(m_importEditorPage, &ImportEditorPage::newProjectEntry, [this] {
        m_projectName = m_importEditorPage->projectName();
        m_entry = m_importEditorPage->entry();
    });

    connect(m_scratchPage, &ScratchPage::newProjectEntry, [this] {
        m_projectName = m_scratchPage->projectName();
        m_entry = m_scratchPage->entry();
    });
}


QString ProjectSetupWizard::projectPath() {
    return m_lastPage->projectPath();
}


void ProjectSetupWizard::accept() {
    qDebug() << "ProjectSetupWizard::accept()";

    QString path = m_lastPage->projectPath();
    QDir destDir(path);

    qDebug() << "    " << path;

    if (destDir.exists()) {
        QMessageBox::critical(this, "Creation failed!", "Target directory already exists.", QMessageBox::Ok);
        return;
    } else {
        destDir.mkpath(path);
    }

    QFile projectLabel(destDir.path() + "/.cosirmaproject");
    if (!projectLabel.open(QIODevice::WriteOnly)) qDebug() << "Failed to create project identifier file.";

    m_entry.data.info.timestamp = QDateTime::currentDateTime();
    std::pair<bool, QString> writing = Entry(destDir.path() + "/history/entry_1", m_entry).create();
    //! \todo Do something with the information!
    if (!writing.second.isEmpty()) qDebug() << writing.second;

    QWizard::accept();
}


//=============================================================================
// FIRST PAGE
//=============================================================================

FirstPage::FirstPage(QWidget* parent) : QWizardPage(parent) {
    setTitle(tr("Select Mode"));

    QPixmap pixmap(":/logos/cosirma-logo-text-below.png");
    setPixmap(QWizard::WatermarkPixmap, pixmap.scaled(200, 200, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    m_radioButton_template = new QRadioButton(tr("Create from Template"));
    m_radioButton_import = new QRadioButton(tr("Create from Imported Structure"));
    m_radioButton_scratch = new QRadioButton(tr("Create from Scratch"));

    m_radioButton_template->setChecked(true);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_radioButton_template);
    layout->addWidget(m_radioButton_import);
    layout->addWidget(m_radioButton_scratch);
    setLayout(layout);
}


int FirstPage::nextId() const {
    if (m_radioButton_template->isChecked()) {
        return ProjectSetupWizard::Page_Template;
    } else if (m_radioButton_import->isChecked()) {
        return ProjectSetupWizard::Page_Import;
    } else {
        return ProjectSetupWizard::Page_Scratch;
    }
}


//=============================================================================
// TEMPLATE PAGE
//=============================================================================

TemplatePage::TemplatePage(QWidget* parent) : QWizardPage(parent) {

    setTitle("Select Template");

    m_creator = new TemplateProjectCreatorWidget(this);
    connect(m_creator, &TemplateProjectCreatorWidget::frameLoadCompleted, [this](bool success) {
        m_frameLoaded = success;
        emit completeChanged();
        emit newProjectEntry();
    });
    connect(m_creator, &TemplateProjectCreatorWidget::frameLoading, [this] {
        m_frameLoaded = false;
        emit completeChanged();
    });

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_creator);
    setLayout(layout);
}


void TemplatePage::initializePage() {
    m_creator->enter();
}


int TemplatePage::nextId() const {
    return ProjectSetupWizard::Page_Last;
}


bool TemplatePage::isComplete() const {
    return m_frameLoaded;
}


QString TemplatePage::projectName() {
    return m_creator->projectName();
}


const ExtendedEntryData& TemplatePage::entry() {
    return m_creator->getEntry();
}


//=============================================================================
// IMPORT PAGE
//=============================================================================

ImportPage::ImportPage(QWidget* parent) : QWizardPage(parent) {

    m_fileReader = new FileReadWidget(this);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_fileReader);
    setLayout(layout);

    connect(m_fileReader, &FileReadWidget::readyToRead, [this](bool ready) {
        m_ready = ready;
        emit completeChanged();
    });
}


int ImportPage::nextId() const {
    return ProjectSetupWizard::Page_ImportEditor;
}


bool ImportPage::isComplete() const {
    return m_ready;
}


const ExtendedEntryData& ImportPage::entry() {
    return m_fileReader->getEntry();
}


bool ImportPage::validatePage() {
    qDebug() << "Reading entry...";
    if (!m_fileReader->readEntry()) {
        QMessageBox::critical(nullptr, "Read Error", "Failed to read the structure.", QMessageBox::Ok);
        return false;
    }
    emit newEntryCreated();
    return true;
}


//=============================================================================
// IMPORT EDITOR PAGE
//=============================================================================

ImportEditorPage::ImportEditorPage(QWidget* parent) : QWizardPage(parent) {
    m_editor = new EntryEditor();
    m_editorWidget = new EntryEditorWidget(m_editor);
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_editorWidget);
    layout->setStretch(0, 1);
    setLayout(layout);
}


int ImportEditorPage::nextId() const {
    return ProjectSetupWizard::Page_Last;
}


bool ImportEditorPage::validatePage() {
    emit newProjectEntry();
    return true;
}


const ExtendedEntryData& ImportEditorPage::entry() {

    m_entry = m_editor->modifiedEntry();

    m_entry.data.info.type = EntryInfo::Modification;
    m_entry.data.info.description = "Initial state";
    m_entry.data.info.timestamp = QDateTime::currentDateTime();
    m_entry.data.info.info = QString("Imported from file %1").arg("todo: access the file path");

    return m_entry;
}


QString ImportEditorPage::projectName() {
    return "imported";
}


void ImportEditorPage::setEntry(const ExtendedEntryData& entry) {
    m_editor->setEntry(entry);
    m_editorWidget->update();
}


//=============================================================================
// SCRATCH PAGE
//=============================================================================

ScratchPage::ScratchPage(QWidget* parent) : QWizardPage(parent) {

    m_creator = new CreateScratchWidget(this);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_creator);
    setLayout(layout);

    connect(m_creator, &CreateScratchWidget::entryCreated, [this] {
        m_ready = true;
        emit completeChanged();
    });
}


int ScratchPage::nextId() const {
    return ProjectSetupWizard::Page_Last;
}


bool ScratchPage::isComplete() const {
    return m_ready;
}


bool ScratchPage::validatePage() {
    emit newProjectEntry();
    return true;
}


QString ScratchPage::projectName() {
    return "new_project";
}


const ExtendedEntryData& ScratchPage::entry() {
    m_entry = m_creator->entry();

    m_entry.data.info.type = EntryInfo::Modification;
    m_entry.data.info.description = "Initial state";
    m_entry.data.info.timestamp = QDateTime::currentDateTime();
    m_entry.data.info.info = QString("Imported from file %1").arg("todo: access the file path");

    return m_entry;
}

//=============================================================================
// LAST PAGE
//=============================================================================

LastPage::LastPage(QWidget* parent) : QWizardPage(parent) {
    setTitle("Create Project");
    QPixmap pixmap(":/logos/cosirma-logo-text-below.png");
    setPixmap(QWizard::WatermarkPixmap, pixmap.scaled(200, 200, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    QLabel* nameLabel = new QLabel("Project Name:");
    m_lineEdit_name = new QLineEdit;

    QLabel* locationLabel = new QLabel("Project Location:");
    m_lineEdit_location = new QLineEdit(ProgramSettings::getInstance().locationSettings().projectDirPath);

    QPushButton* pushButton_location = new QPushButton("...");
    pushButton_location->setFixedWidth(30);

    QGridLayout* layout = new QGridLayout;
    layout->addWidget(nameLabel, 0, 0);
    layout->addWidget(m_lineEdit_name, 0, 1);
    layout->addWidget(locationLabel, 1, 0);
    layout->addWidget(m_lineEdit_location, 1, 1);
    layout->addWidget(pushButton_location, 1, 2);
    setLayout(layout);

    // Set a default project name, if the setter isn't called.
    setProjectName("temp");

    connect(m_lineEdit_name, &QLineEdit::editingFinished, this, &LastPage::handleLineEditUpdates);
    connect(m_lineEdit_location, &QLineEdit::editingFinished, this, &LastPage::handleLineEditUpdates);

    connect(pushButton_location, &QPushButton::clicked, [this] {
        QString path = QFileDialog::getExistingDirectory(this, tr("Choose save location"), m_lineEdit_location->text());
        if (!path.isEmpty()) {
            m_lineEdit_location->setText(path);
        }
    });
}


int LastPage::nextId() const {
    return -1;
}


bool LastPage::isComplete() const {
    return m_ready;
}

/*!
 * \brief Sets a suggested project name
 *
 * If the selected project folder already has a project with the given name, a number is appended to the name.
 *
 * \param name  The root of the name, a number might get appended to it
 */
void LastPage::setProjectName(const QString& name) {
    QString actualName = name;
    QDir dir(m_lineEdit_location->text());
    if (dir.exists()) {
        QSet<QString> projects = dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).toSet();
        int i = 1;
        QString tmpName = name;
        while (projects.contains(tmpName)) {
            tmpName = name + "_" + QString::number(i);
            i++;
        }
        actualName = tmpName;
    }
    m_lineEdit_name->setText(actualName);

    handleLineEditUpdates();
}


QString LastPage::projectPath() {
    return m_lineEdit_location->text() + "/" + m_lineEdit_name->text();
}


void LastPage::handleLineEditUpdates() {
    QDir dest(projectPath());
    m_ready = !dest.exists();
    emit completeChanged();
}
