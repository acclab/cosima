/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PROJECTSETUPWIZARD_H
#define PROJECTSETUPWIZARD_H

#include "api/entry.h"

#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QRadioButton>
#include <QWizard>


class TemplateProjectCreatorWidget;
class ImportProjectCreatorWidget;
class FileReadWidget;
class EntryEditor;
class EntryEditorWidget;
class CreateScratchWidget;

class TemplatePage;
class ImportPage;
class ImportEditorPage;
class ScratchPage;
class LastPage;


class ProjectSetupWizard : public QWizard {
    Q_OBJECT

  public:
    enum { Page_First, Page_Template, Page_Import, Page_ImportEditor, Page_Scratch, Page_Last };

    ProjectSetupWizard(QWidget* parent = nullptr);

    QString projectPath();

    void accept() override;

  private:
    TemplatePage* m_templatePage;
    ImportPage* m_importPage;
    ImportEditorPage* m_importEditorPage;
    ScratchPage* m_scratchPage;
    LastPage* m_lastPage;

    QString m_projectName;
    ExtendedEntryData m_entry;
};


//=============================================================================
// FIRST PAGE
//=============================================================================

class FirstPage : public QWizardPage {

    Q_OBJECT

  public:
    FirstPage(QWidget* parent = nullptr);

    int nextId() const override;

  private:
    QRadioButton* m_radioButton_template;
    QRadioButton* m_radioButton_import;
    QRadioButton* m_radioButton_scratch;
};


//=============================================================================
// TEMPLATE PAGE
//=============================================================================

class TemplatePage : public QWizardPage {
    Q_OBJECT

  public:
    TemplatePage(QWidget* parent = nullptr);

    void initializePage() override;
    int nextId() const override;
    bool isComplete() const override;

    QString projectName();
    const ExtendedEntryData& entry();

  private:
    TemplateProjectCreatorWidget* m_creator;
    bool m_frameLoaded = false;

  signals:
    void newProjectEntry();
};


//=============================================================================
// IMPORT PAGE
//=============================================================================

class ImportPage : public QWizardPage {
    Q_OBJECT

  public:
    ImportPage(QWidget* parent = nullptr);

    int nextId() const override;
    bool isComplete() const override;
    bool validatePage() override;

    const ExtendedEntryData& entry();

  private:
    FileReadWidget* m_fileReader;
    // ImportProjectCreatorWidget* m_creator;
    bool m_ready = false;

  signals:
    void newEntryCreated();
};


//=============================================================================
// IMPORT EDITOR PAGE
//=============================================================================

class ImportEditorPage : public QWizardPage {
    Q_OBJECT

  public:
    ImportEditorPage(QWidget* parent = nullptr);

    int nextId() const override;
    bool validatePage() override;

    QString projectName();
    const ExtendedEntryData& entry();

    void setEntry(const ExtendedEntryData& entry);

  private:
    EntryEditor* m_editor;
    EntryEditorWidget* m_editorWidget;
    //    FileReadWidget* m_fileReader;
    // ImportProjectCreatorWidget* m_creator;

    ExtendedEntryData m_entry;


  signals:
    void newProjectEntry();
};


//=============================================================================
// SCRATCH PAGE
//=============================================================================

class ScratchPage : public QWizardPage {
    Q_OBJECT

  public:
    ScratchPage(QWidget* parent = nullptr);

    int nextId() const override;
    bool isComplete() const override;
    bool validatePage() override;

    QString projectName();
    const ExtendedEntryData& entry();

  private:
    CreateScratchWidget* m_creator;

    ExtendedEntryData m_entry;

    bool m_ready = false;


  signals:
    void newProjectEntry();
};


//=============================================================================
// LAST PAGE
//=============================================================================

class LastPage : public QWizardPage {
    Q_OBJECT

  public:
    LastPage(QWidget* parent = nullptr);

    void setProjectName(const QString& name);
    int nextId() const override;
    bool isComplete() const override;

    QString projectPath();

  private:
    QLineEdit* m_lineEdit_name;
    QLineEdit* m_lineEdit_location;

    bool m_ready = false;

  private slots:
    void handleLineEditUpdates();
};


#endif  // PROJECTSETUPWIZARD_H
