/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "plotcellglwidget.h"

#include "api/programsettings.h"
#include "api/project.h"
#include "api/state/enums.h"
#include "api/util.h"
#include "api/utils/beamdatautil.h"

#include "gui/rendering/arrowmodel.h"
#include "gui/rendering/cubemodel.h"
#include "gui/rendering/particlemodel.h"
#include "gui/rendering/spheremodel.h"

#include <QDebug>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QtOpenGL>
#include <QtWidgets>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

#include <stdlib.h>
#include <cmath>
#include <limits>


PlotCellGLWidget::PlotCellGLWidget(QWidget* parent) : QOpenGLWidget(parent) {
    qDebug() << "PlotCellGLWidget::PlotCellGLWidget()" << this;

    setMouseTracking(true);
    m_zoomScalingFactor = 1.0;
    m_perspective = false;

    setPlotMode(PlotMode::Plain);
    setModelOrientation(0, 0, 0);
}


PlotCellGLWidget::~PlotCellGLWidget() {
    disconnect(m_particles);
    disconnect(m_particleFrame);

    delete m_particles;

    for (auto model : m_models) {
        delete model;
    }
    m_models.clear();
}


QSize PlotCellGLWidget::minimumSizeHint() const {
    return QSize(50, 50);
}


QSize PlotCellGLWidget::sizeHint() const {
    return QSize(400, 400);
}


void PlotCellGLWidget::setVisualSettings(const VisualSettings& vSettings) {
    m_visualSettings = &vSettings;
}


void PlotCellGLWidget::setFrame(const XyzData& frame) {
    qDebug() << this << "::setFrame()";

    if (!QOpenGLContext::currentContext()) {
        qDebug() << "    NO OPENGL CONTEXT YET...";
    }

    if (m_particles) {
        disconnect(m_particles);
        disconnect(m_particleFrame);

        delete m_particles;

        delete m_axisTripodX;
        delete m_axisTripodY;
        delete m_axisTripodZ;
    }


    bool prevShowParticleFrameFaces = false;
    bool prevShowBeamFaces = false;
    bool prevShowBeamLines = false;

    // Remove the objects this class put into the model list.
    if (m_particles != nullptr) {
        removeModel(m_particles);
    }
    if (m_particleFrame != nullptr) {
        prevShowParticleFrameFaces = m_particleFrame->showFaces();
        removeModel(m_particleFrame);
    }
    if (m_beamArrow != nullptr) {
        prevShowBeamFaces = m_beamArrow->showFaces();
        prevShowBeamLines = m_beamArrow->showLines();
        removeModel(m_beamArrow);
    }

    // Recreate them and insert them into the list again!
    m_particles = new ParticleModel(frame, m_visualSettings->atomTypeColors(), m_visualSettings->atomTypeSizes());
    connect(m_visualSettings, &VisualSettings::atomTypeColorsChanged, m_particles, &ParticleModel::on_setColors);
    connect(m_visualSettings, &VisualSettings::atomTypeSizesChanged, m_particles, &ParticleModel::on_setRadii);

    m_particleFrame = new CubeModel(frame.cellLims, QColor(), m_visualSettings->cellBoundaryColor(), 0);
    m_particleFrame->setShowFaces(prevShowParticleFrameFaces);
    m_particleFrame->setShowLines(ProgramSettings::getInstance().visualSettings().cellBoundaryEnabled());
    connect(m_visualSettings, &VisualSettings::cellBoundaryColorChanged, [this](const QColor& color) {
        m_particleFrame->setLineColor(color);
        update();
    });
    connect(m_visualSettings, &VisualSettings::cellBoundaryEnabledChanged, [this](bool show) {
        m_particleFrame->setShowLines(show);
        update();
    });
    addModel(m_particleFrame);  // Render the particle frame with the default render loop


    ArrowModel::ColorGradient defaultGradient;

    m_beamArrow = new ArrowModel(defaultGradient);
    m_beamArrow->setShowFaces(prevShowBeamFaces);
    m_beamArrow->setShowLines(prevShowBeamLines);
    addModel(m_beamArrow);  // Render the beam arrow with the default render loop.

    if (prevShowBeamFaces || prevShowBeamLines) {
        // Reset the beam parameters
        setBeamArrowTransformFromBeam(m_beam);
    }

    // Setup the axis tripod

    ArrowModel::ColorGradient red(QColor(255, 0, 0));
    ArrowModel::ColorGradient green(QColor(0, 255, 0));
    ArrowModel::ColorGradient blue(QColor(0, 0, 255));

    float arrowBodyLength = 0.5f;
    float arrowScale = 40.0f;

    m_axisTripodX = new ArrowModel(red, arrowBodyLength, true);
    m_axisTripodX->transform.setScale(arrowScale);
    m_axisTripodX->transform.setPosition(0, 0, 0);
    m_axisTripodX->transform.rotate(0.0f, glm::radians(90.0f), 0.0f);


    m_axisTripodY = new ArrowModel(green, arrowBodyLength, true);
    m_axisTripodY->transform.setScale(arrowScale);
    m_axisTripodY->transform.setPosition(0, 0, 0);
    m_axisTripodY->transform.rotate(glm::radians(90.0f), 0.0f, 0.0f);


    m_axisTripodZ = new ArrowModel(blue, arrowBodyLength, true);
    m_axisTripodZ->transform.setScale(arrowScale);
    m_axisTripodZ->transform.setPosition(0, 0, 0);
    m_axisTripodZ->transform.rotate(0, 0, 0);

    initView(frame.cellLims);
    update();
}


void PlotCellGLWidget::addModel(AbstractModel* model) {
    m_models.push_back(model);
    update();
}


void PlotCellGLWidget::removeModel(AbstractModel* model) {
    auto it = std::remove(m_models.begin(), m_models.end(), model);
    if (it != m_models.end()) {
        m_models.erase(it);
    }
    update();
}


/*!
 * Sets the initial camera position and calculates the plot's space width that will
 * be used to create the projection matrix.
 */
void PlotCellGLWidget::initView(const Box& limits) {
    m_spaceLimits = limits;
    m_cameraLoc = glm::vec3(0, 0, glm::length(m_spaceLimits.diagonal()) * 1.5);
    centerCell();
}


/*!
 * \brief PlotCellGLWidget::createShaderProgram Helper function to create the shader programs
 * \param vert
 * \param frag
 * \return
 */
void PlotCellGLWidget::createShaderProgram(QOpenGLShaderProgram& program, const QString& vert, const QString& frag) {
    qDebug() << "Compiling shader program:" << vert << frag;
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, vert)) {
        qDebug() << "        Failed to add Vertex Shader";
    }
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, frag)) {
        qDebug() << "        Failed to add Fragment Shader";
    }
    if (!program.link()) {
        qDebug() << "        Failed to link the shaders";
    }
}


/*!
 * Sets the GL settings, links shaders, and binds a texture used for the point sprites.
 */
void PlotCellGLWidget::initializeGL() {
    qDebug() << "PlotCellGLWidget::initializeGL()" << this;
    QOpenGLWidget::initializeGL();

    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    setupRendering();
    setupShaders();


    // OpenGL Debugger
    QOpenGLContext* ctx = QOpenGLContext::currentContext();
    qDebug() << ctx << "opened";
    QOpenGLDebugLogger* logger = new QOpenGLDebugLogger(this);

    logger->initialize();  // initializes in the current context, i.e. ctx

    connect(logger, &QOpenGLDebugLogger::messageLogged, [logger] {
        const QList<QOpenGLDebugMessage> messages = logger->loggedMessages();
        if (messages.size() > 0) qDebug() << messages.size();
        for (const QOpenGLDebugMessage& message : messages) qDebug() << message;
    });
    logger->startLogging(QOpenGLDebugLogger::SynchronousLogging);
}


void PlotCellGLWidget::setupShaders() {
    createShaderProgram(m_screenQuadShader, ":/shaders/screen.vert", ":/shaders/screen.frag");
    createShaderProgram(m_screenQuadPeelBlendShader, ":/shaders/screen_peel.vert", ":/shaders/screen_peel.frag");

    createShaderProgram(m_particlesInitShader, ":/shaders/particle.vert", ":/shaders/particle_init.frag");
    createShaderProgram(m_particlesPeelShader, ":/shaders/particle.vert", ":/shaders/particle_peeling.frag");

    createShaderProgram(m_shaderInitShader, ":/shaders/mesh.vert", ":/shaders/mesh_init.frag");
    createShaderProgram(m_shaderPeelShader, ":/shaders/mesh.vert", ":/shaders/mesh_peeling.frag");

    createShaderProgram(m_simpleShader, ":/shaders/mesh.vert", ":/shaders/mesh.frag");
}


void PlotCellGLWidget::setupRendering() {

    // Create the alternating framebuffers for the render loop using depth peeling
    glGenTextures(2, m_frontDepthTexId);
    glGenTextures(2, m_frontColorTexId);
    glGenFramebuffers(2, m_frontFboId);

    for (int i = 0; i < 2; i++) {
        // Create the depth buffer attachment
        glBindTexture(GL_TEXTURE_2D, m_frontDepthTexId[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, width(), height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT,
                     nullptr);

        // Create the color attachment
        glBindTexture(GL_TEXTURE_2D, m_frontColorTexId[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width(), height(), 0, GL_RGBA, GL_FLOAT, nullptr);

        // Create the framebuffer object
        glBindFramebuffer(GL_FRAMEBUFFER, m_frontFboId[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_frontDepthTexId[i], 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_frontColorTexId[i], 0);
    }

    // Create the screen framebuffer
    glGenTextures(1, &m_frontColorBlenderTexId);
    glBindTexture(GL_TEXTURE_2D, m_frontColorBlenderTexId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width(), height(), 0, GL_RGBA, GL_FLOAT, nullptr);

    glGenFramebuffers(1, &m_frontColorBlenderFboId);
    glBindFramebuffer(GL_FRAMEBUFFER, m_frontColorBlenderFboId);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_frontDepthTexId[0], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_frontColorBlenderTexId, 0);


    // Setup screen quad

    m_screenVAO = new QOpenGLVertexArrayObject;
    m_screenVAO->create();
    m_screenVAO->bind();

    // An array of interleaved vertices.
    // 2 floats for position and 2 floats for texture coordinates.
    static const GLfloat vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f, 0.0f,  // Vertex 0
        1.0f,  -1.0f, 1.0f, 0.0f,  // Vertex 1
        1.0f,  1.0f,  1.0f, 1.0f,  // Vertex 2

        -1.0f, -1.0f, 0.0f, 0.0f,  // Vertex 3
        1.0f,  1.0f,  1.0f, 1.0f,  // Vertex 4
        -1.0f, 1.0f,  0.0f, 1.0f   // Vertex 5
    };


    //! \todo make sure the buffer can be deleted when the application is done

    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);

    // 1st attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

    // 2nd attribute buffer : texture coordinates
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}


void PlotCellGLWidget::cleanupRendering() {
    glDeleteFramebuffers(2, m_frontFboId);
    glDeleteFramebuffers(1, &m_frontColorBlenderFboId);
    glDeleteTextures(2, m_frontDepthTexId);
    glDeleteTextures(2, m_frontColorTexId);
    glDeleteTextures(1, &m_frontColorBlenderTexId);

    if (m_screenVAO) {
        m_screenVAO->destroy();
    }
}


void PlotCellGLWidget::renderParticles(QOpenGLShaderProgram& program, const float& spaceW) {
    if (m_beamArrow == nullptr) return;

    program.setUniformValue("beamActive", (m_mode == PlotMode::Beam));
    if (m_mode == PlotMode::Beam) {
        m_beamArrow->setShowFaces(true);
        program.setUniformValue("beamType", m_beam.type);
        program.setUniformValue("beamWidth", m_beam.width);
        program.setUniformValue("beamIniPos", QVector3D(m_beam.position.x, m_beam.position.y, m_beam.position.z));
        program.setUniformValue("beamDirection", QVector3D(m_beam.direction.x, m_beam.direction.y, m_beam.direction.z));
    } else {
        m_beamArrow->setShowFaces(false);
    }

    if (m_perspective) {
        program.setUniformValue("vpSize", QVector2D(width(), height()));
        program.setUniformValue("perspective", true);
    } else {
        program.setUniformValue("pointScalingFactor", std::min(width(), height()) / spaceW / m_zoomScalingFactor);
        program.setUniformValue("perspective", false);
    }

    program.setUniformValue("p", QMatrix4x4(glm::value_ptr(m_projection)).transposed());
    program.setUniformValue("v", QMatrix4x4(glm::value_ptr(m_view)).transposed());

    m_particles->draw(m_model, program);
}


void PlotCellGLWidget::renderModels(QOpenGLShaderProgram& program) {
    program.setUniformValue("p", QMatrix4x4(glm::value_ptr(m_projection)).transposed());
    program.setUniformValue("v", QMatrix4x4(glm::value_ptr(m_view)).transposed());

    for (std::size_t i = 0; i < m_models.size(); i++) {
        //        program.setUniformValue("m", QMatrix4x4(glm::value_ptr(m_model *
        //        m_models[i]->modelMatrix())).transposed());
        m_models[i]->draw(m_model, program);
    }
}


void PlotCellGLWidget::renderTripod(QOpenGLShaderProgram& program) {

    program.setUniformValue("p", QMatrix4x4(glm::value_ptr(m_projection_screen)).transposed());
    program.setUniformValue("v", QMatrix4x4(glm::value_ptr(glm::mat4(1.0))).transposed());

    m_axisTripodX->draw(m_model, program);
    m_axisTripodY->draw(m_model, program);
    m_axisTripodZ->draw(m_model, program);

    renderText(0, 5, "x", QColor(255, 0, 0));
    renderText(10, 5, "y", QColor(0, 255, 0));
    renderText(20, 5, "z", QColor(0, 0, 255));
}


void PlotCellGLWidget::renderText(int x, int y, const QString &text, const QColor& color) {
    QFont font;

    // Identify x and y locations to render text within widget
    int height = this->height();
    GLdouble textPosX = x;
    GLdouble textPosY = height - y;

    // Render text
    QPainter painter(this);
    painter.setPen(color);
    painter.setFont(font);
    painter.drawText(textPosX, textPosY, text);
    painter.end();
}


/*!
 * Helper function for binding textures to the graphics card.
 */
void PlotCellGLWidget::bindTexture(QOpenGLShaderProgram& shader, int textureUnit, GLuint texture,
                                   const char* uniformName) {
    glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + textureUnit));
    glBindTexture(GL_TEXTURE_2D, texture);
    shader.setUniformValue(uniformName, textureUnit);
    glActiveTexture(GL_TEXTURE0);
}

/*!
 * If painting for the first time after a change in data, creates the vertex buffer objects.
 * Paints the particles and boxes according to VBOs.
 */
void PlotCellGLWidget::paintGL() {
    //! \todo call the projection matrix setup from the resizeGL() function.

    float aspectRatio = static_cast<float>(width()) / height();
    float spaceW = static_cast<float>(glm::length(m_spaceLimits.diagonal()));
    if (width() < height()) {
        spaceW /= aspectRatio;
    }
    if (m_perspective) {
        m_projection = glm::perspective(glm::radians(40.0f * m_zoomScalingFactor), aspectRatio, 0.01f, 10000.0f);
    } else {
        float spaceRadius = spaceW / 2.0f;
        float left = -spaceRadius * aspectRatio * m_zoomScalingFactor;
        float right = spaceRadius * aspectRatio * m_zoomScalingFactor;
        float bottom = -spaceRadius * m_zoomScalingFactor;
        float top = spaceRadius * m_zoomScalingFactor;
        m_projection = glm::ortho(left, right, bottom, top, 0.01f, 10000.0f);
    }

    m_projection_screen = glm::ortho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);

    m_view = glm::mat4(1.0f);
    m_view = glm::translate(m_view, -m_focalPoint - glm::vec3(0, 0, m_cameraLoc.z));

    glViewport(0, 0, width(), height());

    // Set the default model matrix of the scene. This is used as basis of all transformations.
    m_model = glm::mat4(1.0f);
    glm::vec3 spaceCenter = m_spaceLimits.center();
    glm::quat rotation = m_rotation * m_orientationRotation;

    m_model = glm::translate(m_model, spaceCenter);
    m_model *= glm::toMat4(rotation);
    m_model = glm::translate(m_model, -spaceCenter);

    //! \todo Maybe set this one in the shaders instead of only view (one uniform less to set)?
    m_modelView = m_view * m_model;


    /*
     * ---------------------------------------
     * 1. Render the default scene to texture
     * ---------------------------------------
     */

    glBindFramebuffer(GL_FRAMEBUFFER, m_frontColorBlenderFboId);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);

    // Render the scene

    m_particlesInitShader.bind();
    renderParticles(m_particlesInitShader, spaceW);
    m_particlesInitShader.release();

    m_shaderInitShader.bind();
    renderModels(m_shaderInitShader);
    m_shaderInitShader.release();


    /*
     * ---------------------------------------------
     * 2. Depth peel and blend to the front texture
     * ---------------------------------------------
     */

    for (int layer = 1; layer <= ProgramSettings::getInstance().visualSettings().depthPeelCount(); ++layer) {
        // Alternate between two buffers
        int currId = layer % 2;
        int prevId = 1 - currId;

        glBindFramebuffer(GL_FRAMEBUFFER, m_frontFboId[currId]);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);

        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);

        // Render scene

        m_particlesPeelShader.bind();
        bindTexture(m_particlesPeelShader, 0, m_frontDepthTexId[prevId], "depthSampler");
        renderParticles(m_particlesPeelShader, spaceW);
        m_particlesPeelShader.release();

        m_shaderPeelShader.bind();
        bindTexture(m_shaderPeelShader, 0, m_frontDepthTexId[prevId], "depthSampler");
        renderModels(m_shaderPeelShader);
        m_shaderPeelShader.release();


        // Blend scene with front texture

        glBindFramebuffer(GL_FRAMEBUFFER, m_frontColorBlenderFboId);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);

        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);

        glBlendEquation(GL_FUNC_ADD);
        glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);

        m_screenQuadPeelBlendShader.bind();
        bindTexture(m_screenQuadPeelBlendShader, 0, m_frontColorTexId[currId], "colorTexture");
        m_screenVAO->bind();
        glDrawArrays(GL_TRIANGLES, 0, 6);
        m_screenVAO->release();
        m_screenQuadPeelBlendShader.release();
    }

    /*
     * ------------------------------------
     * 3. Draw the blended frame to screen
     * ------------------------------------
     */

    QOpenGLFramebufferObject::bindDefault();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    m_screenQuadShader.bind();
    m_screenQuadShader.setUniformValue("backgroundColor", m_bgColor);
    bindTexture(m_screenQuadShader, 0, m_frontColorBlenderTexId, "colorTexture");
    m_screenVAO->bind();
    glDrawArrays(GL_TRIANGLES, 0, 6);
    m_screenVAO->release();
    m_screenQuadShader.release();


    /*
     * ---------------------------
     * 4. Update the depth buffer
     * ---------------------------
     */
    //! \note this is actually not part of the depth peeling algorithm, however, the current implementation of
    //! unprojecting the particle locations is based on the depth buffer of the entire scene.

    glClear(GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    // Only render the particles

    m_particlesInitShader.bind();
    renderParticles(m_particlesInitShader, spaceW);
    m_particlesInitShader.release();

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);


    glViewport(0, 0, 75, 75);

    m_simpleShader.bind();
    renderTripod(m_simpleShader);
    m_simpleShader.release();
}


void PlotCellGLWidget::on_atomsUpdated() {
    qDebug() << "PlotCellGLWidget::on_typesUpdated()";
    m_particles->on_atomsUpdated(m_visualSettings->atomTypeColors(), m_visualSettings->atomTypeSizes());

    update();
}


void PlotCellGLWidget::on_hideType(int type, bool hide) {
    m_particles->on_hideType(type, hide);
    update();
}

void PlotCellGLWidget::on_setTypeRadius(int type, float radius) {
    m_particles->on_setTypeRadius(type, radius);
    update();
}

void PlotCellGLWidget::on_setTypeColors(const QHash<int, QColor>& colors) {
    m_particles->on_setColors(colors);
    update();
}

void PlotCellGLWidget::on_setOutlineBox(const Box& box) {
    m_particleFrame->setBox(box);
    update();
}


/*!
 * Updates the plotted beam. Plotting mode has to be set to PlotCellGLWidget::Beam for this to show anything.
 *
 */
void PlotCellGLWidget::setBeam(BeamData beamData, Box cellLims) {
    if (beamData.type == api::enums::beam::Broad) {
        m_beam.type = 0;
        m_beam.position = BroadBeamUtil::position(cellLims, beamData.face);
        m_beam.rotation = BroadBeamUtil::rotation(beamData.inclination, beamData.azimuth, beamData.face);

    } else if (beamData.type == api::enums::beam::Focused) {
        m_beam.position = beamData.position;
        //! \note If theta is 0 or 180, the information of phi will disappear
        double clampedTheta = std::min(179.9999, std::max(0.0001, beamData.theta));
        m_beam.rotation = glm::radians(glm::vec3(0, clampedTheta, beamData.phi));

        if (beamData.shape == api::enums::beam::Square)
            m_beam.type = 1;
        else if (beamData.shape == api::enums::beam::Circular)
            m_beam.type = 2;
    }

    if (beamData.type == api::enums::beam::Broad) {
    } else {
    }
    m_beam.direction = BroadBeamUtil::direction(m_beam.rotation);
    m_beam.width = static_cast<float>(beamData.width);

    setBeamArrowTransformFromBeam(m_beam);

    update();
}


void PlotCellGLWidget::setBeam(IonTransformData data) {
    //! \note If theta is 0 or 180, the information of phi will disappear
    //! \todo I'm not entirely sure what this message means, however, the small offset seems to work
    double clampedTheta = std::min(179.9999, std::max(0.0001, data.theta));
    glm::vec3 rot = glm::radians(glm::vec3(0, clampedTheta, data.phi));

    m_beam.type = 0;
    m_beam.position = data.position;
    m_beam.width = 0;
    //    m_beam.direction =
    m_beam.rotation = rot;

    setBeamArrowTransformFromBeam(m_beam);

    update();
}


void PlotCellGLWidget::setBeamArrowTransformFromBeam(const BeamParameters& m_beam) {
    m_beamArrow->transform.setPosition(m_beam.position);
    m_beamArrow->transform.setScale(glm::length(m_spaceLimits.diagonal()) / 3);
    m_beamArrow->transform.rotate(m_beam.rotation);
}


/*!
 * Changes the background fill color of the plot.
 */
void PlotCellGLWidget::setBackgroundColor(QColor color) {
    qDebug() << "PlotCellGLWidget::setBackgroundColor()" << this;

    m_bgColor = color;
    update();
}


/*!
 * Gets the background color of the plot.
 */
QColor PlotCellGLWidget::getBackgroundColor() {
    return m_bgColor;
}


/*!
 * Sets the plotting mode.
 */
void PlotCellGLWidget::setPlotMode(PlotCellGLWidget::PlotMode plotMode) {
    m_mode = plotMode;
    update();
}


/*!
 * Overrides QOpenGLWidget::resizeGL().
 * Updates the viewport and scaling factor that will affect the size of the contents.
 */
void PlotCellGLWidget::resizeGL(int width, int height) {
    cleanupRendering();
    setupRendering();

    glViewport(0, 0, width, height);

    //! \todo move the projection matrix creation to its own function and call it from here.
}


/*!
 * Overrides QOpenGLWidget::mousePressEvent().
 * Saves the position of the event.
 */
void PlotCellGLWidget::mousePressEvent(QMouseEvent* event) {
    m_previousCursorPosition = QVector2D(event->pos());
    m_mouseMovedAfterPress = false;
    update();
}


/*!
 * Overrides QOpenGLWidget::mouseMoveEvent().
 * Tracks mouse events. Dragging with the left button rotates the cell.
 * Dragging with the right button moves camera's
 * focal point in xy-plane.
 */
void PlotCellGLWidget::mouseMoveEvent(QMouseEvent* event) {
    QOpenGLWidget::mouseMoveEvent(event);

    if (event->buttons() == Qt::LeftButton) {
        QVector2D diff = QVector2D(event->localPos()) - m_previousCursorPosition;
        float factor = 300.0f / std::min(width(), height()) * m_zoomScalingFactor;

        glm::vec3 xRotationAxis = (m_rotation * m_orientationRotation) * glm::vec3(0, 0, 1);
        float angleX = diff.x() * glm::radians(factor);
        m_rotation = glm::angleAxis(angleX, xRotationAxis) * m_rotation;

        glm::vec3 yRotationAxis(1, 0, 0);
        float angleY = diff.y() * glm::radians(factor);
        m_rotation = glm::angleAxis(angleY, yRotationAxis) * m_rotation;

        // Normalizing the rotation quaternion to prevent error propagation
        m_rotation = glm::normalize(m_rotation);

        m_previousCursorPosition = QVector2D(event->pos());

        emit orientationChanged();
        emit mouseDragged();

    } else if (event->buttons() == Qt::RightButton) {
        QVector2D diff = QVector2D(event->localPos()) - m_previousCursorPosition;
        float spaceW = static_cast<float>(glm::length(m_spaceLimits.diagonal()));
        float factor = std::min(spaceW / width(), spaceW / height()) * m_zoomScalingFactor;
        m_focalPoint += glm::vec3(-diff[0] * factor, diff[1] * factor, 0);
        m_previousCursorPosition = QVector2D(event->pos());

        emit mouseDragged();
    }
    float depth = -1;
    glReadPixels(event->x(), width() - event->y(), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
    if (depth > -1e-5f && depth < 1.0001f) {
        glm::vec3 mousePos(event->x(), height() - event->y(), depth);
        mousePos = glm::unProject(mousePos, m_modelView, m_projection, glm::vec4(0, 0, width(), height()));
        emit mouseMoved(glm::dvec3(mousePos));
    }
    m_mouseMovedAfterPress = true;
    update();
}

/*!
 * Overrides QOpenGLWidget::mouseReleaseEvent().
 * If the mouse has not been moved after the click, emits the clicked signal.
 */
void PlotCellGLWidget::mouseReleaseEvent(QMouseEvent* event) {
    if (!m_mouseMovedAfterPress) {
        float depth = -1;
        glReadPixels(event->x(), height() - event->y(), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
        if (depth > -1e-5f && depth < 1.0001f) {
            glm::vec3 mousePos(event->x(), height() - event->y(), depth);
            mousePos = glm::unProject(mousePos, m_modelView, m_projection, glm::vec4(0, 0, width(), height()));
            emit clicked(glm::dvec3(mousePos));
        }
    } else {
        emit mouseDragged();
    }
    update();
}


/*!
 * Overrides QOpenGLWidget::wheelEvent().
 * Implemnets zooming by changing the scaling factor that will affect the size of the contents.
 */
void PlotCellGLWidget::wheelEvent(QWheelEvent* event) {
    //! \todo remove hard-coded values for the zoom step and zoom count?
    float zoom = 1.0f;
    if (event->delta() < 0) {
        if (m_zoomCount < 20) {
            zoom = 1 / 0.9f;
            m_zoomCount++;
        }
    } else if (event->delta() > 0) {
        if (m_zoomCount > -35) {
            zoom = 0.9f;
            m_zoomCount--;
        }
    }
    m_zoomScalingFactor *= zoom;

    update();

    event->accept();
}

/*!
 * Overrides QOpenGLWidget::leaveEvent().
 * Emits a signal when the cursor leaves the widget.
 */
void PlotCellGLWidget::leaveEvent(QEvent* /*event*/) {
    emit widgetLeft();
}


/*!
 * Zeroes the plot.
 */
void PlotCellGLWidget::zero() {
    //    makeCurrent();
    //    if (m_particlesDrawn) {
    //        m_particleVertex_vbo->destroy();
    //        m_particleColor_vbo->destroy();
    //        m_particleRadius_vbo->destroy();
    //    }

    //    //    for (ArrayObject* a : m_arrayObjects) {
    //    //        if (a->drawn) {
    //    //            a->vertex_vbo->destroy();
    //    //            a->faceColor_vbo->destroy();
    //    //            a->borderColor_vbo->destroy();
    //    //        }

    //    //        if (a != &m_fixedArrayObject && a != &m_arrowObject) delete a;
    //    //    }

    //    //    m_arrayObjects.clear();
    //    //    m_arrayObjects.push_front(&m_fixedArrayObject);
    //    //    if (m_mode == PlotCellGLWidget::Beam) m_arrayObjects.push_back(&m_arrowObject);

    //    m_particlesDrawn = false;

    //    m_frame = nullptr;
}


/*!
 * Sets the orientation of the simulation cell.
 */
void PlotCellGLWidget::setModelOrientation(float angleX, float angleY, float angleZ) {
    m_orientationRotation = glm::quat(glm::vec3(angleX, angleY, angleZ) * glm::radians(1.f));
    m_rotation = glm::quat_identity<glm::quat::value_type, glm::highp>();
    update();
}


/*!
 * Rotates the simulation cell around an axis by an angle.
 */
void PlotCellGLWidget::rotate(const glm::vec3& axis, float angle) {
    glm::vec3 rotationAxis = (m_rotation * m_orientationRotation) * axis;
    m_rotation = glm::angleAxis(glm::radians(angle), rotationAxis) * m_rotation;
    update();
}


/*!
 * Centers the Simulation cell based on the limits that were given.
 *
 */
void PlotCellGLWidget::centerCell() {
    m_focalPoint = m_spaceLimits.center();
    m_zoomScalingFactor = 1.0;
}


/*!
 * If the value of the argument is "true", the perspective projection will be used.
 * If "false", the orthogonal projection will be used.
 */
void PlotCellGLWidget::usePerspective(bool use) {
    if (m_perspective == use) return;
    m_perspective = use;
}


/*!
 * Returns "true" if the perspective projection was used.
 */
bool PlotCellGLWidget::isPerspective() {
    return m_perspective;
}
