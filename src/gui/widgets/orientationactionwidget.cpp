/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "orientationactionwidget.h"

#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QSpacerItem>

OrientationActionWidget::OrientationActionWidget(EntryEditor* editor, QWidget* parent)
        : AbstractActionWidget(editor, parent) {

    auto generateButton = [this](const QIcon& icon, EntryEditor::CellRotationAction action, const QString& tooltip) {
        QPushButton* pb = new QPushButton(this);
        pb->setIconSize(QSize(20, 20));
        pb->setIcon(icon);
        pb->setToolTip(tooltip);
        connect(pb, &QPushButton::clicked, [=] {
            EntryEditor::CellRotationAction a(action);
            m_editor->edit(a);
        });
        return pb;
    };

    QIcon cwIcon(":/icons/redo-alt");
    QIcon acwIcon(":/icons/undo-alt");

    QGridLayout* grid = new QGridLayout;
    grid->addWidget(new QLabel("x"), 0, 0);
    grid->addWidget(generateButton(acwIcon, EntryEditor::CellRotationAction::xCounterClockwise,
                                   "Rotate 90 degrees counter clockwise around the x-axis."),
                    0, 1);
    grid->addWidget(generateButton(cwIcon, EntryEditor::CellRotationAction::xClockwise,
                                   "Rotate 90 degrees clockwise around the x-axis."),
                    0, 2);
    grid->addWidget(new QLabel("y"), 1, 0);
    grid->addWidget(generateButton(acwIcon, EntryEditor::CellRotationAction::yCounterClockwise,
                                   "Rotate 90 degrees counter clockwise around the y-axis."),
                    1, 1);
    grid->addWidget(generateButton(cwIcon, EntryEditor::CellRotationAction::yClockwise,
                                   "Rotate 90 degrees clockwise around the y-axis."),
                    1, 2);
    grid->addWidget(new QLabel("z"), 2, 0);
    grid->addWidget(generateButton(acwIcon, EntryEditor::CellRotationAction::zCounterClockwise,
                                   "Rotate 90 degrees counter clockwise around the z-axis."),
                    2, 1);
    grid->addWidget(generateButton(cwIcon, EntryEditor::CellRotationAction::zClockwise,
                                   "Rotate 90 degrees clockwise around the z-axis."),
                    2, 2);

    grid->addItem(new QSpacerItem(1, 20, QSizePolicy::Expanding, QSizePolicy::MinimumExpanding), 1, 3);

    setLayout(grid);
}
