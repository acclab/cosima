/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "simulator.h"

#include "api/entry.h"
#include "api/programsettings.h"
#include "api/simulation/mdparameters.h"

#include <QDebug>
#include <QDirIterator>
#include <QFile>
#include <QFileDialog>
#include <QMetaType>
#include <QSet>
#include <QTemporaryDir>
#include <QTimer>


Simulator::Simulator(ParcasBaseController* controller, QObject* parent) : QObject(parent), m_controller(controller) {
    qRegisterMetaType<Simulator::Status>();
}


Simulator::~Simulator() {
    qDebug() << this << "::~Simulator()";
    // If the temporary directory wasn't used, remove it!
    //! \todo this code removes the temp folder either way... Fix it!!!
    //    if (m_tempDir && m_tempDir->isValid()) {
    //        QDir d(m_tempDir->path());
    //        if (d.isEmpty()) d.removeRecursively();
    //        delete m_tempDir;
    //    }

    clearParcasProcess();
}


ParcasBaseController* Simulator::controller() {
    return m_controller;
}


const QString& Simulator::runtimePath() const {
    return m_runtimePath;
}


std::pair<bool, QString> Simulator::initRuntime(const ExtendedEntryData& entry) {
    qDebug() << "ProcessHandler::initRuntime()";
    bool useTemp = ProgramSettings::getInstance().locationSettings().useTempRuntimeDir;
    QString path = useTemp ? "" : nextRuntimePath(ProgramSettings::getInstance().locationSettings().runtimeDirPath);
    qDebug() << "    " << useTemp << path;
    return initRuntime(entry, useTemp, path);
}


std::pair<bool, QString> Simulator::initRuntime(const ExtendedEntryData& entry, bool useTemp, const QString& path) {
    qDebug() << this << "::initRuntime()";

    if (!m_controller) return std::pair<bool, QString>(false, "No simulation selected!");

    // Setup the runtime path for use with the controller
    m_runtimePath = path;
    if (useTemp) {
        if (m_tempDir) {
            QDir d(m_tempDir->path());
            if (d.isEmpty()) d.removeRecursively();
            delete m_tempDir;
        }
        m_tempDir = new QTemporaryDir;
        m_runtimePath = m_tempDir->path();
    }

    if (!m_controller->createLocalSimulation(m_runtimePath, entry)) {
        qDebug() << "Creation of the runtime environment failed!";
        return std::pair<bool, QString>(false, "Creation of the runtime environment failed!");
    }

    qDebug() << "Runtime directory created at " + m_runtimePath;

    setCurrentEntry(entry);

    return std::pair<bool, QString>(true, "");
}


void Simulator::setCurrentEntry(ExtendedEntryData entry) {
    m_currentEntry = entry;
}


ExtendedEntryData Simulator::currentEntry() {
    return m_currentEntry;
}


/*!
 * \brief Public interface starting the simulation.
 */
void Simulator::simulate() {
    qDebug() << this << "::simulate()";
    if (!m_controller) {
        qDebug() << "    No controller set!";
        emit statusUpdate(Status::Finished);
        return;
    }

    emit statusUpdate(Status::Started);

    m_running = true;
    QTimer::singleShot(0, this, &Simulator::simulationLoop);
}


/*!
 * \brief Public interface stopping the simulation.
 */
void Simulator::stop() {
    qDebug() << this << "::stop()";

    emit statusUpdate(Status::Stopping);

    clearParcasProcess();
    m_running = false;
}


/*!
 * \brief Public interface for checking if the simulator is running.
 * \return true if the simulation loop is active, otherwise false.
 */
bool Simulator::isRunning() {
    return m_running;
}


void Simulator::preSimulationStep() {
    m_stepDone = true;
}

void Simulator::simulationStep() {
    // If the process is already running, we can't start another one!!!
    if (m_parcasProcess) {
        //! \todo implement this feature...
    }

    m_parcasProcess = new QProcess(this);

    connect(m_parcasProcess, &QProcess::errorOccurred, this, &Simulator::handleProcessErrorOccured);
    //    connect(m_parcasProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [this] {
    //        qDebug() << "Exit status received in connect";
    //        m_stepDone = true;
    //        clearParcasProcess();
    //    });
    connect(m_parcasProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
            &Simulator::handleProcessFinished);
    connect(m_parcasProcess, &QProcess::readyReadStandardError, this, &Simulator::handleProcessStandardError);
    connect(m_parcasProcess, &QProcess::readyReadStandardOutput, this, &Simulator::handleProcessStandardOutput);
    connect(m_parcasProcess, &QProcess::started, this, &Simulator::handleProcessStarted);
    connect(m_parcasProcess, &QProcess::stateChanged, this, &Simulator::handleProcessStateChanged);

    QDir::setCurrent(runtimePath());
    m_parcasProcess->setWorkingDirectory(runtimePath());
    m_parcasProcess->start("bash", QStringList() << runtimePath() + "/run-parcas.sh");
    qDebug() << "bash" << runtimePath() + "/run-parcas.sh";
    qDebug() << "    PARCAS started: " << m_parcasProcess->waitForStarted();
}


void Simulator::postSimulationStep() {
    m_stepDone = true;
}


void Simulator::setSimulationStep(SimulationStep state) {
    m_step = state;
    m_stepDone = false;
}


void Simulator::simulationLoop() {
    QTime timer;
    timer.start();
    while (timer.elapsed() < 100) {
        if (!m_running) {
            qDebug() << "    "
                     << "Finished!";
            emit statusUpdate(Status::Finished);
            emit finished();
            return;
        }

        if (m_stepDone) {
            qDebug() << "    "
                     << "current step is done";
            switch (m_step) {
                case PreSimulation:
                    qDebug() << "        "
                             << "moving on from PRE_SIMULATION to SIMULATION";
                    setSimulationStep(Simulation);
                    simulationStep();
                    break;
                case Simulation:
                    qDebug() << "        "
                             << "moving on from SIMULATION to POST_SIMULATION";
                    setSimulationStep(PostSimulation);
                    postSimulationStep();
                    break;
                case PostSimulation:
                    qDebug() << "        "
                             << "moving on from POST_SIMULATION to PRE_SIMULATION";
                    setSimulationStep(PreSimulation);
                    preSimulationStep();
                    break;
            }
        }
    }

    // Relaunch this function with for another 100 ms round of waiting.
    QTimer::singleShot(0, this, &Simulator::simulationLoop);
}

void Simulator::handleProcessErrorOccured(QProcess::ProcessError error) {
    qDebug() << error;
}

void Simulator::handleProcessStandardError() {
    QProcess* p = static_cast<QProcess*>(sender());
    QByteArray buf = p->readAllStandardError();
    qDebug() << buf;
    //    emit standardError(buf);
}

void Simulator::handleProcessStandardOutput() {
    QProcess* p = static_cast<QProcess*>(sender());
    QByteArray buf = p->readAllStandardOutput();
    qDebug() << buf;
    //    // Parse the standard output, for more frames
    //    QString message = QString::fromStdString(buf.toStdString());
    //    if (message.startsWith("Wrote frame")) {
    //        qDebug() << "Copying the newest runtime frame to the project history";
    //        Project::getInstance().history.addFrame(m_framePaths.last() + "/mdlat.xyz");
    //        emit newFrameAvailable();
    //    }

    //    emit standardOutput(buf);
}

void Simulator::handleProcessStarted() {
    qDebug() << this << "::callbackProcessStarted()";
}

void Simulator::handleProcessFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << this << "::callbackProcessFinished()";
    qDebug() << "    runParcas->isOpen(): " << m_parcasProcess->isOpen();
    qDebug() << "    runParcas->pid(): " << m_parcasProcess->pid();
    qDebug() << "    " << exitCode << exitStatus;

    clearParcasProcess();
    m_stepDone = true;
}

void Simulator::handleProcessStateChanged(QProcess::ProcessState newState) {
    qDebug() << this << "::callbackProcessStateChanged()" << newState;
    qDebug() << "    runParcas->isOpen(): " << m_parcasProcess->isOpen();
    qDebug() << "    runParcas->pid(): " << m_parcasProcess->pid();
    if (newState == QProcess::ProcessState::NotRunning) {
        if (m_parcasProcess->exitCode() == 200) {
            qDebug() << "The simulation produced an error!";
            QFile errorFile(m_runtimePath + "/errors.out");
            QString message = "";
            if (errorFile.open(QIODevice::ReadOnly)) {
                message = errorFile.readAll();
            }
            emit statusUpdate(Status::Error, message);
        }
        clearParcasProcess();
        m_stepDone = true;
    }
}


/*!
 * \brief Helper function that appends a number at the end of the next runtime directory to avoid overwriting old
 * simulations.
 *
 * \param dir The runtime root
 * \return A string with the new path
 */
QString Simulator::nextRuntimePath(const QString& dir) {
    if (dir.isEmpty()) return "";
    QSet<QString> names = QDir(dir).entryList(QDir::AllEntries | QDir::NoDotAndDotDot).toSet();

    int i = 1;
    QString name = "runtime_0";
    while (names.contains(name)) {
        name = QString("runtime_%2").arg(i);
        i++;
    }
    return QString("%1/%2").arg(dir, name);
}


/*!
 * \brief Helper function to kill and cleanup the external system process.
 */
void Simulator::clearParcasProcess() {
    qDebug() << this << "::clearParcasProcess()";

    if (m_parcasProcess && m_parcasProcess->isOpen()) {
        m_parcasProcess->terminate();
        m_parcasProcess->waitForFinished();
    }

    if (m_parcasProcess) delete m_parcasProcess;
}
