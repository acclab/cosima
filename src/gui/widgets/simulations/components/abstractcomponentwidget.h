/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTCOMPONENTWIDGET_H
#define ABSTRACTCOMPONENTWIDGET_H

#include "api/entry.h"
#include "api/state/parameters/baseparameters.h"

#include "gui/state/enums.h"

#include <QWidget>


class QIcon;


class AbstractComponentWidget : public QWidget {
    Q_OBJECT

  public:
    explicit AbstractComponentWidget(QWidget* parent = nullptr);

    virtual void setEntry(ExtendedEntryData* entry);
    ExtendedEntryData* entry();

    virtual void setParameters(api::BaseParameters parameters);
    virtual api::BaseParameters parameters() = 0;

    void setReturnParameter(api::BaseParameters& returnParams, const api::key_t& key, const QVariant& newValue);

  protected:
    ExtendedEntryData* m_entry = nullptr;

    QIcon loadIcon(gui::enums::state::Status status);

  private:
    api::BaseParameters m_original;

  signals:
    void modified(bool errors = false);
    void needsAttention(bool errors = false);
};

#endif  // ABSTRACTCOMPONENTWIDGET_H
