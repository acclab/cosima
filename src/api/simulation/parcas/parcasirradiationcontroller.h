/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARCASIRRADIATIONCONTROLLER_H
#define PARCASIRRADIATIONCONTROLLER_H

#include "api/data/iontransformdata.h"
#include "api/iongenerator.h"
#include "api/simulation/parcas/parcasbasecontroller.h"
#include "api/simulation/parcas/parcasrelaxationcontroller.h"


class ParcasIrradiationController : public ParcasBaseController {
  public:
    ParcasIrradiationController();

    IonTransformData* currentIonTransform();

    bool writeLocalScript(const QString& path, const ExtendedEntryData& entry) override;
    bool writeStandaloneScript(const QString& path, const ExtendedEntryData& entry, const ExportData& data) override;

    bool writeInputFile(const QString& path, const ExtendedEntryData& entry) override;
    bool writeRelaxInputFile(const QString& path, const ExtendedEntryData& entry);
    bool writeCascadeInputFile(const QString& path, const ExtendedEntryData& entry);


    void insertCascadeParameters(api::BaseParameters parameters);
    api::BaseParameters cascadeParameters();

    void insertRelaxParameters(api::BaseParameters parameters);
    api::BaseParameters relaxParameters();

    void setupDefaultState() override;

    QString name() override;

    QDataStream& save(QDataStream& out) const override;
    QDataStream& load(QDataStream& in) override;

  private:
    void generateIons(const ExtendedEntryData& entry);

    api::BaseParameters m_cascadeParameters;
    api::BaseParameters m_relaxParameters;

    IonGenerator m_ionGenerator;
    int m_currentIon = 0;
};

#endif  // PARCASIRRADIATIONCONTROLLER_H
