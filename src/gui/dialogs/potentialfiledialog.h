/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef POTENTIALFILEDIALOG_H
#define POTENTIALFILEDIALOG_H


#include "api/data/datafile.h"
#include "api/data/generators/reppotgenerator.h"
#include "api/entry.h"
#include "api/potential.h"

#include <QDialog>
#include <QFileDialog>
#include <QListWidget>
#include <QStandardItem>

#include <memory>


namespace Ui {
class PotentialFileDialog;
}

/*!
 * \brief A dialog for modifying the paths to potential files.
 *
 * Used for replacing potential file paths on a pair by pair basis. Alternatively, several potential file paths can be
 * imported simultaneously as long as the names of the imported files follow the PARCAS naming convention.
 *
 * Connects to the state class PotentialFileState.
 */
class PotentialFileDialog : public QDialog {
    Q_OBJECT

  public:
    explicit PotentialFileDialog(QHash<ElementPair, DataFile<ReppotFileData>> potentialFiles,
                                 QWidget* parent = nullptr);
    ~PotentialFileDialog();

    const QHash<ElementPair, DataFile<ReppotFileData>>& potentialFiles();

  private:
    Ui::PotentialFileDialog* ui;
    QHash<ElementPair, DataFile<ReppotFileData>> m_potentialFiles;

    QVector<ElementPair> m_shownPairPairs;
    QVector<ElementPair> m_shownEamPairs;

    ElementPair* m_selectedPair = nullptr;

    ReppotGenerator m_reppotGenerator;

  private slots:
    void updateTables();
    void replaceSelected();
    void createSelected();

    void plotFile(const DataFile<ReppotFileData>& file);

    bool confirmReplace();
};

#endif  // POTENTIALFILEDIALOG_H
