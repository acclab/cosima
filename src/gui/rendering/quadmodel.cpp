/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "quadmodel.h"

#include "api/util.h"


QuadModel::QuadModel(const QColor& faceColor, const QColor& lineColor, QObject* parent)
        : AbstractGeometryModel(parent) {
    m_faceColor = faceColor;
    m_lineColor = lineColor;

    setupVertexData();
}


void QuadModel::setupVertexData() {
    std::vector<glm::vec3> positions{
        glm::vec3(-0.5f, -0.5f, 0.0f),  // vertex 0
        glm::vec3(0.5f, -0.5f, 0.0f),   // vertex 1
        glm::vec3(0.5f, 0.5f, 0.0f),    // vertex 2
        glm::vec3(-0.5f, 0.5f, 0.0f)    // vertex 3
    };

    std::vector<unsigned int> faceIndices{
        0, 1, 2,  // bottom right
        2, 3, 0   // top left
    };

    std::vector<unsigned int> lineIndices{
        0, 1,  // bottom
        1, 2,  // right
        2, 3,  // top
        3, 0   // left
    };

    std::vector<glm::vec4> faceColors(positions.size());
    std::fill(std::begin(faceColors), std::end(faceColors), util::colorToVector(m_faceColor));

    std::vector<glm::vec4> lineColors(positions.size());
    std::fill(std::begin(lineColors), std::end(lineColors), util::colorToVector(m_lineColor));


    setFaceVertexData(positions, faceColors, faceIndices);
    setLineVertexData(positions, lineColors, lineIndices);
}


/*!
 * \brief Position the quad in the desired direction with a proper size.
 * \param position
 * \param rotation  In degrees
 * \param scale
 */
void QuadModel::setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec2& size) {
    transform.setPosition(position);
    transform.rotate(glm::radians(rotation));
    transform.setScale(glm::vec3(size, 0.0));
}


void QuadModel::setFaceColor(const QColor& color) {
    m_faceColor = color;
    setupVertexData();
}


void QuadModel::setLineColor(const QColor& color) {
    m_lineColor = color;
    setupVertexData();
}
