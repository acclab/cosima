/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FRAME_H
#define FRAME_H

#include "xyzdata.h"

/*!
 * \brief A class for handling frames.
 *
 * In COSIRMA entries consist of frames. For more information on the project structure of COSIRMA see Project.
 *
 * \todo At the moment this class is only used for reading and writing XyzData objects. However, the frames can also
 * include movies and info files. Frame should be reimpleted so that it can handle more complex cases. Frame should
 * resemble Entry in functionality.
 */
class FrameReaderWriter {
  public:
    FrameReaderWriter(const QString& dirPath = "");
    bool write(const XyzData& xyzData);
    bool read(XyzData& xyzData);

  private:
    QString m_path = "";
    QString m_xyzPath = "/mdlat.xyz";
    QString m_binaryPath = "/mdlat.bin";
};

Q_DECLARE_METATYPE(FrameReaderWriter)

#endif  // FRAME_H
