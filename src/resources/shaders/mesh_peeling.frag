#version 330

in vec4 color;

uniform sampler2D depthSampler;

out vec4 colorOut;

void main(void)
{
    float clipDepth = texelFetch(depthSampler, ivec2(gl_FragCoord.xy), 0).r;
    if (gl_FragCoord.z <= clipDepth) {
        discard;
    }

    colorOut = vec4(color.rgb * color.a, color.a);
}
