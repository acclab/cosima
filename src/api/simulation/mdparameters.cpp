/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "mdparameters.h"

#include <QDataStream>
#include <QFile>
#include <QList>
#include <QMap>

#include "api/simulation/parcas/parameters/atomtypesparameterkeys.h"
#include "api/simulation/parcas/parameters/elstopparameterkeys.h"
#include "api/simulation/parcas/parameters/generalparameterkeys.h"
#include "api/simulation/parcas/parameters/interactionparameterkeys.h"
#include "api/simulation/parcas/parameters/outputparameterkeys.h"
#include "api/simulation/parcas/parameters/pressurecontrolparameterkeys.h"
#include "api/simulation/parcas/parameters/recoilparameterkeys.h"
#include "api/simulation/parcas/parameters/simulationcellparameterkeys.h"
#include "api/simulation/parcas/parameters/speedupparameterkeys.h"
#include "api/simulation/parcas/parameters/temperaturecontrolparameterkeys.h"


MDParameters::MDParameters() {}

void MDParameters::removeParameter(const parameter& key) {
    m_parameters.remove(key);
}

void MDParameters::setParameter(const parameter& key, const ParameterValue& value) {
    m_parameters.insert(key, value);
}

const ParameterValue& MDParameters::value(const parameter& key) {
    return m_parameters[key];
}

const QMap<parameter, ParameterValue>& MDParameters::parameters() const {
    return m_parameters;
}


void MDParameters::updateParameters(const MDParameters& other) {
    updateParameters(other.parameters());
}


void MDParameters::updateParameters(const QMap<parameter, ParameterValue>& parameters) {
    // If the value of the input parameters object is not the same as the original one, update it!
    for (const parameter& key : parameters.keys()) {
        if (parameters[key] != m_parameters[key]) {
            qDebug() << "Updating" << key.name;
            qDebug() << "    " << parameters[key];
            qDebug() << "    " << m_parameters[key];
            m_parameters[key] = parameters[key];
        }
    }

    //    // If the new parameter set has a key that's not included in the current parameter set, include it!
    //    for (const parameter& key : parameters.keys()) {
    //        if (!m_parameters.keys().contains(key)) {
    //            this->setParameter(key, parameters[key]);
    //        }
    //    }
}


bool MDParameters::save(const QString& path) {
    QFile outFile(path);
    if (!outFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Could not write to file:" << path << "Error string:" << outFile.errorString();
        return false;
    }

    QDataStream out(&outFile);
    out.setVersion(QDataStream::Qt_5_7);
    out << m_parameters;

    return true;
}


bool MDParameters::load(const QString& path) {
    QFile inFile(path);
    if (!inFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not read the file:" << path << "Error string:" << inFile.errorString();
        return false;
    }

    QDataStream in(&inFile);
    in.setVersion(QDataStream::Qt_5_7);
    in >> m_parameters;

    return true;
}


bool MDParameters::write(const QString& path) {
    QFile outFile(path);
    if (!outFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Could not write to file:" << path << "Error string:" << outFile.errorString();
        return false;
    }

    QStringList blockNames;
    blockNames << "General";
    blockNames << "Atom Types";
    blockNames << "Interactions";
    blockNames << "Simulation Cell";
    blockNames << "Temperature";
    blockNames << "Pressure";
    blockNames << "Output";
    blockNames << "Recoil";
    blockNames << "Elstop";
    blockNames << "Speedup";

    QMap<QString, QList<parameter>> blocks;
    blocks.insert("General", generalKeys);
    blocks.insert("Atom Types", atomTypesKeys);
    blocks.insert("Interactions", interactionKeys);
    blocks.insert("Simulation Cell", simulationCellKeys);
    blocks.insert("Temperature", temperatureControlKeys);
    blocks.insert("Pressure", pressureControlKeys);
    blocks.insert("Output", outputKeys);
    blocks.insert("Recoil", recoilKeys);
    blocks.insert("Elstop", elstopKeys);
    blocks.insert("Speedup", speedupKeys);

    //! \todo remove this parameter when testing the writer is finished!
    bool toBeRemoved_true = false;

    QTextStream stream(&outFile);
    foreach (QString blockName, blockNames) {
        stream << "# " << blockName << endl;
        foreach (const parameter& p, blocks[blockName]) {
            qDebug() << m_parameters[p];
            switch (m_parameters[p].dim) {
                case ParameterValue::Scalar:
                    if (toBeRemoved_true || m_parameters[p].value.isValid()) {
                        stream << p.name.leftJustified(9, ' ') << "= " << m_parameters[p].value.toString() << endl;
                    }
                    break;
                case ParameterValue::Array1D:
                    for (int i = 0; i < m_parameters[p].value1d.size(); ++i) {
                        if (toBeRemoved_true || m_parameters[p].value1d[i].isValid()) {
                            int x = m_parameters[p].arrayZeroIndex ? i : i + 1;
                            stream << (p.name + "(" + QString::number(x) + ")").leftJustified(9, ' ') << "= "
                                   << m_parameters[p].value1d[i].toString() << endl;
                        }
                    }
                    break;
                case ParameterValue::Array2D:
                    for (int i = 0; i < m_parameters[p].value2d.size(); ++i) {
                        int x = m_parameters[p].value2d[i][0].toInt();
                        int y = m_parameters[p].value2d[i][1].toInt();
                        x = m_parameters[p].arrayZeroIndex ? x : x + 1;
                        y = m_parameters[p].arrayZeroIndex ? y : y + 1;
                        QVariant value = m_parameters[p].value2d[i][2];
                        if (toBeRemoved_true || value.isValid()) {
                            stream << (p.name + "(" + QString::number(x) + "," + QString::number(y) + ")")
                                          .leftJustified(9, ' ')
                                   << "= " << value.toString() << endl;
                        }
                    }
                    break;
            }
        }
        stream << endl;
    }

    return true;
}


ParameterValue MDParameters::operator[](const parameter& key) const {
    return m_parameters[key];
}


QDebug operator<<(QDebug d, const MDParameters& p) {
    QDebugStateSaver saver(d);
    d << "------------------" << endl;
    d << "Parameters:" << endl;
    for (const parameter& key : p.parameters().keys()) {
        d << "  " << key.name << p[key] << endl;
    }
    d << "------------------" << endl;
    return d;
}
