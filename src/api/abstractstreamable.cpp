/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "abstractstreamable.h"


//-------------------------------------------------------------------------------
// Default implementations of virtual functions
//-------------------------------------------------------------------------------

/*!
 * \brief Writes the object into QDebug stream
 * \param d The debug stream
 * \return The debug stream after writing
 */
QDebug AbstractStreamable::debug(QDebug d) const {
    return d;
}

//-------------------------------------------------------------------------------
// Overloaded operators:
//-------------------------------------------------------------------------------

/*!
 * \brief Streaming operator for writing streamable objects into QDataStream.
 *
 * Uses AbstractStreamable#save().
 *
 * \param stream The stream
 * \param settings The streamed object
 * \return The stream after writing
 */
QDataStream& operator<<(QDataStream& stream, const AbstractStreamable& settings) {
    return settings.save(stream);
}

/*!
 * \brief Streaming operator for reading streamable objects from QDataStream.
 *
 * Uses AbstractStreamable#load().
 *
 * \param stream The stream
 * \param settings The streamed object
 * \return The stream after reading
 */
QDataStream& operator>>(QDataStream& stream, AbstractStreamable& settings) {
    return settings.load(stream);
}

/*!
 * \brief Streaming operator for wrinting streamable objects into QDebug.
 *
 * Uses AbstractStreamable#debug().
 *
 * \param d The debug stream
 * \param settings The streamed object
 * \return The debug stream after writing
 */
QDebug operator<<(QDebug d, const AbstractStreamable& settings) {
    return settings.debug(d);
}
