/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "clusteranalyzer.h"

#include <cmath>
#include <limits>


ClusterAnalyzer::ClusterAnalyzer(const ExtendedEntryData& entry, const glm::bvec3& pbc) : m_entry(entry), m_pbc(pbc) {}

void ClusterAnalyzer::analyze(double cutoff) {
    m_clusters.clear();

    // Constructing neighbour list. Candidates for a neighbour are first reduced by dividing the atoms into a 3D grid.
    int resX = std::min(1000.0, std::max(1.0, (m_entry.frame.atomLims.x2 - m_entry.frame.atomLims.x1) / cutoff));
    int resY = std::min(1000.0, std::max(1.0, (m_entry.frame.atomLims.y2 - m_entry.frame.atomLims.y1) / cutoff));
    int resZ = std::min(1000.0, std::max(1.0, (m_entry.frame.atomLims.z2 - m_entry.frame.atomLims.z1) / cutoff));

    int n = m_entry.frame.atoms.size();
    double cutoffSquared = cutoff * cutoff;

    std::vector<std::vector<std::vector<std::vector<int>>>> grid;
    grid.resize(resX);
    for (int i = 0; i < resX; ++i) {
        grid[i].resize(resY);
        for (int j = 0; j < resY; ++j) grid[i][j].resize(resZ);
    }

    for (int i = 0; i < n; ++i) {
        const Atom& atom = m_entry.frame.atoms[i];
        int xInd = std::max(0, std::min(int((atom.position.x - m_entry.frame.atomLims.x1) /
                                            (m_entry.frame.atomLims.x2 - m_entry.frame.atomLims.x1) * resX),
                                        resX - 1));
        int yInd = std::max(0, std::min(int((atom.position.y - m_entry.frame.atomLims.y1) /
                                            (m_entry.frame.atomLims.y2 - m_entry.frame.atomLims.y1) * resY),
                                        resY - 1));
        int zInd = std::max(0, std::min(int((atom.position.z - m_entry.frame.atomLims.z1) /
                                            (m_entry.frame.atomLims.z2 - m_entry.frame.atomLims.z1) * resZ),
                                        resZ - 1));
        grid[xInd][yInd][zInd].push_back(i);
    }

    std::vector<std::vector<int>> nList;
    nList.resize(n);

    for (int i = 0; i < resX; ++i) {
        for (int j = 0; j < resY; ++j) {
            for (int k = 0; k < resZ; ++k) {

                // Finding the neighbouring grid cells
                std::vector<std::vector<int>*> cells;
                for (int a : {i - 1, i, i + 1}) {
                    if (m_pbc.x && resX > 1) a = mod(a, resX);
                    if (a < 0 || a >= resX) continue;
                    for (int b : {j - 1, j, j + 1}) {
                        if (m_pbc.y && resY > 1) b = mod(b, resY);
                        if (b < 0 || b >= resY) continue;
                        for (int c : {k - 1, k, k + 1}) {
                            if (m_pbc.z && resZ > 1) c = mod(c, resZ);
                            if (c < 0 || c >= resZ) continue;
                            cells.push_back(&grid[a][b][c]);
                        }
                    }
                }

                // Finding the neighbours based on the distance squared
                for (int i1 : grid[i][j][k]) {
                    for (std::vector<int>* cell : cells) {
                        for (auto it = cell->begin(); it != cell->end(); ++it) {
                            int i2 = *it;
                            if (i1 >= i2) continue;
                            double dx =
                                std::abs(m_entry.frame.atoms[i1].position.x - m_entry.frame.atoms[i2].position.x);
                            double dy =
                                std::abs(m_entry.frame.atoms[i1].position.y - m_entry.frame.atoms[i2].position.y);
                            double dz =
                                std::abs(m_entry.frame.atoms[i1].position.z - m_entry.frame.atoms[i2].position.z);

                            if (m_pbc.x) dx = std::min(dx, m_entry.frame.cellLims.x2 - m_entry.frame.cellLims.x1 - dx);
                            if (m_pbc.y) dy = std::min(dy, m_entry.frame.cellLims.y2 - m_entry.frame.cellLims.y1 - dy);
                            if (m_pbc.z) dz = std::min(dz, m_entry.frame.cellLims.z2 - m_entry.frame.cellLims.z1 - dz);

                            double d2 = dx * dx + dy * dy + dz * dz;

                            if (d2 <= cutoffSquared) {
                                nList[i1].push_back(i2);
                                nList[i2].push_back(i1);
                            }
                        }
                    }
                }
            }
        }
    }


    // Doing searches in the neighbour list network until all atoms have been handled. Each search gives a cluster.
    std::vector<bool> added(n, false);
    for (int i = 0; i < n; ++i) {
        if (added[i]) continue;
        Cluster c;
        std::vector<int> toBeHandled;
        toBeHandled.push_back(i);
        added[i] = true;
        while (toBeHandled.size() != 0) {
            int handling = toBeHandled.back();
            toBeHandled.pop_back();
            c.ids.push_back(handling);
            for (int atom : nList[handling]) {
                if (!added[atom]) {
                    toBeHandled.push_back(atom);
                    added[atom] = true;
                }
            }
        }
        m_clusters.push_back(c);
    }
}

/*!
 * Convinience function for returning the lasrgest cluster as a new XyzData struct
 */
XyzData ClusterAnalyzer::largestCluster() {
    XyzData data;
    data = m_entry.frame;
    data.atoms.clear();

    if (m_clusters.size() == 0) return data;

    double xmin = std::numeric_limits<double>::max();
    double xmax = std::numeric_limits<double>::lowest();
    double ymin = std::numeric_limits<double>::max();
    double ymax = std::numeric_limits<double>::lowest();
    double zmin = std::numeric_limits<double>::max();
    double zmax = std::numeric_limits<double>::lowest();

    int largestClusterIndex = 0;
    for (unsigned long i = 1; i < m_clusters.size(); ++i) {
        if (m_clusters[i].ids.size() > m_clusters[largestClusterIndex].ids.size()) largestClusterIndex = i;
    }

    for (int i : m_clusters[largestClusterIndex].ids) {
        const Atom a = m_entry.frame.atoms[i];
        data.atoms.push_back(a);
        xmin = std::min(xmin, a.position.x);
        xmax = std::max(xmax, a.position.x);
        ymin = std::min(ymin, a.position.y);
        ymax = std::max(ymax, a.position.y);
        zmin = std::min(zmin, a.position.z);
        zmax = std::max(zmax, a.position.z);
    }
    data.atomLims = Box(xmin, xmax, ymin, ymax, zmin, zmax);

    return data;
}

int ClusterAnalyzer::mod(int i, int n) {
    return (i % n + n) % n;
}
