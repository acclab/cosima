/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef API_H
#define API_H


#include "api/data/exportdata.h"
#include "api/entryeditor.h"
#include "api/simulation/mdparameters.h"
#include "api/simulation/simulator.h"
#include "api/simulationlist.h"

#include <QObject>
#include <QPointer>


/*!
 * \brief Core of COSIRMA
 *
 * Binds together interactive components that are used for modifying and running the simulations, as well as editing the
 * latest entry in a project's history.
 *
 * \todo Implement error signaling so that when something goes wrong the UI can infrom the user.
 */
class Api : public QObject {
    Q_OBJECT

  public:
    enum class SimulationStatus { Aborted, Started, Stopping, Finished, Error };
    Q_ENUM(SimulationStatus);

    explicit Api(QObject* parent = nullptr);

    bool isProjectOpen() const;

    SimulationList simulationList;  //!< \brief List of COSIRMA simulations
    EntryEditor entryEditor;        //!< \brief Entry editor

  private:
    QPointer<QThread> simulatorThread = nullptr;
    QPointer<Simulator> simulator = nullptr;

    Entry* m_simulationEntry = nullptr;


  public slots:
    void loadProject(const QString& path);
    void closeProject(bool save);
    void saveProject();

    bool exportStandaloneSimulation(const QString& path, const ExportData& data, const ExtendedEntryData& entryData);

    bool startSimulation(const ExtendedEntryData& entryData);
    void stopSimulation();

  private slots:
    void handleSimulatorStatusChanged(Simulator::Status status, const QString& message = nullptr);

  signals:
    void projectOpening();
    void projectOpened();  //!< \brief Emitted when a project is opened.
    void projectClosing();
    void projectClosed();  //!< \brief Emitted when a project is closed.

    void stopSimulationSignal();

    void simulationStatusChanged(SimulationStatus status);
    void simulationStatusChanged(SimulationStatus status, const QString& message);
    void errorMessage(const QString& message);
};

Q_DECLARE_METATYPE(Api::SimulationStatus);

#endif  // API_H
