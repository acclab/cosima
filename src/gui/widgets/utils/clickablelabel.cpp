/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "clickablelabel.h"


void ClickableLabel::setColor(const QColor& color) {
    m_color = color;
    setStyleSheet(QString("background-color: rgb(%1,%2,%3);border: 1px solid black;")
                      .arg(color.red())
                      .arg(color.green())
                      .arg(color.blue()));
}


const QColor& ClickableLabel::color() {
    return m_color;
}


/*!
 * Overrided mousePressEvent.
 * If the mouse is pressed on the label, clicked() signal is emitted.
 */
void ClickableLabel::mousePressEvent(QMouseEvent* /*event*/) {
    emit clicked();
}
