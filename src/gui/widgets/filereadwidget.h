/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FILEREADWIDGET_H
#define FILEREADWIDGET_H

#include "api/entry.h"

#include <QCheckBox>
#include <QComboBox>
#include <QFileInfo>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QWidget>

namespace Ui {
class FileReadWidget;
}

class FileReadWidget : public QWidget {
    Q_OBJECT

  public:
    explicit FileReadWidget(QWidget* parent = nullptr);
    ~FileReadWidget();

    QString filePath();
    bool readEntry();
    const ExtendedEntryData& getEntry() { return entry; }
    bool readyforNext();
    void closeFile();

  private:
    enum FileType { XYZ, Lammps, Other };

    struct Component {
        enum ComponentType { None, X, Y, Z };

        Component() { type = None; }
        Component(ComponentType t) { type = t; }

        ComponentType type;
        QString toString() {
            switch (type) {
                case X:
                    return "X";
                case Y:
                    return "Y";
                case Z:
                    return "Z";
                default:
                    return "";
            }
        }
    };

    struct Unit {
        Unit(const QString t = "", const double f = 0.0) {
            text = t;
            factor = f;
        }
        QString text;
        double factor;
    };

    struct Variable {
        enum VariableType { None, Position, Velocity, Type, Symbol, Id };

        Variable() {}
        Variable(VariableType t, QString s,
                 const QVector<Component::ComponentType>& c = QVector<Component::ComponentType>(),
                 const QVector<QString>& u = QVector<QString>()) {
            type = t;
            name = s;
            components = c;
            units = u;
        }
        VariableType type;
        QString name;
        QVector<Component::ComponentType> components;
        QVector<QString> units;
    };


    struct ColumnData {
        ColumnData(Variable::VariableType v = Variable::None, Component::ComponentType c = Component::None,
                   QString u = "", double f = 0, QString basis = "") {
            variable = v;
            component = c;
            unit = u;
            customUnitFactor = f;
            customUnitBasis = basis;
        }

        Variable::VariableType variable;
        Component::ComponentType component;
        QString unit, customUnitBasis;
        double customUnitFactor;
    };

    class File {
      public:
        File() {}
        File(QString path) { setPath(path); }

        void setPath(QString path) {
            path_m = path;
            typeFound = false;
            linesRead = false;
            fileParsed = false;
            columsCounted = false;

            atoms_m = -1;
            atomsLine_m = -1;
            columns_m = 0;
        }

        QString path() { return path_m; }

        bool exists() {
            QFileInfo file(path_m);
            return file.exists() && file.isFile();
        }

        FileType type() {
            if (!typeFound) findType();
            return type_m;
        }

        const QStringList& lines() {
            if (!linesRead) readLines();
            return lines_m;
        }

        int atoms() {
            if (!fileParsed) parseFile();
            return atoms_m;
        }

        int atomsLine() {
            if (!fileParsed) parseFile();
            return atomsLine_m;
        }

        int velocitiesLine() {
            if (!fileParsed) parseFile();
            return velocitiesLine_m;
        }

        int columns() {
            if (!columsCounted) countColumns();
            return columns_m;
        }

        void setAtoms(int n) {
            fileParsed = true;
            columsCounted = false;
            atoms_m = n;
        }

        void setAtomsLine(int line) {
            fileParsed = true;
            columsCounted = false;
            atomsLine_m = line;
        }

        bool isOpen() { return lines_m.count() != 0; }

      private:
        void findType();
        bool isLammps();
        void readLines();
        void parseFile();
        void countColumns();

        QString path_m;

        bool typeFound = false;
        FileType type_m;

        bool fileParsed = false;
        int atoms_m;
        int atomsLine_m;
        int velocitiesLine_m;

        bool linesRead = false;
        QStringList lines_m;

        bool columsCounted = false;
        int columns_m = 0;
    };

    struct Column {
        QComboBox* variable;
        QComboBox* component;
        QComboBox* unit;
        QLineEdit* customFactor;
        QLabel* customUnit;
    };


    QVector<ColumnData> defaultColumnData();
    QVector<ColumnData> currentColumnData();
    QHash<std::pair<Variable::VariableType, Component::ComponentType>, int> indices(const QVector<ColumnData>& data);
    QHash<std::pair<Variable::VariableType, Component::ComponentType>, double> unitFactors(
        const QVector<ColumnData>& data);
    QVector<int> factors(const QVector<ColumnData>& data);
    void setMasses();
    void setCellLimits(double factor);
    void readVelocities();


    Ui::FileReadWidget* ui;

    File selectedFile;

    QHash<Variable::VariableType, Variable> variables;
    QHash<FileType, QList<Variable::VariableType> > availableVariables;

    QHash<QString, Unit> units;

    QVector<Column> columns;
    QList<QWidget*> tableWidgets;

    ExtendedEntryData entry;

  private slots:
    void browseFile();
    void changeFilePath(QString path);
    void openFile();
    void updateText();
    void writeTable();
    void updateColumnVariable(int i);
    void updateColumnUnit(int i);

  signals:
    void readyToRead(bool);
};

#endif  // FILEREADWIDGET_H
