/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "graphselector.h"

#include <QDebug>


GraphSelector::GraphSelector(QWidget* parent) : QGraphicsView(parent) {
    setMaximumHeight(200);
    setMinimumHeight(200);
    setMaximumWidth(200);
    setMinimumWidth(200);

    scene = new QGraphicsScene;
    setScene(scene);

    selectedRect = new QGraphicsRectItem();
}

GraphSelector::~GraphSelector() {
    delete selectedRect;
    delete scene;
}


/*!
 * Overrided mousePressEvent.
 * Tracks the mouse clicks on the widget. Emits selectionChanged() signal.
 */
void GraphSelector::mousePressEvent(QMouseEvent* event) {

    float x = static_cast<float>(event->x() - 10);
    float y = static_cast<float>(event->y() - 10);

    if (x > 0 && y > 0 && x < 180 && y < 180) {
        int xi = static_cast<int>(x / 180 * res);
        int yi = static_cast<int>(y / 180 * res);
        selectedRect->setRect(10 + xi * 180.0 / res, 10 + yi * 180.0 / res, 180.0 / res, 180.0 / res);

        emit selectionChanged(xi, yi);
        i = xi;
        j = yi;
    }
}


/*!
 * Draws the square and the grid.
 */
void GraphSelector::init() {
    QBrush grayBrush(Qt::gray);
    QBrush darkGrayBrush(Qt::darkGray);
    QPen outlinePen(Qt::black);


    scene->addRect(10, 10, 180, 180, outlinePen, grayBrush);

    for (int i = 1; i < res; ++i) {
        scene->addLine(10, 10 + 180.0 / res * i, 190, 10 + 180.0 / res * i, outlinePen);
        scene->addLine(10 + 180.0 / res * i, 10, 10 + 180.0 / res * i, 190, outlinePen);
    }

    selectedRect =
        scene->addRect(10 + static_cast<int>(res / 2.0) * 180 / res, 10 + static_cast<int>(res / 2.0) * 180 / res,
                       180 / res, 180 / res, outlinePen, darkGrayBrush);
    i = res / 2;
    j = res / 2;
}

/*!
 * Set the amount of bins in the grid in x- and y-directions
 */
void GraphSelector::setRes(int resolution) {
    res = resolution;
}

/*!
 * Returns the selected bin's index in x-direction
 */
int GraphSelector::getI() {
    return i;
}

/*!
 * Returns the selected bin's index in y-direction
 */
int GraphSelector::getJ() {
    return j;
}
