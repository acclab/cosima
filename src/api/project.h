/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PROJECT_H
#define PROJECT_H

#include "history.h"

/*
 * !!!Currently outside the doxygen comment!!!
 *
 * Here is an explation on the project directory structure of COSIRMA. The project tree looks like this:
 *
 * project_direcotry/
 *  - .cosirmaproject
 *  - history/
 *      - entry_1/
 *      - entry_2/
 *          - info.txt
 *          - types.bin
 *          - elstop/
 *          - potentials/
 *          - frames/
 *              - frame_0/
 *              - frame_1/
 *                  - mdlat.bin
 *                  - mdlat.xyz
 */

/*!
 * \brief A singleton class for managing a project.
 *
 * Can be connected to a COSIRMA project directory. The singleton structure was used to provide easy access to the
 * project history from anywhere in the application. If at some point more projects than one need to be loaded at the
 * same time, this class should be changed to be non-singleton.
 *
 *
 * The project directory should contain the project identifier file ".cosirmaproject". COSIRMA uses it to load project
 * directories through a file dialog. The ".cosirmaproject" file doubles as a settings file, by containing the current
 * project level settings of the project. This file is updated always when the application is closed, so when opened
 * again, the state is restored.
 *
 * The project directory also includes the history directory, that the History object of this class is connected to. The
 * project history consists of entries. An entry corresponds to a state in the history of the project, and it contains
 * the inforamation needed to recreate that state. It includes "info.txt" file that is a human-readable file containing
 * information on the entry. It also has "types.bin" file. It is a binary file containing the atom type data (type
 * numbers, element symbols, and masses) of the entry. The elstop and potential files that were used at the time of the
 * entry are stored to directories "elstop/" and "potetials/".
 *
 * Frames contain the atom data of entries. An entry can have one or more frames depending on its type. A
 * modification entry always has only one frame. An entry like this is created when the entry is modified. When, for
 * example, the atom type data or the atom positions of the entry are modified in COSIRMA, a new modification entry is
 * created. Existing entries should never be modified. The second type of entry are simulation entries. These etries are
 * are created when simulations are run. In them, the first frame "frame_0" is the starting point of the simulation, and
 * every following frame corresponds to one PARCAS simulation. One COSIRMA simualtion can have multiple PARCAS
 * simulations. For example, irradiation simulation is a series of cascade and relaxation simulations.
 *
 * The atom data of the frame is written to the human-readable "mdlat.xyz" file. It can also be written into the binary
 * file "mdlat.bin" based on the settings that the user has selected. In a modification entry, these files contain the
 * only atom data that the entry has. In the case of a simulation entry, the files have the atom data at the end of each
 * PARCAS simulation.
 *
 * The idea behind COSIRMA's project handling is that there are classes, such as History, Entry, Frame, and
 * PotentialFile, that are idividually connected to some file or directory in the project. These classes is used to
 * access the data in the project directory.
 */
class Project {
  public:
    static Project& getInstance();

    void setPath(const QString& path);
    bool loadHistory();
    bool unloadHistory();
    const QString& path() const;
    bool isOpen() const;

    History history; /*!< \brief Manages the project history and current state */

  private:
    Project() {}
    QString m_projectDirectoryPath;
};

#endif  // PROJECT_H
