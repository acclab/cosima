/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BEAMWIDGET_H
#define BEAMWIDGET_H

#include "abstractcomponentwidget.h"

#include <QWidget>


namespace Ui {
class BeamWidget;
}

class BeamWidget : public AbstractComponentWidget {
    Q_OBJECT

  public:
    explicit BeamWidget(QWidget* parent = nullptr);
    ~BeamWidget();

    void setParameters(api::BaseParameters parameters) override;
    api::BaseParameters parameters() override;

  private:
    Ui::BeamWidget* ui;
    Box m_cellLims;

  signals:
    void beamUpdated();
};

#endif  // BEAMWIDGET_H
