/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "cylinderselectorwidget.h"
#include "ui_cylinderselectorwidget.h"

#include "utils/wheelguard.h"

#include <QColorDialog>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>


CylinderSelectorWidget::CylinderSelectorWidget(PlotCellGLWidget* context, QWidget* parent)
        : AbstractAtomSelectorWidget(context, parent), ui(new Ui::CylinderSelectorWidget) {
    ui->setupUi(this);

    connect(ui->xSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->ySB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->zSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });

    connect(ui->phiSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double v) {
        ui->phiJumpslider->blockSignals(true);
        ui->phiJumpslider->setValue(static_cast<int>(v));
        ui->phiJumpslider->blockSignals(false);
        updateDimensions();
    });
    connect(ui->thetaSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this](double v) {
        ui->thetaJumpslider->blockSignals(true);
        ui->thetaJumpslider->setValue(static_cast<int>(v));
        ui->thetaJumpslider->blockSignals(false);
        updateDimensions();
    });

    connect(ui->lengthSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });
    connect(ui->radiusSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [this] { updateDimensions(); });

    connect(ui->phiJumpslider, &JumpSlider::valueChanged, [this] { ui->phiSB->setValue(ui->phiJumpslider->value()); });
    connect(ui->thetaJumpslider, &JumpSlider::valueChanged,
            [this] { ui->thetaSB->setValue(ui->thetaJumpslider->value()); });

    connect(ui->colorLabel, &ClickableLabel::clicked, [this] {
        QColorDialog dialog(this);
        dialog.setOption(QColorDialog::DontUseNativeDialog, true);
        dialog.setOption(QColorDialog::ShowAlphaChannel, true);
        dialog.setWindowTitle("Color Picker");
        dialog.setCurrentColor(m_color);

        if (dialog.exec() == QDialog::Accepted && dialog.currentColor() != m_color) {
            setColor(dialog.currentColor());
        }
    });

    QVector<QWidget*> wheelGuardedWidgets = {ui->thetaSB, ui->phiSB, ui->lengthSB, ui->radiusSB,
                                             ui->xSB,     ui->ySB,   ui->zSB};
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(new WheelGuard(w));
    }

    setFocusProxy(ui->xSB);
}


CylinderSelectorWidget::~CylinderSelectorWidget() {
    if (m_context) {
        m_context->removeModel(m_cylinder);
        m_context->removeModel(m_wireframeCylinder);
    }

    delete m_cylinder;
    delete m_wireframeCylinder;

    delete ui;
}


const AbstractAtomSelection& CylinderSelectorWidget::selector() {
    return m_selector;
}


void CylinderSelectorWidget::init() {
    if (!m_entry) return;
    blockSignalsFromComponents(true);

    m_color = QColor(0, 0, 255, 100);

    const Box& lims = m_entry->frame.cellLims;

    ui->xSB->setValue((lims.x1 + lims.x2) / 2);
    ui->ySB->setValue((lims.y1 + lims.y2) / 2);
    ui->zSB->setValue((lims.z1 + lims.z2) / 2);

    ui->radiusSB->setValue(sqrt(pow(lims.x2 - lims.x1, 2) + pow(lims.y2 - lims.y1, 2)) / 2);
    ui->lengthSB->setValue(lims.z2 - lims.z1);

    ui->phiSB->setValue(0);
    ui->phiJumpslider->setValue(0);
    ui->thetaSB->setValue(0);
    ui->thetaJumpslider->setValue(0);


    Cylinder initialCylinder = Cylinder(glm::dvec3(0, 0, 0), glm::dvec3(0, 0, 1), 1, 1);
    m_cylinder = new CylinderModel(initialCylinder, m_color, m_color.rgb(), 60, false);
    m_wireframeCylinder = new CylinderModel(initialCylinder, m_color, m_color.rgb(), 60, true);

    m_cylinder->setShowFaces(true);
    m_cylinder->setShowLines(true);
    m_wireframeCylinder->setShowFaces(false);
    m_wireframeCylinder->setShowLines(false);

    connect(ui->surfaceCB, &QCheckBox::clicked, [this](bool checked) {
        m_cylinder->setShowFaces(checked);
        m_cylinder->setShowLines(checked);
        m_context->update();
    });
    connect(ui->wireframeCB, &QCheckBox::clicked, [this](bool checked) {
        m_wireframeCylinder->setShowLines(checked);
        m_context->update();
    });

    setColor(m_color);

    ui->surfaceCB->setCheckState(Qt::Checked);
    ui->wireframeCB->setCheckState(Qt::Unchecked);

    blockSignalsFromComponents(false);

    updateDimensions();
}


void CylinderSelectorWidget::connectToPlot() {
    m_context->addModel(m_cylinder);
    m_context->addModel(m_wireframeCylinder);
}


void CylinderSelectorWidget::hidePlottables() {
    QSignalBlocker b1(ui->wireframeCB);
    QSignalBlocker b2(ui->surfaceCB);

    ui->wireframeCB->setChecked(false);
    ui->surfaceCB->setChecked(false);
}


void CylinderSelectorWidget::blockSignalsFromComponents(bool b) {
    ui->xSB->blockSignals(b);
    ui->ySB->blockSignals(b);
    ui->zSB->blockSignals(b);
    ui->radiusSB->blockSignals(b);
    ui->lengthSB->blockSignals(b);
    ui->phiSB->blockSignals(b);
    ui->thetaSB->blockSignals(b);
    ui->phiJumpslider->blockSignals(b);
    ui->thetaJumpslider->blockSignals(b);
    ui->wireframeCB->blockSignals(b);
    ui->surfaceCB->blockSignals(b);
}


void CylinderSelectorWidget::setColor(const QColor& color) {
    m_color = color;
    ui->colorLabel->setStyleSheet(QString("background-color: rgba(%1,%2,%3,%4);border: 1px solid black;")
                                      .arg(color.red())
                                      .arg(color.green())
                                      .arg(color.blue())
                                      .arg(color.alpha()));
    m_cylinder->setFaceColor(color);
    m_cylinder->setLineColor(color.rgb());
    m_wireframeCylinder->setLineColor(color.rgb());
}


void CylinderSelectorWidget::updateDimensions() {
    glm::dvec3 center(ui->xSB->value(), ui->ySB->value(), ui->zSB->value());
    double theta = glm::radians(ui->thetaSB->value());
    double phi = glm::radians(ui->phiSB->value());

    glm::dvec3 zDirection(0, 0, 1);
    glm::dvec3 newDirection(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
    glm::dvec3 rotationAxis = (newDirection == zDirection) ? zDirection : glm::cross(zDirection, newDirection);
    double rotationAngle = acos(glm::dot(zDirection, newDirection));

    Cylinder c(center, newDirection, ui->radiusSB->value(), ui->lengthSB->value());

    m_selector = CylinderAtomSelection(c);
    glm::mat4 m(1.0f);
    m = glm::translate(m, glm::vec3(c.center));
    m = glm::rotate(m, static_cast<float>(rotationAngle), glm::vec3(rotationAxis));
    m = glm::scale(m, glm::vec3(c.radius, c.radius, c.length));

    m_cylinder->setCylinder(c);
    m_wireframeCylinder->setCylinder(c);

    m_context->update();
}
