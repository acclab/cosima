/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATOMSELECTIONS_H
#define ATOMSELECTIONS_H

#include "api/atom.h"
#include "api/geometries.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include <glm/vec3.hpp>

#include <vector>

/*!
 * \brief An abstract class for classes that are used to define atom selections
 */
class AbstractAtomSelection {
  public:
    virtual ~AbstractAtomSelection();
    /*!
     * \brief Determines if the \a atom is in the selection.
     * \param atom An atom
     * \return true if \p atom is in selection
     */
    virtual bool atomInSelection(const Atom& atom) const = 0;
};


/*!
 * \brief An atom selection that includes all atoms
 */
class AllAtomSelection : public AbstractAtomSelection {
  public:
    bool atomInSelection(const Atom&) const;
};


/*!
 * \brief An atom selection that excludes all atoms
 */
class NullAtomSelection : public AbstractAtomSelection {
  public:
    bool atomInSelection(const Atom&) const;
};


/*!
 * \brief An atom selection that inverts another atom selection
 */
class InverseAtomSelection : public AbstractAtomSelection {
  public:
    InverseAtomSelection(const AbstractAtomSelection* selection = nullptr);

    bool atomInSelection(const Atom& atom) const;
    void setAtomSelection(const AbstractAtomSelection* selection);

  private:
    const AbstractAtomSelection* m_selection;
};


/*!
 * \brief An array of atom selections
 *
 * This class can be used to combine multiple atom selections. Atom selections can be added internally or externally. In
 * the internal case, ArrayAtomSelection gets the ownership of the added selection.
 */
class ArrayAtomSelection : public AbstractAtomSelection {
  public:
    /*!
     * \brief Combination mode of the selection. Can be \c Union or \c Intersection. The modes follow the rules of basic
     * set theory.
     * \sa atomInSelection()
     */
    enum Mode { Union, Intersection };

    ArrayAtomSelection(Mode mode = Union);
    ~ArrayAtomSelection();

    bool atomInSelection(const Atom& atom) const;

    void addExternalSelection(const AbstractAtomSelection* selection);
    void addInternalSelection(AbstractAtomSelection* selection);

    void setMode(Mode mode);

    void clear();

  private:
    std::vector<AbstractAtomSelection*> m_ownedSelections;
    std::vector<const AbstractAtomSelection*> m_selections;
    Mode m_mode;
};


/*!
 * \brief An atom selection based on a box geometry
 *
 * The selection includes atoms that are inside a volume spanned by a box.
 *
 * \sa Box
 */
class BoxAtomSelection : public AbstractAtomSelection {
  public:
    BoxAtomSelection(const Box& box = Box());

    bool atomInSelection(const Atom& atom) const;

  private:
    Box m_box;
};


/*!
 * \brief An atom selection based on a sphere geometry
 *
 * The selection includes atoms that are inside a volume spanned by a sphere.
 *
 * \sa Sphere
 */
class SphereAtomSelection : public AbstractAtomSelection {
  public:
    SphereAtomSelection(const Sphere& sphere = Sphere());

    bool atomInSelection(const Atom& atom) const;

  private:
    Sphere m_sphere;
    double m_r2 = 0;
};


/*!
 * \brief An atom selection based on a cylinde geometry
 *
 * The selection includes atoms that are inside a volume spanned by a cylinder.
 *
 * \sa Cylinder
 */
class CylinderAtomSelection : public AbstractAtomSelection {
  public:
    CylinderAtomSelection(const Cylinder& cylinder = Cylinder());

    bool atomInSelection(const Atom& atom) const;

  private:
    Cylinder m_cylinder;
    double m_r2 = 0;
    glm::dvec3 m_normedDirection;
};


/*!
 * \brief An atom selection based on atom type numbers
 *
 * The selection includes atoms that have a specific atom type number.
 */
class TypeSelection : public AbstractAtomSelection {
  public:
    TypeSelection(int typeNumber = 0);

    bool atomInSelection(const Atom& atom) const;

  private:
    int m_typeNumber;
};

#endif  // ATOMSELECTIONS_H
