/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "timewidget.h"
#include "ui_timewidget.h"

#include "api/state/parameters/timeparameters.h"
#include "api/util.h"

#include "gui/widgets/utils/wheelguard.h"


TimeWidget::TimeWidget(QWidget* parent) : AbstractComponentWidget(parent), ui(new Ui::TimeWidget) {
    ui->setupUi(this);

    QList<QWidget*> wheelGuardedWidgets = {ui->doubleSpinBox_time, ui->doubleSpinBox_dt};

    WheelGuard* guard = new WheelGuard(this);
    for (QWidget* w : wheelGuardedWidgets) {
        w->setFocusPolicy(Qt::StrongFocus);
        w->installEventFilter(guard);
    }

    setFocusProxy(ui->doubleSpinBox_time);

    connect(ui->doubleSpinBox_time, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
            [=] { emit modified(); });
    connect(ui->doubleSpinBox_dt, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [=] { emit modified(); });
}


TimeWidget::~TimeWidget() {
    delete ui;
}


void TimeWidget::setParameters(api::BaseParameters parameters) {
    QSignalBlocker blocker1(ui->doubleSpinBox_time);
    QSignalBlocker blocker2(ui->doubleSpinBox_dt);

    AbstractComponentWidget::setParameters(parameters);
    ui->doubleSpinBox_time->setValue(parameters[api::parameters::time::TIME].value().toDouble());
    ui->doubleSpinBox_dt->setValue(parameters[api::parameters::time::DELTA].value().toDouble());
}


api::BaseParameters TimeWidget::parameters() {
    api::BaseParameters params;
    setReturnParameter(params, api::parameters::time::TIME, ui->doubleSpinBox_time->value());
    setReturnParameter(params, api::parameters::time::DELTA, ui->doubleSpinBox_dt->value());
    return params;
}
