﻿/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IMPORTWIDGET_H
#define IMPORTWIDGET_H

#include "api/atom.h"
#include "api/entry.h"
#include "gui/widgets/abstractprojectcreatorwidget.h"

namespace Ui {
class ImportProjectCreatorWidget;
}

/*! \brief Widget for importing an atomic structure.
 *
 * Any column based file can be read and further automation has been added
 * for .xyz and Lammps data files. After reading, the particle types and
 * symbols can be managed and simulation cell size changed.
 */
class ImportProjectCreatorWidget : public AbstractProjectCreatorWidget {
    Q_OBJECT

  public:
    explicit ImportProjectCreatorWidget(QWidget* parent = nullptr);
    ~ImportProjectCreatorWidget() override;

    bool next() override;
    bool previous() override;
    void enter() override;
    const ExtendedEntryData& getEntry() override;

    bool nextEnabled() override;

    QString projectName() override;

  private:
    void updateEntry();

    Ui::ImportProjectCreatorWidget* ui;
    ExtendedEntryData m_entry;
};


#endif  // IMPORTWIDGET_H
