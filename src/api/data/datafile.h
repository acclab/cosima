/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef DATAFILE_H
#define DATAFILE_H

#include "api/data/datafilecontent.h"

#include <QFileInfo>
#include <QString>


//! \todo rename this to something more fitting...
template <class T> class DataFile {
  public:
    QString path() const { return m_path; }
    QString filename() const { return m_filename; }
    QString filepath() const { return QString("%1/%2").arg(path()).arg(filename()); }
    bool hasData() const { return m_hasData; }

    void setData(const DataFileContent<T>& data) {
        m_data = data;
        m_hasData = m_data.values().size() > 0;
    }
    const DataFileContent<T>& data() const { return m_data; }

    bool write(const QString& path) {
        if (hasData()) {
            return m_data.write(path);
        }
        return false;
    }
    bool read(const QString& path) {
        QFileInfo info(path);

        m_path = info.absolutePath();
        m_filename = info.fileName();

        if (info.exists()) {
            m_hasData = m_data.read(path);
        }

        return true;
    }

  private:
    QString m_path = "";
    QString m_filename = "";
    bool m_hasData = false;

    DataFileContent<T> m_data;
};

#endif  // DATAFILE_H
