/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "entryinfo.h"

#include "fileio.h"

/*!
 * \brief Writes the object to a file.
 *
 * The writing is done in human-readable format. If the file exists, it is overwritten, if it doesn't, a new file is
 * created.
 *
 * \param path Path to the file
 * \return \c true if the writing was successful
 */
bool EntryInfo::write(const QString& path) const {

    if (path.isEmpty()) return false;

    QHash<QString, QString> data;

    data["description"] = description;

    switch (type) {
        case Type::Modification:
            data["type"] = "modification";
            break;
        case Type::Simulation:
            data["type"] = "simulation";
            break;
    }

    data["timestamp"] = timestamp.toString("yyyy-MM-dd_HH:mm:ss.zzz");

    return FileIO(path).writeFile(data);
}


/*!
 * \brief Reads the attribute values from a file
 * \param path Path to the file
 * \return \c true if the reading was successful
 */
bool EntryInfo::read(const QString& path) {
    FileIO reader(path);
    QHash<QString, QString> data = reader.readFile();
    if (!data.isEmpty()) {
        description = data["description"];
        name = data["name"];

        if (data["type"] == "modification") {
            type = Type::Modification;

        } else if (data["type"] == "simulation") {
            type = Type::Simulation;
        }

        timestamp = QDateTime::fromString(data["timestamp"], "yyyy-MM-dd_HH:mm:ss.zzz");

        return true;
    }

    description = "";
    name = "";
    type = Type::Modification;
    timestamp = QDateTime();
    return false;
}
