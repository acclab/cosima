/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "abstractgeometrymodel.h"

#include "gui/rendering/linemesh.h"
#include "gui/rendering/trianglemesh.h"

#include <QOpenGLContext>


AbstractGeometryModel::AbstractGeometryModel(QObject* parent) : AbstractModel(parent) {
    m_lineMesh = new LineMesh();
    m_faceMesh = new TriangleMesh();

    m_showFaces = true;
    m_showLines = true;
}


AbstractGeometryModel::~AbstractGeometryModel() {
    if (m_lineMesh) delete m_lineMesh;
    if (m_faceMesh) delete m_faceMesh;
}


void AbstractGeometryModel::draw(const glm::mat4& model, QOpenGLShaderProgram& shader) {
    shader.setUniformValue("m", QMatrix4x4(glm::value_ptr(model * modelMatrix())).transposed());

    if (m_showLines) {
        if (!m_lineMesh->isInitialized()) m_lineMesh->initialize();
        m_lineMesh->draw(shader);
    }

    if (m_showFaces) {
        if (!m_faceMesh->isInitialized()) m_faceMesh->initialize();
        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(1.0, 1.0f);
        m_faceMesh->draw(shader);
        glDisable(GL_POLYGON_OFFSET_FILL);
    }
}


void AbstractGeometryModel::setLineVertexData(const std::vector<glm::vec3>& positions,
                                              const std::vector<glm::vec4>& colors,
                                              const std::vector<unsigned int>& indices) {
    m_lineMesh->setPositions(positions);
    m_lineMesh->setColors(colors);
    m_lineMesh->setIndices(indices);
}


void AbstractGeometryModel::setFaceVertexData(const std::vector<glm::vec3>& positions,
                                              const std::vector<glm::vec4>& colors,
                                              const std::vector<unsigned int>& indices) {
    m_faceMesh->setPositions(positions);
    m_faceMesh->setColors(colors);
    m_faceMesh->setIndices(indices);
}


bool AbstractGeometryModel::showFaces() {
    return m_showFaces;
}


bool AbstractGeometryModel::showLines() {
    return m_showLines;
}


void AbstractGeometryModel::setShowFaces(bool show) {
    m_showFaces = show;
}


void AbstractGeometryModel::setShowLines(bool show) {
    m_showLines = show;
}
