/*
 * Taken from https://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html
 */


#include <QStringList>

#include "treeitem.h"


TreeItem::TreeItem(const QList<QVariant>& data, TreeItem* parent) {
    m_parentItem = parent;
    m_itemData = data;
}


TreeItem::~TreeItem() {
    qDeleteAll(m_childItems);
}


void TreeItem::appendChild(TreeItem* item) {
    m_childItems.append(item);
}


bool TreeItem::removeChildren(int position, int count) {
    if (position < 0 || position + count > m_childItems.size()) {
        return false;
    }

    for (int row = 0; row < count; ++row) {
        delete m_childItems.takeAt(position);
    }

    return true;
}


TreeItem* TreeItem::child(int row) {
    return m_childItems.value(row);
}


int TreeItem::childCount() const {
    return m_childItems.count();
}


int TreeItem::columnCount() const {
    return m_itemData.count();
}


QVariant TreeItem::data(int column) const {
    return m_itemData.value(column);
}


bool TreeItem::setData(int column, const QVariant& data) {
    if (column < 0 || column >= m_itemData.size()) {
        return false;
    }
    m_itemData[column] = data;
    return true;
}


TreeItem* TreeItem::parent() {
    return m_parentItem;
}


int TreeItem::row() const {
    if (m_parentItem) return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));
    return 0;
}
