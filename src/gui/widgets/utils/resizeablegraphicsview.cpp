/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "resizeablegraphicsview.h"


ResizeableGraphicsView::ResizeableGraphicsView(QWidget* parent) : QGraphicsView(parent) {
    m_x = 0;
    m_y = 0;
    m_w = 260;
    m_h = 260;
}

/*!
 * Overrides  QGraphicsView::resizeEvent().
 * The contents of the view is scaled when the GraphicsView is resized.
 */
void ResizeableGraphicsView::resizeEvent(QResizeEvent* event) {
    fitInView(m_x, m_y, m_w, m_h, Qt::KeepAspectRatio);
    QGraphicsView::resizeEvent(event);
}

/*!
 * Sets the view rectangle.
 */
void ResizeableGraphicsView::setView(float x, float y, float w, float h) {
    m_x = x;
    m_y = y;
    m_w = w;
    m_h = h;
}
