/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "templateprojectcreatorwidget.h"
#include "ui_templateprojectcreatorwidget.h"

#include "api/programsettings.h"

#include <QDir>

TemplateProjectCreatorWidget::TemplateProjectCreatorWidget(QWidget* parent)
        : AbstractProjectCreatorWidget(parent), ui(new Ui::TemplateProjectCreatorWidget) {
    ui->setupUi(this);

    ui->widget_animatedBlock->hide();
    m_nextEnabled = false;

    connect(ui->listWidget, &QListWidget::currentRowChanged, this,
            &TemplateProjectCreatorWidget::currentTemplateChanged);

    // Connect the FrameReaderThread
    connect(&m_frameReader, &FrameReaderThread::frameReadFinished, this, [this](bool success) {
        if (success) {
            ui->widget_animatedBlock->draw(m_frameReader.frame(), ProgramSettings::getInstance().visualSettings());
            ui->widget_animatedBlock->show();
        } else {
            ui->widget_animatedBlock->hide();
        }
        emit frameLoadCompleted(success);
        //        emit frameLoadCompleted(success, m_path);
        //        if (success)
        //            frameLoaded();
        //        else
        //            frameLoadingFailed();
    });
}

TemplateProjectCreatorWidget::~TemplateProjectCreatorWidget() {
    delete ui;
}

void TemplateProjectCreatorWidget::enter() {
    //    m_selectedPath = "";
    //    m_pathCandidate = "";
    m_path = "";
    loadTemplateList();
}

const ExtendedEntryData& TemplateProjectCreatorWidget::getEntry() {
    m_entry = ExtendedEntryData();
    //    if (!m_selectedPath.isEmpty()) {
    //        m_entry.data = Entry(m_selectedPath + "/history/entry_1").data();
    //        m_entry.frame = m_frameReader.frame();
    //    }
    if (!m_path.isEmpty()) {
        m_entry.data = Entry(m_path + "/history/entry_1").data();
        m_entry.frame = m_frameReader.frame();
    }
    return m_entry;
}

QString TemplateProjectCreatorWidget::projectName() {
    //    return QDir(m_selectedPath).dirName();
    return QDir(m_path).dirName();
}

void TemplateProjectCreatorWidget::loadTemplateList() {
    ui->listWidget->clear();
    QDir dir(QCoreApplication::applicationDirPath() + "/templates/");
    if (dir.exists()) {
        QStringList templates = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        templates.sort();
        for (QString templateName : templates) {
            ui->listWidget->addItem(templateName);
        }
        ui->listWidget->blockSignals(true);
        ui->listWidget->setCurrentRow(0);
        ui->listWidget->blockSignals(false);
        currentTemplateChanged(0);
    }
}

// void TemplateProjectCreatorWidget::frameLoaded() {
//    ui->widget_animatedBlock->draw(m_frameReader.frame(), ProgramSettings::getInstance().visualSettings());
//    ui->widget_animatedBlock->show();
//    //    m_selectedPath = m_pathCandidate;
//    //    m_pathCandidate = "";
//    m_nextEnabled = true;
//    emit enableNext(true);
//}

// void TemplateProjectCreatorWidget::frameLoadingFailed() {
//    ui->widget_animatedBlock->hide();
//    //    m_pathCandidate = "";
//    //    m_selectedPath = "";
//    m_path = "";
//    m_nextEnabled = false;
//    emit enableNext(false);
//}


void TemplateProjectCreatorWidget::currentTemplateChanged(int /* row */) {

    if (ui->listWidget->currentItem()) {
        emit frameLoading();

        QString templateName = ui->listWidget->currentItem()->text();

        //        m_pathCandidate = QCoreApplication::applicationDirPath() + "/templates/" + templateName;
        //        m_selectedPath = "";

        //        m_nextEnabled = false;
        //        emit enableNext(false);

        //        m_frameReader.read(m_pathCandidate + "/history/entry_1/frames/frame_0");

        m_path = QCoreApplication::applicationDirPath() + "/templates/" + templateName;
        m_frameReader.read(m_path + "/history/entry_1/frames/frame_0");
    }
}
