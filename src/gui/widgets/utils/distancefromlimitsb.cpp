/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "distancefromlimitsb.h"


DistanceFromLimitSB::DistanceFromLimitSB(QWidget* parent) : QDoubleSpinBox(parent) {
    dispMode = DistanceFromLimitSB::Normal;
}

/*!
 * Overrided textFromValue() function.
 * Calculates a text from the value while taking into account the display mode.
 */
QString DistanceFromLimitSB::textFromValue(double value) const {
    if (dispMode == DistanceFromLimitSB::DistanceFromMaximum) {
        value = maximum() - value;
    } else if (dispMode == DistanceFromLimitSB::DistanceFromMinimum) {
        value = value - minimum();
    }
    return QDoubleSpinBox::textFromValue(value);
}

/*!
 * Overrided valueFromText() function.
 * Converts the text into a number. Number is changed accordingly based on the display mode.
 */
double DistanceFromLimitSB::valueFromText(const QString& text) const {
    double ret = text.toDouble();
    if (dispMode == DistanceFromLimitSB::DistanceFromMaximum) {
        ret = maximum() - ret;
    } else if (dispMode == DistanceFromLimitSB::DistanceFromMinimum) {
        ret = minimum() + ret;
    }
    return ret;
}

/*!
 * Overrided validate() function.
 * Allows only QStrings that are or will be floating point numbers.
 */
QValidator::State DistanceFromLimitSB::validate(QString& text, int& /*pos*/) const {
    bool ok;
    text.toDouble(&ok);
    if (ok)
        return QValidator::State::Acceptable;
    else if (text == "" || text == "+" || text == "-")
        return QValidator::State::Intermediate;
    return QValidator::State::Invalid;
}

DistanceFromLimitSB::DisplayMode DistanceFromLimitSB::displayMode() {
    return dispMode;
}

/*!
 * Sets the display mode.
 */
void DistanceFromLimitSB::setDisplayMode(DistanceFromLimitSB::DisplayMode mode) {
    dispMode = mode;
}
