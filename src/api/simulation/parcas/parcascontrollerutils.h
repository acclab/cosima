/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef PARCASCONTROLLERUTILS_H
#define PARCASCONTROLLERUTILS_H

#include "api/entry.h"
#include "api/simulation/mdparameters.h"
#include "api/state/parameters/baseparameters.h"


class ParcasControllerUtils {
  public:
    ParcasControllerUtils();

    bool writeRelaxFile(const ExtendedEntryData& entry, const QString& path, const api::BaseParameters& parameters);
    bool writeCascadeFile(const ExtendedEntryData& entry, const QString& path, const api::BaseParameters& parameters);

  private:
    bool writeGeneralParameters(const ExtendedEntryData& entry, const api::BaseParameters& parameters);
    bool writeTemperatureControlParameters(const ExtendedEntryData& entry, const api::BaseParameters& parameters);
    bool writePressureControlParameters(const api::BaseParameters& parameters);
    bool writeSimulationCellInteractionParameters(const ExtendedEntryData& entry,
                                                  const api::BaseParameters& parameters);
    bool writeElstopParameters(const api::BaseParameters& parameters);
    bool writeRecoilParameters(const api::BaseParameters& parameters);
    bool writeSpeedupParameters(const api::BaseParameters& parameters);
    bool writeOutputParameters(const api::BaseParameters& parameters);

    MDParameters m_params;
};

#endif  // PARCASCONTROLLERUTILS_H
