/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TYPEATOMSELECTORWIDGET_H
#define TYPEATOMSELECTORWIDGET_H

#include "abstractatomselectorwidget.h"
#include "api/atomselections.h"

#include <QHash>

class TypeAtomSelectorWidget : public AbstractAtomSelectorWidget {
  public:
    explicit TypeAtomSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr);

    virtual const AbstractAtomSelection& selector() override;
    void connectToPlot() override {}

    void init() override;
    void updateTypes() override;

  private:
    ArrayAtomSelection m_selector;

    QHash<int, bool> m_selected;

    QWidget* m_contents = nullptr;
};

#endif  // TYPEATOMSELECTORWIDGET_H
