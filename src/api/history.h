/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef HISTORY_H
#define HISTORY_H

#include "api/entry.h"

#include <QException>

/*!
 * \brief Handles the project history
 *
 * Contains the history of a project. Can read entries from a given history directory path. One entry can be loaded at a
 * time for atom data inspection. Can also be used for creating a new entry and writing it to the project hierarchy. for
 * more information on COSIRMA's project structure, see Project.
 *
 * \todo Sort out the signals. entryAdded() might be unnecessary.
 */
class History : public QObject {
    Q_OBJECT

  public:
    explicit History(const QString& path = "", QObject* parent = nullptr);
    ~History();

    int entryCount() const;
    const Entry* entry(int entryIndex, int frameIndex = -1) const;
    const Entry* newestEntry() const;
    //    Entry* mutableNewestEntry();

    Entry* addNewEntry(ExtendedEntryData& entryData);
    Entry* createEntry(ExtendedEntryData& entryData, bool writeFrameData);

    void addEntry(Entry* entry);
    bool removeEntry(const Entry* entry);
    void updateEntry(int entryIndex, const ExtendedEntryData& entryData);

    void load(const QString& path);
    void unload();

    const QVector<Entry*>& entries() const;

  private:
    void connectEntry(Entry* entry, int entryIndex);

    QVector<Entry*> m_entries;
    QString m_directoryPath;

    Entry* m_emptyEntry;


  signals:
    void loaded();
    void unloaded();
    void entryUpdated(int entryIndex, int frameIndex);  //!< \brief Emitted when there is a new latest entry.
    void entryCreated(int entryIndex);
    void entryAdded(int entryIndex);  //!< \brief Emitted when a new entry is added to the history.
    void entryRemoved(int entryIndex);

    void frameAdded(const QString& frameInfo, int entryIndex,
                    int frameIndex);  //!< \brief Emitted when a new frame is appended to the last entry.
};


class HistoryException : public QException {
  public:
    HistoryException(QString _message) : message(_message) {}
    void raise() const override { throw *this; }
    HistoryException* clone() const override { return new HistoryException(*this); }

    const QString message;
};

#endif  // HISTORY_H
