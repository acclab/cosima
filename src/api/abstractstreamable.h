/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ABSTRACTSTREAMABLE_H
#define ABSTRACTSTREAMABLE_H

#include <QDataStream>
#include <QDebug>

/*!
 * \brief An abstract class for streamable objects.
 *
 * Has virtual functions for streaming a object into and out of QDataStream. The streamed contents should be read and
 * written in the same order. Also has a function for writing the object into QDebug stream.
 */
class AbstractStreamable {
  public:
    virtual ~AbstractStreamable() {}

    /*!
     * \brief Writes the object into QDataStream
     * \param out The stream object
     * \return The stream object after writing
     */
    virtual QDataStream& save(QDataStream& out) const = 0;

    /*!
     * \brief Reads the object from QDataStream
     * \param in The stream object
     * \return The stream object after reading
     */
    virtual QDataStream& load(QDataStream& in) = 0;
    virtual QDebug debug(QDebug d) const;
};

QDataStream& operator<<(QDataStream& stream, const AbstractStreamable& settings);
QDataStream& operator>>(QDataStream& stream, AbstractStreamable& settings);

QDebug operator<<(QDebug d, const AbstractStreamable& settings);


#endif  // ABSTRACTSTREAMABLE_H
