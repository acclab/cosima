/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "api/history.h"
#include "api/programsettings.h"
#include "api/project.h"

#include "gui/dialogs/dialogexportsimulation.h"
#include "gui/dialogs/dialogsetupbackend.h"
#include "gui/dialogs/dialogstartsimulation.h"
#include "gui/dialogs/informationpromptdialog.h"
#include "gui/dialogs/newprojectdialog.h"
#include "gui/dialogs/settingsdialog.h"
#include "gui/widgets/projectsetup/projectsetupwizard.h"

#include <QDebug>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QTabBar>
#include <QTimer>

/*!
 * \brief Constructs an instance of MainWindow that is connected to the given Api object.
 * \param a Api object
 * \param parent Parent widget
 */
MainWindow::MainWindow(Api* a, QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow), m_api(a) {
    ui->setupUi(this);

    // Initialize the main menu toolbar.
    connect(ui->action_newProject, &QAction::triggered, this, &MainWindow::createProject);
    connect(ui->action_openProject, &QAction::triggered, this, &MainWindow::openProject);
    connect(ui->action_save, &QAction::triggered, m_api, &Api::saveProject);
    connect(ui->action_settings, &QAction::triggered, this, &MainWindow::showSettings);
    connect(ui->action_userManual, &QAction::triggered, this, &MainWindow::showUserManual);
    connect(ui->action_license, &QAction::triggered, this, &MainWindow::showLicenseFile);
    connect(ui->action_about, &QAction::triggered, this, &MainWindow::showAbout);

    initIcons();

    QWidget* barBackGround = new QWidget(ui->centralWidget);
    barBackGround->setGeometry(0, 0, 100000, 60);
    barBackGround->setStyleSheet("QWidget{background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop:0 "
                                 "#404040,stop:0.4 #707070,stop:0.6 #707070,stop:1 #606060)}");
    barBackGround->lower();

    ui->mainTabWidget->setObjectName("mainTabWidget");
    ui->mainTabWidget->tabBar()->setObjectName("mainTabBar");

    ui->mainTabWidget->setStyleSheet(
        "#mainTabWidget::tab-bar {left: 10px;}"
        "#mainTabWidget::pane {background-color: transparent;border: none;}"
        "#mainTabBar::tab{height: 60px;width: 150px; background-color: transparent;color: black;font-size: 16px;}"
        "#mainTabBar::tab:disabled {color: transparent;background-color: transparent;}"
        "#mainTabBar::tab:selected {background-color: #404040; color: white}"
        "#mainTabBar::tab:hover {background-color: #505050}");

    // Setting the window size
    QByteArray geometry = ProgramSettings::getInstance().windowGeometry();
    if (geometry.isEmpty()) {
        const QRect availableGeometry = QApplication::desktop()->availableGeometry(this);
        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2, (availableGeometry.height() - height()) / 2);
    } else {
        restoreGeometry(geometry);
    }

    connect(ui->pushButton_startStop, &QPushButton::clicked, [this](bool checked) {
        if (checked) {
            handleStartSimulation();
        } else {
            handleStopSimulation();
        }
    });

    // Connecting to the api
    connect(m_api, &Api::projectOpened, this, &MainWindow::onProjectOpen);
    connect(m_api, &Api::projectClosing, this, &MainWindow::onProjectClosing);
    connect(m_api, &Api::projectClosed, this, &MainWindow::onProjectClose);

    connect(m_api, QOverload<Api::SimulationStatus>::of(&Api::simulationStatusChanged), this,
            QOverload<Api::SimulationStatus>::of(&MainWindow::handleSimulationStatus));
    connect(m_api, QOverload<Api::SimulationStatus, const QString&>::of(&Api::simulationStatusChanged), this,
            QOverload<Api::SimulationStatus, const QString&>::of(&MainWindow::handleSimulationStatus));

    connect(m_api, &Api::errorMessage, this, [this](const QString& message) {
        QString text = "The simulation ended with an error.\n" + message;
        if (QMessageBox::warning(this, "Simulation Error", text, QMessageBox::Ok) == QMessageBox::Ok) {
            qDebug() << "The simulation should be removed";
        }
    });

    connect(ui->pushButton_discardChanges, &QPushButton::clicked, this, [=] { m_api->entryEditor.discardChanges(); });
    connect(ui->pushButton_save, &QPushButton::clicked, this, [=] {
        // Create the entry in the history
        simulationEntryData = m_api->entryEditor.saveToHistory();
        // Set the newly created entry to the simulation list widget. The simulation list widget will pass it on to the
        // correct simulation widget.
        m_simulationListWidget->setEntry(simulationEntryData);

        //! \todo should the project be saved here???
    });
    //! \todo These two should be handled through the API, not by accessing its members
    // connect(&m_api->entryEditor, &EntryEditor::changed, ui->saveEntryPB, &QPushButton::setEnabled);
    // connect(&m_api->simulationList, &SimulationList::changed, this, &MainWindow::updateStartSimulationButton);

    connect(ui->pushButton_discardChanges_simulation, &QPushButton::clicked, this, [=] {
        // Reset the simulationEntryData ontop of the old one to revert to the state before.
        // The simulationListWidget calls the discard function internally, so no need to call it explicitly here.
        discardChanges();
    });
    connect(ui->pushButton_save_simulation, &QPushButton::clicked, this, [=] {
        saveChanges();

        //! \todo should the project be saved here???
        // Save the project
        m_api->saveProject();
    });

    //! \todo this should be asked from the api
    if (Project::getInstance().isOpen()) {
        onProjectOpen();
    } else {
        onProjectClose();
    }

    // Connection for debugging focus holder
    /*
    connect(qApp, &QApplication::focusChanged, [this](QWidget *old, QWidget *n) {
        if (n) qDebug() << n;
    });
    */
}

MainWindow::~MainWindow() {
    delete ui;
}

/*!
 * \brief Overdriven closeEvent.
 *
 * Checks if a project is open and if there is unsaved progress.
 */
void MainWindow::closeEvent(QCloseEvent* event) {

    if (Project::getInstance().isOpen() && isModified()) {
        //! \todo Check if the project is dirty before asking this question.
        //!     "You have unsaved changes to the project, do you want to save the project before exiting?"
        QMessageBox::StandardButton answer =
            QMessageBox::warning(this, "Exit application", "Do you want to save the project before exiting?",
                                 QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if (answer == QMessageBox::Yes) {
            saveChanges();
            m_api->saveProject();  //! \todo check what this does
            event->accept();
        } else if (answer == QMessageBox::No) {
            discardChanges();
            event->accept();
        } else {
            event->ignore();
            return;
        }
    } else {
        event->accept();
    }

    ProgramSettings::getInstance().setWindowGeometry(saveGeometry());
    ProgramSettings::getInstance().save();  //! \todo check what this does
}


void MainWindow::saveChanges() {
    //! \todo add functions to the entry editor here as well
    // Get the modified entry with the injected atom types and lists of potential and elstop files.
    simulationEntryData = new ExtendedEntryData(m_simulationListWidget->entry());
    // Overwrite the simulation parameters with the updated state of the widgets.
    m_simulationListWidget->setEntry(simulationEntryData);
    m_simulationListWidget->saveChanges();
}


void MainWindow::discardChanges() {
    //! \todo add functions to the entry editor here as well
    m_simulationListWidget->setEntry(simulationEntryData);
    m_simulationListWidget->discardChanges();
}


bool MainWindow::isModified() {
    //! \todo add functions to the entry editor here as well
    return m_simulationListWidget->isModified();
}


/*!
 * Initializes the icons in the toolbar, ui->tabWidget, and some buttons.
 */
void MainWindow::initIcons() {

    QPixmap pixmap(":/logos/cosirma-logo-text-right");
    ui->label_cosirmaLogo->setPixmap(pixmap.scaled(this->width()/2, this->height()/2, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    // Tab widget
    int height = ui->mainTabWidget->tabBar()->height();

    setupTabIcon(0, ":/icons/home", ":/icons/home-invert", height);
    setupTabIcon(1, ":/icons/cubes", ":/icons/cubes-invert", height);
    setupTabIcon(2, ":/icons/cogs", ":/icons/cogs-invert", height);
    setupTabIcon(3, ":/icons/chart-bar", ":/icons/chart-bar-invert", height);

    ui->mainTabWidget->setIconSize(QSize(height, height));

    // Toolbar
    height = ui->menuBar->height();

    setupActionIcon(ui->action_newProject, ":/icons/folder-plus-invert", height);
    setupActionIcon(ui->action_openProject, ":/icons/folder-open-invert", height);
    setupActionIcon(ui->action_save, ":/icons/save-invert", height);

    setupActionIcon(ui->action_settings, ":/icons/cog-invert", height);

    setupActionIcon(ui->action_refresh, ":/icons/redo-alt-invert", height);

    setupActionIcon(ui->action_userManual, ":/icons/file-pdf-invert", height);
    setupActionIcon(ui->action_license, ":/icons/info-circle-invert", height);
    setupActionIcon(ui->action_about, ":/icons/question-circle-invert", height);

    // Buttons
    height = static_cast<int>(ui->pushButton_openProject->height() * 0.7);
    ui->pushButton_openProject->setIcon(QIcon(":/icons/folder-open"));
    ui->pushButton_openProject->setIconSize(QSize(height, height));
    ui->pushButton_newProject->setIcon(QIcon(":/icons/folder-plus"));
    ui->pushButton_newProject->setIconSize(QSize(height, height));

    connect(ui->pushButton_openProject, &QPushButton::clicked, this, &MainWindow::openProject);
    connect(ui->pushButton_newProject, &QPushButton::clicked, this, &MainWindow::createProject);
}


/*!
 * \brief Helper function to setup the tab widget icons
 * \param tab
 * \param path1
 * \param path2
 * \param height
 */
void MainWindow::setupTabIcon(int tab, QString path1, QString path2, int height) {
    QIcon icon;
    icon.addFile(path1, QSize(height, height), QIcon::Normal);
    icon.addFile(":/icons/transparent", QSize(height, height), QIcon::Disabled);
    icon.addFile(path2, QSize(height, height), QIcon::Normal, QIcon::On);
    ui->mainTabWidget->setTabIcon(tab, icon);
}


/*!
 * \brief Helper function to setup the icons of the menu item actions.
 * \param action
 * \param path
 */
void MainWindow::setupActionIcon(QAction* action, QString path, int height) {
    QIcon icon;
    icon.addFile(path, QSize(height, height), QIcon::Normal);
    action->setIcon(icon);
}


void MainWindow::openProject() {
    //! \todo Move the project checking logic to the api!

    if (m_api->isProjectOpen()) {
        QMessageBox::StandardButton reply = QMessageBox::question(
            this, "Open New Project?", "Current project will be closed, do you want to save the changes?",
            QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if (reply == QMessageBox::Yes) {
            //! \todo add functions to the entry editor here as well
            m_simulationListWidget->saveChanges();
            m_api->saveProject();
            m_api->closeProject(true);
        } else if (reply == QMessageBox::No) {
            m_api->closeProject(false);
        } else {
            return;
        }
    }

    QString filePath = "noPath";

    QFileDialog* fileDialog = new QFileDialog();
    fileDialog->setOption(QFileDialog::DontUseNativeDialog, true);
    fileDialog->setWindowTitle("Choose project");

    // Connecting the file dialog so that if a directory that contains a file named ".cosirmaproject"
    // is clicked, the dialog closes
    connect(fileDialog, &QFileDialog::directoryEntered, [fileDialog, &filePath](QString path) {
        QDir dir(path);
        for (const QString& fileName : dir.entryList(QDir::Files | QDir::Hidden)) {
            if (!fileName.compare(".cosirmaproject")) {
                fileDialog->close();
                filePath = path;
                return;
            }
        }
    });

    fileDialog->setFileMode(QFileDialog::Directory);
    fileDialog->setViewMode(QFileDialog::Detail);

    QDir dir(ProgramSettings::getInstance().locationSettings().projectDirPath);
    QString firstProject;
    if (dir.exists()) {
        fileDialog->setDirectory(dir.path());
        QStringList projects = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        if (!projects.isEmpty()) firstProject = projects.first();
        fileDialog->selectFile(firstProject);
    } else {
        fileDialog->setDirectory(QDir::homePath());
    }

    if (fileDialog->exec()) {
        filePath = fileDialog->selectedFiles().last();
    }

    delete fileDialog;

    if (filePath.compare("noPath") != 0) {
        m_api->loadProject(filePath);
    }
}

void MainWindow::createProject() {
    qDebug() << "MainWindow::createProject()";

    if (Project::getInstance().isOpen()) {
        QMessageBox::StandardButton reply =
            QMessageBox::question(this, "Create New Project?", "Current project will be closed. Continue?",
                                  QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            m_api->closeProject(true);
        } else {
            return;
        }
    }

    //    NewProjectDialog dialogNewProject(this);
    //    dialogNewProject.setModal(true);
    //    if (dialogNewProject.exec() == QDialog::Accepted) {
    //        m_api->loadProject(dialogNewProject.getPath());
    //    }

    ProjectSetupWizard wizard(this);
    wizard.setModal(true);
    if (wizard.exec() == QDialog::Accepted) {
        m_api->loadProject(wizard.projectPath());
    }
}

void MainWindow::showSettings() {
    SettingsDialog dialog(this);
    dialog.setModal(true);
    connect(&dialog, &SettingsDialog::enableSpeedupModule, m_simulationListWidget,
            &SimulationListWidget::onEnableSpeedupModule);
    if (dialog.exec() == QDialog::Accepted) {
        dialog.save();
    }
}

/**
 * @brief MainWindow::showUserManual
 *
 * Opens a modal window with a button to open the user manual in a separate window, using the default PDF viewer.
 */
void MainWindow::showUserManual() {
    qDebug() << "MainWindow::showUserManual()";

    InformationPromptDialog* dialog = new InformationPromptDialog(this);
    dialog->setTitle("User Manual COSIRMA");
    dialog->addLogo(":/logos/cosirma-logo-text-right", 400, 200, BorderLayout::North);

    int height = static_cast<int>(ui->pushButton_openProject->height() * 0.7);
    QPushButton* button = new QPushButton(dialog);
    button->setAttribute(Qt::WA_DeleteOnClose);
    button->setText("Open the User Manual");
    button->setIcon(QIcon(":/icons/file-pdf"));
    button->setIconSize(QSize(height, height));

    connect(button, &QPushButton::clicked, []() {
        // Locate the file and copy it to the temporary folder for opening
        QFile helpFile(":/files/user-manual.pdf");
        helpFile.copy(QDir::tempPath().append("/cosirma-user-manual.pdf"));
        QDesktopServices::openUrl(QUrl::fromLocalFile(QDir::tempPath().append("/cosirma-user-manual.pdf")));
    });

    dialog->setContent(button);

    dialog->show();
}

/**
 * @brief MainWindow::showLicenseFile
 *
 * Opens a modal window with the GPL license text.
 */
void MainWindow::showLicenseFile() {
    qDebug() << "MainWindow::showLicenseFile()";

    InformationPromptDialog* dialog = new InformationPromptDialog(this);

    dialog->setTitle("Copyright COSIRMA");
    dialog->addLogo(":/logos/cosirma-logo-text-below", 200, 200, BorderLayout::West);
    dialog->setContent(":/files/copyright.html");

    dialog->show();
}

/**
 * @brief MainWindow::showAbout
 *
 * Opens a model window with a short description of the application.
 */
void MainWindow::showAbout() {
    qDebug() << "showAbout()";

    InformationPromptDialog* dialog = new InformationPromptDialog(this);

    dialog->setTitle("About COSIRMA");
    dialog->addLogo(":/logos/cosirma-logo-text-below", 200, 200, BorderLayout::West);
    dialog->setContent(":/files/about.html");

    dialog->show();
}


void MainWindow::handleStartSimulation() {
    qDebug() << "startSimulation()";

    if (isModified()) {
        QMessageBox::StandardButton reply =
            QMessageBox::information(this, "Unsaved changes", "Save changes to the input data before continuing?",
                                     QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if (reply == QMessageBox::Yes) {
            saveChanges();
        } else if (reply == QMessageBox::No) {
            discardChanges();
        } else {
            return;
        }
    }

    // It is on the API's responsibility to make sure the simulation can't be started if it already is running. If the
    // interface is allowed to the start the simulation (i.e. button enabled) it is OK to just call the start method.

    if (ProgramSettings::getInstance().backendSettings().showSimulationStartDialog) {
        DialogStartSimulation* dialog = new DialogStartSimulation(this);
        if (dialog->exec() == QDialog::Accepted) {
            dialog->save();
            m_api->startSimulation(*simulationEntryData);
        } else {
            updateStartSimulationButton(true, false, "Start Simulation");
        }
    } else {
        m_api->startSimulation(*simulationEntryData);
    }
}


void MainWindow::handleStopSimulation() {
    qDebug() << "MainWindow::stopSimulation()";
    m_api->stopSimulation();
}


void MainWindow::onProjectClosing() {}


void MainWindow::onProjectClose() {
    setWindowTitle("COSIRMA");
    ui->mainTabWidget->setTabEnabled(1, false);
    ui->mainTabWidget->setTabEnabled(2, false);
    ui->mainTabWidget->setTabEnabled(3, false);
    ui->mainTabWidget->setCurrentIndex(0);

    delete m_entryEditorWidget;
    m_entryEditorWidget = nullptr;

    delete m_simulationListWidget;
    m_simulationListWidget = nullptr;

    delete m_trackOutputWidget;
    m_trackOutputWidget = nullptr;
}


void MainWindow::onProjectOpen() {
    setWindowTitle("COSIRMA " + Project::getInstance().path());

    onProjectClose();

    m_entryEditorWidget = new EntryEditorWidget(&(m_api->entryEditor), this);
    ui->widget_editorContent->layout()->addWidget(m_entryEditorWidget);

    m_simulationListWidget = new SimulationListWidget(&(m_api->simulationList), this);
    connect(m_simulationListWidget, &SimulationListWidget::enableSimulationButtons, this,
            &MainWindow::handleEnableSimulationButtons);
    ui->widget_simulationListContent->layout()->addWidget(m_simulationListWidget);

    // Disable the simulation buttons by default
    handleEnableSimulationButtons(false);

    m_trackOutputWidget = new TrackOutputWidget(this);
    ui->widget_outputContent->layout()->addWidget(m_trackOutputWidget);

    ui->mainTabWidget->setTabEnabled(1, true);
    ui->mainTabWidget->setTabEnabled(2, true);
    ui->mainTabWidget->setTabEnabled(3, true);
    ui->mainTabWidget->setCurrentIndex(1);

    // Use the newest entry to initialize the GUI when the project has been opened.
    simulationEntryData = new ExtendedEntryData(Project::getInstance().history.newestEntry()->extendedData());
    m_api->entryEditor.setEntry(*simulationEntryData);
    m_simulationListWidget->setEntry(simulationEntryData);
}


/*!
 * \brief Helper function to easier set the state of the start simulation button.
 * \param enabled Whether the button should be clickable (different graphical state)
 * \param checked Whether the button should show Start or Stop
 * \param tooltip A small helping text when hovering the button.
 */
void MainWindow::updateStartSimulationButton(bool enabled, bool checked, const QString& tooltip) {
    qDebug() << "MainWindow::updateStartSimulationButton()";
    ui->pushButton_startStop->setEnabled(enabled);
    ui->pushButton_startStop->setChecked(checked);
    ui->pushButton_startStop->setToolTip(tooltip);
    if (checked) {
        ui->pushButton_startStop->setText("Stop");
    } else {
        ui->pushButton_startStop->setText("Start");
    }
}


/*!
 * \brief Callback function for the API signals. Depending on the status of the incoming signal, this function should
 * update the GUI to mirror the state of the API.
 * \param status
 * \param message
 */
void MainWindow::handleSimulationStatus(Api::SimulationStatus status) {
    handleSimulationStatus(status, "Something went wrong, please try again after checking your parameters.");
}


/*!
 * \brief Callback function for the API signals. Depending on the status of the incoming signal, this function should
 * update the GUI to mirror the state of the API.
 * \param status
 * \param message
 */
void MainWindow::handleSimulationStatus(Api::SimulationStatus status, const QString& message) {
    qDebug() << "MainWindow::handleSimulationStatus";
    switch (status) {
        case Api::SimulationStatus::Aborted:
            qDebug() << "    ABORTED";
            updateStartSimulationButton(true, false, "Start Simulation");
            //! \todo ask to Remove the unfinished entry!
            break;
        case Api::SimulationStatus::Started:
            qDebug() << "    STARTED";
            updateStartSimulationButton(true, true, "Stop Simulation");
            break;
        case Api::SimulationStatus::Stopping:
            qDebug() << "    STOPPING";
            updateStartSimulationButton(false, true, "Stopping Simulation...");
            break;
        case Api::SimulationStatus::Finished:
            qDebug() << "    FINISHED";
            updateStartSimulationButton(true, false, "Start Simulation");
            break;
        case Api::SimulationStatus::Error:
            qDebug() << "    ERROR";
            QMessageBox::critical(this, "Error!", message, QMessageBox::Ok);
            updateStartSimulationButton(true, false, "Start Simulation");
            break;
    }
}


void MainWindow::handleEnableSimulationButtons(bool enable) {
    ui->pushButton_startStop->setEnabled(enable);
    ui->pushButton_exportSimulation->setEnabled(enable);
}


void MainWindow::on_pushButton_exportSimulation_clicked() {
    qDebug() << this << "Export simulation environment to path <insert path>";
    if (!m_simulationListWidget->selectedWidget()) {
        qDebug() << "    select a simulation!";
        QMessageBox::information(this, "No simulation selected", "Please, select a simulation before exporting!",
                                 QMessageBox::Ok);
        return;
    }


    if (isModified()) {
        QMessageBox::StandardButton reply =
            QMessageBox::information(this, "Unsaved changes", "Save changes to the input data before continuing?",
                                     QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if (reply == QMessageBox::Yes) {
            saveChanges();
        } else if (reply == QMessageBox::No) {
            discardChanges();
        } else {
            return;
        }
    }


    DialogExportSimulation* dialog = new DialogExportSimulation(this);
    if (dialog->exec() == QDialog::Accepted) {
        qDebug() << "export the simulation environment";
        ExportData data = dialog->data();
        qDebug() << data.type;
        m_api->exportStandaloneSimulation(data.path, data, *simulationEntryData);
    } else {
        qDebug() << "do not export anything!";
    }
}
