/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BOXSELECTOR_H
#define BOXSELECTOR_H

#include "api/atomselections.h"
#include "api/geometries.h"
#include "gui/widgets/abstractatomselectorwidget.h"

#include <QCheckBox>
#include <QWidget>


namespace Ui {
class BoxSelectorWidget;
}

/*!
 * \brief QWidget for selecting the size of a box within given limits.
 *
 * DImensions of a box can be set with three RangeSliderBoxes.
 *
 * \sa RangeSliderBox
 */
class BoxSelectorWidget : public AbstractAtomSelectorWidget {
    Q_OBJECT

  public:
    explicit BoxSelectorWidget(PlotCellGLWidget* context, QWidget* parent = nullptr);
    ~BoxSelectorWidget() override;

    const AbstractAtomSelection& selector() override;

    void init() override;
    void updateAtoms() override;
    void updateCellLims() override;
    void connectToPlot() override;
    void hidePlottables() override;

  private:
    Ui::BoxSelectorWidget* ui;

    BoxAtomSelection m_selector;

    CubeModel* m_cube = nullptr;
    CubeModel* m_wireframeCube = nullptr;

    QColor m_color;


  private slots:
    void setLimits(const Box& b);
    Box getDimensions();
    void setDimensions(const Box&);

    void blockSignalsFromComponents(bool);

    Box calculateLimits();
    void setColor(const QColor& color);
    void updateDimensions();

  signals:
    void xDimensionChanged(double, double);
    void yDimensionChanged(double, double);
    void zDimensionChanged(double, double);
};

#endif  // BOXSELECTOR_H
