/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "elstopgenerator.h"

#include "api/atomtype.h"
#include "api/data/generators/zbl96.h"
#include "api/elementreader.h"

#include <QDebug>
#include <QFile>
#include <QMap>
#include <QTextStream>


void ElstopGenerator::generate(const QList<SubstratePart>& substrate, const ExtendedEntryData& entry,
                               double substrateDensity, double xmin, double xmax, double xstep) {
    qDebug() << this << "::generate()";

    m_substrateName = "";
    int totalAmount = 0;
    for (const SubstratePart& part : substrate) {
        m_substrateName += part.symbol + (part.amount > 1 ? QString::number(part.amount) : "");
        totalAmount += part.amount;
    }

    qDebug() << "    " << m_substrateName << substrateDensity << xmin << xmax << xstep << totalAmount;


    QList<AtomType> allTypes;
    for (int key : entry.data.types.allTypes.keys()) {
        allTypes << entry.data.types.allTypes[key];
    }

    // Mapping Atom Symbol to another Atom Symbol along with the generated Elstop data for that Symbol pair.
    QMap<QString, QMap<QString, Elstop>> atomPairElstops;

    Zbl96 zbl;
    for (const AtomType& t1 : allTypes) {
        for (const AtomType& t2 : allTypes) {
            qDebug() << t1.atomNumber << t2.atomNumber << t1.mass << t2.mass;

            // Generate elstop data for the atom pair
            zbl.generate(t1.atomNumber, t2.atomNumber, t1.mass, t2.mass, xmin, xmax, xstep, DEFAULT);

            // Create empty placeholder if the t1 symbol isn't already in the map.
            if (!atomPairElstops.contains(t1.symbol)) {
                atomPairElstops.insert(t1.symbol, QMap<QString, Elstop>());
            }

            // Create the elstop data convenience wrapper.
            Elstop elstop;
            elstop.density = zbl.density();
            elstop.values = zbl.values();

            // Introduce the symbol-to-symbol map.
            atomPairElstops[t1.symbol].insert(t2.symbol, elstop);
        }
    }


    // Create the weighted elstop data based on Bragg's rule.
    //    m_elstopMap.clear();
    m_data.clear();

    for (const AtomType& atom : allTypes) {
        if (atom.atomNumber < 1) continue;

        qDebug() << atom.symbol << " -> " << m_substrateName;

        // List of coordinate pairs:
        //      x: v [m/s]
        //      y: F [eV/A]
        QList<QPair<double, double>> xyValues;

        // Copy the X values from one list of elstop value pairs (this column is the same in all of the lists).
        for (const QPair<double, double> values : atomPairElstops[atom.symbol][atom.symbol].values) {
            xyValues.append(QPair<double, double>(values.first, 0));
        }

        // Calculate the Y values from the atom-pair elstop data sets
        for (const SubstratePart& part : substrate) {
            qDebug() << "    " << atom.symbol << " -> " << part.symbol << " amount: " << part.amount;

            // Get the elstop data for the symbol pair
            const Elstop& elstop = atomPairElstops[atom.symbol][part.symbol];
            // Loop over all coordinates to extract the Y-value. Use the Y-value to calculate the weighted elstop data
            // based on the amount of the element species in the compound against the density of that element.
            for (int i = 0; i < elstop.values.size(); ++i) {
                const QPair<double, double>& value = elstop.values[i];
                if (value.first != xyValues[i].first) {
                    qDebug() << value.first << xyValues[i].first;
                    //! \todo throw an exception! This shouldn't be possible though as all elstop data is generated with
                    //! the same X-axis.
                }
                // Update the Y-value with the scaled value (based on Bragg's rule). The densities and amounts are mixed
                // between the parentheses to avoid integer division.
                xyValues[i].second += value.second * (part.amount / elstop.density) * (substrateDensity / totalAmount);
            }
        }


        DataFileContent<ElstopFileData> elstopData;
        for (const QPair<double, double>& xy : xyValues) {
            elstopData.append(ElstopFileData(xy.first, xy.second));
        }

        // Insert the newly computed Symbol->Substrate elstop data into the map.
        //        m_elstopMap.insert(atom.symbol, xyValues);
        m_data[atom.symbol] = elstopData;
    }
}


const QMap<QString, DataFileContent<ElstopFileData>>& ElstopGenerator::data() {
    return m_data;
}
