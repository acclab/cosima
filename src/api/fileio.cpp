/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "fileio.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>

/*!
 * \brief Constructs an object that is connected to a file.
 * \param path Path to the file
 */
FileIO::FileIO(const QString& path) {
    m_filePath = path;
}

/*!
 * \brief Reads the contents of the connected file.
 *
 * \return Map from parameter name (QString) to value (QString). If the reading is unsuccessful, returns an empty map.
 */
QHash<QString, QString> FileIO::readFile() {
    QHash<QString, QString> data;
    QFile file(m_filePath);
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);

        while (!stream.atEnd()) {
            QString line = stream.readLine();
            if (line.length() == 0) continue;

            QStringList list = line.split("=");

            if (list.length() < 2) continue;

            QString key = line.section('=', 0, 0, QString::SectionSkipEmpty).trimmed();
            QString value = line.section("=", 1, QString::SectionSkipEmpty).trimmed();

            QStringList words = value.split(QRegExp("\\s+"), QString::SkipEmptyParts);

            if (words.length() > 1) {
                if (value.at(0) == '\"' && value.at(value.length() - 1) == '\"') {
                    value = line.section('=', 1, 1, QString::SectionSkipEmpty).replace('\"', "").trimmed();
                } else {
                    value = words[0];
                }
            }

            data[key] = value;
        }
    }
    return data;
}

/*!
 * \brief Writes the given parameters to the connected file.
 * \param data Map from parameter name (QString) to the value (QString)
 * \return \c true if the writing is successful
 */
bool FileIO::writeFile(const QHash<QString, QString>& data) {
    QFile file(m_filePath);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
        QTextStream stream(&file);
        for (auto i = data.begin(); i != data.end(); ++i) {
            // Put the value in quotation marks if it has multiple words and is not in quotations already.
            if (!i.value().isEmpty() && !(i.value().at(0) == '\"' && i.value().at(i.value().length() - 1) == '\"') &&
                i.value().split(QRegExp("\\s+")).length() > 1) {
                stream << QString("%1 = \"%2\"\n").arg(i.key(), i.value());
            } else {
                stream << QString("%1 = %2\n").arg(i.key(), i.value());
            }
        }
    } else {
        qDebug() << "Writing to " + m_filePath + " failed";
        return false;
    }
    file.close();
    return true;
}
