/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "framereaderthread.h"

/*!
 * \brief Constructs an object.
 * \param parent Parent of the object
 */
FrameReaderThread::FrameReaderThread(QObject* parent) : QThread(parent) {}

FrameReaderThread::~FrameReaderThread() {
    wait();
}

/*!
 * \brief Starts reading the frame from the specified directory.
 * \param frameDirPath Path to the frame directory
 */
void FrameReaderThread::read(const QString& frameDirPath) {
    m_framePath = frameDirPath;
    if (!isRunning()) {
        start(LowPriority);
    } else {
        m_condition.wakeOne();
    }
}


/*!
 * \brief Accesses the frame that was previously read from the directory.
 * \return A const refernce to the XyzData object of the frame
 */
const XyzData& FrameReaderThread::frame() {
    return m_xyzData;
}

/*!
 * \brief Overridden run() function
 */
void FrameReaderThread::run() {
    FrameReaderWriter f(m_framePath);
    bool success = f.read(m_xyzData);
    emit frameReadFinished(success);
}
