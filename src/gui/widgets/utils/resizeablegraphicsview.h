/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef RESIZEABLEGRAPHICSVIEW_H
#define RESIZEABLEGRAPHICSVIEW_H

#include <QGraphicsView>


/*!
 * \brief QGraphicsView with overridden resizeEvent.
 *
 * Now content scales to the size of the view.
 */
class ResizeableGraphicsView : public QGraphicsView {
    Q_OBJECT

  public:
    explicit ResizeableGraphicsView(QWidget* parent = nullptr);
    void setView(float, float, float, float);

  protected:
    void resizeEvent(QResizeEvent* event);

  private:
    float m_x, m_y, m_w, m_h;
};

#endif  // RESIZEABLEGRAPHICSVIEW_H
