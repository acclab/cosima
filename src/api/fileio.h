/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FILEIO_H
#define FILEIO_H

#include <QHash>
#include <QString>

/*!
 * \brief Reads/writes key-value pairs from/into a file in human readable format.
 *
 * Format of each line: [string] = [string], where the left hand side is a key and the right hand side is a value.
 * If the value is in quotations (""), it is interpreted as a string with multiple words. Similarly, when writing a
 * value with more than one word, quotations are added around the string. The underlying data structure is
 * QHash<QString,QString>.
 */
class FileIO {
  public:
    FileIO(const QString& path);
    QHash<QString, QString> readFile();
    bool writeFile(const QHash<QString, QString>& data);

  private:
    QString m_filePath;
};

#endif  // FILEIO_H
