/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ARROWMODEL_H
#define ARROWMODEL_H

#include "gui/rendering/abstractgeometrymodel.h"

#include <QColor>
#include <QObject>


class ArrowModel : public AbstractGeometryModel {
    Q_OBJECT

  public:
    struct ColorGradient {
        ColorGradient() {}

        ColorGradient(QColor color) {
            m_color1 = color;
            m_color2 = color;
            m_color3 = color;
            m_color4 = color;
            m_color5 = color;
        }

        QColor m_color1 = QColor(255, 0, 0);   /*!< \brief Color of the tip (rgba 0-1)*/
        QColor m_color2 = QColor(255, 0, 0);   /*!< \brief Color of the first ring (rgba 0-1)*/
        QColor m_color3 = QColor(255, 125, 0); /*!< \brief Color of the second ring (rgba 0-1)*/
        QColor m_color4 = QColor(255, 255, 0); /*!< \brief Color of the third ring (rgba 0-1)*/
        QColor m_color5 = QColor(255, 125, 0); /*!< \brief Color of the middle point of the end circle (rgba 0-1)*/
    };

    explicit ArrowModel(ColorGradient gradient, float bodyLength = 0.95f, bool flip = false, QObject* parent = nullptr);

  private:
    void setupVertexData();

    unsigned int m_resolution; /*!< \brief Amount of points on one ring*/
    float m_headLength;        /*!< \brief Length of the head of the arrow as a fraction of the arrow's total length*/
    float m_headRadius;        /*!< \brief Radius of the head of the arrow as a fraction of the arrow's total length*/
    float m_bodyRadius;        /*!< \brief Radius of the body of the arrow as a fraction of the arrow's total length*/
    float m_bodyLength;
    float m_arrowLength;

    ColorGradient m_gradient; /*!< \brief Color of the arrow. Assign up to five different colors. */
    bool m_flip;              /*!< \brief Flip the direction of the arrow */
};

#endif  // ARROWMODEL_H
