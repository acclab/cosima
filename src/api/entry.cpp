/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "entry.h"

#include "util.h"

#include <QDateTime>
#include <QDir>

/*!
 * \brief Constructs an object that is connected to an existing entry directory at \a path.
 *
 * The data in the directory is read only when requested.
 *
 * \param path Path to the directory
 */
Entry::Entry(const QString& path, QObject* parent) : Entry(path, ExtendedEntryData(), false, parent) {}

/*!
 * \brief Constructs an object in prepararion to be written as a new entry directory
 *
 * After this constructor, create() should be called to create a new entry directory based on \a data.
 *
 * \param path Path where the directory will be created.
 * \param entryData Data of the new entry
 */
Entry::Entry(const QString& path, const ExtendedEntryData& entryData, QObject* parent)
        : Entry(path, entryData, true, parent) {}

Entry::Entry(const QString& path, const ExtendedEntryData& entryData, bool loaded, QObject* parent) : QObject(parent) {
    m_data = entryData;
    m_data.path = path;

    m_framesRead = loaded;
    m_potentialsRead = loaded;
    m_elstopFilesRead = loaded;
    m_typesRead = loaded;
    m_infoRead = loaded;
    m_frameLoaded = loaded;
}


Entry::~Entry() {}


/*!
 * \brief Removes the entry from the hard drive. This cannot be undone, make sure to do proper validation before calling
 * this function.
 * \return true if the entry directory was removed successfully.
 */
bool Entry::deleteDirectory() {
    this->free();
    QDir entryDir(m_data.path);
    if (entryDir.exists()) {
        if (!entryDir.removeRecursively()) {
            qDebug() << "Couldn't remove the directory";
            return false;
        }
        return true;
    }
    return false;
}

/*!
 * \brief The path of the entry directory to which the object is connected to.
 * \return Path to entry directory
 */
const QString& Entry::directoryPath() const {
    return m_data.path;
}

/*!
 * \brief Checks if the connected entry directory has a valid name.
 *
 * A valid name is entry_<number> (e.g. entry_1).
 *
 * \return \c true if the name is valid
 */
bool Entry::isValid() const {
    QStringList words = QDir(m_data.path).dirName().split("_");
    return (words.length() == 2) && (words[0] == "entry") && (frameCount() != 0);
}

/*!
 * \brief Writes a new entry directory.
 *
 * Can not be used to overwrite, only to create new directories.
 *
 * Example:
 * \code{.cpp}
 * ExtendedEntryData data;
 * Entry newEntry(".../project/history/entry_1", data)
 * std::pair<bool, QString> writing = newEntry.create();
 * if(writing.first){
 *     // Writing was successful
 * } else {
 *     // Writing was not successful. writing.second contains the reason.
 * }
 * \endcode
 *
 * \return std::pair<bool, QString> where the boolean tells if the writing was successful, and the QString includes a
 * message in the case that the writing fails.
 */
std::pair<bool, QString> Entry::create(bool writeFrameData) {
    qDebug() << "Write frame data?" << writeFrameData;

    QString message;

    QDir entryDir(m_data.path);
    if (entryDir.exists()) {
        qDebug() << "Entry dir already exists" << entryDir;
        message += m_data.path + " already exists.\n";
        return std::pair<bool, QString>(false, message);
    }

    if (!entryDir.mkpath(m_data.path)) {
        qDebug() << "Failed to create directory" << m_data.path;
        message += "Failed to create entry directory " + m_data.path + "\n";
        return std::pair<bool, QString>(false, message);
    }

    if (!m_data.data.types.write(m_data.path + typePath)) {
        qDebug() << "TypeData write error" << m_data.path + typePath;
        message += "Type data writing failed.\n";
    }

    if (!m_data.data.info.write(m_data.path + infoPath)) {
        qDebug() << "EntryInfo write error" << m_data.path + infoPath;
        message += "Entry info writing failed.\n";
    }

    message += writePotentialFiles();

    message += writeElstopFiles();

    if (writeFrameData) {
        QString framePath = nextFramePath();
        framePaths.push_back(framePath);
        FrameReaderWriter f(framePath);
        if (!f.write(extendedData().frame)) {
            message += "Failed to write the atom data";
            entryDir.removeRecursively();
            return std::pair<bool, QString>(false, message);
        }
    } else {
        qDebug() << "Skipping the data";
    }
    return std::pair<bool, QString>(true, message);
}


/*!
 * \brief Creates a new frame to the entry and writes it to the hard drive.
 * \param newFrame New frame
 * \return \c true if the writing was successful
 */
bool Entry::addFrame(const XyzData& newFrame) {
    QString framePath = nextFramePath();
    FrameReaderWriter f(framePath);
    if (f.write(m_data.frame)) {
        framePaths.push_back(framePath);
        m_data.frame = newFrame;
        m_frameLoaded = true;
        return true;
    } else {
        return false;
    }
}

bool Entry::addFrame(const QString& fromPath, const QString& toPath) {
    qDebug() << this << "addFrame()";

    QString path = toPath;
    if (path == nullptr) {
        path = nextFramePath();
    }
    qDebug() << "    from:" << fromPath;
    qDebug() << "    to:  " << path;

    // Copy the XyzData frame to the project folder
    if (fromPath != nullptr) {
        //        QFile::copy(fromPath, path);
        FrameReaderWriter reader(fromPath);
        XyzData data;
        if (!reader.read(data)) {
            qDebug() << "    "
                     << "    "
                     << "couldn't read:" << fromPath;
            return false;
        }
        FrameReaderWriter writer(path);
        if (!writer.write(data)) {
            qDebug() << "    "
                     << "    "
                     << "couldn't write:" << path;
            return false;
        }
    }

    framePaths.push_back(path);
    //    m_frameLoaded = false;
    //    m_framesRead = false;

    qDebug() << framePaths.size() - 1 << path;

    emit frameAdded(framePaths.size() - 1);

    return true;
}

/*!
 * \brief Frees the used memory.
 *
 * When the data is requested after freeing, it has to be read again from the directory.
 */
void Entry::free() {
    //! \todo: Ugly hack to remember the path but releasing all the other memory usage
    QString path = m_data.path;
    m_data = ExtendedEntryData();
    m_data.path = path;

    m_framesRead = false;
    m_potentialsRead = false;
    m_elstopFilesRead = false;
    m_typesRead = false;
    m_infoRead = false;
    m_frameLoaded = false;
    m_prevSelectedFrame = -1;
}


/*!
 * \brief The atom types.
 *
 * The types are read from the direcotry if necessary.
 *
 * \return atom types
 */
const TypeData& Entry::types() const {
    if (!m_typesRead) readTypes();
    return m_data.data.types;
}

/*!
 * \brief The entry info.
 *
 * The info is read from the direcotry if necessary.
 *
 * \return entry info
 */
const EntryInfo& Entry::info() const {
    if (!m_infoRead) readInfo();
    return m_data.data.info;
}

/*!
 * \brief The name of the entry.
 *
 * The entry's name from EntryInfo is used if it is not "". If it is empty, the name of the entry directory is used
 * insted.
 *
 * \return Name of the entry
 */
QString Entry::name() const {
    if (!m_data.data.info.name.isEmpty()) return m_data.data.info.name;
    if (m_data.path.isEmpty()) return "";
    return QDir(m_data.path).dirName();
}

/*!
 * \brief The potential files in the entry.
 *
 * The files are read from the directory if necessary.
 *
 * \return Vector of potential files
 */
const QHash<ElementPair, DataFile<ReppotFileData>>& Entry::potentialFiles() const {
    //    if (!m_potentialsRead) readPotentialFiles(m_data.path + potentialsPath);
    return m_data.data.potentialFiles;
}

void Entry::loadReppotFiles(const QString& path) {
    //    m_potentialsRead = false;
    readPotentialFiles(path);
}

/*!
 * \brief The elstop files in the entry.
 *
 * The files are read from the directory if necessary.
 *
 * \return Vector of elstop files
 */
const QMap<QString, DataFile<ElstopFileData>>& Entry::elstopFiles() const {
    //    if (!m_elstopFilesRead) readElstopFiles(m_data.path + elstopPath);
    return m_data.data.elstopFiles;
}

void Entry::loadElstopFiles(const QString& path) {
    //    m_elstopFilesRead = false;
    readElstopFiles(path);
}

/*!
 * \brief The number of frames in the entry.
 * \return The number of frame directories in <entry_path>/frames/.
 */
int Entry::frameCount() const {
    qDebug() << this << "::frameCount()" << m_framesRead;
    if (!m_framesRead) readFrames();
    return framePaths.count();
}

/*!
 * \brief The path to the specified frame in the entry.
 * \return The path to the \e ith frame
 */
QString Entry::framePath(int i) const {
    if (i < 0 || i >= framePaths.count()) return "";
    return framePaths[i];
}

/*!
 * \brief Checks if the entry is empty
 * \return \c true is the entry has no frames in it
 */
bool Entry::isEmpty() const {
    return frameCount() == 0;
}

/*!
 * \brief The underlying EntryData object
 *
 * Types, potential files, elstop files, and frame paths are read from the directory if necessary.
 *
 * \return entry data
 */
const EntryData& Entry::data() const {
    if (!m_typesRead) readTypes();
    if (!m_potentialsRead) readPotentialFiles(m_data.path + potentialsPath);
    if (!m_elstopFilesRead) readElstopFiles(m_data.path + elstopPath);
    if (!m_framesRead) readFrames();

    return m_data.data;
}

/*!
 * \brief The underlying ExtendedEntryData object
 *
 * Types, potential files, elstop files, and frame paths are read from the directory if necessary. Additionally,
 * the latest frame is loaded from the directory if it has not yet been done. This function can be slow, which is why
 * data(), types(), info(), eltopFiles(), or potentialFiles() should be used instead, if the frame data is not needed.
 *
 * \return entry data
 */
const ExtendedEntryData& Entry::extendedData() const {
    if (!m_typesRead) readTypes();
    //    if (!m_potentialsRead) readPotentialFiles(m_data.path + potentialsPath);
    //    if (!m_elstopFilesRead) readElstopFiles(m_data.path + elstopPath);
    if (!m_framesRead) readFrames();
    if (!m_frameLoaded) loadSelectedFrame();

    return m_data;
}


void Entry::setExtendedData(const ExtendedEntryData& data) {
    m_data = data;

    m_framesRead = false;
    m_potentialsRead = false;
    m_elstopFilesRead = false;
    m_typesRead = false;
    m_infoRead = false;
    m_frameLoaded = false;
}


// ExtendedEntryData* Entry::mutableExtendedData() {
//    // Cache the entry data if it hasn't already been loaded
//    extendedData();
//    return &m_data;
//}

/*!
 * \brief The latest frame in the entry
 *
 * The frame is loaded from the directory if necessary.
 *
 * \return latest frame
 */
const XyzData& Entry::frame() const {
    if (!m_frameLoaded) loadSelectedFrame();
    return m_data.frame;
}

void Entry::readPotentialFiles(const QString& path) const {
    if (m_potentialsRead) return;

    m_data.data.potentialFiles.clear();

    qDebug() << path;

    QDir potDir(path);
    // Loop over all type pairs to find the needed files.
    for (const AtomType type1 : extendedData().data.types.allTypes) {
        for (const AtomType type2 : extendedData().data.types.allTypes) {
            if (type2.typeNumber < type1.typeNumber) continue;

            // Only insert unique symbol pairs.
            ElementPair pair(type1.symbol, type2.symbol);
            if (!m_data.data.potentialFiles.contains(pair)) {
                // Create the file entry
                DataFile<ReppotFileData> file;
                // Try and read it from file, the file will remember if it got any data, and the GUI will prompt the
                // user for more input if it couldn't find anything.
                QString filename = QString("reppot.%1.%2.in").arg(type1.symbol).arg(type2.symbol);
                file.read(potDir.path() + "/" + filename);
                m_data.data.potentialFiles.insert(pair, file);
            }
        }
    }

    m_potentialsRead = true;
}

void Entry::readElstopFiles(const QString& path) const {
    if (m_elstopFilesRead) return;

    //    m_data.data.elstopFiles.clear();

    //    QDir elstopDir(path);
    //    // Loop over all type pairs to find the needed files.
    //    for (const AtomType type1 : extendedData().data.types.allTypes) {
    //        for (const AtomType type2 : extendedData().data.types.allTypes) {
    //            if (type2.typeNumber < type1.typeNumber) continue;

    //            // Only insert unique interactions
    //            if (!m_data.data.elstopFiles.contains(type1.symbol)) {
    //                // Create the file entry
    //                DataFile<ElstopFileData> file;
    //                // Try and read it from file, the file will remember if it got any data, and the GUI will prompt
    //                the
    //                // user for more input if it couldn't find anything.
    //                QString filename = QString("elstop.%1.%2.in").arg(type1.symbol).arg(type2.symbol);
    //                file.read(elstopDir.path() + "/" + filename);
    //                m_data.data.elstopFiles.insert(type1.symbol, file);
    //            }
    //        }
    //    }

    m_elstopFilesRead = true;
}

void Entry::readFrames() const {
    qDebug() << this << "::readFrames()";

    framePaths.clear();
    QMap<int, QString> frameMap;

    QDir dir(m_data.path + framesPath);
    if (!dir.exists()) {
        m_framesRead = true;
        return;
    }

    QStringList dirNames = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

    for (const QString& dirName : dirNames) {
        QStringList list = dirName.split("_");
        if (list.length() == 2) {
            frameMap[list[1].toInt()] = dir.path() + "/" + dirName;
        }
    }
    framePaths = QVector<QString>::fromList(frameMap.values());

    // Automatically select the last frame.
    m_selectedFrame = framePaths.count() - 1;
    m_framesRead = true;
}

bool Entry::readInfo() const {
    m_infoRead = true;
    return m_data.data.info.read(m_data.path + infoPath);
}

/*!
 * Tries to read the types from a file. If the reading fails, the types are generated from the latest frame, and the
 * type file is overwritten.
 */
bool Entry::readTypes() const {
    m_typesRead = true;
    if (m_data.data.types.read(m_data.path + typePath)) return true;
    if (!m_frameLoaded) loadSelectedFrame();
    m_data.data.types = TypeData(m_data.frame);
    if (m_data.data.types.typeList.size() > 0) {
        m_data.data.types.write(m_data.path + typePath);
        return true;
    }
    return false;
}

bool Entry::loadSelectedFrame() const {
    qDebug() << this << "::loadSelectedFrame()";
    qDebug() << "    " << m_selectedFrame << framePaths.size();

    if (!m_framesRead) readFrames();
    if (framePaths.isEmpty()) return false;
    if (m_selectedFrame >= framePaths.count()) return false;
    if (m_selectedFrame == -1) return false;

    m_frameLoaded = true;

    // Frame already loaded into the data object, no need to read it again...
    if (m_selectedFrame == m_prevSelectedFrame) {
        return true;
    }
    m_prevSelectedFrame = m_selectedFrame;
    return FrameReaderWriter(framePaths.at(m_selectedFrame)).read(m_data.frame);
}


QString Entry::writePotentialFiles() {
    QDir potentialsDir(m_data.path + potentialsPath);
    QString message;

    if (!potentialsDir.exists()) {
        return "Potential directory doesn't exist.\n";
    }
    if (!potentialsDir.mkpath(potentialsDir.path())) {
        return "Potential directory creation failed.\n";
    }

    for (const DataFile<ReppotFileData>& file : m_data.data.potentialFiles) {
        if (file.hasData()) {
            QFile outputFile(file.filepath());
            if (outputFile.open(QIODevice::WriteOnly)) {
                QTextStream stream(&outputFile);
                for (const ReppotFileData& data : file.data().values()) {
                    stream << data << endl;
                }
            }
            outputFile.close();
        }
    }

    return message;
}


QString Entry::writeElstopFiles() {
    QDir elstopDir(m_data.path + elstopPath);
    QString message;

    if (!elstopDir.exists()) {
        return "Elstop directory doesn't exist.\n";
    }
    if (!elstopDir.mkpath(elstopDir.path())) {
        return "Elstop directory creation failed.\n";
    }

    for (const DataFile<ElstopFileData>& file : m_data.data.elstopFiles) {
        if (file.hasData()) {
            QFile outputFile(file.filepath());
            if (outputFile.open(QIODevice::WriteOnly)) {
                QTextStream stream(&outputFile);
                for (const ElstopFileData& data : file.data().values()) {
                    stream << data << endl;
                }
            }
            outputFile.close();
        }
    }

    return message;
}


QString Entry::nextFramePath() const {
    if (!m_framesRead) readFrames();
    int newNumber = 0;
    if (!framePaths.isEmpty()) {
        newNumber = QDir(framePaths.last()).dirName().split("_")[1].toInt() + 1;
    }
    return QString("%1/frame_%2").arg(m_data.path + framesPath).arg(newNumber);
}


void Entry::selectFrame(int frameIndex) {
    qDebug() << this << "::selectFrame()";

    m_selectedFrame = frameIndex;
    m_frameLoaded = false;
}
