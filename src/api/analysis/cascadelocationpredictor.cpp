/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "cascadelocationpredictor.h"

#include <glm.hpp>


CascadeLocationPredictor::CascadeLocationPredictor(const ExtendedEntryData& entry) : m_entry(entry) {}


/*!
 * \param ionPos Initial position of the ion
 * \param theta Theta angle of the ion in degrees
 * \param phi Phi angle of the ion in degrees
 * \param cutoff Distances between atoms and the ion trace, that are shorter than r, will be registered.
 * \param recursions Maximum number of times the tracing will be performed. For example, recursions = 2 means that the
 * tracing will be performed in the initial cell and the first periodic image.
 *
 * Predicts and returns the cascade location.
 */
glm::dvec3 CascadeLocationPredictor::predict(glm::dvec3 ionPosition, double theta, double phi, double cutoff,
                                             int recursions) {
    double p = phi / 180 * M_PI;
    double t = theta / 180 * M_PI;

    glm::dvec3 direction(sin(t) * cos(p), sin(t) * sin(p), cos(t));
    direction = direction / glm::length(direction);  // Normalizing the direction vector

    double r2 = cutoff * cutoff;

    std::pair<bool, glm::dvec3> prediction = trace(ionPosition, direction, r2, recursions);
    if (prediction.first) return prediction.second;
    return ionPosition;
}


/*!
 * Recursive function for finding the cascade location. First the distances from all the atoms to the ion trace are
 * checked. If no atoms meet the cutoff criterium, the ion is traced on the cell boundary. If the ion hits a periodic
 * boundary, the tracing continues with the shifted ion coordinate. Otherwise the tracing is stopped.
 */
std::pair<bool, glm::dvec3> CascadeLocationPredictor::trace(const glm::dvec3& position, const glm::dvec3& direction,
                                                            double cutoffSquared, int recursions) {
    glm::dvec3 closest;
    double closestDistSquared = std::numeric_limits<double>::max();
    bool closestFound = false;

    for (const Atom& a : m_entry.frame.atoms) {
        glm::dvec3 ap = a.position - position;
        glm::dvec3 projection =
            glm::dot(ap, direction) * direction;  // Projection vector of the atom position on the ion trace
        glm::dvec3 dif = ap - projection;         // Shortest vector from the ion trace to the atom

        // Ignore atom if the distance from the ion trace is larger than the cut-off radius or
        // if the ion is moving away from the atom
        if (glm::dot(dif, dif) > cutoffSquared || glm::dot(direction, ap) < 0) continue;

        double distSquared = glm::dot(projection, projection);
        if (distSquared < closestDistSquared) {
            closest = position + projection;
            closestDistSquared = distSquared;
            closestFound = true;
        }
    }

    if (closestFound) return std::pair<bool, glm::dvec3>(true, closest);

    // If this is the last level of recursion, stop
    if (recursions == 1) return std::pair<bool, glm::dvec3>(false, position);

    // Finding the face of the cell that the ion track intersects first.
    // First we find all the potential intersections. The sign of the distance tells if the
    // intersection point is in front on the track.
    std::vector<Intersection> intersections;
    if (direction.x != 0) {
        double dx1 = (m_entry.frame.cellLims.x1 - position.x) / direction.x;
        if (dx1 >= 0 && direction.x < 0)
            intersections.push_back(
                Intersection(dx1, m_entry.frame.pbc.x, glm::dvec3(m_entry.frame.cellLims.dx(), 0, 0)));

        double dx2 = (m_entry.frame.cellLims.x2 - position.x) / direction.x;
        if (dx2 >= 0 && direction.x > 0)
            intersections.push_back(
                Intersection(dx2, m_entry.frame.pbc.x, glm::dvec3(-m_entry.frame.cellLims.dx(), 0, 0)));
    }

    if (direction.y != 0) {
        double dy1 = (m_entry.frame.cellLims.y1 - position.y) / direction.y;
        if (dy1 >= 0 && direction.y < 0)
            intersections.push_back(
                Intersection(dy1, m_entry.frame.pbc.y, glm::dvec3(0, m_entry.frame.cellLims.dy(), 0)));

        double dy2 = (m_entry.frame.cellLims.y2 - position.y) / direction.y;
        if (dy2 >= 0 && direction.y > 0)
            intersections.push_back(
                Intersection(dy2, m_entry.frame.pbc.y, glm::dvec3(0, -m_entry.frame.cellLims.dy(), 0)));
    }

    if (direction.z != 0) {
        double dz1 = (m_entry.frame.cellLims.z1 - position.z) / direction.z;
        if (dz1 >= 0 && direction.z < 0)
            intersections.push_back(
                Intersection(dz1, m_entry.frame.pbc.z, glm::dvec3(0, 0, m_entry.frame.cellLims.dz())));

        double dz2 = (m_entry.frame.cellLims.z2 - position.z) / direction.z;
        if (dz2 >= 0 && direction.z > 0)
            intersections.push_back(
                Intersection(dz2, m_entry.frame.pbc.z, glm::dvec3(0, 0, -m_entry.frame.cellLims.dz())));
    }

    if (intersections.size() == 0) return std::pair<bool, glm::dvec3>(false, position);

    // Sorting the intersection points in order to find the first intersection
    std::sort(intersections.begin(), intersections.end(),
              [](const Intersection& i1, const Intersection& i2) { return i1.distance < i2.distance; });

    // If the first intersection is in non-periodic direction, stop the recursion
    if (!intersections.front().periodic) return std::pair<bool, glm::dvec3>(false, position);

    // If the intersection is in periodic direction, start a new tracing on the opposite side of the cell
    glm::dvec3 b = position + intersections.front().distance * direction;  // First intersection point
    return trace(b + intersections.front().shift, direction, cutoffSquared, recursions - 1);
}
