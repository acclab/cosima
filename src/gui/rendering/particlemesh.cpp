/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "particlemesh.h"

#include "api/util.h"
#include "api/visualsettings.h"

#include <QOpenGLBuffer>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>


ParticleMesh::ParticleMesh(QObject* parent) : AbstractMesh(parent) {}

ParticleMesh::ParticleMesh(const std::vector<glm::vec3>& positions, const std::vector<glm::vec4>& colors,
                           const std::vector<float>& radii, const std::vector<unsigned int>& indices, QObject* parent)
        : AbstractMesh(positions, colors, indices, parent) {
    m_radii = radii;
}


ParticleMesh::~ParticleMesh() {
    if (m_radiusVbo) {
        m_radiusVbo->destroy();
        m_texture->destroy();
    }

    delete m_radiusVbo;
    delete m_texture;
}


void ParticleMesh::initialize() {
    AbstractMesh::initialize();

    m_texture = new QOpenGLTexture(QImage(":/textures/particle.png"));
    m_texture->create();
    m_texture->setMagnificationFilter(QOpenGLTexture::Linear);
    m_texture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
}


void ParticleMesh::setupVertexBufferObjects() {
    AbstractMesh::setupVertexBufferObjects();

    m_radiusVbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_radiusVbo->create();
    m_radiusVbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    updateVertexBufferObject(m_radiusVbo, m_radii);
}


void ParticleMesh::setupVertexArrayObject() {
    AbstractMesh::setupVertexArrayObject();

    QOpenGLFunctions* glFuncs = QOpenGLContext::currentContext()->functions();

    m_radiusVbo->bind();
    glFuncs->glEnableVertexAttribArray(3);
    glFuncs->glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 0, nullptr);
}


void ParticleMesh::draw(QOpenGLShaderProgram& /*shader*/) {
    m_texture->bind();
    m_vao->bind();
    glDrawElements(GL_POINTS, indexCount(), GL_UNSIGNED_INT, indices().data());
    m_vao->release();
    m_texture->release();
}


void ParticleMesh::setRadii(const std::vector<float>& radii) {
    m_radii = radii;
    if (isInitialized()) {
        updateVertexBufferObject(m_radiusVbo, m_radii);
    }
}
