/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef SIMULATIONCELLPARAMETERKEYS_H
#define SIMULATIONCELLPARAMETERKEYS_H

#include "api/simulation/mdparameterkeys.h"

const parameter MDLATXYZ{"mdlatxyz", false, 1};
const parameter LATFLAG{"latflag", true, 1};
const parameter NATOMS{"natoms", false, 0};
const parameter BOX_X{"box(1)", true, 0.0};
const parameter BOX_Y{"box(2)", true, 0.0};
const parameter BOX_Z{"box(3)", true, 0.0};
const parameter PB_X{"pb(1)", true, false};
const parameter PB_Y{"pb(2)", true, false};
const parameter PB_Z{"pb(3)", true, false};

const QList<parameter> simulationCellKeys{MDLATXYZ, LATFLAG, NATOMS, BOX_X, BOX_Y, BOX_Z, PB_X, PB_Y, PB_Z};

#endif  // SIMULATIONCELLPARAMETERKEYS_H
