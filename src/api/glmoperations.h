/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GLMOPERATIONS_H
#define GLMOPERATIONS_H

#include <QDataStream>
#include <QDebug>

#include <glm/vec3.hpp>

/*!
 * \brief Operator for streaming glm::dvec3 into QDataStream.
 * \param stream The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const glm::dvec3& v) {
    return stream << v.x << v.y << v.z;
}

/*!
 * \brief Operator for streaming glm::dvec3 from QDataStream.
 * \param stream The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, glm::dvec3& v) {
    return stream >> v.x >> v.y >> v.z;
}

/*!
 * \brief Operator for streaming glm::dvec3 into QDebug.
 * \param d The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDebug operator<<(QDebug d, const glm::dvec3& v) {
    QDebugStateSaver saver(d);
    d.nospace() << "glm::dvec3(" << v.x << ", " << v.y << ", " << v.z << ")";
    return d;
}


/*!
 * \brief Operator for streaming glm::bvec3 into QDataStream.
 * \param stream The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator<<(QDataStream& stream, const glm::bvec3& v) {
    return stream << v.x << v.y << v.z;
}

/*!
 * \brief Operator for streaming glm::bvec3 from QDataStream.
 * \param stream The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDataStream& operator>>(QDataStream& stream, glm::bvec3& v) {
    return stream >> v.x >> v.y >> v.z;
}

/*!
 * \brief Operator for streaming glm::bvec3 into QDebug.
 * \param d The stream
 * \param v The vector being streamed
 * \return The stream afterwards
 */
inline QDebug operator<<(QDebug d, const glm::bvec3& v) {
    QDebugStateSaver saver(d);
    d.nospace() << "glm::bvec3(" << v.x << ", " << v.y << ", " << v.z << ")";
    return d;
}
#endif  // GLMOPERATIONS_H
