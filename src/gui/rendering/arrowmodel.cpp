/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "arrowmodel.h"

#include "api/util.h"

#include <glm/gtc/constants.hpp>

#include <cmath>


ArrowModel::ArrowModel(ColorGradient gradient, float bodyLength, bool flip, QObject* parent) : AbstractGeometryModel(parent) {
    m_resolution = 20;
    m_gradient = gradient;
    m_flip = flip;

    m_bodyRadius = 0.025f;
    m_headLength = 0.05f;
    m_headRadius = 0.05f;
    m_bodyLength = bodyLength;

    m_arrowLength = m_bodyLength + m_headLength;

    setupVertexData();
}


void ArrowModel::setupVertexData() {
    unsigned int numOfPoints = 2 + 3 * m_resolution;

    std::vector<glm::vec4> colors(numOfPoints);

    glm::vec4 c1 = util::colorToVector(m_gradient.m_color1);
    glm::vec4 c2 = util::colorToVector(m_gradient.m_color2);
    glm::vec4 c3 = util::colorToVector(m_gradient.m_color3);
    glm::vec4 c4 = util::colorToVector(m_gradient.m_color4);
    glm::vec4 c5 = util::colorToVector(m_gradient.m_color5);

    colors[0] = c1;
    colors[numOfPoints - 1] = c5;

    // rings
    for (unsigned int i = 0; i < m_resolution; ++i) {
        colors[i + 1] = c2;
        colors[m_resolution + i + 1] = c3;
        colors[2 * m_resolution + i + 1] = c4;
    }

    std::vector<unsigned int> indices(6 * m_resolution * 3);
    std::vector<glm::vec3> positions(numOfPoints);

    // ending points
    float zOffset = m_flip ? m_bodyLength + m_headLength : 0.0f;

    positions[0] = glm::vec3(0, 0, zOffset);
    positions[numOfPoints - 1] = glm::vec3(0, 0, zOffset - m_arrowLength);

    // rings
    for (unsigned int i = 0; i < m_resolution; ++i) {
        float phi = 2 * glm::pi<float>() / m_resolution * i;
        float x = std::sin(phi);
        float y = std::cos(phi);

        // ring 1
        positions[i + 1] = glm::vec3(m_headRadius * x, m_headRadius * y, zOffset - m_headLength);
        // ring 2
        positions[m_resolution + i + 1] = glm::vec3(m_bodyRadius * x, m_bodyRadius * y, zOffset - m_headLength);
        // ring 3
        positions[2 * m_resolution + i + 1] = glm::vec3(m_bodyRadius * x, m_bodyRadius * y, zOffset - m_arrowLength);

        unsigned int nextIndex = i + 2;
        if (i == m_resolution - 1) {
            nextIndex = 1;
        }

        // triangles between arrow point and first ring
        indices[i * 3] = 0;
        indices[i * 3 + 1] = i + 1;
        indices[i * 3 + 2] = nextIndex;


        // triangles between first and second ring
        indices[m_resolution * 3 + i * 6] = i + 1;
        indices[m_resolution * 3 + i * 6 + 1] = m_resolution + i + 1;
        indices[m_resolution * 3 + i * 6 + 2] = nextIndex;
        indices[m_resolution * 3 + i * 6 + 3] = nextIndex;
        indices[m_resolution * 3 + i * 6 + 4] = m_resolution + i + 1;
        indices[m_resolution * 3 + i * 6 + 5] = m_resolution + nextIndex;

        // triangles between second and third ring
        indices[m_resolution * 9 + i * 6] = m_resolution + i + 1;
        indices[m_resolution * 9 + i * 6 + 1] = 2 * m_resolution + i + 1;
        indices[m_resolution * 9 + i * 6 + 2] = m_resolution + nextIndex;
        indices[m_resolution * 9 + i * 6 + 3] = m_resolution + nextIndex;
        indices[m_resolution * 9 + i * 6 + 4] = 2 * m_resolution + i + 1;
        indices[m_resolution * 9 + i * 6 + 5] = 2 * m_resolution + nextIndex;

        // triangles between third ring and end point of the arrow
        indices[m_resolution * 15 + i * 3] = 2 * m_resolution + i + 1;
        indices[m_resolution * 15 + i * 3 + 1] = numOfPoints - 1;
        indices[m_resolution * 15 + i * 3 + 2] = 2 * m_resolution + nextIndex;
    }

    setFaceVertexData(positions, colors, indices);
}
