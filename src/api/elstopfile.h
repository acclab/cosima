/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ELSTOPFILE_H
#define ELSTOPFILE_H

#include <QString>

/*!
 * \brief A class for handling elstop files
 *
 * Can be used to check if a elstop file follows PARCAS naming convention, and what are the element and substrate that
 * it handles. On the other hand, it can also be used to determine the file name from the given element and substrate.
 * Works very similarly to PotentialFile.
 *
 * The parcas naming convention is elstop.<element>.<substrate>.in (e.g. elstop.Si.SiO2.in)
 */
class ElstopFile {
  public:
    ElstopFile(const QString& pathToFile = "");
    bool isValid() const;
    bool exists() const;
    QString name() const;
    const QString& path() const;

    QString element;    //!< \brief Symbol of the element (e.g. Si)
    QString substrate;  //!< \brief Name of the medium substrate (e.g. SiO2)

  private:
    bool m_isValid = false;
    QString m_path;
};

#endif  // ELSTOPFILE_H
