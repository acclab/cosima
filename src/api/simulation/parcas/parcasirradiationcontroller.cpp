/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "parcasirradiationcontroller.h"

#include "api/programsettings.h"
#include "api/simulation/parcas/parcascontrollerutils.h"

#include "api/state/parameters/ionparameters.h"
#include "api/state/parameters/iontransformparameters.h"
#include "api/state/parameters/outputparameters.h"
#include "api/state/parameters/pressurecontrolparameters.h"
#include "api/state/parameters/temperaturecontrolparameters.h"
#include "api/state/parameters/timeparameters.h"


ParcasIrradiationController::ParcasIrradiationController() {}


bool ParcasIrradiationController::writeLocalScript(const QString& path, const ExtendedEntryData& entry) {

    // has to be done before the "writeInputFile()" command.
    generateIons(entry);

    QMap<QString, QString> parameterMap;
    parameterMap.insert("{{parcas}}", ProgramSettings::getInstance().backendSettings().parcasPath);

    //! \todo Wrap this with the useMpi boolean found in the backendSettings
    parameterMap.insert("{{parallel_exe}}", QString("mpirun -np %1").arg(ProgramSettings::getInstance().backendSettings().mpiCores));

    return writeTemplateFile(":/scripts/local-parcas-exe-template.sh", path + "/run-parcas.sh", parameterMap);
}


bool ParcasIrradiationController::writeStandaloneScript(const QString& path, const ExtendedEntryData& entry,
                                                        const ExportData& data) {
    qDebug() << this << "writeStandaloneScript()";

    // has to be done before the "writeInputFile()" command.
    generateIons(entry);

    QMap<QString, QString> parameterMap;
    parameterMap.insert("{{parcas}}", data.parcasPath);
    // Currently hard-coded to the
    if (true) {
        parameterMap.insert("{{processor}}", "python3 processor.py");
        if (!writeTemplateFile(":/tools/processor.py", path + "/processor.py", QMap<QString, QString>())) {
            qDebug() << "Couldn't write `processor.py' to " << path;
            return false;
        }
    } else {
        parameterMap.insert("{{processor}}", ProgramSettings::getInstance().backendSettings().processorPath);
    }
    int startWithRelax = parameters()[api::parameters::irradiation::START_WITH_RELAX_ON].value().toBool() ? 1 : 0;
    parameterMap.insert("{{start_with_relax}}", QString::number(startWithRelax));
    if (parameters()[api::parameters::irradiation::RELAX_ON].value().toBool()) {
        parameterMap.insert(
            "{{relax_interval}}",
            QString::number(parameters()[api::parameters::irradiation::RELAX_INTERVAL].value().toInt()));
    } else {
        parameterMap.insert("{{relax_interval}}", QString::number(0));
    }
    parameterMap.insert("{{pbx}}", QString::number(entry.frame.pbc.x ? 1 : 0));
    parameterMap.insert("{{pby}}", QString::number(entry.frame.pbc.y ? 1 : 0));
    parameterMap.insert("{{pbz}}", QString::number(entry.frame.pbc.z ? 1 : 0));
    parameterMap.insert("{{center}}", "1");
    int mergeWithBaseType = cascadeParameters()[api::parameters::ion::BASE_TYPE_MERGE_ON].value().toBool() ? 1 : 0;
    parameterMap.insert("{{merge_ions}}", QString::number(mergeWithBaseType));
    int mergeToType = cascadeParameters()[api::parameters::ion::BASE_TYPE].value().toInt();
    parameterMap.insert("{{merge_to_type}}", QString::number(mergeToType));
    parameterMap.insert("{{remove_sputtered}}", "1");

    int saveCascadeMovie = cascadeParameters()[api::parameters::output::MOVIE_ON].value().toBool() ? 1 : 0;
    parameterMap.insert("{{save_movie_cascade}}", QString::number(saveCascadeMovie));
    int saveRelaxMovie = relaxParameters()[api::parameters::output::MOVIE_ON].value().toBool() ? 1 : 0;
    parameterMap.insert("{{save_movie_relax}}", QString::number(saveRelaxMovie));
    parameterMap.insert("{{parallel_exe}}", data.parameters["parallel_exe"].toString());

    if (!writeTemplateFile(":/scripts/standalone-parcas-irradiation-simulation-template.sh",
                           path + "/run-simulation.sh", parameterMap))
        return false;

    if (!writeInputFile(path, entry)) return false;

    if (!writeFiles(path, data.files)) return false;

    m_ionGenerator.write(path + "/ions.dat");

    return true;
}


bool ParcasIrradiationController::writeInputFile(const QString& path, const ExtendedEntryData& entry) {
    if (!writeRelaxInputFile(path + "/md.in.relax", entry)) return false;
    if (!writeCascadeInputFile(path + "/md.in.cascade", entry)) return false;
    return true;
}

bool ParcasIrradiationController::writeRelaxInputFile(const QString& path, const ExtendedEntryData& entry) {
    ParcasControllerUtils utils;
    return utils.writeRelaxFile(entry, path, relaxParameters());
}


/*!
 * \brief Write a md.in file based on the current simulation state.
 *
 * A call to this function will increase the current ion counter.
 *
 * \param path The filepath to the saved md.in file. This should include the /in/md.in suffix.
 * \param entry
 * \return
 */
bool ParcasIrradiationController::writeCascadeInputFile(const QString& path, const ExtendedEntryData& entry) {
    if (m_currentIon < 0 || m_currentIon >= m_ionGenerator.ions().size()) return false;

    IonGenerator::Ion ion = m_ionGenerator.ions().at(m_currentIon++);
    cascadeParameters().set(api::parameters::ion::ENERGY, api::value_t(ion.energy));
    cascadeParameters().set(api::parameters::ion_transform::PHI, api::value_t(ion.transform.phi));
    cascadeParameters().set(api::parameters::ion_transform::THETA, api::value_t(ion.transform.theta));
    cascadeParameters().set(api::parameters::ion_transform::X_POSITION, api::value_t(ion.transform.position.x));
    cascadeParameters().set(api::parameters::ion_transform::Y_POSITION, api::value_t(ion.transform.position.y));
    cascadeParameters().set(api::parameters::ion_transform::Z_POSITION, api::value_t(ion.transform.position.z));

    ParcasControllerUtils utils;
    return utils.writeCascadeFile(entry, path, cascadeParameters());
}


void ParcasIrradiationController::insertCascadeParameters(api::BaseParameters parameters) {
    m_cascadeParameters.insert(parameters);
}


api::BaseParameters ParcasIrradiationController::cascadeParameters() {
    return m_cascadeParameters;
}


void ParcasIrradiationController::insertRelaxParameters(api::BaseParameters parameters) {
    m_relaxParameters.insert(parameters);
}


api::BaseParameters ParcasIrradiationController::relaxParameters() {
    return m_relaxParameters;
}


void ParcasIrradiationController::setupDefaultState() {
    m_relaxParameters.insert(api::TimeParameters());
    m_relaxParameters.insert(api::TemperatureControlParameters());
    m_relaxParameters.insert(api::PressureControlParameters());
    m_relaxParameters.insert(api::OutputParameters());

    m_cascadeParameters.insert(api::IonParameters());
    m_cascadeParameters.insert(api::IonTransformParameters());
    m_cascadeParameters.insert(api::TimeParameters());
    m_cascadeParameters.insert(api::TemperatureControlParameters());
    m_cascadeParameters.insert(api::PressureControlParameters());
    m_cascadeParameters.insert(api::OutputParameters());
}


QString ParcasIrradiationController::name() {
    return "Irradiation";
}


QDataStream& ParcasIrradiationController::save(QDataStream& out) const {
    out << m_cascadeParameters;
    out << m_relaxParameters;
    return ParcasBaseController::save(out);
}


QDataStream& ParcasIrradiationController::load(QDataStream& in) {
    in >> m_cascadeParameters;
    in >> m_relaxParameters;
    return ParcasBaseController::load(in);
}


/*!
 * \brief Returns the transform of the currently selected ion. If the \sa writeCascadeInputFile() function has been
 * called prior to calling this function, the counter will have advanced to the next ion.
 *
 * \return The transform of the current ion counter.
 */
IonTransformData* ParcasIrradiationController::currentIonTransform() {
    return &m_ionGenerator.mutableIons()[m_currentIon].transform;
}


void ParcasIrradiationController::generateIons(const ExtendedEntryData& entry) {
    m_ionGenerator.generate(entry, parameters(), cascadeParameters());
}
