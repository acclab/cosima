/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Follows mostly the guide in https://doc.qt.io/qt-5/qtwidgets-itemviews-simpletreemodel-example.html.
 */


#include "historytreemodel.h"

#include "api/project.h"

#include <QDebug>


/*!
 * \brief Constructs a tree model object based on a History object.
 *
 * Contains the entries of the project, and the frames under them.
 *
 * \param history The history object
 * \param parent The parent object
 */
HistoryTreeModel::HistoryTreeModel(const History& history, QObject* parent) : QAbstractItemModel(parent) {
    QList<QVariant> rootData{"Name", "Description", "Frames"};
    m_rootItem = new TreeItem(rootData);

    // Add all existing entries to the model.
    for (int i = 0; i < history.entryCount(); ++i) {
        const Entry* entry = history.entry(i);

        //    for (const Entry* entry : history.entries()) {
        QList<QVariant> columnData{entry->name(), entry->info().description, entry->frameCount()};
        TreeItem* item = new TreeItem(columnData, m_rootItem);
        for (int j = 0; j < entry->frameCount(); ++j) {
            QList<QVariant> frameData{"Frame " + QString::number(j), "", ""};
            item->appendChild(new TreeItem(frameData, item));
        }
        m_rootItem->appendChild(item);
    }
}


HistoryTreeModel::~HistoryTreeModel() {
    delete m_rootItem;
}


int HistoryTreeModel::columnCount(const QModelIndex& parent) const {
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return m_rootItem->columnCount();
}


void HistoryTreeModel::addEntry(const Entry* entry) {

    QModelIndex idx = index(rowCount(), 0);
    int first = rowCount();
    int last = rowCount() + entry->frameCount();

    beginInsertRows(idx, first, last);

    insertRows(first, entry->frameCount() + 1);
    QList<QVariant> columnData{entry->name(), entry->info().description, entry->frameCount()};
    TreeItem* item = new TreeItem(columnData, m_rootItem);
    for (int j = 0; j < entry->frameCount(); ++j) {
        QList<QVariant> frameData{"Frame " + QString::number(j), "", ""};
        item->appendChild(new TreeItem(frameData, item));
    }
    m_rootItem->appendChild(item);

    endInsertRows();
}


void HistoryTreeModel::removeEntry(int entryIndex) {
    TreeItem* entryItem = m_rootItem->child(entryIndex);
    QModelIndex idx = index(entryIndex, 0);
    int first = entryIndex;
    int last = entryIndex + entryItem->childCount();
    beginRemoveRows(idx, first, last);
    const bool success = entryItem->removeChildren(0, entryItem->childCount());
    m_rootItem->removeChildren(entryIndex, 1);
    endRemoveRows();
}


void HistoryTreeModel::addFrameToEntry(const QString& frameInfo, int entryIndex, int frameIndex) {
    qDebug() << "HistoryTreeModel::addFrameToEntry()";
    qDebug() << "    " << frameInfo << entryIndex << frameIndex;

    TreeItem* entryItem = m_rootItem->child(entryIndex);
    qDebug() << entryItem << entryItem->data(0) << entryItem->data(1) << entryItem->data(2);

    // Update the frame counter
    QModelIndex idx = index(entryIndex, 0);
    if (entryItem->setData(2, frameIndex + 1)) {
        emit dataChanged(idx, idx, {Qt::DisplayRole, Qt::EditRole});
    }

    // Insert the frame at the end of the frame list
    int first = entryItem->row();
    int last = first;

    beginInsertRows(idx, first, last);

    //    insertRows(first, last);
    QList<QVariant> frameData{"Frame " + QString::number(frameIndex)};
    entryItem->appendChild(new TreeItem(frameData, entryItem));

    endInsertRows();
}


void HistoryTreeModel::frameAddedToEntry(int entryIndex, int frameIndex) {
    qDebug() << this << "::frameAddedToEntry() not yet implemented!";
}


QVariant HistoryTreeModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) return QVariant();

    if (role != Qt::DisplayRole) return QVariant();

    TreeItem* item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column());
}


Qt::ItemFlags HistoryTreeModel::flags(const QModelIndex& index) const {
    if (!index.isValid()) return nullptr;

    return QAbstractItemModel::flags(index);
}


QVariant HistoryTreeModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) return m_rootItem->data(section);

    return QVariant();
}


QModelIndex HistoryTreeModel::index(int row, int column, const QModelIndex& parent) const {
    if (!hasIndex(row, column, parent)) return QModelIndex();

    TreeItem* parentItem;

    if (!parent.isValid())
        parentItem = m_rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem* childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}


QModelIndex HistoryTreeModel::parent(const QModelIndex& index) const {
    if (!index.isValid()) return QModelIndex();

    TreeItem* childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem* parentItem = childItem->parent();

    if (parentItem == m_rootItem) return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}


int HistoryTreeModel::rowCount(const QModelIndex& parent) const {
    TreeItem* parentItem;
    if (parent.column() > 0) return m_rootItem->childCount();

    if (!parent.isValid())
        parentItem = m_rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}
