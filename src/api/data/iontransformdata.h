/*
 * Copyright (C) 2020, University of Helsinki
 *     Prof. Kai Nordlund <kai.nordlund@helsinki.fi>
 *
 * Authors: Jarno Laakso, Christoffer Fridlund
 *
 *
 * This file is part of COSIRMA.
 *
 * COSIRMA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * COSIRMA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with COSIRMA.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IONTRANSFORMDATA_H
#define IONTRANSFORMDATA_H

#include <glm/vec3.hpp>


struct IonTransformData {

    /*!
     * \brief Position of the ion.
     */
    glm::dvec3 position;

    /*!
     * \brief  Azimuth angle [deg] of the direction of the beam.
     *
     * The azimuth angle in the global spherical coordinates.
     */
    double phi = 0;

    /*!
     * \brief Inclination angle [deg] of the direction of the beam.
     *
     * The inclination angle in the global spherical coordinates.
     */
    double theta = 0;


    /*!
     * \brief Equality operator
     * \param d The data struct being compared to
     * \return \c true if all the attributes are \em exactly the same.
     */
    bool operator==(const IonTransformData& d) const {
        return (position == d.position) && (phi == d.phi) && (theta == d.theta);
    }

    /*!
     * \brief Inequality operator
     * \param d The data struct being compared to
     * \return \c true if one or more attributes are not \em exactly the same.
     */
    bool operator!=(const IonTransformData& d) const { return !operator==(d); }
};

#endif  // IONTRANSFORMDATA_H
